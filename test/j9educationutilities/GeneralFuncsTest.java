/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationutilities;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Administrator
 */
public class GeneralFuncsTest {

    public GeneralFuncsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of arrayDiff method, of class GeneralFuncs.
     */
    @Test
    public void testArrayDiff() {
        String[] a = {"hello", "is", "mary", "is", "here"};
        String[] b = {"hello", "the", "truck", "is", "not", "here", "yet"};
        ArrayList<String> intersect = new ArrayList<String>();
        ArrayList<String> aOnly = new ArrayList<String>();
        ArrayList<String> bOnly = new ArrayList<String>();
        GeneralFuncs.arrayDiff(a, b, intersect, aOnly, bOnly);
        assertTrue(aOnly.size() == 2);
        assertTrue(aOnly.contains("mary"));
        assertTrue(aOnly.contains("is"));
    }
}