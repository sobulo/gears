/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import static org.junit.Assert.assertEquals;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.GeneralFuncs;
import j9educationutilities.InstanceTestHelper;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetStudentTest {
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static InetStudent[] testStudents;
    private static Objectify ofy;


    @BeforeClass
    public static void setUpClass() throws Exception {
        helper.setUp();
        ofy = ObjectifyService.begin();
        InstanceTestHelper.initializeObjects();
        testStudents = InstanceTestHelper.getTestStudentList();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        testStudents = null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
        ofy = null;
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getBirthday method, of class InetStudent.
     */
    @Test
    public void test1GetBirthday() {
        System.out.println("getBirthday");
        InetStudent instance = testStudents[0];
        Date expResult = GeneralFuncs.getDate(1990, 1, 14);
        System.out.println("Bday Date: " + expResult);
        Date result = instance.getBirthday();
        assertEquals(expResult, result);
    }
    
    @Test
    public void test2AddGuardian() throws MissingEntitiesException, DuplicateEntitiesException
    {
    	Objectify ofy = ObjectifyService.begin();
    	String gkStr = ofy.getFactory().keyToString(InstanceTestHelper.getTestGuardianList()[0].getKey());
    	String skStr = ofy.getFactory().keyToString(testStudents[0].getKey());
    	InetDAO4Updates.addGuardianToStudent(gkStr, skStr);
    }

    /**
     * Test of getGuardianKeys method, of class InetStudent.
     */
    @Test
    public void test3GetGuardianKeys() {
        System.out.println("getGuardianKeys");
        Objectify ofy = ObjectifyService.begin();
        InetStudent instance = ofy.find(testStudents[0].getKey());
        Key<InetGuardian> expResult = (Key<InetGuardian>) InstanceTestHelper.getTestGuardianList()[0].getKey();
        Key<InetGuardian> result = instance.getFirstGuardianKey();
        assertEquals(expResult, result);
        expResult = null;
        result = instance.getSecondGuardianKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCourseKeys method, of class InetStudent.
     */
    @Test
    public void test4GetCourseKeys() {
        System.out.println("getCourseKeys");
        InetStudent instance = testStudents[1];
        boolean expResult = false;
        Key<InetLevel> levKey = InstanceTestHelper.getTestLevelList()[1].getKey();
        boolean result = instance.getCourseKeys(levKey).iterator().hasNext();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCurrentLevelKey method, of class InetStudent.
     */
    @Test
    public void test5GetCurrentLevelKey() {
        System.out.println("getCurrentLevelKey");
        InetStudent instance = testStudents[0];
        Key<InetLevel> expResult = InstanceTestHelper.getTestLevelList()[0].getKey();
        Key<InetLevel> result = instance.getLevels().iterator().next();
        assertEquals(expResult, result);
    }

    /**
     * Test that instance objects being used have also been persisted in
     * datastore as part of the InstanceHelper.initializeObjects call
     */
    @Test
    public void test6ReadPersistedStudent()
    {
         InetStudent u = ofy.find(testStudents[1].getKey());
         assertEquals(testStudents[1].getFname(), u.getFname());
    }
}
