/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationutilities.InstanceTestHelper;

import java.util.HashSet;
import java.util.Iterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetSchoolTest {
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;

    static InetSchool testSchool;
    static InetLevel[] testLevels;

    @BeforeClass
    public static void setUpClass() throws Exception {
        helper.setUp();
        ofy = ObjectifyService.begin();
        InstanceTestHelper.initializeObjects();
        testSchool = InstanceTestHelper.getTestSchool();
        testLevels = InstanceTestHelper.getTestLevelList();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        testSchool = null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getInetLevels method, of class InetSchool.
     */
    @Test
    public void test1GetInetLevels() {
        System.out.println("getInetLevels");
        testSchool.addInetLevelKey(testLevels[0].getKey());
        Key<InetLevel> expResult = testLevels[0].getKey();
        Iterator<Key<InetLevel>> resultIterator = testSchool.getInetLevelKeys().iterator();
        boolean found = false;
        while(resultIterator.hasNext())
        {
            if(expResult.equals(resultIterator.next()))
            {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    /**
     * Test of addInetLevel method, of class InetSchool.
     */
    @Test (expected=NullPointerException.class)
    public void test2AddInetLevel() {
        System.out.println("addInetLevel");
        testSchool.addInetLevelKey(testLevels[0].getKey());
        boolean expResult = false;
        boolean result = testSchool.addInetLevelKey(testLevels[0].getKey());
        assertEquals(expResult, result);
        result = testSchool.addInetLevelKey(null);
        expResult = true;
        result = testSchool.addInetLevelKey(testLevels[0].getKey());
        assertEquals(expResult, result);
    }

    /**
     * Test of removeInetLevel method, of class InetSchool.
     */
    @Test (expected=NullPointerException.class)
    public void test3RemoveInetLevel() {
        System.out.println("removeInetLevel");
        testSchool.addInetLevelKey(testLevels[0].getKey());
        testSchool.addInetLevelKey(testLevels[1].getKey());
        boolean expResult = true;
        boolean result = testSchool.removeInetLevelKey(testLevels[1].getKey());
        assertEquals(expResult, result);
        expResult = false;
        result = testSchool.removeInetLevelKey(testLevels[1].getKey());
        assertEquals(expResult, result);
        result = testSchool.removeInetLevelKey(null);
    }

    /**
     * Test of getAccronym method, of class InetSchool.
     */
    @Test
    public void test4GetID() {
        System.out.println("getAccronym");
        InetSchool instance = testSchool;
        String expResult = InetConstants.SCHOOL_ID;
        String result = instance.getKey().getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAccronym method, of class InetSchool.
     */
    @Test
    public void test5SetAccronym() {
        System.out.println("setAccronym");
        String accronym = "DEF";
        testSchool.setAccronym(accronym);
        assertEquals(accronym, testSchool.getAccronym());
    }

    /**
     * Test of getAddress method, of class InetSchool.
     */
    @Test
    public void test6GetAddress() {
        System.out.println("getAddress");
        String expResult = "123 watch me 3peat, nba state";
        String result = testSchool.getAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAddress method, of class InetSchool.
     */
    @Test
    public void test7SetAddress() {
        System.out.println("setAddress");
        String address = "123 we recently moved st, uni state";
        testSchool.setAddress(address);
        assertEquals(address, testSchool.getAddress());
    }

    /**
     * Test of getEmail method, of class InetSchool.
     */
    @Test
    public void test8GetEmail() {
        System.out.println("getEmail");
        String expResult = "baller@greats.com";
        String result = testSchool.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmail method, of class InetSchool.
     */
    @Test
    public void test9SetEmail() {
        System.out.println("setEmail");
        String email = "admin@abc.com";
        testSchool.setEmail(email);
        assertEquals(email, testSchool.getEmail());
    }


    /**
     * Test of getPhoneNumbers method, of class InetSchool.
     */
    public void test10GetPhoneNumbers() {
        System.out.println("getPhoneNumbers");
        boolean expResult = false;
        boolean result = testSchool.getPhoneNumbers().iterator().hasNext();
        System.out.println("Result: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of addPhoneNumber method, of class InetSchool.
     */
    @Test
    public void test11AddPhoneNumber() {
        System.out.println("addPhoneNumber");
        String num = "1234";
        HashSet<String> temp = new HashSet<String>();
        assertTrue(temp.add(num));
        testSchool.setPhoneNumbers(temp);
        assertEquals(num, testSchool.getPhoneNumbers().iterator().next());
    }

    /**
     * Test of getSchoolName method, of class InetSchool.
     */
    @Test
    public void test12GetSchoolName() {
        System.out.println("getSchoolName");
        String expResult = "Kobe High School";
        String result = testSchool.getSchoolName();
        assertEquals(expResult, result);
    }

    /**
    * Test that instance objects being used have also been persisted in
    * datastore as part of the InstanceHelper.initializeObjects call
    */
    @Test
    public void read13PersistedSchool()
    {
         InetSchool ts = ofy.find(testSchool.getKey());
        //InetSchool ts = pm.getObjectById(InetSchool.class, testSchool.getKey());
        assertEquals(testSchool.getSchoolName(), ts.getSchoolName());
    }

    /**
     * Test of setSchoolName method, of class InetSchool.
     */
    @Test
    public void test14SetSchoolName() {
        System.out.println("setSchoolName");
        String schoolName = "ABC Rocks";
        testSchool.setSchoolName(schoolName);
        assertEquals(schoolName, testSchool.getSchoolName());
    }
}
