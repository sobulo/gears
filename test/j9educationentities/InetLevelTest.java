/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.InstanceTestHelper;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetLevelTest {
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;

    static InetLevel[] testLevels;

    @BeforeClass
    public static void setUpClass() throws Exception {
        helper.setUp();
        ofy = ObjectifyService.begin();
        InstanceTestHelper.initializeObjects();
        testLevels = InstanceTestHelper.getTestLevelList();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        testLevels = null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
        ofy = null;
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getKey method, of class InetUser.
     */
    @Test
    public void testSimpleGet() {
        System.out.println("Testing if attribute can be read");
        InetLevel instance = testLevels[0];
        String expResult = "JSS 1";
        String result = instance.getLevelName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    public void testSetKey()
    {
        System.out.println("Testing key generation");
        Key<InetLevel> levelKey = testLevels[0].getKey();
        String expResult = "2009-2010~2nd Term~JSS 1~A".toLowerCase();
        String keyName = levelKey.getName();
        assertEquals(expResult, keyName);
    }

    @Test(expected=DuplicateEntitiesException.class)
    public void testDuplicateException() throws DuplicateEntitiesException, MissingEntitiesException
    {
        InetDAO4Creation.createLevel(
                testLevels[0].getTerm(), testLevels[0].getAcademicYear(),
                testLevels[0].getLevelName(), testLevels[0].getSubLevelName());
    }

    /**
     * Test that instance objects being used have also been persisted in
     * datastore as part of the InstanceHelper.initializeObjects call
     */
    @Test
    public void readPersistedLevel()
    {
        InetLevel tl = ofy.find(testLevels[0].getKey());
        assertTrue(tl != null);
        assertEquals(testLevels[0].getAcademicYear(), tl.getAcademicYear());
    }
}
