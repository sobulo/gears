/**
 * 
 */
package j9educationentities.messaging;

import static org.junit.Assert.assertNotNull;
import j9educationentities.InetConstants;
import j9educationentities.InetDAO4CommonReads;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.appengine.tools.development.testing.LocalTaskQueueTestConfig;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


/**
 * @author Segun Razaq Sobulo
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MessagingDAOTest {
    private static final LocalTaskQueueTestConfig taskConfig = new LocalTaskQueueTestConfig().setDisableAutoTaskExecution(false);
    private static final LocalServiceTestHelper helper = new
            LocalServiceTestHelper(new LocalDatastoreServiceTestConfig(), taskConfig);
    
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		helper.setUp();
		InetDAO4CommonReads.registerClassesWithObjectify();
	}    
    
	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
		helper.tearDown();
	}
	
	@Test
	public void writeBillingController() throws DuplicateEntitiesException
	{
		MessagingDAO.createBillingController(InetConstants.EMAIL_BILL_CONTROLLER_ID, "testme", 5000, 100, 1000);		
	}
	
	@Test
	public void xreadBillingController()
	{
		Objectify ofy = ObjectifyService.begin();
		Key<EmailBillingController> bc = new Key<EmailBillingController>(EmailBillingController.class, InetConstants.EMAIL_BILL_CONTROLLER_ID);
		EmailBillingController bcObj = ofy.find(bc);
		assertNotNull(bcObj);
	}
}
