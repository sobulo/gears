/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import static org.junit.Assert.assertEquals;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.InstanceTestHelper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetSingleTestScoresTest {

    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static InetSingleTestScores[] testScores;
    private static Objectify ofy;

    public InetSingleTestScoresTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        helper.setUp();
        ofy = ObjectifyService.begin();
        InstanceTestHelper.initializeObjects();
        testScores = InstanceTestHelper.getTestScoreList();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        testScores = null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
        ofy = null;
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCourseKey method, of class InetSingleTestScores.
     */
    @Test
    public void testGetCourseKey() {
        System.out.println("getCourseKey");
        InetSingleTestScores instance = testScores[0];
        String expResult = "2009-2010~2nd Term~JSS 1~A~English".toLowerCase();
        String result = instance.getCourseKey().getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getWeight method, of class InetSingleTestScores.
     */
    @Test
    public void aTestGetWeight() {
        System.out.println("getWeight");
        InetSingleTestScores instance = testScores[0];
        double expResult = 0.2;
        double result = instance.getWeight();
        assertEquals(expResult, result, 0.000001);
    }

    @Test
    public void aTestAddScores()
    {
        System.out.println("addScores");
        System.out.println("scores before adding: ");
        printScores(testScores[0]);
     
        InetTestScores ts = ofy.find(testScores[0].getKey());
        ts.addStudentScore(InstanceTestHelper.getTestStudentList()[0].getKey(), 10.0);
        ofy.put(ts);
        System.out.println("scores after adding");
        printScores((InetSingleTestScores) ts);
        
        ts = ofy.find((Key<InetSingleTestScores>) testScores[0].getKey());
        System.out.println("***found***: " + ts.getNumOfScores());   
    }
    
    @Test
    public void zReadPersistedNested()
    {
        InetTestScores ts = ofy.find(testScores[0].getKey());
        double expectedResult = 10.00;
        System.out.println("***found: " + ts.getNumOfScores());
        double result = ts.getTestScore(InstanceTestHelper.getTestStudentList()[0].getKey());
        printScores((InetSingleTestScores) ts);
        assertEquals(expectedResult, result, 0.00001);
    }    

    @Test
    public void zUpdateTestScores() throws MissingEntitiesException
    {
        HashMap<Key<InetStudent>,Double> studentScores = new HashMap<Key<InetStudent>,Double>(2);
        studentScores.put(InstanceTestHelper.getTestStudentList()[0].getKey(), 20.00);
        studentScores.put(InstanceTestHelper.getTestStudentList()[1].getKey(), 30.00);
        int expResult = 1;
        
        int result = (InetDAO4ComplexUpdates.updateTestScores((Key<InetSingleTestScores>) testScores[0].getKey(), studentScores, true)).size();
        assertEquals(expResult, result);
        
        InetSingleTestScores ts = ofy.find((Key<InetSingleTestScores>) testScores[0].getKey());
        System.out.println("***found**: " + ts.getNumOfScores());        
    }

    private void printScores(InetSingleTestScores t)
    {
        Iterator<Entry<String,Double>> scoreData = t.getStudentScores();
        int count = 0;
        while(scoreData.hasNext())
        {
            Entry<String,Double> e = scoreData.next();
            Key<InetStudent> studKey = ObjectifyService.factory().stringToKey(e.getKey());
            System.out.println("Student: " + studKey.getName() + " Score: " +
                    e.getValue());
            count++;
        }
        System.out.println("Printed " + count + " scores");
    }


    @Test
    public void readPersistedSimple()
    {
        InetSingleTestScores ts = ofy.find((Key<InetSingleTestScores>) testScores[0].getKey());
        System.out.println("***found*: " + ts.getNumOfScores());
        assertEquals(testScores[0].getTestName(), ts.getTestName());
    }
}