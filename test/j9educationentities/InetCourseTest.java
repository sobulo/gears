/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.InstanceTestHelper;

import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.appengine.tools.development.testing.LocalTaskQueueTestConfig;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetCourseTest {
    //TODO, try and get tasks working!!
    private static final LocalTaskQueueTestConfig taskConfig = new LocalTaskQueueTestConfig().setDisableAutoTaskExecution(false);
    private static final LocalServiceTestHelper helper = new
            LocalServiceTestHelper(new LocalDatastoreServiceTestConfig(), taskConfig);
    private static Objectify ofy;
    private static InetCourse[] testCourse;

    public InetCourseTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        ofy = ObjectifyService.begin();
        helper.setUp();
        InstanceTestHelper.initializeObjects();
        testCourse = InstanceTestHelper.getTestCourseList();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        testCourse = null;
        ofy=null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCumulativeScores method, of class InetCourse.
     */
    @Test
    public void test1GetCumulativeScores() {
        System.out.println("getCumulativeScores");
        String expResult = InetConstants.CUMULATIVE_TEST_NAME.toLowerCase();
        String result = InetDAO4CommonReads.
                getNameFromLastKeyPart(testCourse[0].getCumulativeScores());
        assertEquals(expResult, result);
    }

    /**
     * Test of getInetLevelKey method, of class InetCourse.
     */
    @Test
    public void test2GetInetLevelKey() {
        System.out.println("getInetLevelKey");
        Key<InetLevel> expResult = InstanceTestHelper.getTestLevelList()[0].getKey();
        Key<InetLevel> result = testCourse[0].getInetLevelKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of getKey method, of class InetCourse.
     */
    @Test
    public void test3GetKey() {
        System.out.println("getKey");
        Key<InetCourse> result = testCourse[0].getKey();
        assertTrue(ofy.getFactory().getRawKey(result).isComplete());
    }

    /**
     * Test of getTeacherKeys method, of class InetCourse.
     */
    @Test
    public void test4GetTeacherKeys() {
        System.out.println("getTeacherKeys");
        Key<InetTeacher> result = testCourse[0].getTeacherKey();
        System.out.println("DEBUG1: " + testCourse[0].getKey());
        System.out.println("DEBUG2: " + testCourse[0].getTeacherKey());
        assertTrue(result != null);
    }

    /**
     * Test of getTestScores method, of class InetCourse.
     */
    @Test
    public void test5GetTestScores() {
        System.out.println("getTestScores");
        InetCourse instance = testCourse[0];
        Key expResult = InstanceTestHelper.getTestScoreList()[0].getKey();
        Iterator<Key<InetSingleTestScores>> result = instance.getTestScoresKeys().iterator();
        assertEquals(expResult, result.next());
    }

    /**
     * Test of getCourseName method, of class InetCourse.
     */
    @Test
    public void test6GetCourseName() {
        System.out.println("getCourseName");
        InetCourse instance = testCourse[1];
        String expResult = "Mathematics";
        String result = instance.getCourseName();
        assertEquals(expResult, result);
    }

    @Test
    public void test7AddStudentsToCourse() throws MissingEntitiesException, DuplicateEntitiesException
    {
        System.out.println("addStudentsToCourse");
        int expectedResult = 1;
        String[] studentKeyStrings = new String[expectedResult];
        studentKeyStrings[0] = ofy.getFactory().keyToString(InstanceTestHelper.getTestStudentList()[0].getKey());
        String courseKeyStr = ofy.getFactory().keyToString(testCourse[0].getKey());
        String lkStr = ofy.getFactory().keyToString(InstanceTestHelper.getTestLevelList()[0].getKey());
        //TODO modify instance-test-helper to properly setup objects or alternatively look into whether support
        //for taskqueues now present for testing framework
        InetDAO4Updates.addStudentsToCourseTests(lkStr, courseKeyStr, studentKeyStrings, true, true);
    }

    @Test
    public void test8CheckStudentCount()
    {
        InetCourse c = ofy.find(testCourse[0].getKey());
        assertEquals(testCourse[0].getCourseName(), c.getCourseName());
        InetCumulativeScores cumScore = ofy.find(c.getCumulativeScores());
        int expectedResult = 1;
        int count = cumScore.getNumOfScores();
        printScores(cumScore);
        assertEquals(expectedResult, count);
    }

    @Test
    public void test9AddStudentsToCourse2() throws MissingEntitiesException, DuplicateEntitiesException
    {
        System.out.println("addStudentsToCourse");
        int expectedResult = 1;
        String[] studentKeyStrings = new String[expectedResult];
        studentKeyStrings[0] = ofy.getFactory().keyToString(InstanceTestHelper.getTestStudentList()[1].getKey());
        String courseKeyStr = ofy.getFactory().keyToString(testCourse[0].getKey());
        String lkStr = ofy.getFactory().keyToString(InstanceTestHelper.getTestLevelList()[0].getKey());
        InetDAO4Updates.addStudentsToCourseTests(lkStr, courseKeyStr, studentKeyStrings, true, true);
    }

    /**
     * Test that instance objects being used have also been persisted in  
     * datastore as part of the InstanceHelper.initializeObjects call
     */
    @Test
    public void test9ReadPersistedCourse()
    {
        System.out.println("****" + testCourse[0].getKey() + "********");
        InetCourse tc = ofy.find(testCourse[0].getKey());
        assertEquals(testCourse[0].getCourseName(), tc.getCourseName());
        InetSingleTestScores s = (InetSingleTestScores)
                ofy.find(InstanceTestHelper.getTestScoreList()[0].getKey());
        assertEquals(InstanceTestHelper.getTestScoreList()[0].getTestName(), s.getTestName());
        InetCumulativeScores cumScore = ofy.find(tc.getCumulativeScores());
        int expectedResult = 2;
        int result = cumScore.getStudentKeys().size();
        assertEquals(expectedResult, result);
    }

    private void printScores(InetCumulativeScores t) {
        Iterator<Entry<String, Double>> scoreData = t.getStudentScores();
        int count = 0;
        while (scoreData.hasNext()) {
            Entry<String, Double> e = scoreData.next();
            Key<InetStudent> studKey = ObjectifyService.factory().stringToKey(e.getKey());
            System.out.println("Student: " + studKey.getName() + " Score: " +
                    e.getValue());
            count++;
        }
        System.out.println("Printed " + count + " scores");
    }

}