/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationentities;



import static org.junit.Assert.assertEquals;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.InstanceTestHelper;

import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetCumulativeScoresTest {

    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;
    static InetCumulativeScores testScore;


    public InetCumulativeScoresTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        helper.setUp();
        ofy = ObjectifyService.begin();
        InstanceTestHelper.initializeObjects();
        testScore = getCumulativeScore(InstanceTestHelper.getTestCourseList()[0].getKey());
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        testScore = null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
        ofy = null;
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private static InetCumulativeScores getCumulativeScore(Key<InetCourse> ck)
    {
        return ofy.find(InetCumulativeScores.getCumScoreKey(ck));
    }

    private void printScores(InetCumulativeScores t) {
        Iterator<Entry<String, Double>> scoreData = t.getStudentScores();
        int count = 0;
        while (scoreData.hasNext()) {
            Entry<String, Double> e = scoreData.next();
            Key<InetStudent> studKey = ObjectifyService.factory().stringToKey(e.getKey());
            System.out.println("Student: " + studKey.getName() + " Score: " +
                    e.getValue());
            count++;
        }
        System.out.println("Printed " + count + " scores");
    }

    @Test
    public void test1AddStudents() throws MissingEntitiesException, DuplicateEntitiesException {
        String[] studentKeyStrings = new String[1];
        String skStr = ofy.getFactory().keyToString(InstanceTestHelper.getTestStudentList()[0].getKey());
        studentKeyStrings[0] = skStr;
        String ckStr = ofy.getFactory().keyToString(InstanceTestHelper.getTestCourseList()[0].getKey());
        String lkStr = ofy.getFactory().keyToString(InstanceTestHelper.getTestLevelList()[0].getKey());
        //TODO ... modify instance test helper to properly setup objects or alternatively look into
        //whether support for taskqueues now included in testing framework
        InetDAO4Updates.addStudentsToCourseTests(lkStr, ckStr, studentKeyStrings, true, true);
    }

    @Test
    public void test2ReadPersistedSimple() {

        InetCumulativeScores ts = (InetCumulativeScores) ofy.find(testScore.getKey());
        assertEquals(testScore.getTestName(), ts.getTestName());
    }

    @Test
    public void test3ReadPersistedNested()
    {
        InetCumulativeScores ts = (InetCumulativeScores) ofy.find(testScore.getKey());
        Key studKey = null;
        Iterator<Key<InetStudent>> studKeyItr = ts.getStudentKeys().iterator();
        if (studKeyItr.hasNext()) {
            studKey = studKeyItr.next();
        }
        assertEquals(InstanceTestHelper.getTestStudentList()[0].getKey(),
                studKey);
        System.out.println("Scores after reading from db: ");
        printScores(ts);
    }
}
