package j9educationentities;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationentities.InetHold.InetHoldType;
import j9educationutilities.InstanceTestHelper;

import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class InetHoldContainerTest {
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;
    private static InetHoldContainer holdContainer;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		helper.setUp();
		InstanceTestHelper.initializeObjects();
		holdContainer = new InetHoldContainer(InstanceTestHelper.getTestLevelList()[0].getKey());
		ofy = ObjectifyService.begin();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		InstanceTestHelper.releaseObjects();
		helper.tearDown();
	}
	
	@Test
	public void aaPersit()
	{
		Key<InetHoldContainer> hck = ofy.put(holdContainer);
		assertEquals(hck, holdContainer.getKey());
	}
	
	@Test
	public void addTests()
	{
		Objectify ofy2 = ObjectifyService.beginTransaction();
		try
		{
			InetHold hold1 = new InetHold(holdContainer.getKey(), InstanceTestHelper.getTestStudentList()[0].getKey(), InetHoldType.HOLD_FINANCIAL);
			InetHold hold2 = new InetHold(holdContainer.getKey(), InstanceTestHelper.getTestStudentList()[0].getKey(), InetHoldType.HOLD_ACADEMIC_PROBATION);
			InetHold hold3 = new InetHold(holdContainer.getKey(), InstanceTestHelper.getTestStudentList()[1].getKey(), InetHoldType.HOLD_FINANCIAL);
			ArrayList<InetHold> holdObjs = new ArrayList<InetHold>();
			holdObjs.add(hold3);
			holdObjs.add(hold2);
			holdObjs.add(hold1);
			ofy2.put(holdObjs);
			assertEquals(hold1.getKey().getParent(), hold2.getHoldContainer());
			
			assertTrue(holdContainer.addHold(hold1.getKey()));
			assertTrue(holdContainer.addHold(hold2.getKey()));
			assertTrue(holdContainer.addHold(hold3.getKey()));
			assertEquals(false, holdContainer.addHold(hold1.getKey()));
			ofy2.put(holdContainer);
			ofy2.getTxn().commit();
		}
		finally
		{
			if(ofy2.getTxn().isActive())
				ofy2.getTxn().rollback();
		}
	}
	
	@Test
	public void readPersisted()
	{
		InetHoldContainer hc= ofy.find(holdContainer.getKey());
		assertEquals(hc.getHolds().size(), holdContainer.getHolds().size());
		assertTrue(holdContainer.removeHold(hc.getHolds().iterator().next()));
	}

}
