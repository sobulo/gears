/**
 * 
 */
package j9educationentities;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationentities.InetHold.InetHoldType;
import j9educationutilities.InstanceTestHelper;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetHoldTest {
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;
    private static InetHold sampleHold;
    
    private final static String TEST_MESSAGE = "Please pay your bills on time";
    private final static String TEST_NOTE = "Made 2 attempts to collect payment";
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		helper.setUp();
		InstanceTestHelper.initializeObjects();
		Key<InetHoldContainer> holdGrouping = new Key<InetHoldContainer>(InetHoldContainer.class, InstanceTestHelper.getTestLevelList()[0].getKey().getName()); 
		sampleHold = new InetHold(holdGrouping, 
				InstanceTestHelper.getTestStudentList()[0].getKey(), InetHoldType.HOLD_FINANCIAL);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		InstanceTestHelper.releaseObjects();
		helper.tearDown();
	}
	
	@Test
	public void aSetMessage()
	{
		assertTrue(sampleHold.getMessage() == null);
		sampleHold.setMessage(new Text(TEST_MESSAGE));
		assertTrue(sampleHold.getMessage() != null);
	}
	
	@Test
	public void bGetMessage()
	{
		assertEquals(TEST_MESSAGE, sampleHold.getMessage().getValue());
	}
	
	@Test
	public void cWriteHold()
	{
		ofy = ObjectifyService.begin();
		Key<InetHold> holdKey = ofy.put(sampleHold);
		assertEquals(holdKey, sampleHold.getKey());
		System.out.println("Hold Key: " + holdKey);
		
	}
	
	@Test
	public void dReadHold()
	{
		InetHold persistedHold = ofy.find(sampleHold.getKey());
		assertEquals(sampleHold.getMessage(), persistedHold.getMessage());
	}
}
