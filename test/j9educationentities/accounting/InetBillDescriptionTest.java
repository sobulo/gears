/**
 * 
 */
package j9educationentities.accounting;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetStudent;
import j9educationgwtgui.shared.BillDescriptionItem;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.InstanceTestHelper;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.appengine.tools.development.testing.LocalTaskQueueTestConfig;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


/**
 * @author Segun Razaq Sobulo
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetBillDescriptionTest {
    //TODO, try and get tasks working!!
    private static final LocalTaskQueueTestConfig taskConfig = new LocalTaskQueueTestConfig().setDisableAutoTaskExecution(false);
    private static final LocalServiceTestHelper helper = new
            LocalServiceTestHelper(new LocalDatastoreServiceTestConfig(), taskConfig);
    private static InetBillDescription billDescription;
    private static InetStudent[] students;
    private static Key<InetBillPayment> savedPK;
    
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		helper.setUp();
		InstanceTestHelper.initializeObjects();
		students = InstanceTestHelper.getTestStudentList();
		InetDAO4CommonReads.registerClassesWithObjectify();
		LinkedHashSet<BillDescriptionItem> bdi = new LinkedHashSet<BillDescriptionItem>();
		bdi.add(new BillDescriptionItem("books", "student textbooks", 150));
		bdi.add(new BillDescriptionItem("lesson fee", "afterschool program", 1050));
		
		billDescription = InetDAO4Accounts.createBillDescription("2009", "1st Term", null, "Tuition", bdi);
		assertEquals(2, billDescription.getItemizedBill().size());
		assertEquals(1200, billDescription.getTotalAmount(), .000000000000000000001);
		
	}
	
	@Test
	public void readPersistedObject()
	{
		Objectify ofy = ObjectifyService.begin();
		InetBillDescription bd = ofy.find(billDescription.getKey());
		String expectedResult = "1st Term";
		assertEquals(expectedResult, bd.getTerm());
		assertEquals(2, bd.getItemizedBill().size());
		assertEquals(1200, bd.getTotalAmount(), .000000000000000000001);
	}
	
	@Test
	public void createBill() throws DuplicateEntitiesException
	{
		InetBill bill1 = InetDAO4Accounts.createBill(billDescription, students[0].getKey());
		InetBill bill2 = InetDAO4Accounts.createBill(billDescription, students[1].getKey());
		assertEquals(bill1.getTotalAmount(), bill2.getTotalAmount(), 0.0000000000000001);
		assertEquals(bill1.getTotalAmount(), billDescription.getTotalAmount(), 0.000000000001);
	}
	
	@Test
	public void readBill()
	{
		Objectify ofy = ObjectifyService.begin();
		Key<InetBill> billKey = new Key<InetBill>(InetBill.class, InetBill.getKeyString(billDescription.getKey(), students[0].getKey()));
		InetBill bill = ofy.find(billKey);
		assertNotNull(bill);
	}
	
	@Test
	public void createPayment() throws MissingEntitiesException, ManualVerificationException
	{
		Key<InetBill> billKey = new Key<InetBill>(InetBill.class, InetBill.getKeyString(billDescription.getKey(), students[0].getKey()));
		savedPK = (InetDAO4Accounts.createPayment(billKey, 400, "xyz", "test payment", new Date())).getKey();
		InetDAO4Accounts.createPayment(billKey, 600, "xyz", "test payment", new Date());
		Objectify ofy = ObjectifyService.begin();
		InetBill bill = ofy.find(billKey);
		assertEquals(1000, bill.getSettledAmount(), 0.0001);
	}
	
	@Test
	public void readPayment()
	{
		Objectify ofy = ObjectifyService.begin();
		System.out.println("SAVED: " + savedPK);
		InetBillPayment bp = ofy.find(savedPK);
		assertEquals(400, bp.getAmount(), 0.0000001);
	}
	
	@Test
	public void queryObjects()
	{
		Objectify ofy = ObjectifyService.begin();
		List<InetBill> bills = InetDAO4Accounts.getBills(billDescription.getKey(), ofy);
		assertEquals(2, bills.size());
		Key<InetBill> billKeyToFind = null;
		if(bills.get(0).getUserKey().equals(students[0].getKey()))
			billKeyToFind = bills.get(0).getKey();
		else
			billKeyToFind = bills.get(1).getKey();
		List<InetBillPayment> billPayments = InetDAO4Accounts.getPayments(billKeyToFind, ofy);
		assertEquals(2, billPayments.size());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
		helper.tearDown();
	}
}
