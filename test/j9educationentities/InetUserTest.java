/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationgwtgui.shared.exceptions.MultipleEntitiesException;
import j9educationutilities.InstanceTestHelper;

import java.util.HashSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetUserTest {

    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;

    static InetUser[] testUsers;

    public InetUserTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        helper.setUp();
        ofy = ObjectifyService.begin();
        InstanceTestHelper.initializeObjects();
        testUsers = InstanceTestHelper.getTestUserList();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        testUsers = null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
        ofy = null;
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getKey method, of class InetUser.
     */
    @Test
    public void test1SimpleGet() {
        System.out.println("Testing if attribute can be read");
        InetUser instance = testUsers[0];
        String expResult = "James";
        String result = instance.getFname();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getKey method, of class InetUser.
     */
    @Test
    public void test2GetKey() {
        System.out.println("getKey");
        InetUser instance = testUsers[0];
        String expResult = "jwade";
        String result = instance.getKey().getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class InetUser.
     */
    @Test
    public void test3GetEmail() {
        System.out.println("getEmail");
        InetUser instance = testUsers[1];
        String expResult = "emc2@useful.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmail method, of class InetUser.
     */
    @Test
    public void test4SetEmail() {
        System.out.println("setEmail");
        String email = "emc3@moreuseful.com";
        InetUser instance = testUsers[0];
        instance.setEmail(email);
        assertEquals(email, instance.getEmail());
    }

    /**
     * Test of getFname method, of class InetUser.
     */
    @Test
    public void test5GetFname() {
        System.out.println("getFname");
        InetUser instance = testUsers[0];
        String expResult = "James";
        String result = instance.getFname();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLname method, of class InetUser.
     */
    @Test
    public void test6GetLname() {
        System.out.println("getLname");
        InetUser instance = testUsers[0];
        String expResult = "Wade";
        String result = instance.getLname();
        assertEquals(expResult, result);
    }

        /**
     * Test of addNumber method, of class InetUser.
     */
    @Test
    public void test7AddNumber() {
        System.out.println("addNumber");
        String num = "1234";
        InetUser instance = testUsers[0];
        boolean expResult = true;
        HashSet<String> temp = new HashSet<String>();
        boolean result = temp.add(num);
        instance.setPhoneNumbers(temp);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPhoneNumbers method, of class InetUser.
     */
    @Test
    public void test8GetPhoneNumbers() {
        System.out.println("getPhoneNumbers");
        InetUser instance = testUsers[0];
        String expResult = "1234";
        String result = instance.getPhoneNumbers().iterator().next();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataGem method, of class InetUser.
     */
    /*@Test
    public void testGetDataGem() {
        System.out.println("getDataGem");
        InetUser instance = null;
        DataGem expResult = null;
        DataGem result = instance.getDataGem();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    
    /**
     * Test that instance objects being used have also been persisted in
     * datastore as part of the InstanceHelper.initializeObjects call
     */
    @Test
    public void test9ReadPersistedUser() throws MultipleEntitiesException
    {
         InetUser u = ofy.find(testUsers[1].getKey());
         assertEquals(testUsers[1].getFname(), u.getFname());
         u = InetDAO4CommonReads.getUserByEmail(ofy, InetUser.class, testUsers[1].getEmail());
         assertTrue(u != null);
    }
}