/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Administrator
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetGradeDescriptionsTest {

    static InetGradeDescriptions testGradeDescs;
    final static String outputFile = "sampleslettergrades.txt";

    public InetGradeDescriptionsTest() 
    {
        
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        testGradeDescs = new InetGradeDescriptions("F", 1.0);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addLetterGrade method, of class InetGradeDescriptions.
     */
    @Test
    public void testAddLetterGrade() {
        System.out.println("addLetterGrade");
        String gradeDescription = "A";
        Double gradeDescCutoff = 90.0;
        InetGradeDescriptions instance = testGradeDescs;
        instance.addGradeDescription(gradeDescription, gradeDescCutoff, 4.0);
        instance.addGradeDescription("B", 80.0, 3.0);
    }

    /**
     * Test of addLetterGrade method, of class InetGradeDescriptions.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAddLetterGradeBad() {
        System.out.println("addLetterGrade-fail");
        testGradeDescs.addGradeDescription("C", 70.0, 4.0);
    }

    /**
     * Test of addLetterGrade method, of class InetGradeDescriptions.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAddLetterGradeBadAlso() {
        System.out.println("addLetterGrade-fail");
        testGradeDescs.addGradeDescription("X", 95.0, 3.5);
    }

    /**
     * Test of getGradeDescription method, of class InetGradeDescriptions.
     */
    @Test
    public void testGetGradeDescription() {
        System.out.println("getGradeDescription");
        Double grade = 93.4;
        InetGradeDescriptions instance = testGradeDescs;
        String expResult = "A";
        String result = instance.getLetterGrade(grade);
        assertEquals(expResult, result);
    }

    @Test
    public void testWrite() throws IOException
    {
        System.out.println("testing map write");
        File f = new File(outputFile);
        ObjectOutputStream outputter = new ObjectOutputStream(new FileOutputStream(f));
        outputter.writeObject(testGradeDescs);
        outputter.close();
        assertTrue(true);
    }

    @Test
    public void testWriteRead() throws IOException, ClassNotFoundException
    {
        System.out.println("testing map read");
        File f = new File(outputFile);
        ObjectInputStream inputter = new ObjectInputStream(new FileInputStream(f));
        Object o = inputter.readObject();
        String expResult = "B";
        String result = ((InetGradeDescriptions) o).getLetterGrade(84.3);
        assertEquals(expResult, result);
    }

}