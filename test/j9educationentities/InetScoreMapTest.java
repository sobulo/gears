/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationutilities.GeneralFuncs;
import j9educationutilities.InstanceTestHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InetScoreMapTest {
    private static InetScoreMap scores;
    private static Key<InetStudent>[] sk;
    private static InetStudent[] testStudentList;
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;

    final static String outputFile = "tmpsamplescore.txt";

    public InetScoreMapTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        helper.setUp();
        InstanceTestHelper.initializeObjects();
        ofy = ObjectifyService.begin();
        scores = new InetScoreMap();
        sk = new Key[2];
        testStudentList = new InetStudent[2];
            testStudentList[0] = InetDAO4Creation.createStudent(
                    "johnfresh", "doe", GeneralFuncs.getDate(1990, 1, 14), "j.doe@x.com",
                    null, "jdoenew", "123", ofy.getFactory().keyToString(InstanceTestHelper.getTestLevelList()[0].getKey()));

            testStudentList[1] = InetDAO4Creation.createStudent(
                    "amyfresh", "wyn", null, "s.luv@ht.com", null, "awynnew",
                    "123", ofy.getFactory().keyToString(InstanceTestHelper.getTestLevelList()[1].getKey()));

        sk[0] = testStudentList[0].getKey();
        sk[1] = testStudentList[1].getKey();
        InstanceTestHelper.releaseObjects();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        scores = null;
        sk = null;
        InstanceTestHelper.releaseObjects();
        helper.tearDown();
        ofy = null;
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addScore method, of class InetScoreMap.
     */
    @Test
    public void test1AddScore() {
        System.out.println("addScore");
        Key k = sk[0];
        Double s = 56.9;
        InetScoreMap instance = scores;
        boolean expResult = true;
        boolean result = instance.addScore(k, s);
        assertEquals(expResult, result);
        expResult = false;
        Key<InetStudent> k2 = sk[1];
        s = 90.0;
        instance.addScore(k2, s);
        result = instance.addScore(k2, s);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeScore method, of class InetScoreMap.
     */
    @Test
    public void test2RemoveScore() {
        System.out.println("removeScore");
        Key<InetStudent> k = sk[1];
        InetScoreMap instance = scores;
        boolean expResult = true;
        boolean result = instance.removeScore(k);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateScore method, of class InetScoreMap.
     */
    @Test
    public void test3UpdateScore() {
        System.out.println("updateScore");
        Key<InetStudent> k = sk[1];
        Double s = 90.1;
        InetScoreMap instance = scores;
        boolean expResult = false;
        boolean result = instance.updateScore(k, s);
        assertEquals(expResult, result);
        expResult = true;
        k = sk[0];
        result = instance.updateScore(k, s);
        assertEquals(expResult, result);
    }

    /**
     * Test of getScores method, of class InetScoreMap.
     */
    @Test
    public void test4GetScores() {
        System.out.println("getScores");
        Double expResult = 90.1;
        Double result = scores.getScores().next().getValue();
        assertEquals(expResult, result);
    }

    @Test
    public void test5WriteMap() throws IOException
    {
        System.out.println("testing map write");
        File f = new File(outputFile);
        ObjectOutputStream outputter = new ObjectOutputStream(new FileOutputStream(f));
        outputter.writeObject(scores);
        outputter.close();
        assertTrue(true);
    }

    @Test
    public void test6ReadMap() throws IOException, ClassNotFoundException
    {
        System.out.println("testing map read");
        File f = new File(outputFile);
        ObjectInputStream inputter = new ObjectInputStream(new FileInputStream(f));
        Object o = inputter.readObject();
        Entry<String,Double> result = ((InetScoreMap) o).getScores().next();
        Entry<String,Double> expResult = scores.getScores().next();
        assertEquals(expResult.getKey(), result.getKey());
        assertEquals(expResult.getValue(), result.getValue());
    }
}