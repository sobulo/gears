/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.InstanceTestHelper;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
public class StringOptionsTest {
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());

    private static Objectify ofy;

    static StringOptions testOption;

    public StringOptionsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        ofy = ObjectifyService.begin();
        InetDAO4CommonReads.registerClassesWithObjectify();
        helper.setUp();
        HashSet<String> temp = new HashSet<String>();
        temp.add("apple");
        temp.add("pear");
        testOption = InetDAO4Creation.createStringOptions("frt", "fruits");
        //refresh
        testOption = ofy.find(testOption.getKey());
        assertTrue(testOption.getOptionElements().size() == 0);
        testOption.setOptionElements(temp);

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    	InstanceTestHelper.releaseObjects();
        helper.tearDown();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setDisplayName method, of class StringOptions.
     */
    @Test
    public void testSetDisplayName() {
        System.out.println("setDisplayName");
        String displayName = "american fruits";
        testOption.setDisplayName(displayName);
    }

    /**
     * Test of getDisplayName method, of class StringOptions.
     */
    @Test
    public void testGetDisplayName() {
        System.out.println("getDisplayName");
        String expResult = "american fruits";
        String result = testOption.getDisplayName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getKey method, of class StringOptions.
     */
    @Test
    public void testGetKey() {
        System.out.println("getKey");
        String expResult = "frt";
        String result = testOption.getKey().getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOptionElements method, of class StringOptions.
     * @throws PanelServiceMissingException 
     */
    @Test
    public void testSetOptionElements() throws MissingEntitiesException {
        System.out.println("setOptionElements");
        HashSet<String> optionElements = new HashSet<String>();
        optionElements.add("strawberry");
        optionElements.add("raspberry");
        InetDAO4Updates.updateStringOptions(testOption.getKey().getName(), optionElements);
    }

      /**
     * Test of getOptionElements method, of class StringOptions.
     */
    @Test
    public void testGetOptionElements() {
        StringOptions tc = ofy.get(StringOptions.class, testOption.getKey().getName());
        System.out.println("getOptionElements");
        HashSet result = tc.getOptionElements();
        assertTrue(result.contains("raspberry"));
    }

    @Test(expected=NotFoundException.class)
    public void testPersistenceRead()
    {
        ofy.get(StringOptions.class, "nada");
    }

    @Test
    public void testPersistenceWrite() throws EntityNotFoundException
    {
        //lets check ofy plays well with low-level datastore read
        Entity e = ofy.getDatastore().get(ofy.getFactory().getRawKey(testOption.getKey()));

        //write it back using low level write
        ofy.getDatastore().put(e);

    }

    @Test
    public void testPerisitenceReadAgain()
    {
        DatastoreService ds = ofy.getDatastore();
        try {
            Entity e = ds.get(ofy.getFactory().getRawKey(testOption.getKey()));
            System.out.println(e);
            Map<String,Object> props = e.getProperties();
            assertTrue(!props.isEmpty());
            Set keys = props.keySet();
            Iterator<String> keyitr = keys.iterator();
            while(keyitr.hasNext())
            {
                String k = keyitr.next();
                System.out.println(k + "--" + props.get(k));
            }
            assertTrue(true);
            //readPersistedOption();
        } catch (EntityNotFoundException ex) {
            System.out.println("didn't find key");
            assertTrue(false);
        }

        //readPersistedOption();
    }
}