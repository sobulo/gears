<%-- 
    Document   : adduser
    Created on : Jun 20, 2010, 4:58:01 PM
    Author     : Segun Razaq Sobulo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="j9educationactions.login.LoginPortal" %>
<%@page import="j9educationgwtgui.shared.PanelServiceLoginRoles" %>
<%@page import="j9educationgwtgui.shared.exceptions.LoginValidationException" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add a new user</title>
    </head>
    <body>
    <%
	  	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
		try 
		{
	    	LoginPortal.verifyRole(allowedRoles, request);
	    }
		catch(LoginValidationException ex)
		{
			throw new RuntimeException(ex.fillInStackTrace());
		}    
    %>
        <h1>Add User</h1>
        <form name="adduser" action="/adduser" method="post">
            <b>First Name</b><input type="text" name="fname"/><br />
            <b>Last Name</b><input type="text" name="lname"/><br />
            <b>Email</b><input type="text" name="email"/><br />
            <b>Login Name</b><input type="text" name="login"/><br />
            <b>Password</b><input type="text" name="password"/><br />
            <input type="submit" value ="Add User" />
        </form>
    </body>
</html>
