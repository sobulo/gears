<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="j9educationentities.*" %>
<%@page import="java.util.Set" %>
<%@page import="com.googlecode.objectify.*" %>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <%
    Objectify ofy = ObjectifyService.begin();
    InetSchool school = InetDAO4CommonReads.getSchool(ofy);
    Set<Key<InetLevel>> levelKeys = school.getInetLevelKeys();
    out.println("<ul>");
    for(Key<InetLevel> lk : levelKeys)
    {
    	InetHoldContainer hc = InetDAO4Creation.createHoldContainer(lk);
    	out.println("<li>" + hc.getKey().toString() + "</li>");
    }
    out.println("</ul>");
    %>
</body>
</html>