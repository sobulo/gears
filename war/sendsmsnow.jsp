<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="j9educationactions.login.LoginPortal" %>
<%@page import="j9educationgwtgui.shared.PanelServiceLoginRoles" %>
<%@page import="j9educationgwtgui.shared.exceptions.LoginValidationException" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Send SMS Now</title>
</head>
<body>
    <%
	  	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
		try 
		{
	    	LoginPortal.verifyRole(allowedRoles, request);
	    }
		catch(LoginValidationException ex)
		{
			throw new RuntimeException(ex.fillInStackTrace());
		}    
    %>
        <h1>Add User</h1>
        <form name="adduser" action="/sendsms" method="post">
            <b>Destination</b><input type="text" name="destination"/><br />
            <b>Message</b><input type="text" name="message"/><br />
            <input type="submit" value ="Send SMS" />
        </form>
        
</body>
</html>