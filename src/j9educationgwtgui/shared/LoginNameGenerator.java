/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface LoginNameGenerator extends Serializable{
    public String generateLoginName(String[] components);
    public String getDescription();
}
