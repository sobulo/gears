/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared;

/**
 *
 * @author Segun Razaq Sobulo
 */
public final class PanelServiceConstants {
    //string option names
    public final static String ACADEMIC_YEAR_LABEL = "Academic Years";
    public final static String TERM_LABEL = "Academic Terms";
    public final static String CLASS_NAME_LABEL = "Class Names";
    public final static String SUB_CLASS_NAME_LABEL = "Sub-Class Names";
    public final static String DEFAULT_SUBJECTS_LABEL = "Default Subjects";

    //download types
    public final static String DOWNLOAD_ROSTER = "roster";
    public final static String DOWNLOAD_GRADES = "grades";
    public final static String DOWNLOAD_CLASS_GRADES = "classgrades";
    public final static String DOWNLOAD_CLASS_GRADES_WITH_GPA = "classgpa";
    public final static String DOWNLOAD_CLASS_GRADES_WITH_CUM = "classcumulative";
    public final static String DOWNLOAD_CLASS_WITH_SUM = "classsummary";
    public enum ClassGradesGPAType {NONE, GPA, CUMULATIVE, SUMMARY};
    
    //consts for form upload panels
    public final static String GOOD_DATA_PREFIX = "<i></i>";
    public final static String LOAD_PARAM_NAME = "ld";
    public final static String TESTKEY_PARAM_NAME = "tks";
    public final static String CLASSKEY_PARAM_NAME = "cks";
    
    public final static String DATA_SEPERATOR = "||";
    public final static String DATA_SEPERATOR_REGEX = "\\|\\|";
    
    //data validation constants
    public final static String REGEX_EMAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    public final static String REGEX_NUMS_ONLY = "^\\d{6,}$";
    public final static String REGEX_PHONE_REPLACE = "-|\\(|\\)";
    
    //custom login provider
    public final static String PROVIDER_GREP = "grep";
    public final static String HOLD_RESOLVED_TRUE = "closed";
    public final static String HOLD_RESOLVED_FALSE = "open";
    
    public final static String GRADE_HEADER = "Grade";
    
    //school info fields
    public final static int SCHOOL_INFO_NAME_IDX = 0;
    public final static int SCHOOL_INFO_ADDR_IDX = 1;
    public final static int SCHOOL_INFO_NUMS_IDX = 2;
    public final static int SCHOOL_INFO_EMAIL_IDX = 3;
    public final static int SCHOOL_INFO_WEB_IDX = 4;
    public final static int SCHOOL_INFO_ACCR_IDX = 5;
    
    public final static int COURSE_UNIT_IDX = 1;
    
    //notices for welcome page
	public final static String PUBLIC_NOTE_ID = "General Public";
	public final static String TEACHER_NOTE_ID = "All Teachers";
	public final static String STUDENT_NOTE_ID = "All Students";
	public final static String GUARDIAN_NOTE_ID = "All Parents/Guardians";
	public final static String ADMIN_NOTE_ID = "All Admins";
	
	public final static String[] ROLE_NOTES = {PUBLIC_NOTE_ID, STUDENT_NOTE_ID, GUARDIAN_NOTE_ID, TEACHER_NOTE_ID, ADMIN_NOTE_ID};
	
	public final static String ROLE_TYPE_REQ = "usertype";
	
	public final static String STUDENT_DATA_INDICATOR = "S";
	public final static String GUARDIAN_DATA_INDICATOR = "G";
}
