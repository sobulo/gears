/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared;

import java.io.Serializable;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class GradeDescription implements Serializable
{
    private static final long serialVersionUID = 8878555067612366216L;
    public String letterGrade;
    public double gpa;

    public GradeDescription(){}

    public GradeDescription(String letterGrade, double gpa)
    {
        this.letterGrade = letterGrade;
        this.gpa = gpa;
    }
}
