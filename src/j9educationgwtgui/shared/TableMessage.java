package j9educationgwtgui.shared;

import j9educationgwtgui.shared.TableMessageHeader.TableMessageContent;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;

/**
 * A simple dto model object
 *
 * ArrayList for each type supported, user is expected to know index containing
 * corresponding to requested and send it as parameter to appropiate getter/setter
 * for the requested type
 */
public class TableMessage implements Serializable, Comparable<TableMessage>{
    private String[] textFields;
    private Double[] numberFields;
    private Date[] dateFields;
    private String messageId;
	private int sortField;
	private TableMessageContent sortType;


    public TableMessage(){}

    /**
     * @param numOfTextFields
     * @param numOfDoubleFields
     * @param numOfDateFields
     */
    public TableMessage(int numOfTextFields, int numOfDoubleFields,
            int numOfDateFields)
    {
		if (numOfDateFields > 0) {
			dateFields = new Date[numOfDateFields];
			sortType = TableMessageContent.DATE;
		}
		if (numOfDoubleFields > 0) {
			numberFields = new Double[numOfDoubleFields];
			sortType = TableMessageContent.NUMBER;
		}
		if (numOfTextFields > 0) {
			textFields = new String[numOfTextFields];
			sortType = TableMessageContent.TEXT;
		}		
		this.messageId = "";
		this.sortField = 0;
    }

    public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public int getNumberOfDateFields()
    {
        return( dateFields == null ? 0 : dateFields.length);
    }

    public int getNumberOfTextFields()
    {
        return( textFields == null ? 0 : textFields.length);
    }

    public int getNumberOfDoubleFields()
    {
        return( numberFields == null ? 0 : numberFields.length);
    }

    public String getText(int index) {
        return textFields[index];
    }

    public void setText(int index, String val) {
        textFields[index] = val;
    }

    public Double getNumber(int index) {
        return numberFields[index];
    }

    public void setNumber(int index, Number val) {
    	Double set = val == null? null : val.doubleValue();
        numberFields[index] = set;
    }

    public Date getDate(int index) {
        return dateFields[index];
    }

    public void setDate(int index, Date val) {
        dateFields[index] = val;
    }

    @Override
    public String toString() {
        String result = "Text: ";
        if (textFields == null) {
            result += "None ";
        } else {
            for (int i = 0; i < textFields.length; i++) {
                result += i + "->" + getText(i) + " ";
            }
        }
        result += "\nNumber: ";
        if (numberFields == null) {
            result += "None ";
        } else {
            for (int i = 0; i < numberFields.length; i++) {
                result += i + "->" + getNumber(i) + " ";
            }
        }
        result += "\nDate: ";
        if (dateFields == null) {
            result += "None ";
        } else {
            for (int i = 0; i < dateFields.length; i++) {
                result += i + "->" + getDate(i) + " ";
            }
        }
        return result;
    }
    
    public static void sort(List<TableMessage> list, int sortField, TableMessageContent fieldType, boolean isAscending)
    {
    	Collections.sort(list, new MessageComparator(fieldType, sortField, isAscending));
    }
    
    private static class MessageComparator implements Comparator<TableMessage>
    {
    	int sortField;
    	TableMessageContent fieldType;
    	boolean isAscending;
    	
    	MessageComparator(TableMessageContent fieldType, int sortField, boolean isAscending)
    	{
    		this.sortField = sortField;
    		this.fieldType = fieldType;
    		this.isAscending = isAscending;
    	}

    	/* (non-Javadoc)
    	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
    	 */
    	@Override
    	public int compare(TableMessage o1, TableMessage o2) {
    		int returnVal = 0; 
    		try
    		{
    		if(o1 instanceof TableMessageHeader && o2 instanceof TableMessageHeader)
    			return 0;
    		if(o1 instanceof TableMessageHeader)
    			return -1;
    		else if(o2 instanceof TableMessageHeader)
    			return 1;
    		else if(fieldType.equals(TableMessageContent.TEXT))
    			returnVal = o1.getText(sortField).compareTo(o2.getText(sortField));
    		else if(fieldType.equals(TableMessageContent.NUMBER))
    			returnVal = o1.getNumber(sortField).compareTo(o2.getNumber(sortField));
    		else if(fieldType.equals(TableMessageContent.DATE))
    			returnVal = o1.getDate(sortField).compareTo(o2.getDate(sortField));
    		else
    			returnVal = 0;
    		}
    		catch(RuntimeException ex)
    		{
    			GWT.log("obj1: " + o1.toString());
    			GWT.log("obj2: " + o2.toString());
    			GWT.log("Sort Field: " + sortField);
    			GWT.log("Field Type: " + fieldType);
    			GWT.log("Ascending: " + isAscending);
    			throw ex;
    		}
    		return returnVal * (isAscending?1:-1);
    	}
    	
    }
    
	public static Comparator<TableMessage> getMessageComparator(int sortField, TableMessageContent fieldType)
	{
		return new MessageComparator(fieldType, sortField, true);
	}    
	
	@Override
	public int compareTo(TableMessage o)
	{
		MessageComparator comparator = new MessageComparator(sortType,
				sortField, true);
		return o == null ? 1 : comparator.compare(this, o);
	}
	
    @Override
    public boolean equals(Object o)
    {
      if (o instanceof TableMessage) {
        if(messageId.equals(((TableMessage) o).messageId))
        {
        	if(messageId.equals(""))
        	{
        		return (compareTo((TableMessage) o) == 0);
        	}
        	return true;
        }
      }
      return false;
    }
	
}