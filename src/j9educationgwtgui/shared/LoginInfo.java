/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.shared;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Segun Razaq Sobulo
 */

public class LoginInfo implements Serializable {
    private boolean loggedIn = false;
    private HashMap<String, String> loginUrl;
    private String logoutUrl;
    private String name;
    private String loginID;
    private String schoolName;
    private String schoolAccronym;
    private String message;
    private PanelServiceLoginRoles role;

    public LoginInfo() {
	}
    
    public PanelServiceLoginRoles getRole() {
        return role;
    }

    public void setRole(PanelServiceLoginRoles role) {
        this.role = role;
    }


    public String getSchoolAccronym() {
        return schoolAccronym;
    }

    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setSchoolAcrronym(String schoolKey) {
        this.schoolAccronym = schoolKey;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public HashMap<String, String> getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(HashMap<String, String> loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String displayName) {
        this.name = displayName;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String userKey) {
        this.loginID = userKey;
    }
}
