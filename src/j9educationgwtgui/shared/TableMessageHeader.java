/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.shared.GWT;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class TableMessageHeader extends TableMessage{
    public final static int TM_DIFF_HEADER = 0;
    public final static int TM_DIFF_VALS = 1;
    public final static int TM_DIFF_POS = 2;
    public enum TableMessageContent{TEXT, NUMBER, DATE};
    private TableMessageContent[] typeOrdering;
    private String[] formatOptions;
    private String[] groupNames;
    private boolean[] isEditable;
    //private int typeCursor;
    private String caption;

    public TableMessageHeader()
    {
        super();
    }

    public TableMessageContent getHeaderType(int index)
    {
        return typeOrdering[index];
    }

    public int getNumberOfHeaders()
    {
        return typeOrdering.length;
    }
    
    public TableMessageHeader(int numOfHeaders)
    {
        super(numOfHeaders, 0, 0);
        typeOrdering = new TableMessageContent[numOfHeaders];
        isEditable = new boolean[numOfHeaders];
        groupNames = new String[numOfHeaders];
        formatOptions = new String[numOfHeaders];
        //typeCursor = 0;
    }

    
    public String getGroupName(int index) {
		return groupNames[index];
	}

	public void setGroupName(int index, String groupName) {
		groupNames[index] = groupName;
	}

	public void setText(int headerPosition, String val, TableMessageContent 
            headerContentType)
    {
        super.setText(headerPosition, val);
        typeOrdering[headerPosition] = headerContentType;
        //typeOrdering[typeCursor] = headerContentType;
        //typeCursor++;
    }
    
    public void setEditable(int headerPosition, boolean isEditable)
    {
    	TableMessageContent headerContentType = getHeaderType(headerPosition);
    	if(headerContentType == TableMessageContent.NUMBER)
    	{
    		this.isEditable[headerPosition] = isEditable;
    	}
    	else
    		throw new UnsupportedOperationException("Only number editing supported");
    }
    
    public boolean isEditable(int headerPosition)
    {
    	TableMessageContent headerContentType = getHeaderType(headerPosition);
    	if(headerContentType == TableMessageContent.NUMBER)
    	{
    		return this.isEditable[headerPosition];
    	}
    	else
    		throw new UnsupportedOperationException("Only number editing supported");    	
    }

    @Override
    public void setNumber(int index, Number val)
    {
        throw new UnsupportedOperationException("only texts supported");
    }

    @Override
    public void setDate(int index, Date val)
    {
        throw new UnsupportedOperationException("only texts supported");
    }

    @Override
    public void setText(int index, String val)
    {
        throw new UnsupportedOperationException("use setText(index, val, " +
                "TableMessageContent)");
    }

    public String getCaption()
    {
        return caption;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }
    
     
    public String getFormatOption(int i) {
		return formatOptions[i];
	}

	public void setFormatOption(int i, String formatOption) {
		this.formatOptions[i] = formatOption;
	}

	public static TableMessage[] deepTableMessageCompare(TableMessage oldM, TableMessage newM, String[] diffTextTitles, String[] diffNumberTitles, String[] diffDateTitles)
    {
    	GWT.log("Comparing field checks");
    	if(oldM.getNumberOfTextFields() != newM.getNumberOfTextFields())
    		throw new RuntimeException("Text field lengths do not match " + oldM.getNumberOfTextFields() + " vs. " + newM.getNumberOfTextFields());
    	if(oldM.getNumberOfDoubleFields() != newM.getNumberOfDoubleFields())
    		throw new RuntimeException("Number field lengths do not match " + oldM.getNumberOfDoubleFields() + " vs. " + newM.getNumberOfDoubleFields());
    	if(oldM.getNumberOfDateFields() != newM.getNumberOfDateFields())
    		throw new RuntimeException("Number field lengths do not match " + oldM.getNumberOfDateFields() + " vs. " + newM.getNumberOfDateFields());
    	
    	//Get the differences, we loop through and compare each position
    	GWT.log("Text field init");
    	ArrayList<Integer> diffTextIndex = new ArrayList<Integer>();
    	for(int i = 0; i < oldM.getNumberOfTextFields(); i++)
    	{
    		String oldVal = oldM.getText(i);
    		String newVal = newM.getText(i);
    		if(oldVal == newVal) continue;
    		if((oldVal == null && newVal != null) || !oldVal.equals(newVal))
    			diffTextIndex.add(i);
    	}
    	
    	GWT.log("num field init");    	
    	ArrayList<Integer> diffNumberIndex = new ArrayList<Integer>();
    	for(int i = 0; i < oldM.getNumberOfDoubleFields(); i++)
    	{
    		Double oldVal = oldM.getNumber(i);
    		Double newVal = newM.getNumber(i);
    		if(oldVal == newVal) continue;
    		if((oldVal == null && newVal != null) || !oldVal.equals(newVal))
    			diffNumberIndex.add(i);
    	}
    	
    	GWT.log("date field init");
    	ArrayList<Integer> diffDateIndex = new ArrayList<Integer>();
    	for(int i = 0; i < oldM.getNumberOfDateFields(); i++)
    	{
    		Date oldVal = oldM.getDate(i);
    		Date newVal = newM.getDate(i);
    		if(oldVal == newVal) continue;
    		if((oldVal == null && newVal != null) || !oldVal.equals(newVal))
    			diffDateIndex.add(i);
    	}
    	
    	//Now we report the diffs, we don't use loops above because we need to track header counts which is
    	//only possible after we have all mnthe diffs
    	GWT.log("Text field diffs");
    	int changeCount = diffTextIndex.size() + diffNumberIndex.size() + diffDateIndex.size();
    	TableMessageHeader h = new TableMessageHeader(changeCount);
    	TableMessage meta = new TableMessage(0,changeCount, 0);
    	TableMessage m = new TableMessage(diffTextIndex.size(), diffNumberIndex.size(), diffDateIndex.size());
    	GWT.log("Entering loop: ");
    	for(int i = 0; i < diffTextIndex.size(); i++)
    	{
    		GWT.log("Field IDX: " + diffTextIndex.get(i));
    		m.setText(i, newM.getText(diffTextIndex.get(i)));
    		h.setText(i, diffTextTitles[diffTextIndex.get(i)], TableMessageContent.TEXT);
    		meta.setNumber(i, diffTextIndex.get(i));
    		GWT.log("Field: " + h.getText(i));
    	}
    	
    	GWT.log("num field diffs");
    	int headerCount = diffTextIndex.size();
    	for(int i = 0; i < diffNumberIndex.size(); i++)
    	{
    		m.setNumber(i, newM.getNumber(diffNumberIndex.get(i)));
    		h.setText(headerCount, diffNumberTitles[diffNumberIndex.get(i)], TableMessageContent.NUMBER);
    		meta.setNumber(headerCount++, diffNumberIndex.get(i));
    	}
    	
    	GWT.log("date field diffs");
    	for(int i = 0; i < diffDateIndex.size(); i++)
    	{
    		m.setDate(i, newM.getDate(diffDateIndex.get(i)));
    		h.setText(headerCount, diffDateTitles[diffDateIndex.get(i)], TableMessageContent.DATE);
    		meta.setNumber(headerCount++, diffDateIndex.get(i));
    	}
    	
    	TableMessage[] result = new TableMessage[3];
    	result[TM_DIFF_HEADER] = h;
    	result[TM_DIFF_VALS] = m;
    	result[TM_DIFF_POS] = meta;
    	return result;
    }
}