/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared;


/**
 *
 * @author Segun Razaq Sobulo
 */
public class DefaultLoginNameGenerator implements LoginNameGenerator{
    private int fnameLength = 5;
    private int lnameLength = 5;

    /**
     * Generates a description stating generator returns substring of fname and
     * lastname
     * @return description of generator
     */
    @Override
    public String getDescription()
    {
        return "Generates a login name using a concatenation of first " +
                lnameLength + " characters of lastname and first " + fnameLength
                + " firstname";
    }

    public DefaultLoginNameGenerator()
    {
    }

    public DefaultLoginNameGenerator(int fnameLength, int lnameLength)
    {
        this.fnameLength = fnameLength;
        this.lnameLength = lnameLength;
    }

    /**
     *
     * @param components should be a String array of size 2 with first element
     * containing lastname and second containing firstname
     * @return a 5 char substring of lastname appended to 5 char substring of
     * firstname
     */
    @Override
    public String generateLoginName(String[] components) {
    	String lnameComp = components[0].replaceAll("\\W", "");
    	String fnameComp = components[1].replaceAll("\\W", "");
        int endIndex = Math.min(lnameLength, lnameComp.length());
        lnameComp = lnameComp.substring(0, endIndex);
        endIndex = Math.min(fnameLength, fnameComp.length());
        fnameComp = fnameComp.substring(0, endIndex);
        return (lnameComp + fnameComp).toLowerCase();
    }
}
