/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared.exceptions;



/**
 *
 * @author Segun Razaq Sobulo
 */
public class MissingEntitiesException extends Exception{
    public MissingEntitiesException(){}

    public MissingEntitiesException(String s)
    {
        super(s);
    }
}
