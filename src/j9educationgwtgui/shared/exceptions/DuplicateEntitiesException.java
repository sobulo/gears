/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared.exceptions;



/**
 *
 * @author Segun Razaq Sobulo
 */
public class DuplicateEntitiesException extends Exception{

    public DuplicateEntitiesException(){}

    public DuplicateEntitiesException(String s) {
        super(s);
    }
}
