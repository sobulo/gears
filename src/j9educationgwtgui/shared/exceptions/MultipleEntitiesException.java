/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared.exceptions;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class MultipleEntitiesException extends Exception{
    public MultipleEntitiesException(){}

    public MultipleEntitiesException(String s)
    {
        super(s);
    }
}
