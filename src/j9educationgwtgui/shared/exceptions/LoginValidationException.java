/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared.exceptions;


/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginValidationException extends Exception
{
    public LoginValidationException(){
    }

    public LoginValidationException(String s){
        super(s);
    }
}
