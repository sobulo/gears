/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.shared;

import java.io.Serializable;

/**
 *
 * @author Segun Razaq Sobulo
 */
public enum PanelServiceLoginRoles implements Serializable{
    ROLE_SUPER, ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT, ROLE_GUARDIAN, ROLE_PUBLIC, ROLE_HOLD_EXISTS;
}
