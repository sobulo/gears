/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.server;

import j9educationactions.login.LoginPortal;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetCourse;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetDAO4ComplexUpdates;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetDAO4Updates;
import j9educationentities.InetGuardian;
import j9educationentities.InetHold;
import j9educationentities.InetHold.InetHoldType;
import j9educationentities.InetHoldContainer;
import j9educationentities.InetLevel;
import j9educationentities.InetStudent;
import j9educationentities.InetTeacher;
import j9educationentities.InetUser;
import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.TableMessageHeader.TableMessageContent;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.BCrypt;
import j9educationutilities.HTMLReports;
import j9educationutilities.PasswordGenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * 
 * @author Administrator
 */
@SuppressWarnings("serial")
public class UserManagerImpl extends RemoteServiceServlet implements
		UserManager {

	private static final Logger log = Logger.getLogger(UserManagerImpl.class
			.getName());

	@Override
	public HashMap<String, String> getStudentLevels()
			throws LoginValidationException, MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		String loginId = LoginPortal.getStudentID(this.getThreadLocalRequest());
		Key<InetStudent> studentKey = new Key<InetStudent>(InetStudent.class, loginId);
		return InetDAO4CommonReads.getStudentLevels(studentKey, ofy);
	}

	@Override
	public HashMap<String, String> getStudentLevels(String studKeyStr)
			throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_GUARDIAN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		Objectify ofy = ObjectifyService.begin();
		Key<InetStudent> studentKey = ofy.getFactory().stringToKey(studKeyStr);
		return InetDAO4CommonReads.getStudentLevels(studentKey, ofy);
	}

	@Override
	public HashMap<String, String> getGuardianChildren()
			throws LoginValidationException {
		Objectify ofy = ObjectifyService.begin();
		Key<InetGuardian> guardianKey = new Key<InetGuardian>(InetGuardian.class,
				LoginPortal.getGuardianID(this.getThreadLocalRequest()));
		return getGuardianChildren(guardianKey, ofy);
	}
	
	private static HashMap<String, String> getGuardianChildren(Key<InetGuardian> guardianKey, Objectify ofy)
	{
		List<InetStudent> children = InetDAO4CommonReads.getGuardianStudents(
				ofy, guardianKey);
		Iterator<InetStudent> childItr = children.iterator();
		HashMap<String, String> result = new HashMap<String, String>(
				children.size());
		while (childItr.hasNext()) {
			InetStudent stud = childItr.next();
			String studKeyStr = ofy.getFactory().keyToString(stud.getKey());
			String displayName = stud.getLname() + ", " + stud.getFname();
			result.put(studKeyStr, displayName);
		}
		return result;		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * j9educationgwtgui.client.UserManager#getStudentInfo(java.lang.String)
	 */
	@Override
	public TableMessage getStudentInfo(String loginName)
			throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class, loginName);
		return getStudentInfo(ofy, studKey);
	}

	private static TableMessage getStudentInfo(Objectify ofy, Key<InetStudent> studKey)
			throws MissingEntitiesException {
		InetStudent student = ofy.find(studKey);
		if (student == null) 
		{
			String message = "Unable to generate student info(%s) as non existent";
			log.severe(String.format(message, studKey.getName()));
			throw new MissingEntitiesException(String.format(message,
					studKey.toString()));
		}
		int numOfPhones = 0;
		if (student.getPhoneNumbers() != null)
			numOfPhones = student.getPhoneNumbers().size();
		TableMessage result = new TableMessage(4 + numOfPhones, 0, 1);
		result.setText(0, student.getLoginName());
		result.setText(1, student.getFname());
		result.setText(2, student.getLname());
		result.setText(3, student.getEmail());

		// append phonenumbers
		int i = 4;
		HashSet<String> phoneNums = student.getPhoneNumbers();

		if (phoneNums != null) {
			for (String num : phoneNums)
				result.setText(i++, num);
		}

		result.setDate(0, student.getBirthday());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see j9educationgwtgui.client.UserManager#getStudentInfo()
	 */
	@Override
	public TableMessage getStudentInfo() throws MissingEntitiesException,
			LoginValidationException {
		String loginID = LoginPortal.getStudentID(getThreadLocalRequest());
		Objectify ofy = ObjectifyService.begin();
		Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class,
				loginID);
		return getStudentInfo(ofy, studKey);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see j9educationgwtgui.client.UserManager#getAllStudentIDs(boolean)
	 */
	@Override
	public HashMap<String, String> getAllStudentIDs(boolean useLogins) throws LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		List<InetStudent> students = InetDAO4CommonReads
				.fetchAllEnrolledStudents(ofy);
		HashMap<String, String> result = new HashMap<String, String>(
				students.size());
		for (InetStudent stud : students) {
			String key;
			String val = stud.getFname() + " " + stud.getLname() + ","
					+ stud.getLoginName();
			if (useLogins)
				key = stud.getLoginName();
			else
				key = ofy.getFactory().keyToString(stud.getKey());
			result.put(key, val);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see j9educationgwtgui.client.UserManager#getAllGuardianIDs(boolean)
	 */
	@Override
	public HashMap<String, String> getAllGuardianIDs(boolean useLogins) throws LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		List<InetGuardian> guardians = InetDAO4CommonReads
				.fetchAllGuardians(ofy);
		HashMap<String, String> result = new HashMap<String, String>(
				guardians.size());
		for (InetGuardian guard : guardians) {
			String key;
			String val = guard.getFname() + " " + guard.getLname() + ","
					+ guard.getLoginName();
			if (useLogins)
				key = guard.getLoginName();
			else
				key = ofy.getFactory().keyToString(guard.getKey());
			result.put(key, val);
		}
		return result;
	}
	
	@Override
	public HashMap<String, String> getAllTeacherIDs() throws LoginValidationException
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		return getAllTeacherIDs(true);
	}
	
	private HashMap<String, String> getAllTeacherIDs(boolean useLogins) throws LoginValidationException 
	{
		Objectify ofy = ObjectifyService.begin();
		List<InetTeacher> teachers = InetDAO4CommonReads
				.fetchAllTeachers(ofy);
		HashMap<String, String> result = new HashMap<String, String>(teachers.size());
		for (InetTeacher teach : teachers) 
		{
			String key;
			String val = teach.getFname() + " " + teach.getLname() + "," + teach.getLoginName();
			if (useLogins)
				key = teach.getLoginName();
			else
				key = ofy.getFactory().keyToString(teach.getKey());
			result.put(key, val);
		}
		return result;
	}	

	private static TableMessage getUserInfo(Objectify ofy, Key<? extends InetUser> userKey)
			throws MissingEntitiesException 
	{
		InetUser user = ofy.find(userKey);
		if (user == null) {
			String message = "Unable to generate student info(%s) as non existent";
			log.severe(String.format(message, userKey.toString()));
			throw new MissingEntitiesException(String.format(message,
					userKey.getName()));
		}
		int numOfPhones = 0;
		if(user.getPhoneNumbers() != null)
			numOfPhones = user.getPhoneNumbers().size();
		TableMessage result = new TableMessage(4 + numOfPhones, 0, 1);
		result.setText(0, user.getLoginName());
		result.setText(1, user.getFname());
		result.setText(2, user.getLname());
		result.setText(3, user.getEmail());

		// append phonenumbers
		int i = 4;
		HashSet<String> phoneNums = user.getPhoneNumbers();

		if (phoneNums != null) {
			for (String num : phoneNums)
				result.setText(i++, num);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * j9educationgwtgui.client.UserManager#getGuardianInfo(java.lang.String)
	 */
	@Override
	public TableMessage getUserInfo(String loginName, PanelServiceLoginRoles role)
			throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<? extends InetUser> userKey = InetDAO4CommonReads.getUserKey(loginName, role);
		return getUserInfo(ofy, userKey);		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see j9educationgwtgui.client.UserManager#getGuardianInfo()
	 */
	@Override
	public TableMessage getUserInfo() throws MissingEntitiesException,
			LoginValidationException {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends InetUser> userKey= LoginPortal.getUserKey(getThreadLocalRequest());
		return getUserInfo(ofy, userKey);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#updateStudent(java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.util.HashSet, java.lang.String)
	 */
	@Override
	public void updateStudent(String fname, String lname, Date dob,
			String email, HashSet<String> phoneNums, String loginName)
			throws MissingEntitiesException, LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		InetDAO4Updates.updateStudent(fname, lname, dob, email, phoneNums, loginName);
		log.warning("UPDATED student " + loginName + " --by user " + userAuditId);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#updateStudent(java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.util.HashSet)
	 */
	@Override
	public void updateStudent(String fname, String lname, Date dob,
			String email, HashSet<String> phoneNums)
			throws MissingEntitiesException, LoginValidationException {
		String loginName = LoginPortal.getStudentID(getThreadLocalRequest());
		InetDAO4Updates.updateStudent(fname, lname, dob, email, phoneNums, loginName);
		log.warning("UPDATED student " + loginName + "-- by user(self) " + loginName);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#updateGuardian(java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.util.HashSet, java.lang.String)
	 */
	@Override
	public void updateUser(String fname, String lname,
			String email, HashSet<String> phoneNums, String loginName, PanelServiceLoginRoles role)
			throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Key<? extends InetUser> userKey = InetDAO4CommonReads.getUserKey(loginName, role);
		InetDAO4Updates.updateUser(fname, lname, email, phoneNums, userKey);
		log.warning("UPDATED user " + userKey + " --by user " + userAuditId);
	}
	
	@Override
	public String createUser(String fname, String lname,
			String email, HashSet<String> phoneNums, String loginName, String password, PanelServiceLoginRoles role)
			throws LoginValidationException, DuplicateEntitiesException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Key<? extends InetUser> userKey;
		switch(role)
		{
			case ROLE_TEACHER:
			{
				userKey = (InetDAO4Creation.createTeacher(fname, lname, email, phoneNums, loginName, password)).getKey();
				break;
			}
			default:
			{
				throw new UnsupportedOperationException("Creation of: " + role + " NOT supported by this function");
			}
		}
		
		log.warning("Created " + role + userKey + " --by user " + userAuditId);
		return userKey.getName();
	}	

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#getGuardianChildren(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getGuardianChildren(String loginName) throws LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetGuardian> guardKey = new Key<InetGuardian>(InetGuardian.class, loginName);
		return getGuardianChildren(guardKey, ofy);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#getStudentParents(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getStudentParents(String loginName)
			throws MissingEntitiesException, LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetStudent> sk = new Key<InetStudent>(InetStudent.class, loginName);
		Collection<InetGuardian> guards = InetDAO4CommonReads.getStudentGuardians(ofy, sk);
		HashMap<String, String> result = new HashMap<String, String>(guards.size());
		for(InetGuardian g : guards)
			result.put(ofy.getFactory().keyToString(g.getKey()), g.getLname() + ", " + g.getFname());
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#addGuardianToStudent(java.lang.String, java.lang.String)
	 */
	@Override
	public void addGuardianToStudent(String guardLoginName, String studLoginName)
			throws MissingEntitiesException, DuplicateEntitiesException, LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class, studLoginName);
		Key<InetGuardian> guardKey = new Key<InetGuardian>(InetGuardian.class, guardLoginName);
		InetDAO4Updates.addGuardianToStudent(guardKey, studKey);
		log.warning("UPDATED student " + studKey + " with guardian " + guardKey + " --by user " + userAuditId);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#resetPassword(java.lang.String, j9educationgwtgui.shared.PanelServiceLoginRoles)
	 */
	@Override
	public String resetPassword(String loginName, PanelServiceLoginRoles role)
			throws MissingEntitiesException, LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Key<? extends InetUser> userKey = InetDAO4CommonReads.getUserKey(loginName, role);
		
		//generate new password
		PasswordGenerator gen = new PasswordGenerator();
        String password = gen.generatePassword();        
        if(password.length() == 0)
        	throw new IllegalStateException("Password Generator failed");
        
        //update password
        InetDAO4Updates.changePassword(userKey, password);
        log.warning("RESET password for " + userKey + " --by user " + userAuditId);
        return "User: " + userKey.getName() +" Password: " + password;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#changePassword(java.lang.String, java.lang.String)
	 */
	@Override
	public String changePassword(String oldPassword, String newPassword)
			throws MissingEntitiesException, LoginValidationException {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends InetUser> userKey = LoginPortal.getUserKey(getThreadLocalRequest()); 
		InetUser user = ofy.find(userKey);
		
		if(user == null)
		{
			String msgFmt = "Unable to change password for %s as user does not exist";
			log.severe(String.format(msgFmt, userKey.toString()));
			throw new MissingEntitiesException(String.format(msgFmt, userKey.getName()));
		}
		else if(!BCrypt.checkpw(oldPassword, user.getPassword()))
		{
			String msgFmt = "Unable to change password for %s. Invalid current password specified";
			log.severe(String.format(msgFmt, userKey.toString()));
			throw new LoginValidationException(String.format(msgFmt, userKey.getName()));			
		}
		InetDAO4Updates.changePassword(userKey, newPassword);
		log.warning("UPDATED password for " + userKey + " --by user(self)");
		return user.getFname() + " " + user.getLname();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#deleteStudent(java.lang.String)
	 */
	@Override
	public String deleteStudent(String studKeyStr) 
		throws MissingEntitiesException, ManualVerificationException, LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		//delete the student
		Objectify ofy = ObjectifyService.beginTransaction();
		InetStudent deletedStudent = InetDAO4ComplexUpdates.deleteStudent(studKeyStr, ofy);
		
		//schedule removal from level list
		Set<Key<InetLevel>> levelKeys = deletedStudent.getLevels();
		for(Key<InetLevel> lk : levelKeys)
		{
			String levelKeyStr = ofy.getFactory().keyToString(lk);
			TaskQueueHelper.scheduleRemoveStudentFromLevel(studKeyStr, levelKeyStr);
		}
		
		log.warning("DELETE student " + deletedStudent.getKey() + " --by user " + userAuditId);
		return deletedStudent.getFname() + " " + deletedStudent.getLname();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#deleteGuardian(java.lang.String)
	 */
	@Override
	public String deleteGuardian(String loginName)
			throws MissingEntitiesException, LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		InetGuardian guard = InetDAO4ComplexUpdates.deleteGuardian(loginName);
		
		Objectify ofy = ObjectifyService.begin();
		List<Key<InetStudent>> children = 
			InetDAO4CommonReads.getGuardianStudentsKeys(ofy, guard.getKey());
		
		//schedule removal of guardian from student
		for(Key<InetStudent> child : children)
			TaskQueueHelper.scheduleRemoveGuardianFromStudentList(loginName, child.getName());

		log.warning("DELETE guardian " + guard.getKey() + " --by user " + userAuditId);
		return guard.getFname() + " " + guard.getLname();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#updateUser(java.lang.String, java.lang.String, java.lang.String, java.util.HashSet, java.lang.String)
	 */
	@Override
	public void updateUser(String fname, String lname, String email,
			HashSet<String> phoneNums)
			throws MissingEntitiesException, LoginValidationException {
		Key<? extends InetUser> userKey = LoginPortal.getUserKey(getThreadLocalRequest());
		InetDAO4Updates.updateUser(fname, lname, email, phoneNums, userKey);
		log.warning("UPDATE user info for " + userKey + " --by user(self)");
		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#setStudentHold(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean)
	 */
	@Override
	public String createStudentHold(String levelKeyStr, String studKeyStr,
			String holdType, String message, String note) throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
		Key<InetHoldContainer> hcKey = new Key<InetHoldContainer>(InetHoldContainer.class, levelKey.getName());
		Key<InetStudent> studKey = ofy.getFactory().stringToKey(studKeyStr);
		InetHold hold = InetDAO4Creation.createHold(hcKey, studKey, InetHold.InetHoldType.valueOf(holdType), message, note, false);
		log.warning("Created hold " + hold.getKey() + " for student " + studKey + " --by user " + userAuditId);
		return(hold.getHoldType().toString().replace("_", " ") + " for " + studKey.getName()
				+ " / " + InetDAO4CommonReads.keyToPrettyString(hcKey));
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#getHoldTypes()
	 */
	@Override
	public String[] getHoldTypes() throws LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		InetHoldType[] holdTypes = InetHoldType.values();
		String[] result = new String[holdTypes.length];
		for(int i = 0; i < holdTypes.length; i++)
			result[i] = holdTypes[i].toString();
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#getStudentHolds(java.lang.String, boolean)
	 */
	@Override
	public ArrayList<TableMessage> getStudentHolds(String levelKeyStr,
			boolean unresolvedOnly) throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetLevel> levKey = ofy.getFactory().stringToKey(levelKeyStr);
		Key<InetHoldContainer> hcKey = new Key<InetHoldContainer>(InetHoldContainer.class, levKey.getName());
		String caption = InetDAO4CommonReads.keyToPrettyString(hcKey) + " holds";
		InetStudentHoldsPopulator dataPop = new InetStudentHoldsPopulator(unresolvedOnly, caption);
		InetDAO4CommonReads.getStudentHolds(hcKey, dataPop, ofy);
		return dataPop.getData();
	}
	
	public class InetStudentHoldsPopulator implements InetObjectDataPopulator
	{
		ArrayList<TableMessage> data;
		Objectify ofy;
		boolean unresolvedOnly;
		
		public InetStudentHoldsPopulator(boolean unresolvedOnly, String caption)
		{
			ofy = ObjectifyService.begin();
			data = new ArrayList<TableMessage>();
			TableMessageHeader header = new TableMessageHeader(4);
			header.setText(0, "Student ID", TableMessageContent.TEXT);
			header.setText(1, "Class", TableMessageContent.TEXT);
			header.setText(2, "Type", TableMessageContent.TEXT);
			header.setText(3, "Status", TableMessageContent.TEXT);
			this.unresolvedOnly = unresolvedOnly;
			data.add(header);
		}

		/* (non-Javadoc)
		 * @see j9educationgwtgui.server.InetObjectDataPopulator#populateInfo(j9educationentities.GAEPrimaryKeyEntity[])
		 */
		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] objects) {
			InetHold hold = (InetHold) objects[0];
			if(unresolvedOnly && hold.isResolved())
				return;
			TableMessage m = new TableMessage(6, 0, 0);
			String holdKeyStr = ofy.getFactory().keyToString(hold.getKey());
			m.setMessageId(holdKeyStr);		
			m.setText(0, hold.getStudentKey().getName());
			m.setText(1, InetDAO4CommonReads.keyToPrettyString(hold.getKey().getParent()));
			m.setText(2, hold.getHoldType().toString());
			m.setText(3, hold.isResolved() ? PanelServiceConstants.HOLD_RESOLVED_TRUE : PanelServiceConstants.HOLD_RESOLVED_FALSE);
			m.setText(4, hold.getMessage().getValue());
			m.setText(5, hold.getNote().getValue());
			data.add(m);
		}
		
		public ArrayList<TableMessage> getData()
		{
			return data;
		}
		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#editStudentHold(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public String editStudentHold(String holdKeyStr, String holdType,
			String message, String note, boolean resolutionStatus)
			throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetHold> hk = ofy.getFactory().stringToKey(holdKeyStr);
		InetHold hold = InetDAO4ComplexUpdates.editHold(hk, InetHoldType.valueOf(holdType), message, note, resolutionStatus);
		log.warning("UPDATED student hold " + hk + " for student " + hold.getStudentKey() + " --by user " + userAuditId);
		return (hold.getStudentKey().getName() + "/" + InetDAO4CommonReads.keyToPrettyString(hold.getHoldContainer()));
	}
	
	static class CourseTeacherPopulator implements InetObjectDataPopulator
	{
		
		HashMap<String, String> data;
		Objectify ofy;
		
		public CourseTeacherPopulator(Objectify ofy) {
			data = new HashMap<String, String>();
			this.ofy = ofy;
		}

		/* (non-Javadoc)
		 * @see j9educationgwtgui.server.InetObjectDataPopulator#populateInfo(j9educationentities.GAEPrimaryKeyEntity[])
		 */
		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] objects) {
			InetCourse course = (InetCourse) objects[0];
			Key<InetTeacher> teachKey = course.getTeacherKey();
			data.put(ofy.getFactory().keyToString(course.getKey()), teachKey == null ? "" : teachKey.getName());
		}
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#getCourseTeacherMappings(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getCourseTeacherMappings(String levelKeyStr)
			throws MissingEntitiesException, LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		Objectify ofy = ObjectifyService.begin();
		CourseTeacherPopulator ctPop = new CourseTeacherPopulator(ofy);
		Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
		InetDAO4CommonReads.getLevelCourseInfo(levelKey, ctPop, ofy);
		return ctPop.data;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#setCourseTeacherMapping(java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public String setCourseTeacherMapping(String courseKeyStr,
			String teacherLogin, boolean isAdd)
			throws MissingEntitiesException, DuplicateEntitiesException,
			LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		String userAudit = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetCourse> ck = ofy.getFactory().stringToKey(courseKeyStr);
		Key<InetTeacher> tk = new Key<InetTeacher>(InetTeacher.class, teacherLogin);
		InetDAO4Updates.setCourseTeacher(tk, ck, isAdd);
		String action = (isAdd?"set":"removed");
		log.warning("Update of course " + ck + ", teacher " + tk + " " + action + " by --user " + userAudit);
		return "successfuly " + action + " teacher " + tk.getName() + " for subject " + ck.getName();
	}
	
	@Override
	public HashMap<String, String> getTeacherCourses() throws LoginValidationException
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_TEACHER};
		String teacherLogin = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		Key<InetTeacher> teacherKey = new Key<InetTeacher>(InetTeacher.class, teacherLogin);
		Objectify ofy = ObjectifyService.begin();
		return InetDAO4CommonReads.getTeacherCourseMap(teacherKey, ofy);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.UserManager#getDetailedUserInfo(java.lang.String, j9educationgwtgui.shared.PanelServiceLoginRoles)
	 */
	@Override
	public String getDetailedUserInfo(String loginName,
			PanelServiceLoginRoles role) throws MissingEntitiesException,
			LoginValidationException {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		Objectify ofy = ObjectifyService.begin();
		Key<? extends InetUser> userKey = InetDAO4CommonReads.getUserKey(loginName, role);
		InetUser user = ofy.find(userKey);
		if(user == null)
		{
			String msgFmt = "Unable to generate details as user object not found for: ";
			log.severe(msgFmt + userKey);
			throw new MissingEntitiesException(msgFmt + userKey.getName()); 
		}
		
		String result = "Unsupported role: " + role.toString();
		if(role.equals(PanelServiceLoginRoles.ROLE_STUDENT))
			result = HTMLReports.getStudentDetails((InetStudent) user);
		else if(role.equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
			result = HTMLReports.getGuardianDetails((InetGuardian) user, ofy);
		else if(role.equals(PanelServiceLoginRoles.ROLE_TEACHER))
			result = HTMLReports.getTeacherDetails((InetTeacher) user, ofy);
		return result;
	}
}
