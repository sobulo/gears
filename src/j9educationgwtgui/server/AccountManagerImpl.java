/**
 * 
 */
package j9educationgwtgui.server;

import j9educationactions.login.LoginPortal;
import j9educationactions.pdfreports.BillingInvoiceGenerator;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetStudent;
import j9educationentities.accounting.InetBill;
import j9educationentities.accounting.InetBillDescription;
import j9educationentities.accounting.InetBillPayment;
import j9educationentities.accounting.InetDAO4Accounts;
import j9educationgwtgui.client.AccountManager;
import j9educationgwtgui.shared.BillDescriptionItem;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.TableMessageHeader.TableMessageContent;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class AccountManagerImpl extends RemoteServiceServlet implements AccountManager{

	private static final Logger log = Logger.getLogger(AccountManagerImpl.class.getName());
	
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#createBillTemplate(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.util.LinkedHashSet)
	 */
	@Override
	public String createBillTemplate(String name, String year, String term,
			Date dueDate, LinkedHashSet<BillDescriptionItem> itemizedBill) throws DuplicateEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());			
		InetBillDescription billTemplate = InetDAO4Accounts.createBillDescription(year, term, dueDate, name, itemizedBill);
		log.warning("CREATED " + billTemplate.getKey() + " --by user " + userAuditId);
		return billTemplate.getKey().getName();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getBillTemplateList()
	 */
	@Override
	public HashMap<String, String> getBillTemplateList() throws LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		return InetDAO4Accounts.getAllBillDescriptions(ObjectifyService.begin());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#createUserBill(java.lang.String, java.lang.String[])
	 */
	@Override
	public String createUserBill(String billTemplate, String[] users) throws LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		StringBuilder failures = new StringBuilder();
		int count = 0;
		for(String student : users)
		{
			log.warning("attempting bill creation for: " + student);
			try
			{
				TaskQueueHelper.scheduleCreateUserBill(billTemplate, student);
				count++;
			}
			catch(RuntimeException ex)
			{
				log.warning("Error occured: " + ex.getMessage());
				failures.append(student).append(",");
			}
		}
		StringBuilder result = new StringBuilder("Bill creation scheduled succesfully for ").append(count).append(" students.");
		if(count != users.length)
			result.append(" Please retry for the following users as failures occured: ").append(failures);
		log.warning("BILL CREATION SCHEDULED for: " + users.length + " students --by user " + userAuditId);
		return result.toString();
	}
	
	private List<TableMessage> getBillListSummary(List<InetBill> billList)
	{
		try
		{
			return getBillListSummary(billList, false, true, null);
		}
		catch(MissingEntitiesException ex)
		{
			throw new RuntimeException("Unexpected error: " + ex.getMessage());
		}
	}
	
	private TableMessageHeader getBillSummaryHeaderForBlotter()
	{
		TableMessageHeader result = new TableMessageHeader(5);
		result.setText(0, "Student ID", TableMessageContent.TEXT);
		result.setText(1, "Last Name", TableMessageContent.TEXT);
		result.setText(2, "First Name", TableMessageContent.TEXT);
		result.setText(3, "Total Due", TableMessageContent.NUMBER);
		result.setText(4, "Amount Paid", TableMessageContent.NUMBER);
		return result;
	}
	
	private static TableMessage getBillSummaryMessage(InetBill bill, boolean includeBillBio, boolean includeStudentBio, String[] studFields)
	{
		int numOfTextFields = 1; int textFieldCursor = 0;
		if(includeBillBio) numOfTextFields += 2;
		if(includeStudentBio) numOfTextFields += 3;
		TableMessage billInfo = new TableMessage(numOfTextFields, 2, 0);
	
		if(includeStudentBio)
		{
			billInfo.setText(textFieldCursor++, studFields[0]);
			billInfo.setText(textFieldCursor++, studFields[1]);
			billInfo.setText(textFieldCursor++, studFields[2]);			
		}
		
		if(includeBillBio)
		{
			billInfo.setText(textFieldCursor++, InetDAO4CommonReads.keyToPrettyString(bill.getBillTemplateKey()));
			billInfo.setText(textFieldCursor++, bill.getKey().getName());
		}
		
		billInfo.setText(textFieldCursor++, bill.isSettled()? "True" : "False");					
		billInfo.setNumber(0, bill.getTotalAmount());
		billInfo.setNumber(1, bill.getSettledAmount());
		return billInfo;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBills(java.lang.String)
	 */
	@Override
	public List<TableMessage> getStudentBills(String studentID) throws LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		List<InetBill> billInfoList = InetDAO4Accounts.getStudentBills(new Key<InetStudent>(InetStudent.class, studentID), ObjectifyService.begin());
		return getBillListSummary(billInfoList);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#savePayment(java.lang.String, double, java.util.Date, java.lang.String, java.lang.String)
	 */
	@Override
	public String savePayment(String billKey, double amount, Date payDate,
			String referenceID, String comments) throws MissingEntitiesException, ManualVerificationException, LoginValidationException 
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());			
		InetBillPayment payment = InetDAO4Accounts.createPayment(new Key<InetBill>(InetBill.class, billKey), amount, referenceID, comments, payDate);
		log.warning("CREATED: " + payment.getKey() + " students --by user " + userAuditId);
		return new StringBuilder("Payment ID: ").append(payment.getKey().getId()).append( " Amount: ").
			append(payment.getAmount()).append(" date: ").append(payment.getPaymentDate()).append( " Reference ID: ").append(payment.getReferenceId()).
			append(" Bill: ").append(InetDAO4CommonReads.keyToPrettyString(payment.getKey().getParent())).toString();
	}
	
	public static List<TableMessage> getBillDescriptionInfo(Key<InetBillDescription> bdKey) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
		InetBillDescription billDesc = ofy.find(bdKey);
		if(billDesc == null)
		{
			String msgFmt = "Unable to find bill description: ";
			log.severe(msgFmt + bdKey);
			throw new MissingEntitiesException(msgFmt + bdKey.getName());
		}
		
		HashSet<BillDescriptionItem> itemizedBill = billDesc.getItemizedBill();
		List<TableMessage> result = new ArrayList<TableMessage>(1 + itemizedBill.size());
		//setup summary info
		TableMessage summary = new TableMessage(3, 1, 1);
		summary.setText(0, billDesc.getName());
		summary.setText(1, billDesc.getAcademicYear());
		summary.setText(2, billDesc.getTerm());
		summary.setNumber(0, billDesc.getTotalAmount());
		summary.setDate(0, billDesc.getDueDate());
		result.add(summary);
		
		//setup header for itemized bill
		TableMessageHeader itemizedHeader = new TableMessageHeader(3);
		itemizedHeader.setText(0, "Type", TableMessageContent.TEXT);
		itemizedHeader.setText(1, "Description", TableMessageContent.TEXT);
		itemizedHeader.setText(2, "Amount", TableMessageContent.NUMBER);
		result.add(itemizedHeader);
		
		for(BillDescriptionItem item : itemizedBill)
		{
			TableMessage itemRow = new TableMessage(2, 1, 0);
			itemRow.setText(0, item.getName());
			itemRow.setText(1, item.getComments());
			itemRow.setNumber(0, item.getAmount());
			result.add(itemRow);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getBillTemplateInfo(java.lang.String)
	 */
	@Override
	public List<TableMessage> getBillTemplateInfo(String billTemplateKeyStr) throws MissingEntitiesException, LoginValidationException 
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Key<InetBillDescription> bdKey = ObjectifyService.begin().getFactory().stringToKey(billTemplateKeyStr);
		return getBillDescriptionInfo(bdKey);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillKeys(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin) throws LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	Key<InetStudent> studKey = null;
    	if(isLogin)
    		studKey = new Key<InetStudent>(InetStudent.class, studentID);
    	else
    		studKey = ObjectifyService.begin().getFactory().stringToKey(studentID);
    	return getStudentBillKeys(studKey);
	}
	
	private static HashMap<String, String> getStudentBillKeys(Key<InetStudent> studKey)
	{
		return InetDAO4Accounts.getStudentBillKeys( studKey, ObjectifyService.begin());
	}
	
	private List<TableMessage> getBillPayments(Key<InetBill> billKey, Objectify ofy)
	{
		List<InetBillPayment> payments = InetDAO4Accounts.getPayments(billKey, ofy);
		List<TableMessage> result = new ArrayList<TableMessage>(1 + payments.size());
		//setup header message
		TableMessageHeader header = new TableMessageHeader(5);
		header.setText(0, "Payment ID", TableMessageContent.TEXT);
		header.setText(1, "Payment Amount", TableMessageContent.NUMBER);
		header.setText(2, "Payment Date", TableMessageContent.DATE);
		header.setText(3, "Reference ID", TableMessageContent.TEXT);
		header.setText(4, "Comments", TableMessageContent.TEXT);
		result.add(header);
		
		for(InetBillPayment payInfo : payments)
		{
			TableMessage m = new TableMessage(3, 1, 1);
			m.setText(0, Long.toString(payInfo.getKey().getId()));
			m.setText(1, payInfo.getReferenceId());
			m.setText(2, payInfo.getComments());
			m.setNumber(0, payInfo.getAmount());
			m.setDate(0, payInfo.getPaymentDate());
			result.add(m);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillAndPayment(java.lang.String)
	 */
	@Override
	public List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException, LoginValidationException
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN, PanelServiceLoginRoles.ROLE_STUDENT};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetBill> billKey = ofy.getFactory().stringToKey(billKeyStr);
		InetBill studBill = ofy.find(billKey);
		
		if(studBill == null)
		{
			String msgFmt = "Unable to find student bill: ";
			log.severe(msgFmt + billKey);
			throw new MissingEntitiesException(msgFmt + billKey.getName());
		}
		List<TableMessage> result = new ArrayList<TableMessage>();
		
		result.add(getBillSummaryMessage(studBill, false, false, null)); //add student bill
		result.addAll(getBillDescriptionInfo(studBill.getBillTemplateKey())); //add bill description
		result.addAll(getBillPayments(billKey, ofy));
		
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getBillsForTemplate(java.lang.String)
	 */
	@Override
	public List<TableMessage> getBillsForTemplate(String billDescKeyStr) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());				
		Objectify ofy = ObjectifyService.begin();
		Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(billDescKeyStr);
		//TODO change this code to use query cursors otherwise could turn out to be a performance
		//bottle neck
		List<InetBill> bills = InetDAO4Accounts.getBills(bdKey, ofy);
		
		List<TableMessage> result = new ArrayList<TableMessage>(bills.size() + 1);
		result.add(getBillSummaryHeaderForBlotter());
		result.addAll(getBillListSummary(bills, true, true, ofy));
		return result;
	}
	
	public static List<TableMessage> getBillListSummary(Collection<InetBill> bills, boolean includeStudentBio, 
			boolean includeBillBio, Objectify ofy) throws MissingEntitiesException
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(bills.size());
		Map<Key<InetStudent>,InetStudent> studentMap = null;
		String[] studFields = null;
		
		if(includeStudentBio)
		{
			HashSet<Key<InetStudent>> studentKeys = new HashSet<Key<InetStudent>>(bills.size());
			for(InetBill b : bills)
				studentKeys.add((Key<InetStudent>) b.getUserKey());
			studentMap = InetDAO4CommonReads.getEntities(studentKeys, ofy, true);
			studFields = new String[3];
		}
		
		for(InetBill b : bills)
		{
			if(includeStudentBio)
			{
				InetStudent billedStudent = studentMap.get(b.getUserKey());
				studFields[0] = b.getUserKey().getName();
				studFields[1] = billedStudent.getLname();
				studFields[2] = billedStudent.getFname();
			}
			TableMessage m = getBillSummaryMessage(b, includeBillBio, includeStudentBio, studFields);
			result.add(m);
		}
		return result;		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getBillPayments(java.lang.String)
	 */
	@Override
	public List<TableMessage> getBillPayments(String billKeyStr)
			throws MissingEntitiesException, LoginValidationException 
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());				
		Key<InetBill> billKey = new Key<InetBill>(InetBill.class, billKeyStr);
		return getBillPayments(billKey, ObjectifyService.begin());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillKeysFromTemplate(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getStudentBillKeysFromTemplate(
			String templateKeyStr) throws MissingEntitiesException, LoginValidationException {		
		List<TableMessage> billInfo = getBillsForTemplate(templateKeyStr);
		if(billInfo.size() == 0) return new HashMap<String, String>();
		billInfo.remove(0); //discard header
		HashMap<String, String> result = new HashMap<String, String>(billInfo.size());
		for(TableMessage billMessage : billInfo)
			result.put(billMessage.getText(4), billMessage.getText(2) + ", " + billMessage.getText(1));
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getInvoiceDownloadLink(java.lang.String, java.lang.String[])
	 */
	@Override
	public String getInvoiceDownloadLink(String billTemplateKeyStr,
			String[] billKeyStrs) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());				
		return BillingInvoiceGenerator.getBillingInvoicesLink(billTemplateKeyStr, billKeyStrs, getThreadLocalRequest());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillKeys()
	 */
	@Override
	public HashMap<String, String> getStudentBillKeys()
			throws LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_STUDENT};
    	String userID = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	return getStudentBillKeys(new Key<InetStudent>(InetStudent.class, userID));
	}

}
