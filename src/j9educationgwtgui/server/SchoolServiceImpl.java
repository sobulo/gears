/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.server;


import j9educationactions.login.LoginPortal;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.ApplicationParameters;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetDAO4ComplexUpdates;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetDAO4Updates;
import j9educationentities.InetGradeDescriptions;
import j9educationentities.InetLevel;
import j9educationentities.InetNotice;
import j9educationentities.InetSchool;
import j9educationentities.StringOptions;
import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.shared.GradeDescription;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.TableMessageHeader.TableMessageContent;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
public class SchoolServiceImpl extends RemoteServiceServlet implements SchoolService{

    private static final Logger log = Logger.getLogger(SchoolServiceImpl.class.getName());

    /**
     * Function for creating a new school
     * @param name school name
     * @param email email address
     * @param accr accronym for school name
     * @param addr location of school
     * @param url website address
     * @return name of school created
     * @throws DuplicateEntitiesException
     * @throws LoginValidationException 
     * @throws MissingEntitiesException 
     */
    @Override
    public String createSchool(String name, String email, String accr,
            String addr, String url, String phoneNumStr, boolean isUpdate) throws
            DuplicateEntitiesException, LoginValidationException, MissingEntitiesException
    {
        PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
        String loginId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
        
        String[] phoneNumParts = phoneNumStr.split(",");
        HashSet<String> phoneNums = new HashSet<String>(phoneNumParts.length);
        for(String num : phoneNumParts)
        	phoneNums.add(num.trim());
        InetSchool s = null;
        if(isUpdate)
        	s = InetDAO4Updates.updateSchool(name, accr, email, url, addr, phoneNums);
        else
        {
            s = InetDAO4Creation.createSchool(name, accr, email, url, addr, phoneNums);
            scheduleSchoolDependenciesCreation();        	
        }

        log.warning("CREATED school " + s.getSchoolName() + " --by user " + loginId);
        return s.getSchoolName();
    }

    private void scheduleSchoolDependenciesCreation()
    {
        String[] optionNames = getOptionNames();

        //schedule string option creation
        for(String option : optionNames)
            TaskQueueHelper.scheduleCreateStringOption(option);
    }

    public Boolean updateAcademicYears(HashSet<String> years) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String loginId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
        InetDAO4Updates.updateStringOptions
                (PanelServiceConstants.ACADEMIC_YEAR_LABEL, years);
        log.warning("UPDATED option " + PanelServiceConstants.ACADEMIC_YEAR_LABEL + " --by user " + loginId);
        return true;
    }

    public Boolean updateAcademicTerms(HashSet<String> terms) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String loginId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        InetDAO4Updates.updateStringOptions(PanelServiceConstants.TERM_LABEL,
                terms);
        log.warning("UPDATED option " + PanelServiceConstants.TERM_LABEL + " --by user " + loginId);
        return true;
    }

    @Override
    public Boolean updateDefaultSubjects(HashSet<String> subjectNames) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String loginId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        InetDAO4Updates.updateStringOptions
                (PanelServiceConstants.DEFAULT_SUBJECTS_LABEL, subjectNames);
        log.warning("UPDATED option " + PanelServiceConstants.DEFAULT_SUBJECTS_LABEL + " --by user " + loginId);
        return true;
    }

    public Boolean updateClassNames(HashSet<String> classNames) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String loginId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        InetDAO4Updates.updateStringOptions
                (PanelServiceConstants.CLASS_NAME_LABEL, classNames);
        log.warning("UPDATED option " + PanelServiceConstants.CLASS_NAME_LABEL + " --by user " + loginId);
        return true;
    }

    public Boolean updateSubClassNames(HashSet<String> subClassNames) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String loginId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        InetDAO4Updates.updateStringOptions(PanelServiceConstants.SUB_CLASS_NAME_LABEL,
                subClassNames);
        log.warning("UPDATED option " + PanelServiceConstants.SUB_CLASS_NAME_LABEL + " --by user " + loginId);
        return true;
    }

    @Override
    public HashMap<String, HashSet<String>> getInitialOptions(String[] 
            optionNames) throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER, PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        HashMap<String, HashSet<String>> initOptionVals = new HashMap<String, HashSet<String>>();
        
        Objectify ofy = ObjectifyService.begin();

        for( String optName : optionNames)
        {
            StringOptions option = ofy.find(StringOptions.class, optName);
            if(option == null)
            {
                MissingEntitiesException ex = new
                        MissingEntitiesException("no obj: " + optName);
                log.severe(ex.getMessage());
                throw ex;
            }
            HashSet<String> temp = new HashSet<String>();
            Iterator<String> optItr = option.getOptionElements().iterator();
            while(optItr.hasNext())
            {
                temp.add(optItr.next());
            }
            initOptionVals.put(optName, temp);
        }
        //initOptionVals.put(InetConstants.ACADEMIC_YEAR_LABEL, r);

        return initOptionVals;
    }

    private String[] getOptionNames()
    {
        String[] optionNames = {PanelServiceConstants.ACADEMIC_YEAR_LABEL,
                                PanelServiceConstants.TERM_LABEL,
                                PanelServiceConstants.CLASS_NAME_LABEL,
                                PanelServiceConstants.SUB_CLASS_NAME_LABEL,
                                PanelServiceConstants.DEFAULT_SUBJECTS_LABEL};
        return optionNames;
    }

    public HashMap<String, HashSet<String>> getInitialOptions() throws 
            MissingEntitiesException, LoginValidationException
    {
        return getInitialOptions(getOptionNames());
    }

    @Override
    public String createClass(String className, String subClassName, String year, 
            String term) throws MissingEntitiesException,
            DuplicateEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    
    	
        //create the level
        InetLevel inetClass = InetDAO4Creation.createLevel(term,
                year, className, subClassName);

        //schedule addition of level to school list
        String levelKeyStr = ObjectifyService.begin().
        getFactory().keyToString(inetClass.getKey());
        
        TaskQueueHelper.scheduleAddLevelToSchool(levelKeyStr);
        
        //schedule creation of hold container
        TaskQueueHelper.scheduleCreateHoldContainer(levelKeyStr);
        log.warning("CREATED class " + inetClass.getKey() + " --by user " + userAuditId);
        return inetClass.toString();
    }

    @Override
    public String[] getSimpleClassNames() throws MissingEntitiesException, LoginValidationException{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());        	
        String[] result = null;
        Objectify ofy = ObjectifyService.begin();
        Set<Key<InetLevel>> keySet = InetDAO4CommonReads.getSchoolLevels(ofy);
        if(keySet == null)
            return (new String[0]);
        int i = 0;
        result = new String[keySet.size()];
        Iterator<Key<InetLevel>> itr = keySet.iterator();
        while(itr.hasNext())
            result[i++] = InetDAO4CommonReads.keyToPrettyString(itr.next());
        return result;
    }

    @Override
    public LoginInfo login(String requestUri) throws MissingEntitiesException, LoginValidationException {
        HttpServletRequest req = this.getThreadLocalRequest();
        return LoginPortal.brokeredLogin(requestUri, req);
    }

    @Override
    public void updateDefaultGradingScheme(LinkedHashMap<Double, GradeDescription> scheme) throws LoginValidationException{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    
        if(scheme == null || scheme.size() == 0)
            return;
        Iterator<Map.Entry<Double, GradeDescription>> schemeItr = scheme.entrySet().iterator();
        Map.Entry<Double, GradeDescription> vals = schemeItr.next();
        InetGradeDescriptions gradeDescs = new
                InetGradeDescriptions(vals.getValue().letterGrade, vals.getValue().gpa);
        while(schemeItr.hasNext())
        {
            vals = schemeItr.next();
            gradeDescs.addGradeDescription(vals.getValue().letterGrade,
                    vals.getKey(), vals.getValue().gpa);
        }
        InetDAO4Updates.updateSchoolGradingScheme(gradeDescs);
        log.warning("UPDATED grading scheme --by user " + userAuditId);
    }

    @Override
    public String[] getDefaultGradingSchemeDescription() throws LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());        	
        Objectify ofy = ObjectifyService.begin();
        InetGradeDescriptions desc = InetDAO4CommonReads.getSchool(ofy).getDefaultGradingScheme();
        String[] result;
        if(desc == null)
            result = new String[0];
        else
            result = desc.gradeDescriptionsToStrings();
        return result;
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.SchoolService#deleteLevel(java.lang.String)
	 */
	@Override
	public void deleteLevel(String levelKeyStr)
			throws MissingEntitiesException, ManualVerificationException,
			LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());  		
		InetLevel deletedLevel = InetDAO4ComplexUpdates.deleteLevel(levelKeyStr);
		//TODO, move below to a task
		InetDAO4ComplexUpdates.removeLevelFromSchool(levelKeyStr);
		log.warning("DELETED level " + deletedLevel.getKey() + " --by user " + userAuditId);
		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.SchoolService#getSchoolInfo()
	 */
	@Override
	public String[] getSchoolInfo()  throws MissingEntitiesException, LoginValidationException
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER, PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	Objectify ofy = ObjectifyService.begin();
    	return getSchoolInfo(ofy);
	}
	
	public static String[] getSchoolInfo(Objectify ofy)
	{
    	InetSchool s = InetDAO4CommonReads.getSchool(ofy);
    	String[]  result = new String[6];
    	result[PanelServiceConstants.SCHOOL_INFO_NAME_IDX] = s.getSchoolName();
    	result[PanelServiceConstants.SCHOOL_INFO_ACCR_IDX] = s.getAccronym();
    	result[PanelServiceConstants.SCHOOL_INFO_ADDR_IDX] = s.getAddress();
    	result[PanelServiceConstants.SCHOOL_INFO_EMAIL_IDX] = s.getEmail();
    	result[PanelServiceConstants.SCHOOL_INFO_WEB_IDX] = s.getWebAddress();
    	String temp = "";
    	int count = 0;
    	if(s.getPhoneNumbers() != null)
    	{
	    	for(String num : s.getPhoneNumbers())
	    	{
	    		count++;
	    		temp += num;
	    		if(count != s.getPhoneNumbers().size())
	    			temp += ", ";
	    	}
    	}
    	result[PanelServiceConstants.SCHOOL_INFO_NUMS_IDX] = temp;	
    	
    	return result;		
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getNotice(java.lang.String)
	 */
	@Override
	public String[] getNotice(String id) throws MissingEntitiesException, LoginValidationException {
		if(!id.equals(PanelServiceConstants.PUBLIC_NOTE_ID))
		{
	    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER, PanelServiceLoginRoles.ROLE_ADMIN, 
	    			PanelServiceLoginRoles.ROLE_STUDENT, PanelServiceLoginRoles.ROLE_TEACHER, PanelServiceLoginRoles.ROLE_GUARDIAN};
	    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());	
		}
		Objectify ofy = ObjectifyService.begin();
		InetNotice notice = ofy.find(InetNotice.class, id);
		if(notice == null)
		{
			String msgFmt = "Unable to load notice: " + id;
			log.severe(msgFmt + id);
			throw new MissingEntitiesException(msgFmt);
		}
		String[] result = new String[2];
		result[0] = notice.getTitle();
		result[1] = notice.getContent();
		return result;
	}
	
	@Override
	public void updateNotice(String id, String title, String content) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER, PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.beginTransaction();
		InetNotice notice = null;
		try
		{
			notice = ofy.find(InetNotice.class, id);
			if(notice == null)
			{
				String msgFmt = "Unable to load notice: " + id;
				log.severe(msgFmt + id);
				throw new MissingEntitiesException(msgFmt);				
			}
			notice.setTitle(title);
			notice.setContent(content);
			ofy.put(notice);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		log.warning("Updated notice " + notice.getKey() + " --by user " + userAuditId);		
	}
	
	@Override
	public List<TableMessage> getApplicationParameter(String ID) {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		ApplicationParameters appObj = ObjectifyService.begin().get(pk);
		HashMap<String, String> params = appObj.getParams();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "Unique Identifier", TableMessageContent.TEXT);
		h.setText(1, "Description", TableMessageContent.TEXT);
		h.setMessageId(appObj.getKey().getString());
		result.add(h);
		for(String s : params.keySet())
		{
			TableMessage m = new TableMessage(2, 0, 0);
			m.setText(0, s);
			m.setText(1, params.get(s));
			m.setMessageId(s);
			result.add(m);
		}
		Collections.sort(result);
		return result;
	}

	@Override
	public String saveApplicationParameter(String ID, HashMap<String, String> val) throws MissingEntitiesException {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		return updateApplicationParameters(pk, val);
	}
	public static String updateApplicationParameters(Key<ApplicationParameters> key, HashMap<String, String> parameters)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ApplicationParameters paramsObject = ofy.find(key);
			if (paramsObject == null)
				throw new MissingEntitiesException("Parameter does not exist, unable to update: " +key);

			HashMap<String, String> oldParameters = paramsObject.getParams();
			paramsObject.setParams(parameters);
			ofy.put(paramsObject);
			ArrayList<String> diffs = new ArrayList<String>();
			for(String o : oldParameters.keySet())
			{
				if(parameters.containsKey(o))
				{
					if(!parameters.get(o).equalsIgnoreCase(oldParameters.get(o)))
						diffs.add("Value for " + o + " changed from: [" + oldParameters.get(o) + "] to: [" + parameters.get(o) + "]");
				}
				else
					diffs.add("Entry for " + o +" REMOVED. Its description at time of removal was: [" + oldParameters.get(o) + "]");
					
			}
			ofy.getTxn().commit();
			for(String o : parameters.keySet())
			{
				if(!oldParameters.containsKey(o))
					diffs.add("Entry for " + o + " ADDED. Its description is: [" + parameters.get(o) + "]");
			}
			StringBuilder result = new StringBuilder("<ul>");
			for(String diff : diffs)
				result.append("<li>").append(diff).append("</li>");
			result.append("</ul>");
			return result.toString();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
}
