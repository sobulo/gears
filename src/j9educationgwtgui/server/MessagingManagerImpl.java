package j9educationgwtgui.server;

import j9educationactions.pdfreports.BillingInvoiceGenerator;
import j9educationactions.pdfreports.GenericExcelDownload;
import j9educationactions.pdfreports.ReportCardGenerator;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetConstants;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetLevel;
import j9educationentities.InetStudent;
import j9educationentities.InetUser;
import j9educationentities.accounting.InetBill;
import j9educationentities.accounting.InetBillDescription;
import j9educationentities.accounting.InetDAO4Accounts;
import j9educationentities.messaging.EmailBillingController;
import j9educationentities.messaging.EmailController;
import j9educationentities.messaging.EmailReportCardController;
import j9educationentities.messaging.MessagingController;
import j9educationentities.messaging.MessagingDAO;
import j9educationentities.messaging.SMSController;
import j9educationgwtgui.client.MessagingManager;
import j9educationgwtgui.shared.CustomMessageTypes;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.TableMessageHeader.TableMessageContent;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class MessagingManagerImpl extends RemoteServiceServlet implements MessagingManager {

	private static final Logger log = Logger.getLogger(MessagingManagerImpl.class.getName());
	
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.MessagingManager#getStudentParentContact(java.lang.String, boolean)
	 */
	@Override
	public List<TableMessage> getStudentParentContact(CustomMessageTypes msgType, String keyStr,
			boolean includeParents) throws MissingEntitiesException 
	{
		Set<Key<InetStudent>> studentKeys = null;
		Objectify ofy = ObjectifyService.begin();
		if(msgType.equals(CustomMessageTypes.BILLING_MESSAGE))
		{
			Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(keyStr);
			List<InetBill> studentBills = InetDAO4Accounts.getBills(bdKey, ofy);
			studentKeys = new HashSet<Key<InetStudent>>(studentBills.size());
			for(InetBill bill : studentBills)
				studentKeys.add((Key<InetStudent>) bill.getUserKey());
		}
		else
		{
			Key<InetLevel> levelKey = ofy.getFactory().stringToKey(keyStr);
			InetLevel level = ofy.find(levelKey);
			if(level == null)
			{
				String msgFmt = "Error, unable to find object for level: ";
				log.severe(msgFmt + levelKey); 
				throw new MissingEntitiesException(msgFmt + levelKey.getName());
			}
			studentKeys = level.getStudentKeys();
		}
		StudentParentContactPopulator contactPopulator = new StudentParentContactPopulator();
		InetDAO4CommonReads.populateStudentParentInformation(studentKeys, contactPopulator, includeParents, ofy);
		return contactPopulator.getData();
	}
	
	private static class StudentParentContactPopulator implements InetObjectDataPopulator
	{
		ArrayList<TableMessage> data;

		StudentParentContactPopulator() {
			data = new ArrayList<TableMessage>();
		}

		/* (non-Javadoc)
		 * @see j9educationgwtgui.server.InetObjectDataPopulator#populateInfo(j9educationentities.GAEPrimaryKeyEntity[])
		 */
		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] objects) {
			InetStudent stud = (InetStudent) objects[0];
			String id = stud.getKey().getName();
			data.add(getInfo(stud, id));
			if(objects[1] != null)
				data.add(getInfo(objects[1], id));
			if(objects[2] != null)
				data.add(getInfo(objects[2], id));
		}
		
		private TableMessage getInfo(GAEPrimaryKeyEntity userWrapper, String id)
		{
			InetUser user = (InetUser) userWrapper;
			TableMessage contactInfo = new TableMessage(5, 0, 0);
			contactInfo.setMessageId(id);
			if(user instanceof InetStudent)
				contactInfo.setText(0, PanelServiceConstants.STUDENT_DATA_INDICATOR);
			else
				contactInfo.setText(0, PanelServiceConstants.GUARDIAN_DATA_INDICATOR);
			
			contactInfo.setText(1, user.getLname());
			contactInfo.setText(2, user.getFname());
			
			String contactVal = "";
			if(user.getEmail() != null)
				contactVal = user.getEmail();
			contactInfo.setText(3, contactVal);
			
			contactVal = "";
			if(user.getPhoneNumbers() != null && user.getPhoneNumbers().size() != 0)
				contactVal = user.getPhoneNumbers().iterator().next();
			contactInfo.setText(4, contactVal);
			
			return contactInfo;
		}
		
		List<TableMessage> getData(){ return data; }
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.MessagingManager#sendMessage(java.util.HashSet, java.lang.String, boolean)
	 */
	@Override
	public void sendMessage(CustomMessageTypes msgType, String[] addresses, String message[],
			boolean isEmail) throws MissingEntitiesException, ManualVerificationException, LoginValidationException {
		Key<? extends MessagingController> controllerKey = null;
		if(msgType.equals(CustomMessageTypes.GENERIC_MESSAGE))
		{
			if(isEmail)
				controllerKey = new Key<EmailController>(EmailController.class, InetConstants.EMAIL_CONTROLLER_ID);
			else
				controllerKey = new Key<SMSController>(SMSController.class, InetConstants.SMS_CONTROLLER_ID);
			String controllerId = MessagingDAO.validateController(controllerKey, addresses.length);
			scheduleMessageSending(controllerId, addresses, message);
		}
		else if(msgType.equals(CustomMessageTypes.GRADING_MESSAGE))
		{
			if(isEmail)
			{
				controllerKey = new Key<EmailReportCardController>(EmailReportCardController.class,
							InetConstants.EMAIL_RC_CONTROLLER_ID);
				String controllerId = MessagingDAO.validateController(controllerKey, addresses.length);
				schedulePDFEmail(controllerId, addresses, message);
			}
			else
			{
				controllerKey = new Key<SMSController>(SMSController.class, InetConstants.SMS_CONTROLLER_ID);
				String controllerId = MessagingDAO.validateController(controllerKey, addresses.length);				
				String[] allTextMessages = ReportCardGenerator.getReportCardSummarry(message[0], Arrays.copyOfRange(message, 1, message.length));
				scheduleSMSTexts(controllerId, addresses, allTextMessages);
			}
		}
		else if(msgType.equals(CustomMessageTypes.BILLING_MESSAGE))
		{
			if(isEmail)
			{
				controllerKey = new Key<EmailBillingController>(EmailBillingController.class,
							InetConstants.EMAIL_BILL_CONTROLLER_ID);
				String controllerId = MessagingDAO.validateController(controllerKey, addresses.length);
				schedulePDFEmail(controllerId, addresses, message);
			}
			else
			{
				controllerKey = new Key<SMSController>(SMSController.class, InetConstants.SMS_CONTROLLER_ID);
				String controllerId = MessagingDAO.validateController(controllerKey, addresses.length);				
				String[] allTextMessages = BillingInvoiceGenerator.getSummaryMessages(message[0], Arrays.copyOfRange(message, 1, message.length));
				scheduleSMSTexts(controllerId, addresses, allTextMessages);
			}			
		}
	}
	
	private static void scheduleMessageSending(String controllerId,
			String[] addresses, String[] message) throws MissingEntitiesException, ManualVerificationException
	{
		for(String address : addresses)
			TaskQueueHelper.scheduleMessageSending(controllerId, address, message);
	}	
	
	private void schedulePDFEmail(String controllerId, String[] addresses, String[] message)
	{
		for(int i = 0; i < addresses.length; i++)
		{
			String[] msgParts = new String[2];
			msgParts[0] = message[0];
			msgParts[1] = message[i+1];
			TaskQueueHelper.scheduleMessageSending(controllerId, addresses[i], msgParts);
		}		
	}
	
	private void scheduleSMSTexts(String controllerId, String[] addresses, String[] allTextMessages) throws ManualVerificationException
	{
		if(allTextMessages == null) throw new ManualVerificationException("Unable to generate message body");
		String[] messageWrapper = new String[1];
		for(int i = 0; i < allTextMessages.length; i++)
		{
			messageWrapper[0] = allTextMessages[i];
			TaskQueueHelper.scheduleMessageSending(controllerId, addresses[i], messageWrapper);
		}		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.MessagingManager#getMessagingControllerNames()
	 */
	@Override
	public HashMap<String, String> getMessagingControllerNames() {
		return MessagingDAO.getMessagingControllerNames();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.MessagingManager#getControllerDetails(java.lang.String)
	 */
	@Override
	public List<TableMessage> getControllerDetails(String controllerName) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerName);
		MessagingController controller = ofy.find(controllerKey);
		if(controller == null)
		{
			String msgFmt = "Unable to find messaging controller: ";
			log.severe(msgFmt + controllerKey);
			throw new MissingEntitiesException(msgFmt + controllerKey.getName());
		}
		
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessage info = new TableMessage(1, 3, 1);
		info.setText(0, controllerKey.getName());
		info.setDate(0, controller.getLastAccessed());
		info.setNumber(0, controller.getTotalMessageLimit());
		info.setNumber(1, controller.getDailyMessageLimit());
		info.setNumber(2, controller.getTotalSentMessages());
		result.add(info);
		//setup header
		TableMessageHeader header = new TableMessageHeader(2);
		header.setText(0, "Access Date", TableMessageContent.DATE);
		header.setText(1, "Messages Sent", TableMessageContent.TEXT);
		result.add(header);
		HashMap<Date, Integer> dailyCounts = controller.getDailyMessageCount();
		for(Date accessDate : dailyCounts.keySet())
		{
			info = new TableMessage(1, 0, 1);
			info.setDate(0, accessDate);
			info.setText(0, dailyCounts.get(accessDate).toString());
			result.add(info);
		}
		return result;
	}
	
	@Override
	public String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header) {
		//log.severe("RECEIVED EXCEL REQUEST");
		return GenericExcelDownload.getGenericExcelDownloadLink(data, header, getThreadLocalRequest());
	}	
}
