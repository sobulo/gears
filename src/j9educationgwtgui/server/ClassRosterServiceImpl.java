/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.server;



import j9educationactions.login.LoginPortal;
import j9educationactions.pdfreports.TranscriptGenerator;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetConstants;
import j9educationentities.InetCourse;
import j9educationentities.InetCumulativeScores;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetDAO4ComplexUpdates;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetDAO4Updates;
import j9educationentities.InetGuardian;
import j9educationentities.InetLevel;
import j9educationentities.InetSingleTestScores;
import j9educationentities.InetStudent;
import j9educationentities.InetTestScores;
import j9educationgwtgui.client.ClassRosterService;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
public class ClassRosterServiceImpl extends RemoteServiceServlet implements ClassRosterService {

    private static final Logger log = Logger.getLogger(ClassRosterServiceImpl.class.getName());

    @Override
    public HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> getClassKeys() throws MissingEntitiesException,
            LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_SUPER};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    
        Objectify ofy = ObjectifyService.begin();
        InetLevelToNestedMap populator = new InetLevelToNestedMap(ofy);
        InetDAO4CommonReads.populateSchoolLevelInfo(populator, ofy);
        return populator.getData();
    }

    @Override
    public String addStudent(String levelKeyStr, String fname, String lname,
            Date dob, String email, HashSet<String> phoneNumbers,
            String loginName, String password) throws
            DuplicateEntitiesException, MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    
        String displayName = "";
        //create student
        InetStudent inetStud = InetDAO4Creation.createStudent(fname,
                lname, dob, email, phoneNumbers, loginName, password, levelKeyStr);

        //add student to level roster via task queue
        ArrayList<String> studentKeys = new ArrayList<String>(1);
        studentKeys.add(loginName);
        TaskQueueHelper.scheduleAddStudentsToClassRoster(levelKeyStr, studentKeys);

        //return value
        displayName = inetStud.toString();
        log.warning("CREATED " + inetStud.getKey() + " --by user " + userAuditId);
        return displayName;
    }

    @Override
    public ArrayList<TableMessage> getClassRosterTable(String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());            
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        return getClassRosterTable(levelKey, ofy);
    }

    public static ArrayList<TableMessage> getClassRosterTable(Key<InetLevel> levelKey, Objectify ofy)
            throws MissingEntitiesException, LoginValidationException
    {       	
        InetStudentToTableMessage data = new InetStudentToTableMessage();
        data.setCaption(InetDAO4CommonReads.keyToPrettyString(levelKey));
        InetDAO4CommonReads.getClassRosterInfo(levelKey, data, ofy);
        ArrayList<TableMessage> result = data.getStudentData();
        return result;
    }


    @Override
    public String[] getSimpleCourseNames(String subjectKey) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());        	
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(subjectKey);
        String[] result;
        InetLevel level = ofy.find(levelKey);
        if(level == null)
        {
        	String msgFmt = "Unable to find level: ";
        	log.severe(msgFmt + levelKey);
            throw new MissingEntitiesException(msgFmt + levelKey.getName());
        }
        Iterator<Key<InetCourse>> itr = level.getCourseList().iterator();
        int numOfCourses = level.numOfCourses();

        if (numOfCourses == 0) {
            return (new String[0]);
        }

        result = new String[numOfCourses];
        int i = 0;
        while (itr.hasNext()) {
            result[i++] = InetDAO4CommonReads.getNameFromLastKeyPart(itr.next());
        }
        return result;
    }

    @Override
    public String addCourse(String subjectName, String classKeyStr) throws
            MissingEntitiesException, DuplicateEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());            
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> lk = ofy.getFactory().stringToKey(classKeyStr);
        String displayName = "";

        //create course
        InetCourse inetCourse = InetDAO4Creation.createCourse(lk, subjectName);
        displayName = inetCourse.getCourseName();
        
        //schedule addition to level list
        String courseKeyStr = ofy.getFactory().keyToString(inetCourse.getKey());
        TaskQueueHelper.scheduleAddCourseToLevel(courseKeyStr, classKeyStr);
        log.warning("CREATED " + inetCourse.getKey() + " --by " + userAuditId);
        return displayName;
    }

    @Override
    public HashMap<String, String> getClassRosterMap(String classKey) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());     	
        InetStudentToHashMap data = new InetStudentToHashMap();
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(classKey);
        InetDAO4CommonReads.getClassRosterInfo(levelKey, data, ofy);
        HashMap<String, String> result = data.getStudentData();
        return result;
    }

    @Override
    public HashMap<String, String> getCourseRosterMap(String courseKeyStr) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());     	
        InetStudentToHashMap data = new InetStudentToHashMap();
        Objectify ofy = ObjectifyService.begin();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        InetDAO4CommonReads.getCourseRosterInfo(courseKey, data, null, ofy);
        HashMap<String, String> result = data.getStudentData();
        return result;
    }    
    
    @Override
    public void addStudentsToCourse(String levelKeyStr, String courseKeyStr, String[] studentKeys)
            throws DuplicateEntitiesException, MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	
        //add students to course tests and schedule addition of course to student list as well
        InetDAO4Updates.addStudentsToCourseTests(levelKeyStr, courseKeyStr, studentKeys, true);
            
        Key<InetCourse> ck = ObjectifyService.begin().getFactory().stringToKey(courseKeyStr);
        log.warning("ADDED " + studentKeys.length + " students to course " + ck + " --by user " + userAuditId);
    }

    @Override
    public void addStudentsToLevel(String levelKeyStr, String[] studentKeys)
            throws DuplicateEntitiesException, MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());     	
        //add students to course tests
        Objectify ofy = ObjectifyService.begin();
        String[] loginStrs = new String[studentKeys.length];
        for(int i = 0; i < loginStrs.length; i++)
            loginStrs[i] = ofy.getFactory().stringToKey(studentKeys[i]).getName();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);

        InetDAO4Updates.addStudentsToLevelRoster(levelKey, loginStrs, true);

        //schedule addition of courseKeys to each student's registered courses
        for (String sk : studentKeys)
            TaskQueueHelper.scheduleAddLevelToStudentClassList(levelKeyStr, sk);
        log.warning("ADDED " + studentKeys.length + " students to level " + levelKey + " --by user " + userAuditId);
    }
    
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#getCourseTable(java.lang.String)
	 */
	@Override
	public ArrayList<TableMessage> getCourseTable(String levelKeyStr)
			throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());  		
		InetCourseToTableMessage data = new InetCourseToTableMessage();
		Objectify ofy = ObjectifyService.begin();
		Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
		data.setCaption(InetDAO4CommonReads.keyToPrettyString(levelKey));
		InetDAO4CommonReads.getLevelCourseInfo(levelKey, data, ofy);
		ArrayList<TableMessage> result = data.getCourseData();
		return result;
	}    
	
	@Override
	public ArrayList<TableMessage> getCoursePublishStatusTable(String levelKeyStr)
	throws MissingEntitiesException, LoginValidationException 
	{
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());  		
		Objectify ofy = ObjectifyService.begin();
		Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
		InetLevel level = ofy.find(levelKey);
		if(level == null)
		{
			String msgFmt = "unable to generate publish status because level obj missing for: ";
			log.severe(msgFmt + levelKey);
			throw new MissingEntitiesException(msgFmt + levelKey.getName());
		}
		Map<Key<InetCourse>, InetCourse> courses = InetDAO4CommonReads.getEntities(level.getCourseList(), ofy, true);
		ArrayList<Key<? extends InetTestScores>> testKeys = new ArrayList<Key<? extends InetTestScores>>();
		for(InetCourse c : courses.values())
			testKeys.addAll(c.getAllTestKeys());
		Map<Key<InetTestScores>, InetTestScores> tests = InetDAO4CommonReads.getMixedEntities(testKeys, ofy, true);
			
		
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(courses.size());
		
        TableMessageHeader header = new TableMessageHeader(4);
        header.setText(0, "Subject Name", TableMessageHeader.TableMessageContent.TEXT);
        header.setText(1, "Number of Tests", TableMessageHeader.TableMessageContent.NUMBER);
        header.setText(2, "Number of Published Tests", TableMessageHeader.TableMessageContent.NUMBER);
        header.setText(3, "Subject Published", TableMessageHeader.TableMessageContent.TEXT);
		header.setCaption(InetDAO4CommonReads.keyToPrettyString(levelKey));
		result.add(header);
		
		for(InetCourse c : courses.values())
		{
			TableMessage m = new TableMessage(2 + c.getNumberOfTestScores() * 2, 2 + c.getNumberOfTestScores(), 0);
			m.setText(0, c.getCourseName());
			m.setText(1, tests.get(c.getCumulativeScores()).isPublished() ? "Yes" : "No");
			m.setNumber(0, c.getNumberOfTestScores());
			int numPublishedTests = 0;
			int testNameIdx = 2;
			int testStatusIdx = 2;
			for(Key<InetSingleTestScores> tk : c.getTestScoresKeys())
			{
				InetSingleTestScores tst = (InetSingleTestScores) tests.get(tk);
				m.setText(testNameIdx++, tst.getTestName());
				m.setText(testNameIdx++, ofy.getFactory().keyToString(tk));
				if(tst.isPublished())
				{
					m.setNumber(testStatusIdx++, 1);
					numPublishedTests++;
				}
				else
					m.setNumber(testStatusIdx++, 0);
			}
			m.setNumber(1, numPublishedTests);
			m.setMessageId(ofy.getFactory().keyToString(c.getKey()));
			result.add(m);
		}
        return result;
	}	

    @Override
    public ArrayList<TableMessage> getCourseRosterTable(String courseKeyStr) 
            throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());      	
        InetStudentToTableMessage data = new InetStudentToTableMessage();
        Objectify ofy = ObjectifyService.begin();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        data.setCaption(InetDAO4CommonReads.getNameFromLastKeyPart(courseKey));
        InetDAO4CommonReads.getCourseRosterInfo(courseKey, data, null, ofy);
        ArrayList<TableMessage> result = data.getStudentData();
        return result;
    }

    @Override
    public HashMap<String, String> getCourseKeys(String classKey) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());      	
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> lk = ofy.getFactory().stringToKey(classKey);

        HashMap<String, String> result = new HashMap<String, String>();
 
        InetLevel level = ofy.find(lk);
        if (level == null) {
            String exMsg = "unable to retrieve class obj for key: " + lk;
            log.severe(exMsg);
            throw new MissingEntitiesException(exMsg);
        }

        int numOfCourses = level.numOfCourses();
        Iterator<Key<InetCourse>> itr = level.getCourseList().iterator();
        if (numOfCourses == 0) {
            return (new HashMap<String, String>());
        }

        while(itr.hasNext())
        {
            Key<InetCourse> i = itr.next();
            String courseName = InetDAO4CommonReads.getNameFromLastKeyPart(i);
            String courseKey = ofy.getFactory().keyToString(i);
            result.put(courseName, courseKey);
        }
        return result;
    }

    @Override
    public String addGuardian(String studKeyStr, String fname, String lname,
            String email, HashSet<String> phoneNumbers, String loginName,
            String password) throws MissingEntitiesException,
            DuplicateEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());      	
        Objectify ofy = ObjectifyService.begin();

        //create guardian
        Key<InetStudent> studKey = ofy.getFactory().stringToKey(studKeyStr);
        
        //check if slots available to add guardian
        if(InetDAO4CommonReads.getStudentGuardKeys(ofy, studKey).size() == 2)
        {
        	String msgFmt = "Missing an open slot. Attempting to add guardian to student %s, student already has 2 guardians specified";
        	log.warning(String.format(msgFmt, studKey.toString()));
        	throw new MissingEntitiesException(String.format(msgFmt, studKey.getName()));
        }
        InetGuardian g = InetDAO4Creation.createGuardian(studKey, fname,
                lname, email, phoneNumbers, loginName, password);

        //schedule addition of guardian to student object
        String guardKeyStr = ofy.getFactory().keyToString(g.getKey());
        TaskQueueHelper.scheduleAddGuardianToStudent(guardKeyStr, studKeyStr);
        log.warning("CREATED GUARDIAN: " + g.getKey() + " --by user " + userAuditId);
        return g.getFname() + " " + g.getLname();
    }
    
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#removeStudentsFromLevel(java.lang.String, java.lang.String[])
	 */
	@Override
	public void removeStudentsFromLevel(String levelKeyStr, String[] studentKeys)
			throws MissingEntitiesException, ManualVerificationException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());  		
		//first check that none of the students are enrolled in courses for this level
		ArrayList<Key<InetStudent>> studKeys = new ArrayList<Key<InetStudent>>(studentKeys.length);
		Objectify ofy = ObjectifyService.begin();
		Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
		for(String sk : studentKeys)
		{
			Key<InetStudent> studKey = ofy.getFactory().stringToKey(sk);
			studKeys.add(studKey);
		}
		Map<Key<InetStudent>, InetStudent> studData = ofy.get(studKeys);
		
		if(studKeys.size() != studData.size())
		{
			String msgFmt = "Unable to fetch all students requested for removal from: ";
			log.severe(msgFmt + levelKey);
			throw new MissingEntitiesException(msgFmt + levelKey.getName());
		}
		
		Collection<InetStudent> students = studData.values();
		
		for(InetStudent stud : students)
		{
			int numOfCourses = stud.getCourseKeys(levelKey).size();
			
			if(stud.getNumberOfLevels() == 1)
			{
				String msgFmt = "Aborted removal of students from class(%s). Student(%s)" +
				"  enrolled in this class ONLY. To remove student from class you must delete student";
				log.warning(String.format(msgFmt, levelKey.toString(), stud.getKey().toString()));
				throw new ManualVerificationException(String.format(msgFmt, 
						levelKey.getName(), stud.getKey().getName()));
			}
			
			if( numOfCourses != 0 )
			{
				String msgFmt = "Aborted removal of students from class(%s). Student(%s)" +
						"  still registered for  subjects(" + numOfCourses + ") in that class";
				log.warning(String.format(msgFmt, levelKey.toString(), stud.getKey().toString()));
				throw new ManualVerificationException(String.format(msgFmt, 
						levelKey.getName(), stud.getKey().getName()));
			}
		}
		
		//now proceed to remove students from level
		InetDAO4ComplexUpdates.removeStudentsFromLevel(studKeys, levelKey);
		
		//schedule removal of level from student list
		for(String studKeyStr : studentKeys)
			TaskQueueHelper.scheduleRemoveLevelFromStudentList(levelKeyStr, studKeyStr);
		log.warning("REMOVED " + studentKeys.length + " students from level " + levelKey + " --by user " + userAuditId);
	}
	
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#removeStudentsFromLevel(java.lang.String, java.lang.String[])
	 */
	@Override
	public void removeStudentsFromCourse(String courseKeyStr, String[] studentKeys)
			throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());  		
		InetDAO4ComplexUpdates.removeStudentsFromCourse(studentKeys, courseKeyStr);
		
		//schedule removal of course from student list
		for(String studKeyStr : studentKeys)
			TaskQueueHelper.scheduleRemoveCourseFromStudentList(courseKeyStr, studKeyStr);
		
		Key<InetCourse> ck = ObjectifyService.begin().getFactory().stringToKey(courseKeyStr);
		log.warning("REMOVED " + studentKeys.length + " from course " + ck + " --by user " + userAuditId);
	}	

    private static class InetStudentToTableMessage implements InetObjectDataPopulator {
        ArrayList<TableMessage> data;

        private InetStudentToTableMessage()
        {
            data = new ArrayList<TableMessage>(InetConstants.DEFAULT_NUM_OF_STUDENTS_PER_COURSE);
            TableMessageHeader m = new TableMessageHeader(5);
            m.setText(0, "Login Name", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(1, "Last Name", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(2, "First Name", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(3, "Email", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(4, "DOB",TableMessageHeader.TableMessageContent.DATE);
            data.add(m);
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] dataContainer) {
            InetStudent student = (InetStudent) dataContainer[0];
            TableMessage m = new TableMessage(4, 0, 1);
            m.setText(0, student.getKey().getName());
            m.setText(1, student.getLname());
            m.setText(2, student.getFname());
            m.setText(3, student.getEmail());
            m.setDate(0, student.getBirthday());
            data.add(m);
        }

        private ArrayList<TableMessage> getStudentData() {
            return data;
        }

        private void setCaption(String caption)
        {
            if(data.size() > 0)
                ((TableMessageHeader) data.get(0)).setCaption(caption);
        }
    }

    private static class InetStudentToHashMap implements InetObjectDataPopulator
    {
        private HashMap<String, String> data;
        Objectify ofy;
        private InetStudentToHashMap() {
            data = new HashMap<String, String>(InetConstants.DEFAULT_NUM_OF_STUDENTS_PER_COURSE);
            ofy = ObjectifyService.begin();
        }
        
        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] dataContainer) {
            InetStudent s = (InetStudent) dataContainer[0];
            //TODO fix duplicate issue here by adding keys to parenthesis
            //String name = s.getLname() + "," + s.getFname();
            String name = s.getLoginName() + " (" + s.getLname() + ")";
            String val = ofy.getFactory().keyToString(s.getKey());
            data.put(name, val);
        }
        private HashMap<String, String> getStudentData() {
            return data;
        }
    }
    
    private static class InetLevelToNestedMap implements InetObjectDataPopulator
    {
    	private Objectify ofy;
    	private HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> data;
    	
    	private InetLevelToNestedMap(Objectify ofy)
    	{
    		this.ofy = ofy;
    		data = new HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>>();
    	}
    	
		/* (non-Javadoc)
		 * @see j9educationgwtgui.server.InetObjectDataPopulator#populateInfo(j9educationentities.GAEPrimaryKeyEntity[])
		 */
		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] objects) {
			InetLevel level = (InetLevel) objects[0];
        	String year = level.getAcademicYear();
        	String term = level.getTerm();
        	String topLevel = level.getLevelName();
        	String subLevel = level.getSubLevelName();
        	
			if(data.get(year) == null)
				data.put(year, new HashMap<String,HashMap<String,HashMap<String,String>>>());
			if(data.get(year).get(term) == null)
				data.get(year).put(term, new HashMap<String,HashMap<String,String>>());
			if(data.get(year).get(term).get(topLevel) == null)
				data.get(year).get(term).put(topLevel, new HashMap<String, String>());
			
			data.get(year).get(term).get(topLevel).put(subLevel, ofy.getFactory().keyToString(level.getKey()));		
		}
		
		private HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> getData()
		{
			return data;
		}
    	
    }
    
    private static class InetLevelToHashMap implements InetObjectDataPopulator
    {
        private HashMap<String, String> data;
        private Objectify ofy;
        
        private InetLevelToHashMap(Objectify ofy) {
            data = new HashMap<String, String>();
            this.ofy = ofy; 
        }
        
        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] dataContainer) {
            InetLevel lev = (InetLevel) dataContainer[0];
            String levKey = ofy.getFactory().keyToString(lev.getKey());
            String year = lev.getAcademicYear();
            String term = lev.getTerm();
            String seperator = PanelServiceConstants.DATA_SEPERATOR;
            StringBuffer sb = new StringBuffer(year.length() + term.length() + seperator.length());
            data.put(levKey, sb.append(year).append(seperator).append(term).toString());
        }
        
        private HashMap<String, String> getStudentData() {
            return data;
        }
    }    
    
    private static class InetCourseToTableMessage implements InetObjectDataPopulator {
        ArrayList<TableMessage> data;

        private InetCourseToTableMessage()
        {
            data = new ArrayList<TableMessage>();
            TableMessageHeader m = new TableMessageHeader(4);
            m.setText(0, "Course Name", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(1, "Teacher ID", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(2, "Number of Tests", TableMessageHeader.TableMessageContent.NUMBER);
            m.setText(3, "Course Units", TableMessageHeader.TableMessageContent.NUMBER);
            m.setEditable(3, true);
            data.add(m);
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] dataContainer) {
            InetCourse course = (InetCourse) dataContainer[0];
            TableMessage m = new TableMessage(2, 2, 0);
            m.setText(0, course.getCourseName());
            String teacherId = "";
            if(course.getTeacherKey() != null)
            	teacherId = course.getTeacherKey().getName();
            m.setText(1, teacherId);
            m.setNumber(0, course.getNumberOfTestScores());
            m.setNumber(PanelServiceConstants.COURSE_UNIT_IDX, course.getNumberOfUnits());
            m.setMessageId(course.getKey().getString());
            data.add(m);
        }

        private ArrayList<TableMessage> getCourseData() {
            return data;
        }

        private void setCaption(String caption)
        {
            if(data.size() > 0)
                ((TableMessageHeader) data.get(0)).setCaption(caption);
        }
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#deleteCourse(java.lang.String)
	 */
	@Override
	public void deleteCourse(String courseKeyStr) throws MissingEntitiesException, 
		ManualVerificationException, LoginValidationException 
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());  		
		InetCourse deletedCourse = InetDAO4ComplexUpdates.deleteCourse(courseKeyStr);
		Objectify ofy = ObjectifyService.begin();
		String levelKeyStr = ofy.getFactory().keyToString(deletedCourse.getInetLevelKey());
		TaskQueueHelper.scheduleRemoveCourseFromLevel(courseKeyStr, levelKeyStr);
		log.warning("DELETED course " + deletedCourse.getKey() + " --by user " + userAuditId);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#getAcademicTermInfo4Levels()
	 */
	@Override
	public HashMap<String, String> getAcademicTermInfo4Levels()
			throws MissingEntitiesException, LoginValidationException
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());  		
		Objectify ofy = ObjectifyService.begin();
		InetLevelToHashMap pop = new InetLevelToHashMap(ofy);
		InetDAO4CommonReads.getLevelBreakDowns(ofy, pop);
		return pop.getStudentData();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#copyCourses(java.util.HashSet, java.lang.String)
	 */
	@Override
	public String copyCourses(String[] courseKeyStrs, String classKeyStr, boolean copyStudents)
			throws MissingEntitiesException, DuplicateEntitiesException,
			LoginValidationException 
	{
		//verify role
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, getThreadLocalRequest());
		
		Objectify ofy = ObjectifyService.begin();
		//fetch old/source courses
		ArrayList<Key<InetCourse>> courseKeys = new ArrayList<Key<InetCourse>>(courseKeyStrs.length);
		for(String ckStr : courseKeyStrs)
		{
			Key<InetCourse> ck = ofy.getFactory().stringToKey(ckStr);
			courseKeys.add(ck);
		}
		Map<Key<InetCourse>,  InetCourse> courseObjs = InetDAO4CommonReads.getEntities(courseKeys, ofy, true);
		
		//fetch tests  and create new course keys for target class. We could wait till courses created
		//before attempting to add to class but would rather fail earlier in case of duplicates
		ArrayList<Key<? extends InetTestScores>> testKeys = new ArrayList<Key<? extends InetTestScores>>();
		Key<InetLevel> levelKey = ofy.getFactory().stringToKey(classKeyStr);
		String[] newCourseKeyStrs = new String[courseKeyStrs.length];
		int count = 0;
		for(InetCourse course : courseObjs.values())
		{
			testKeys.addAll(course.getAllTestKeys());
			String temp = InetCourse.getKeyString(levelKey, course.getCourseName());
			Key<InetCourse> newCourseKey = new Key<InetCourse>(InetCourse.class, temp);
			newCourseKeyStrs[count++] = ofy.getFactory().keyToString(newCourseKey);
		}
		
		//fetch test objects
		Map<Key<InetTestScores>, InetTestScores> testObjs = InetDAO4CommonReads.getMixedEntities(testKeys, ofy, true);
		
		//validate course roster will matchup with level roster
		StringBuilder result = new StringBuilder();
		result.append("Copied " + courseObjs.values().size() + " subjects to " + InetDAO4CommonReads.keyToPrettyString(levelKey));
		if(copyStudents)
		{
			InetLevel level = ofy.find(levelKey);
			Set<Key<InetStudent>> levelRoster = level.getStudentKeys();
			if(levelRoster.size() == 0)
			{
				String msgFmt = "You must have registered students for class %s before you can choose to copy over subject rosters. " +
						"There are currently NO students registered for the class."; 
				log.severe(String.format(msgFmt, levelKey.toString()));
				throw new MissingEntitiesException(String.format(msgFmt, InetDAO4CommonReads.keyToPrettyString(levelKey)));
			}
			
			ArrayList<Key<InetStudent>> students2Copy = new ArrayList<Key<InetStudent>>(levelRoster.size());
			boolean foundRegisteredStudent = false;
			result.append(". Student roster copy counts: ");
			for(InetCourse course : courseObjs.values())
			{
				Set<Key<InetStudent>> sourceSubjectStudents = testObjs.get(course.getCumulativeScores()).getStudentKeys(); 
				int registeredCount = 0;
				for(Key<InetStudent> sk : sourceSubjectStudents)
				{
					if(levelRoster.contains(sk))
					{
						students2Copy.add(sk);
						foundRegisteredStudent = true;
						registeredCount++;
					}
						
				}
				int totalNum = sourceSubjectStudents.size();
				result.append(course.getCourseName()).append("(").append(registeredCount).append("/").append(totalNum).append(") ");
			}
			
			if(!foundRegisteredStudent)
			{
				String msgFmt = "You requested that students rosters be copied over when copying students but NO students registered for those subjects" +
						" are registered for class %s Please register students for class before attempting " +
								"to copy subject rosters or uncheck the copy students flag and simply copy subjects without the student roster";
				log.warning(String.format(msgFmt, levelKey.toString()));
				throw new MissingEntitiesException(String.format(msgFmt, InetDAO4CommonReads.keyToPrettyString(levelKey)));
			}
		}
		
		
		//update level's index of courses, if course exists in level already we fail early
		InetDAO4Updates.addCourseKeyToLevel(newCourseKeyStrs, classKeyStr);
		
		//schedule tasks
		String[] testInfo;
		Set<Key<InetSingleTestScores>> testKeysForCourse;
		for(InetCourse course : courseObjs.values())
		{
			testKeysForCourse = course.getTestScoresKeys();
			testInfo = new String[testKeysForCourse.size()];
			count = 0;
			InetSingleTestScores test;
			String ds = InetConstants.DELIMITTER;
			
			for(Key<InetSingleTestScores> tk : testKeysForCourse)
			{
				test = (InetSingleTestScores) testObjs.get(tk);
				testInfo[count++] = test.getTestName() + ds + test.getMaxScore() + ds + test.getWeight();
			}

			TaskQueueHelper.scheduleCreateCourse(classKeyStr, course.getCourseName(), testInfo, 
					copyStudents?testObjs.get(course.getCumulativeScores()).getStudentKeys():null);
		}
		return result.toString();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#requestReportCards(java.lang.String, java.lang.String[])
	 */
	@Override
	public String requestReportCards(String classKey, String[] studentLogins)
			throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, getThreadLocalRequest());			
		return TranscriptGenerator.getClassReportCards(studentLogins, getThreadLocalRequest(), true, true);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ClassRosterService#updateCourseTestStatus(java.lang.String, java.util.HashMap)
	 */
	@Override
	public String updateCourseTestStatus(String subjectKeyStr,
			HashMap<String, Boolean> testStatusInfoStr)
			throws MissingEntitiesException, LoginValidationException 
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, getThreadLocalRequest());			
		Objectify ofy = ObjectifyService.beginTransaction();
		Key<InetCourse> courseKey = null;
		try
		{		
			courseKey = ofy.getFactory().stringToKey(subjectKeyStr);
			HashMap<Key<InetSingleTestScores>, Boolean> testStatusInfo = new HashMap<Key<InetSingleTestScores>, Boolean>(testStatusInfoStr.size());
			for(String tkStr : testStatusInfoStr.keySet())
			{
				Key<InetSingleTestScores> testKey = ofy.getFactory().stringToKey(tkStr);
				if(!testKey.getParent().equals(courseKey))
				{
					String msgFmt = "Test %s does not have %s as its parent subject";
					log.severe(String.format(msgFmt, testKey.toString(), courseKey.toString()));
					throw new MissingEntitiesException(String.format(msgFmt, testKey.getName(), courseKey.getName()) + ". Refresh browser and try again.");
				}
				testStatusInfo.put(testKey, testStatusInfoStr.get(tkStr));
			}
			ArrayList<Key<? extends InetTestScores>> courseTestKeys = new ArrayList<Key<? extends InetTestScores>>(testStatusInfo.size() + 1);
			courseTestKeys.addAll(testStatusInfo.keySet());
			Key<InetCumulativeScores> cumKey = InetCumulativeScores.getCumScoreKey(courseKey);
			courseTestKeys.add(cumKey);
			Map<Key<InetTestScores>, InetTestScores> tests = InetDAO4CommonReads.getMixedEntities(courseTestKeys, ofy, true);
			
			ArrayList<GAEPrimaryKeyEntity> saveList = new ArrayList<GAEPrimaryKeyEntity>(tests.size());
			saveList.addAll(tests.values());


			boolean cumulativePublishState = true;
			for(Key<InetSingleTestScores> tk : testStatusInfo.keySet())
			{
				boolean published = testStatusInfo.get(tk);
				tests.get(tk).setPublished(published);
				cumulativePublishState = cumulativePublishState && published;
			}
			tests.get(cumKey).setPublished(cumulativePublishState);
			
			ofy.put(tests.values());
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		log.warning("Updated test status for " + courseKey + " --by user " + userAuditId);
		return subjectKeyStr;
	}

	@Override
	public ArrayList<TableMessage> updateCourseTable(
			HashMap<String, Integer> courseUnits) throws MissingEntitiesException, LoginValidationException 
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, getThreadLocalRequest());		
		ArrayList<Key<InetCourse>> updateKeyList = new ArrayList<Key<InetCourse>>(courseUnits.size());
		
		for(String courseKeyStr : courseUnits.keySet())
		{
			Key<InetCourse> k = ObjectifyService.factory().stringToKey(courseKeyStr);
			updateKeyList.add(k);
		}
		
		Key<InetLevel> levelKey = null;
		Objectify ofy = ObjectifyService.begin();
		Map<Key<InetCourse>, InetCourse> courseObjects = InetDAO4CommonReads.getEntities(updateKeyList, ofy, true);
		
		//update courses
		Collection<InetCourse> courses = courseObjects.values();
		for(InetCourse course : courseObjects.values())
		{
			Integer units = courseUnits.get(course.getKey().getString());
			if(units == null)
				throw new MissingEntitiesException("Illegal state, no units found for " + course.getCourseName());
			if(levelKey == null)
				levelKey = course.getInetLevelKey();
			if(!course.getInetLevelKey().equals(levelKey))
				throw new MissingEntitiesException("Found more than one class/level for unit update: " + levelKey.getName() + 
						" VS. " + course.getInetLevelKey().getName());
			
			course.setNumberOfUnits(units);
		}
		ofy.put(courses);
		log.warning(courseObjects.size() + " courses in " + levelKey + " updated with new unit values --by user " + userAuditId);
		return getCourseTable(levelKey.getString());
	}    
}
