/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.server;

import j9educationentities.GAEPrimaryKeyEntity;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import com.googlecode.objectify.Objectify;

/**
 *
 * @author Administrator
 */
public interface InetPopulatorInitializer {
    public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
            throws MissingEntitiesException;
}
