/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.server;




import j9educationactions.login.LoginPortal;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetConstants;
import j9educationentities.InetCourse;
import j9educationentities.InetCumulativeScores;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetDAO4ComplexUpdates;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetGradeDescriptions;
import j9educationentities.InetLevel;
import j9educationentities.InetSingleTestScores;
import j9educationentities.InetStudent;
import j9educationentities.InetTestScores;
import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Administrator
 */
public class ScoreManagerImpl extends RemoteServiceServlet implements ScoreManager {

    private static final Logger log =
            Logger.getLogger(ScoreManagerImpl.class.getName());
    


    @Override
    public String[] getSimpleTestNames(String courseKeyStr) 
            throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        String[] result = null;
        Objectify ofy = ObjectifyService.begin();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        InetCourse c = ofy.find(courseKey);

        if (c == null) {
            throw new MissingEntitiesException("Couln't find object for: " +
                    courseKey.getName());
        }

        Iterator<Key<InetSingleTestScores>> testScores =
                c.getTestScoresKeys().iterator();
        result = new String[c.getNumberOfTestScores()];
        int i = 0;

        while (testScores.hasNext())
        {
            Key testKey = testScores.next();
            result[i++] = InetDAO4CommonReads.getNameFromLastKeyPart(testKey);
        }
        return result;
    }

    @Override
    public String addTest(String courseKeyStr, String testName, Double maxScore,
            Double weight) throws DuplicateEntitiesException,
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        Objectify ofy = ObjectifyService.begin();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);

        //create test
        InetSingleTestScores ts = InetDAO4Creation.createTestScore(
                courseKey, testName, maxScore, weight);
        
        log.warning("CREATED test " + ts.getKey() + " --by user " + userAuditId);
        return ts.getTestName() + " (" + InetDAO4CommonReads.keyToPrettyString(ts.getCourseKey()) + ")";
    }

    @Override
    public ArrayList<TableMessage> getCourseScoresTable(String courseKeyStr) 
            throws MissingEntitiesException, LoginValidationException
    {
    	//TODO teacher eligibility ideally needs to be a lil more secure, we should check that courseKeyStr
    	//matches teacher courses. To avoid running teacher query, perharps stick info in a user session hashmap
    	//and just check whether the make contains courseKeyStr
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
        Objectify ofy = ObjectifyService.begin();
        InetGradesSingleCoursePopulator resultPopulator = new InetGradesSingleCoursePopulator();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        InetDAO4CommonReads.getCourseRosterInfo(courseKey, resultPopulator,
                resultPopulator, ofy);
        ArrayList<TableMessage> result = resultPopulator.getStudentData();
        log.info("Retrieved: " + (result.size() - 1) + " student scores for " +
                courseKey.getName());
        return result;
    }

    @Override
    public ArrayList<TableMessage> getCourseTestsTable(String courseKeyStr) 
            throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        Objectify ofy = ObjectifyService.begin();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        InetCourse course = ofy.find(courseKey);
        if(course == null)
        {
        	String msgFmt = "unable to generate test info for %s as its non existent";
        	log.severe(String.format(msgFmt, courseKey.toString()));
        	throw new MissingEntitiesException(String.format(msgFmt, courseKey.getName()));
        }
        InetCourseTestsPopulator resultPopulator = new InetCourseTestsPopulator(course);
        
        InetDAO4CommonReads.getCourseTestsInfo(courseKey, resultPopulator, ofy);
        ArrayList<TableMessage> result = resultPopulator.getTestData();
        log.info("Retrieved: " + (result.size() - 1) + " student scores for " +
                courseKey.getName());
        return result;
    }    

    @Override
    public ArrayList<TableMessage> getTestScoresTable(String testKeyStr) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        Objectify ofy = ObjectifyService.begin();
        Key<InetSingleTestScores> testKey = ofy.getFactory().stringToKey(testKeyStr);
        return getTestScoresTable(testKey, ofy);
    }
    
    @Override
    public void saveTestScores(String testKeyStr, HashMap<String, Double> testScores) 
    	throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
    	Objectify ofy = ObjectifyService.beginTransaction();
    	Key<InetSingleTestScores> testKey = ofy.getFactory().stringToKey(testKeyStr);
    	InetDAO4ComplexUpdates.updateTestScores(testKey, testScores, true, ofy);
    	String courseKeyStr = ofy.getFactory().keyToString(testKey.getParent());
    	TaskQueueHelper.scheduleUpdateCumulativeScores(courseKeyStr);
    	log.warning("UPDATED test scores " + testKey + " --by user " + userAuditId);
    }


    public static ArrayList<TableMessage> getTestScoresTable(Key<InetSingleTestScores> testKey, 
            Objectify ofy) throws MissingEntitiesException
    {    	
        InetGradesSingleTestPopulator resultPopulator = new InetGradesSingleTestPopulator();
        
        InetDAO4CommonReads.getTestRosterInfo(testKey, resultPopulator,
                resultPopulator, ofy);
        ArrayList<TableMessage> result = resultPopulator.getStudentData();
        log.info("Retrieved: " + (result.size() - 1) + " student scores for " +
                testKey.getName());
        return result;
    }

    @Override
    public HashMap<String, String> getTestKeys(String courseKeyStr) throws 
            MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        Objectify ofy = ObjectifyService.begin();
        Key<InetCourse> ck = ofy.getFactory().stringToKey(courseKeyStr);

        HashMap<String, String> result;

        InetCourse course = ofy.find(ck);
        if (course == null) {
            String exMsg = "unable to retrieve course obj for key: " + ck;
            log.severe(exMsg);
            throw new MissingEntitiesException(exMsg);
        }

        int numOfTests = course.getNumberOfTestScores();
        Iterator<Key<InetSingleTestScores>> itr =
                course.getTestScoresKeys().iterator();

        if (numOfTests == 0) {
            return (new HashMap<String, String>());
        }
        else
            result = new HashMap<String, String>(numOfTests);

        while (itr.hasNext()) {
            Key<InetSingleTestScores> i = itr.next();
            result.put(ofy.getFactory().keyToString(i),
                    InetDAO4CommonReads.getNameFromLastKeyPart(i));
        }
        return result;
    }

    private static ArrayList<TableMessage> getStudentScoreSummaryWithKeyOrLogin(String loginNameOrKeyStr,
            String levelKeyStr, boolean isKey) throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.begin();
        Key<InetStudent> studentKey;
        if(isKey)
            studentKey = ofy.getFactory().stringToKey(loginNameOrKeyStr);
        else
            studentKey = new Key(InetStudent.class, loginNameOrKeyStr);

        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        InetGradesStudentSummaryPopulator gradesPopulator =
                new InetGradesStudentSummaryPopulator();
        InetDAO4CommonReads.getCumulativeStudentScores(studentKey, levelKey,
                gradesPopulator, gradesPopulator, ofy);
        return gradesPopulator.getStudentData();
    }

    @Override
    public ArrayList<TableMessage> getStudentScoreSummary(String loginName, 
            String levelKeyStr) throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        return getStudentScoreSummaryWithKeyOrLogin(loginName, levelKeyStr, false);
    }

    @Override
    public ArrayList<TableMessage> getStudentScoreSummary(String levelKeyStr) throws
            MissingEntitiesException, LoginValidationException
    {
        String loginId = LoginPortal.getStudentID(this.getThreadLocalRequest());
        return getStudentScoreSummaryWithKeyOrLogin(loginId, levelKeyStr, false);
    }

    @Override
    public ArrayList<TableMessage> getStudentScoreDetails(String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException
    {
        String loginId = LoginPortal.getStudentID(this.getThreadLocalRequest());
        return getStudentScoreDetailsWithKeyOrLogin(loginId, levelKeyStr, false);
    }

    private static ArrayList<TableMessage> getStudentScoreDetailsWithKeyOrLogin
            (String loginNameOrKeyStr, String levelKeyStr, boolean isKey) throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.begin();
        Key<InetStudent> studentKey;
        if(isKey)
            studentKey = ofy.getFactory().stringToKey(loginNameOrKeyStr);
        else
            studentKey = new Key(InetStudent.class, loginNameOrKeyStr);
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        InetGradesStudentDetailPopulator gradesPopulator =
                new InetGradesStudentDetailPopulator(studentKey);
        InetDAO4CommonReads.getAllStudentScores(studentKey, levelKey, gradesPopulator, ofy);
        return gradesPopulator.getStudentData();
    }

    @Override
    public ArrayList<TableMessage> getStudentScoreDetails(String loginName, String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        return getStudentScoreDetailsWithKeyOrLogin(loginName, levelKeyStr, false);
    }

    @Override
    public ArrayList<TableMessage> getStudentScoreSummaryWithKey(String studentKeyStr, String levelKeyStr) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
        return getStudentScoreSummaryWithKeyOrLogin(studentKeyStr, levelKeyStr, true);
    }

    @Override
    public ArrayList<TableMessage> getStudentScoreDetailsWithKey(String studentKeyStr, String levelKeyStr) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());    	
        return getStudentScoreDetailsWithKeyOrLogin(studentKeyStr, levelKeyStr, true);
    }

    private static class InetGradesStudentSummaryPopulator implements
            InetObjectDataPopulator, InetPopulatorInitializer
    {
        ArrayList<TableMessage> data;
        Key<InetStudent> studentKey;
        Map<Key<InetCourse>, InetCourse> courseMap;
        InetGradeDescriptions gradeDesc;
        int totalUnits = 0;
        double totalPoints = 0.0;

        public InetGradesStudentSummaryPopulator() {
            data = new ArrayList<TableMessage>();
            courseMap = new HashMap<Key<InetCourse>, InetCourse>();
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] cumWrapper) {
            InetCumulativeScores cumulativeScores = (InetCumulativeScores) cumWrapper[0];
            Double score = cumulativeScores.getTestScore(studentKey);
            InetCourse course = courseMap.get(cumulativeScores.getKey().getParent());
            Integer units = null;
            Double points = null;
            if(course != null)
            {
            	units = course.getNumberOfUnits();
            	totalUnits += units;
            }
            if(score == null)
            {
                //TODO, need log handlers to alert support of cases like this
                //cuz if this happens, there's a bug somewhere in the code
                String msg = "student registered for course but course has no" +
                        "entry for the student";
                log.severe(msg);
                return;
            }
            String grade = "N/A";
            if(!cumulativeScores.isPublished())
            {
            	score = InetConstants.NOT_PUBLISHED_SCORE_DISPLAYED;
            	grade = InetConstants.NOT_PUBLISHED_SCORE_TEXT;
            }
            else
            {
	            if(gradeDesc != null)
	            {
	                grade = gradeDesc.getLetterGrade(score);
	                points = gradeDesc.getGpa(score) * units;
	                totalPoints += points;
	            }
            }
            TableMessage m = new TableMessage(2, 3, 0);
            m.setText(0,InetDAO4CommonReads.
                    getNameFromLastKeyPart(cumulativeScores.getCourseKey()));

            m.setText(1, grade);
            m.setNumber(0, score);
            m.setNumber(1, points);
            m.setNumber(2, units);
            data.add(m);
        }

        @Override
        public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
        {
            //initialize grading scheme
            gradeDesc = InetDAO4CommonReads.getSchool(ofy).getDefaultGradingScheme();

            InetStudent student = (InetStudent) dataContainer[0];
            for(int i = 1; i < dataContainer.length; i++)
            {
            	InetCourse c = (InetCourse) dataContainer[i];
            	courseMap.put(c.getKey(), c);
            }
            studentKey = student.getKey();
            TableMessageHeader header = new TableMessageHeader(5);
            header.setCaption(student.getFname() + " " + student.getLname() + " grade summary. ");
            header.setText(0, "Subject", TableMessageHeader.TableMessageContent.TEXT);
            header.setText(1, "Score" + InetConstants.PERCENT_SIGN, TableMessageHeader.TableMessageContent.NUMBER);
            header.setText(2, InetConstants.GRADE_HEADER, TableMessageHeader.TableMessageContent.TEXT);
            header.setText(3, "Points", TableMessageHeader.TableMessageContent.NUMBER);
            header.setText(4, "Units", TableMessageHeader.TableMessageContent.NUMBER);

            data.add(header);
        }

        ArrayList<TableMessage> getStudentData()
        {
        	if(data.size() > 1)
        	{
        		TableMessageHeader h = (TableMessageHeader) data.get(0);
        		String suffix = "Total Units: " + totalUnits;
        		if(totalUnits != 0 && totalPoints != 0.0)
        		{
        			double gpa = totalPoints / totalUnits;
        			suffix += ". Total Course Points: " + totalPoints + " Semester GPA: " + InetConstants.NUMBER_FORMAT.format(gpa);
        		}
        		h.setCaption(h.getCaption() + suffix);
        	}
            return data;
        }
    }

    private static class InetGradesStudentDetailPopulator implements
            InetObjectDataPopulator
    {
        ArrayList<TableMessage> data;
        Key<InetStudent> studentKey;
        InetGradeDescriptions gradeDesc;

        public InetGradesStudentDetailPopulator(Key<InetStudent> studentKey)
        {
            this.studentKey = studentKey;
            data = new ArrayList<TableMessage>();

            Objectify ofy = ObjectifyService.begin();
            gradeDesc = InetDAO4CommonReads.getSchool(ofy).getDefaultGradingScheme();
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] cumWrapper) {
            InetTestScores testScore = (InetTestScores) cumWrapper[0];
            Double score = testScore.getTestScore(studentKey);

            if(score == null)
            {
                //TODO, need log handlers to alert support of cases like this
                //cuz if this happens, there's a bug somewhere in the code
                String msg = "student registered for course but course has no" +
                        "entry for the student";
                log.severe(msg);
                return;
            }
            
            if(!testScore.isPublished())
            {
            	score = InetConstants.NOT_PUBLISHED_SCORE_DISPLAYED;
            }

            TableMessage newRecord;
            if(testScore instanceof InetSingleTestScores)
            {
                InetSingleTestScores singleTestScore = (InetSingleTestScores) testScore;
                newRecord = new TableMessage(4, 0, 0);
                newRecord.setText(0, testScore.getTestName());
                newRecord.setText(1, singleTestScore.isPublished()?String.valueOf(score):InetConstants.NOT_PUBLISHED_SCORE_TEXT);
                newRecord.setText(2, String.valueOf(testScore.getMaxScore()));
                newRecord.setText(3, String.valueOf(singleTestScore.getWeight()));
            }
            else
            {
                TableMessageHeader headerRecord = new TableMessageHeader(3);
                headerRecord.setText(0, InetDAO4CommonReads.
                        getNameFromLastKeyPart(testScore.getCourseKey()),
                        TableMessageHeader.TableMessageContent.TEXT);
                headerRecord.setText(1, String.valueOf(score),
                        TableMessageHeader.TableMessageContent.TEXT);
                
                String grade = "N/A";
                if(score.equals(InetConstants.NOT_PUBLISHED_SCORE_DISPLAYED))
                	grade = InetConstants.NOT_PUBLISHED_SCORE_TEXT;
                else
                {
		            
		            if(gradeDesc != null)
		                grade = gradeDesc.getLetterGrade(score);
                }
                
                headerRecord.setText(2, grade,
                        TableMessageHeader.TableMessageContent.TEXT);
                newRecord = headerRecord;
            }
            data.add(newRecord);
        }

        ArrayList<TableMessage> getStudentData()
        {
            return data;
        }
    }
    
    private static class InetGradesSingleCoursePopulator implements InetObjectDataPopulator, InetPopulatorInitializer
    {
        ArrayList<TableMessage> data;
        InetTestScores[] testScores;
        private InetGradeDescriptions gradeDesc;
        
        //indexes for static headers
        final static int LNAME_IDX = 0;
        final static int FNAME_IDX = 1;
        final static int LOGIN_IDX = 2;

        //start idx for generated test name headers
        final static int DYN_HEADER_START_IDX = 3;

        InetGradesSingleCoursePopulator() {
            data = new ArrayList<TableMessage>(InetConstants.DEFAULT_NUM_OF_STUDENTS_PER_COURSE);
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper)
        {
            InetStudent student = (InetStudent) studentWrapper[0];
            TableMessage m = new TableMessage(DYN_HEADER_START_IDX + 1, testScores.length, 0);
            
            m.setText(LNAME_IDX, student.getLname());
            m.setText(FNAME_IDX, student.getFname());
            m.setText(LOGIN_IDX, student.getKey().getName());
            String grade = "N/A";
            if(gradeDesc != null)
                grade = gradeDesc.getLetterGrade(testScores[testScores.length-1].
                        getTestScore(student.getKey()));
            m.setText(DYN_HEADER_START_IDX, grade);

            for(int i = 0; i < testScores.length; i++)
                m.setNumber(i, testScores[i].getTestScore(student.getKey()));

            data.add(m);
        }

        ArrayList<TableMessage> getStudentData() {
            return data;
        }

        @Override
        public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
                throws MissingEntitiesException
        {
            //intialize grade description
            gradeDesc = InetDAO4CommonReads.getSchool(ofy).getDefaultGradingScheme();

            //fetch test scores and their header names
            InetCourse course = (InetCourse) dataContainer[0];
            testScores = new InetTestScores[course.getNumberOfTestScores() + 1];
            TableMessageHeader m = new TableMessageHeader(DYN_HEADER_START_IDX +
                    testScores.length + 1); //header message
            m.setCaption(course.getCourseName());
            m.setText(LNAME_IDX, "Last Name",
                    TableMessageHeader.TableMessageContent.TEXT);
            m.setText(FNAME_IDX, "First Name",
                    TableMessageHeader.TableMessageContent.TEXT);
            m.setText(LOGIN_IDX, "Login Name",
                    TableMessageHeader.TableMessageContent.TEXT);

            int i = 0;
            int msgIndex = DYN_HEADER_START_IDX;

            //fetch test scores and their header names
            List<Key<? extends InetTestScores>> batchList = course.getAllTestKeys();
            Map<Key<InetTestScores>, InetTestScores> batchMap = ofy.get(batchList);

            if(batchMap.size() != batchList.size())
            {
                log.severe("unable to retrieve all tests for: " + course.getKey());
            }

            Iterator<Map.Entry<Key<InetTestScores>, InetTestScores>> scoreItr = batchMap.entrySet().iterator();
            while(scoreItr.hasNext())
            {
                Map.Entry<Key<InetTestScores>, InetTestScores> ts = scoreItr.next();
                log.info("Key is: " + ts.getKey() + " and val is: " + ts.getValue());
                testScores[i] = ts.getValue();
                m.setText(msgIndex, testScores[i].getTestName().equals(InetConstants.CUMULATIVE_TEST_NAME)?testScores[i].getTestName() + InetConstants.PERCENT_SIGN : testScores[i].getTestName(),
                        TableMessageHeader.TableMessageContent.NUMBER);
                i++;
                msgIndex++;
            }
            m.setText(msgIndex, InetConstants.GRADE_HEADER, TableMessageHeader.TableMessageContent.TEXT);
            data.add(m);
        }
    }

    private static class InetGradesSingleClassPopulator implements InetObjectDataPopulator, InetPopulatorInitializer
    {
        ArrayList<TableMessage> data;
        Map<Key<InetTestScores>, InetTestScores>  testScores;
        Collection<InetCourse> courseList;
        private InetGradeDescriptions gradeDesc;
        boolean cumulativeOnly;
        
        //indexes for static headers
        final static int LNAME_IDX = 0;
        final static int FNAME_IDX = 1;
        final static int LOGIN_IDX = 2;

        //start idx for generated test name headers
        final static int DYN_HEADER_START_IDX = 3;

        InetGradesSingleClassPopulator(boolean cumulativeOnly) {
        	this.cumulativeOnly = cumulativeOnly;
            data = new ArrayList<TableMessage>(InetConstants.DEFAULT_NUM_OF_STUDENTS_PER_COURSE);
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper)
        {
            InetStudent student = (InetStudent) studentWrapper[0];
            TableMessage m = new TableMessage(DYN_HEADER_START_IDX + courseList.size(), testScores.size(), 0);
            
            m.setText(LNAME_IDX, student.getLname());
            m.setText(FNAME_IDX, student.getFname());
            m.setText(LOGIN_IDX, student.getKey().getName());         
            int gradeIndex = DYN_HEADER_START_IDX;
            int scoreIndex = 0;
            for(InetCourse course : courseList)
            {
            	String grade = "";   
            	if(cumulativeOnly)
            	{
            		InetTestScores ts = testScores.get(course.getCumulativeScores());
            		Double score = ts.getTestScore(student.getKey());
                    m.setNumber(scoreIndex++, score);
                    if(gradeDesc != null && score !=null)
                    	grade = gradeDesc.getLetterGrade(score);
                    m.setText(gradeIndex++, grade);                    
            	}
            	else
            	{
            		List<Key<? extends InetTestScores>> tKeys = course.getAllTestKeys();
            		Double score = null;
            		for(Key<? extends InetTestScores> tk : tKeys)
            		{
            			InetTestScores ts = testScores.get(tk);
            			score = ts.getTestScore(student.getKey());
            			m.setNumber(scoreIndex++, score);
            		}
                    if(score !=null)
                    {
                    	if(gradeDesc == null)
                    		grade = "N/A";
                    	else
                    		grade = gradeDesc.getLetterGrade(score);
                    }
                    m.setText(gradeIndex++, grade);           
            	}            	
            }
            data.add(m);
        }

        ArrayList<TableMessage> getStudentData() {
            return data;
        }

        @Override
        public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
                throws MissingEntitiesException
        {
            //intialize grade description
            gradeDesc = InetDAO4CommonReads.getSchool(ofy).getDefaultGradingScheme();

            //fetch test scores and their header names
            InetLevel level = (InetLevel) dataContainer[0];
            courseList = InetDAO4CommonReads.getEntities(level.getCourseList(), ofy, true).values();
            ArrayList<Key<? extends InetTestScores>> testKeys = new ArrayList<Key<? extends InetTestScores>>();
            for(InetCourse course : courseList)
            {
            	if(cumulativeOnly)
            		testKeys.add(course.getCumulativeScores());
            	else
            		testKeys.addAll(course.getAllTestKeys());
            }
            
            testScores = InetDAO4CommonReads.getMixedEntities(testKeys, ofy, true);
            
            
            TableMessageHeader m = new TableMessageHeader(DYN_HEADER_START_IDX +
                    testScores.size() + courseList.size()); //header message
            
            m.setCaption(InetDAO4CommonReads.keyToPrettyString(level.getKey()));
            
            String groupName = InetConstants.BIO_GROUP_HEADER;
            m.setText(LNAME_IDX, InetConstants.GRD_LNAME_HEADER,
                    TableMessageHeader.TableMessageContent.TEXT);
            m.setGroupName(LNAME_IDX, groupName);
            m.setText(FNAME_IDX, InetConstants.GRD_FNAME_HEADER,
                    TableMessageHeader.TableMessageContent.TEXT);
            m.setGroupName(FNAME_IDX, groupName);
            m.setText(LOGIN_IDX, InetConstants.GRD_LOGIN_HEADER,
                    TableMessageHeader.TableMessageContent.TEXT);
            m.setGroupName(LOGIN_IDX, groupName);

            int msgIndex = DYN_HEADER_START_IDX;
            for(InetCourse course : courseList)
            {
            	groupName = course.getCourseName();
            	if(cumulativeOnly)
            	{
            		InetTestScores ts = testScores.get(course.getCumulativeScores());
                    m.setText(msgIndex, ts.getTestName() + InetConstants.PERCENT_SIGN,
                            TableMessageHeader.TableMessageContent.NUMBER);
                    m.setGroupName(msgIndex++, groupName);
                    m.setText(msgIndex, InetConstants.GRADE_HEADER, TableMessageHeader.TableMessageContent.TEXT);
                    m.setGroupName(msgIndex++, groupName);
            		
            	}
            	else
            	{
            		List<Key<? extends InetTestScores>> tKeys = course.getAllTestKeys();
            		for(Key<? extends InetTestScores> tk : tKeys)
            		{
            			InetTestScores ts = testScores.get(tk);
            			m.setText(msgIndex, ts.getTestName().equals(InetConstants.CUMULATIVE_TEST_NAME)?ts.getTestName() + InetConstants.PERCENT_SIGN : ts.getTestName(),
                                TableMessageHeader.TableMessageContent.NUMBER);
            			m.setGroupName(msgIndex++, groupName);
            		}
                    m.setText(msgIndex, InetConstants.GRADE_HEADER,
                            TableMessageHeader.TableMessageContent.TEXT);
                    m.setGroupName(msgIndex++, groupName);
            	}
            }
            data.add(m);
        }
    }
    
    public static class InetClassGradesStruct implements Serializable
    {
    	public InetClassGradesStruct()
    	{
            grades = new HashMap<String, HashMap<String, Double[]>>();
            testNames = new HashMap<String, String[]>(); 
            maxScores = new HashMap<String, Double[]>();
            testKeys = new HashMap<String, String[]>();
            classRoster = new HashMap<String, StudentNameStruct>();
    	}
    	
        public HashMap<String, HashMap<String, Double[]>> grades;
        public HashMap<String, String[]> testNames;
        public HashMap<String, String[]> testKeys;
        public HashMap<String, Double[]> maxScores;
        public HashMap<String, StudentNameStruct> classRoster;
    }
    
    public static class StudentNameStruct implements Serializable
    {
    	/**
		 * 
		 */
		public StudentNameStruct(String fname, String lname) {
			this.fname = fname;
			this.lname = lname;
		}
    	public String fname;
    	public String lname;
    }

    private static class InetGradesSingleClassHashPopulator implements InetObjectDataPopulator, 
    	InetPopulatorInitializer
    {
        private InetClassGradesStruct data;
        private Map<Key<InetTestScores>, InetTestScores>  testScores;
        private Collection<InetCourse> courseList;


        InetGradesSingleClassHashPopulator() {
        	data = new InetClassGradesStruct();
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper)
        {
            InetStudent student = (InetStudent) studentWrapper[0];
            data.classRoster.put(student.getKey().getName(), new StudentNameStruct(student.getFname(), student.getLname()));
            for(InetCourse course : courseList)
            {
        		Set<Key<InetSingleTestScores>> tKeys = course.getTestScoresKeys();
        		InetCumulativeScores cumScore = (InetCumulativeScores) testScores.get(course.getCumulativeScores());
        		if(cumScore.hasTestScore(student.getKey()))
        			data.grades.get(course.getCourseName().toLowerCase()).put(student.getKey().getName(), new Double[tKeys.size()]);
        		else
        			continue; //student not registered for this course
        		int scoreIndex = 0;
        		for(Key<InetSingleTestScores> tk : tKeys)
        		{
        			InetTestScores ts = testScores.get(tk);
        			if(data.testNames.get(course.getCourseName().toLowerCase())[scoreIndex].equals(ts.getTestName()))
        				data.grades.get(course.getCourseName().toLowerCase()).get(student.getKey().getName())[scoreIndex] = ts.getTestScore(student.getKey());
        			
        			else
        				throw new IllegalStateException("Test name iteration mismatch. Expected: " + 
        						data.testNames.get(course.getCourseName().toLowerCase())[scoreIndex] + " Received: " + ts.getTestName());
        			scoreIndex++;
        		}       	
            }
        }

        public InetClassGradesStruct getStudentData() {
            return data;
        }

        @Override
        public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
                throws MissingEntitiesException
        {
            //fetch test scores and their header names
            InetLevel level = (InetLevel) dataContainer[0];
            courseList = InetDAO4CommonReads.getEntities(level.getCourseList(), ofy, true).values();
            ArrayList<Key<? extends InetTestScores>> testKeys = new ArrayList<Key<? extends InetTestScores>>();
            for(InetCourse course : courseList)
            {
            	List<Key<? extends InetTestScores>> courseTestKeys = course.getAllTestKeys();
            	testKeys.addAll(courseTestKeys);
            	data.grades.put(course.getCourseName().toLowerCase(), new HashMap<String, Double[]>());
            	data.testNames.put(course.getCourseName().toLowerCase(), new String[courseTestKeys.size() - 1]);
            	data.testKeys.put(course.getCourseName().toLowerCase(), new String[courseTestKeys.size() - 1]);
            	data.maxScores.put(course.getCourseName().toLowerCase(), new Double[courseTestKeys.size() - 1]);
            }
            
            testScores = InetDAO4CommonReads.getMixedEntities(testKeys, ofy, true);
           
            for(InetCourse course : courseList)
            {
            	log.warning("Template Course: " + course.getKey());
        		Set<Key<InetSingleTestScores>> tKeys = course.getTestScoresKeys();
        		String[] testNames = data.testNames.get(course.getCourseName().toLowerCase());
        		Double[] maxScores = data.maxScores.get(course.getCourseName().toLowerCase());
        		String[] testKeysStr = data.testKeys.get(course.getCourseName().toLowerCase());
        		int i = 0;
        		for(Key<InetSingleTestScores> tk : tKeys)
        		{
        			log.warning("Template test: " + tk);
        			//String tnameCheck = tk.getName();
        			testNames[i] = testScores.get(tk).getTestName();
        			maxScores[i] = testScores.get(tk).getMaxScore();
        			testKeysStr[i] = ofy.getFactory().keyToString(tk);
        			//log.warning("COURSE: " + course.getCourseName() + " test name: " + 
        			//		testNames[i] + " control: " + tnameCheck + " max score: " + maxScores[i]);
        			//log.warning(tk.toString() + " AND " + testScores.get(tk).getKey());
        			++i;
        		}
        		data.testNames.put(course.getCourseName().toLowerCase(), testNames);
        		data.maxScores.put(course.getCourseName().toLowerCase(), maxScores);
            }
        }
    }    
    
    private static class InetGradesSingleTestPopulator extends
            InetGradesSingleCoursePopulator
    {
        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper)
        {
            InetStudent student = (InetStudent) studentWrapper[0];
            TableMessage m = new TableMessage(DYN_HEADER_START_IDX + 1, testScores.length, 0);

            m.setText(LNAME_IDX, student.getLname());
            m.setText(FNAME_IDX, student.getFname());
            m.setText(LOGIN_IDX, student.getKey().getName());
            m.setNumber(0, testScores[0].getTestScore(student.getKey()));
            data.add(m);
        }


        @Override
        public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
        {
            //fetch test scores and their header names
            InetSingleTestScores ts = (InetSingleTestScores) dataContainer[0];
            testScores = new InetSingleTestScores[1];
            testScores[0] = ts;
            //setup header message
            TableMessageHeader m = new TableMessageHeader(DYN_HEADER_START_IDX + 1);
            m.setCaption(ts.getTestName() + PanelServiceConstants.DATA_SEPERATOR + ts.getMaxScore());
            m.setText(LNAME_IDX, "Last Name", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(FNAME_IDX, "First Name", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(LOGIN_IDX, "Login Name", TableMessageHeader.TableMessageContent.TEXT);
            m.setText(DYN_HEADER_START_IDX, ts.getTestName(),TableMessageHeader.TableMessageContent.NUMBER);
            m.setEditable(DYN_HEADER_START_IDX, true);
            data.add(m);
        }  
    }
    
    private static class InetCourseTestsPopulator implements InetObjectDataPopulator
    {
    	ArrayList<TableMessage> data;
		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] testWrapper)
		{
		    InetSingleTestScores test = (InetSingleTestScores) testWrapper[0];
		    TableMessage m = new TableMessage(3, 0, 0);
		
		    m.setText(0, test.getTestName());
		    m.setText(1, String.valueOf(test.getMaxScore()));
		    m.setText(2, String.valueOf(test.getWeight()));
		    data.add(m);
		}		
		
		public InetCourseTestsPopulator(InetCourse course)
		{
			data = new ArrayList<TableMessage>(course.getNumberOfTestScores());
			String caption = InetDAO4CommonReads.keyToPrettyString(course.getKey());
		    //fetch test scores and their header names
		    //setup header message
		    TableMessageHeader m = new TableMessageHeader(3);
		    m.setCaption(caption);
		    m.setText(0, "Test Name", TableMessageHeader.TableMessageContent.TEXT);
		    m.setText(1, "Maximum Score", TableMessageHeader.TableMessageContent.TEXT);
		    m.setText(2, "Weight", TableMessageHeader.TableMessageContent.TEXT);
		    data.add(m);
		}
		
		public ArrayList<TableMessage> getTestData()
		{
			data.trimToSize();
			return data;
		}
	}
    
    private static class InetCourseTestsWeightAdder implements InetObjectDataPopulator
    {
    	double sum = 0.0;
    	
		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] testWrapper)
		{
		    InetSingleTestScores test = (InetSingleTestScores) testWrapper[0];		
		    sum += test.getWeight();
		}		
		
		public double getTotal()
		{
			return sum;
		}
	}    

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ScoreManager#deleteTest(java.lang.String)
	 */
	@Override
	public void deleteTest(String testKeyStr) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		InetSingleTestScores test = InetDAO4ComplexUpdates.deleteTest(testKeyStr);
		log.warning("DELETED test scores " + test.getKey() + " --by user " + userAuditId);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ScoreManager#totalTestWeight(java.lang.String)
	 */
	@Override
	public double totalTestWeight(String courseKeyStr) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
        Objectify ofy = ObjectifyService.begin();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        InetCourseTestsWeightAdder resultPopulator = new InetCourseTestsWeightAdder();
        
        InetDAO4CommonReads.getCourseTestsInfo(courseKey, resultPopulator, ofy);
        return resultPopulator.getTotal();
	}

	public static ArrayList<TableMessage> getClassScores(Key<InetLevel> levelKey, boolean cumulativeOnly, Objectify ofy) throws MissingEntitiesException
	{
        InetGradesSingleClassPopulator resultPopulator = new InetGradesSingleClassPopulator(cumulativeOnly);    
        InetDAO4CommonReads.getClassRosterInfo(levelKey, resultPopulator, ofy);
        return resultPopulator.getStudentData();		        
	}
	
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.ScoreManager#getClassScores(java.lang.String, boolean)
	 */
	@Override
	public ArrayList<TableMessage> getClassScores(String levelKeyStr,
			boolean cumulativeOnly) throws MissingEntitiesException, LoginValidationException
	{
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        return getClassScores(levelKey, cumulativeOnly, ofy);
	}
	
	public static InetClassGradesStruct getClassScores(Key<InetLevel> levelKey, Objectify ofy) throws MissingEntitiesException
	{
        InetGradesSingleClassHashPopulator resultPopulator = new InetGradesSingleClassHashPopulator();    
        InetDAO4CommonReads.getClassRosterInfo(levelKey, resultPopulator, ofy);
        return resultPopulator.getStudentData();		        
	}

	@Override
	public ArrayList<TableMessage> getClassScoresWithGPA(String levelKeyStr)
			throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        return GPAHelper.getClassGPAGrades(levelKey, ofy);	
    }

	@Override
	public ArrayList<TableMessage>[] getClassScoresWithCumulativeGPA(
			String[] levelStrs) throws MissingEntitiesException,
			LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel>[] levelKeys = new Key[levelStrs.length];
        for(int i = 0; i < levelKeys.length; i++)
        	levelKeys[i] = ObjectifyService.factory().stringToKey(levelStrs[i]);
        HttpSession sess = getThreadLocalRequest().getSession();
		return GPAHelper.getClassCummulativeGPAGrades(levelKeys, ofy, sess);
	}

	@Override
	public ArrayList<TableMessage>[] getCumulativeSummary(String[] levelStrs)
			throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel>[] levelKeys = new Key[levelStrs.length];
        for(int i = 0; i < levelKeys.length; i++)
        	levelKeys[i] = ObjectifyService.factory().stringToKey(levelStrs[i]);
        HttpSession sess = getThreadLocalRequest().getSession();
		return GPAHelper.getClassCummulativeSummary(levelKeys, ofy, sess);
	}

	@Override
	public ArrayList<TableMessage>[] getCumulativeStudentGrades() throws LoginValidationException, MissingEntitiesException {
        String loginId = LoginPortal.getStudentID(this.getThreadLocalRequest());
		return GPAHelper.getStudentCumulativeGrades(loginId, false, false);
	}

	@Override
	public ArrayList<TableMessage>[] getCumulativeStudentGrades(
			String studentId, boolean isKey) throws MissingEntitiesException, LoginValidationException {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN};
    	LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		return GPAHelper.getStudentCumulativeGrades(studentId, isKey, false);
	}	
}
