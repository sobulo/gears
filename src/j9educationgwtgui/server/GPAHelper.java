package j9educationgwtgui.server;

import j9educationentities.ApplicationParameters;
import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetConstants;
import j9educationentities.InetCourse;
import j9educationentities.InetCumulativeScores;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetGradeDescriptions;
import j9educationentities.InetLevel;
import j9educationentities.InetStudent;
import j9educationentities.InetTestScores;
import j9educationgwtgui.shared.DTOConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageFooter;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.TableMessageHeader.TableMessageContent;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class GPAHelper {
	private static final Logger log = Logger.getLogger(GPAHelper.class
			.getName());
	// TODO move this to grade description
	static double GPA_FAIL_THRESHOLD = 2.0;
	static String SESSION_MULTI_SHEET_PREFIX = "com.fertiletech.mxl";
	final static String PART_TIME_STR = "part-time";

	public static ArrayList<TableMessage> getClassGPAGrades(
			Key<InetLevel> levelKey, Objectify ofy)
			throws MissingEntitiesException {
		InetGradesSingleClassGPAPopulator resultPopulator = new InetGradesSingleClassGPAPopulator();
		InetDAO4CommonReads.getClassRosterInfo(levelKey, resultPopulator, ofy);
		return resultPopulator.getStudentData();
	}

	public static ArrayList<TableMessage>[] getClassCummulativeSummary(
			Key<InetLevel>[] levelKeys, Objectify ofy, HttpSession sess)
			throws MissingEntitiesException {
		PassFailPopulator resultPopulator = null;
		HashMap<Key<InetStudent>, CGPAStruct> priorGPA = null;
		boolean isGradLevel = false;
		boolean partTime = false;
		HashMap<Key<InetStudent>, Integer> savedCounts = null;
		for (int i = 0; i < levelKeys.length; i++) {
			partTime = (partTime || levelKeys[i].getName().contains(PART_TIME_STR));
			int cutoff = partTime? 4 : 2;			
			isGradLevel = (i > cutoff && (i == levelKeys.length - 1));
			Key<InetLevel> levKey = levelKeys[i];
			resultPopulator = new PassFailPopulator(priorGPA, isGradLevel, cutoff+2);
			resultPopulator.setSemesterCounts(savedCounts);
			InetDAO4CommonReads
					.getClassRosterInfo(levKey, resultPopulator, ofy);
			priorGPA = resultPopulator.cumulativeGPA;
			savedCounts = resultPopulator.semesterCounts;
		}

		// build summary
		TableMessage m = new TableMessage(1, 2, 0);

		int numOfPasses = resultPopulator.summary[PassFailPopulator.PASS_IDX]
				.size() - 1;
		int numOfRepeats = resultPopulator.summary[PassFailPopulator.REPEAT_IDX]
				.size() - 1;
		int numOfProbations = resultPopulator.summary[PassFailPopulator.PROBATION_IDX]
				.size() - 1;
		int numOfWithdrawals = resultPopulator.summary[PassFailPopulator.WITHDRAW_IDX]
				.size() - 1;
		int numOfExceptions = resultPopulator.summary[PassFailPopulator.EXCEPTION_IDX]
				.size() - 1;
		double total = resultPopulator.getStudentData().size() - 1;

		m.setText(0,
				"No. of candidates that passed entire examination and have no carry overs");
		m.setNumber(0, numOfPasses);
		m.setNumber(1, numOfPasses / total * 100.0);
		resultPopulator.summary[PassFailPopulator.SUMMARY_IDX].add(m);

		m = new TableMessage(1, 2, 0);
		m.setText(0, "No. of candidates with repeat/outstanding courses");
		m.setNumber(0, numOfRepeats);
		m.setNumber(1, numOfRepeats / total * 100.0);
		resultPopulator.summary[PassFailPopulator.SUMMARY_IDX].add(m);
		
		m = new TableMessage(1, 2, 0);
		m.setText(0, "No. of candidates on probation");
		m.setNumber(0, numOfProbations);
		m.setNumber(1, numOfProbations / total * 100.0);
		resultPopulator.summary[PassFailPopulator.SUMMARY_IDX].add(m);

		m = new TableMessage(1, 2, 0);
		m.setText(0, "No. of candidates to withdraw");
		m.setNumber(0, numOfWithdrawals);
		m.setNumber(1, numOfWithdrawals / total * 100.0);
		resultPopulator.summary[PassFailPopulator.SUMMARY_IDX].add(m);

		if (numOfExceptions > 0) {
			m = new TableMessage(1, 2, 0);
			m.setText(0, "Candidates with incomplete system records");
			m.setNumber(0, numOfExceptions);
			m.setNumber(1, numOfExceptions / total * 100.0);
			resultPopulator.summary[PassFailPopulator.SUMMARY_IDX].add(m);
		}

		m = new TableMessage(1, 2, 0);
		m.setText(0, "Total");
		m.setNumber(0, total);
		m.setNumber(1, 100);
		resultPopulator.summary[PassFailPopulator.SUMMARY_IDX].add(m);
		
		total = 0;
		for(int i = 0; i < resultPopulator.gradCounts.length; i++)
			total += resultPopulator.gradCounts[i];
		
		if(isGradLevel && total != 0)
		{
			int count = resultPopulator.gradCounts[PassFailPopulator.GRAD_DISTINTCTION];
			m = new TableMessage(1, 2, 0);
			m.setText(0,
					"No. of candidates eligible for graduation at Distinction level");
			m.setNumber(0, count);
			m.setNumber(1, count / total * 100.0);
			resultPopulator.summary[PassFailPopulator.GRAD_SUMMARY_IDX].add(m);

			count = resultPopulator.gradCounts[PassFailPopulator.GRAD_UPPER];
			m = new TableMessage(1, 2, 0);
			m.setText(0,
					"No. of candidates eligible for graduation at Upper Credit level");
			m.setNumber(0, count);
			m.setNumber(1, count / total * 100.0);
			resultPopulator.summary[PassFailPopulator.GRAD_SUMMARY_IDX].add(m);

			count = resultPopulator.gradCounts[PassFailPopulator.GRAD_LOWER];
			m = new TableMessage(1, 2, 0);
			m.setText(0,
					"No. of candidates eligible for graduation at Lower Credit level");
			m.setNumber(0, count);
			m.setNumber(1, count / total * 100.0);
			resultPopulator.summary[PassFailPopulator.GRAD_SUMMARY_IDX].add(m);

			count = resultPopulator.gradCounts[PassFailPopulator.GRAD_PASS];
			m = new TableMessage(1, 2, 0);
			m.setText(0,
					"No. of candidates eligible for graduation at Pass level");
			m.setNumber(0, count);
			m.setNumber(1, count / total * 100.0);
			resultPopulator.summary[PassFailPopulator.GRAD_SUMMARY_IDX].add(m);

			count = resultPopulator.gradCounts[PassFailPopulator.GRAD_FAIL];
			m = new TableMessage(1, 2, 0);
			m.setText(0,
					"No. of candidates not eligible for graduation");
			m.setNumber(0, count);
			m.setNumber(1, count / total * 100.0);
			resultPopulator.summary[PassFailPopulator.GRAD_SUMMARY_IDX].add(m);
			
			count = resultPopulator.gradCounts[PassFailPopulator.GRAD_EXCEPTION];
			if(count > 0)
			{
				m = new TableMessage(1, 2, 0);
				m.setText(0, "Incomplete GPA History. Unable to determine graduation eligibility");
				m.setNumber(0, count);
				m.setNumber(1, count / total * 100.0);
				resultPopulator.summary[PassFailPopulator.GRAD_SUMMARY_IDX].add(m);
			}
			
			m = new TableMessage(1, 2, 0);
			m.setText(0, "Total");
			m.setNumber(0, total);
			m.setNumber(1, 100);
			resultPopulator.summary[PassFailPopulator.GRAD_SUMMARY_IDX].add(m);
		}

		if (sess != null) {
			String timeStamp = String.valueOf(new Date().getTime());
			sess.setAttribute(SESSION_MULTI_SHEET_PREFIX + timeStamp,
					resultPopulator.summary);
			TableMessageHeader h = (TableMessageHeader) resultPopulator.summary[0]
					.get(0);
			h.setMessageId(timeStamp);
		}
		return resultPopulator.summary;
	}

	public static ArrayList<TableMessage>[] getClassCummulativeGPAGrades(
			Key<InetLevel>[] levelKeys, Objectify ofy, HttpSession sess)
			throws MissingEntitiesException {
		ArrayList<TableMessage>[] result = new ArrayList[levelKeys.length];
		CumulativeGPAPopulator resultPopulator;
		HashMap<Key<InetStudent>, CGPAStruct> priorGPA = null;
		boolean partTime = false;
		HashMap<Key<InetStudent>, Integer> savedCounts = null;
		for (int i = 0; i < levelKeys.length; i++) {
			partTime = (partTime || levelKeys[i].getName().contains(PART_TIME_STR));
			int cutoff = partTime? 4 : 2;			
			boolean isGradLevel = (i > cutoff && (i == levelKeys.length - 1));
			Key<InetLevel> levKey = levelKeys[i];
			resultPopulator = new CumulativeGPAPopulator(priorGPA, isGradLevel, cutoff+2);
			resultPopulator.setSemesterCounts(savedCounts);
			InetDAO4CommonReads
					.getClassRosterInfo(levKey, resultPopulator, ofy);
			result[i] = resultPopulator.getStudentData();
			priorGPA = resultPopulator.cumulativeGPA;
			savedCounts = resultPopulator.semesterCounts;
		}
		if (sess != null && result.length > 0 && result[0].size() > 0) {
			String timeStamp = String.valueOf(new Date().getTime());
			sess.setAttribute(SESSION_MULTI_SHEET_PREFIX + timeStamp, result);
			TableMessageHeader h = (TableMessageHeader) result[0].get(0);
			h.setMessageId(timeStamp);
		}
		return result;
	}

	public static ArrayList<TableMessage>[] getStudentCumulativeGrades(
			String loginNameOrKeyStr, boolean isKey, boolean printMode)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<InetStudent> studentKey;
		if (isKey)
			studentKey = ofy.getFactory().stringToKey(loginNameOrKeyStr);
		else
			studentKey = new Key(InetStudent.class, loginNameOrKeyStr);

		InetStudent student = ofy.get(studentKey);
		Key<InetLevel>[] levelKeys = InetLevel.sortLevels(student.getLevels());
		ArrayList<TableMessage>[] result = new ArrayList[levelKeys.length];
		CGPAStruct cgpa = new CGPAStruct();
		for(int i = 0; i < levelKeys.length; i++)
		{	
			boolean gradYear = (i > 2 && (i == levelKeys.length - 1));
			Key<InetLevel> levelKey = levelKeys[i];
			InetCumulativeStudentGradePopulator gradesPopulator = new InetCumulativeStudentGradePopulator();
			InetDAO4CommonReads.getCumulativeStudentScores(studentKey, levelKey,
					gradesPopulator, gradesPopulator, ofy);
			result[i] = gradesPopulator.getStudentData(cgpa, gradYear, printMode);
		}
		return result;
		
	}

	private static class InetCumulativeStudentGradePopulator implements
			InetObjectDataPopulator, InetPopulatorInitializer {
		ArrayList<TableMessage> data;
		Key<InetStudent> studentKey;
		Map<Key<InetCourse>, InetCourse> courseMap;
		InetGradeDescriptions gradeDesc;
		CGPAStruct gpaInfo;
		HashMap<String, String> courseCodeToNameMap;

		public InetCumulativeStudentGradePopulator() {
			data = new ArrayList<TableMessage>();
			courseMap = new HashMap<Key<InetCourse>, InetCourse>();
			gpaInfo = new CGPAStruct();
		}

		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] cumWrapper) {
			InetCumulativeScores cumulativeScores = (InetCumulativeScores) cumWrapper[0];
			Double score = cumulativeScores.getTestScore(studentKey);
			InetCourse course = courseMap.get(cumulativeScores.getKey()
					.getParent());
			Integer units = null;
			Double points = null;
			if (course != null) {
				units = course.getNumberOfUnits();
				gpaInfo.totalUnits += units;
			}
			if (score == null) {
				// TODO, need log handlers to alert support of cases like this
				// cuz if this happens, there's a bug somewhere in the code
				String msg = "student registered for course but course has no"
						+ "entry for the student";
				log.severe(msg);
				return;
			}
			String grade = "N/A";
			if (!cumulativeScores.isPublished()) {
				score = InetConstants.NOT_PUBLISHED_SCORE_DISPLAYED;
				grade = InetConstants.NOT_PUBLISHED_SCORE_TEXT;
			} else {
				if (gradeDesc != null) {
					double gpaVal =  gradeDesc.getGpa(score);
					grade = gradeDesc.getLetterGrade(score);
					points = gpaVal * units;
					gpaInfo.totalCoursePoints += points;					
				}
			}
			TableMessage m = new TableMessage(3, 3, 0);
			String courseCode = InetDAO4CommonReads
					.getNameFromLastKeyPart(cumulativeScores.getCourseKey()).toUpperCase();
			m.setText(0, courseCode);
			String courseName = courseCodeToNameMap.get(courseCode.toLowerCase().trim());
			if(courseName == null)
				courseName = "";

			m.setText(1, grade);
			m.setText(2, courseName.toUpperCase());
			m.setNumber(0, score);
			m.setNumber(1, points);
			m.setNumber(2, units);
			data.add(m);
		}

		@Override
		public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy) {
			//get course name map
	    	Key<ApplicationParameters> appKey = ApplicationParameters.getKey(DTOConstants.APP_PARAM_COURSE_NAMES);
	    	ApplicationParameters courseNameParams = ofy.get(appKey);
	    	courseCodeToNameMap = courseNameParams.getParams();
			// initialize grading scheme
			gradeDesc = InetDAO4CommonReads.getSchool(ofy)
					.getDefaultGradingScheme();

			InetStudent student = (InetStudent) dataContainer[0];
			String levelString = "No Data Found";
			for (int i = 1; i < dataContainer.length; i++) {
				InetCourse c = (InetCourse) dataContainer[i];
				courseMap.put(c.getKey(), c);
				levelString = InetDAO4CommonReads.keyToPrettyString(c.getInetLevelKey());
			}
			
			studentKey = student.getKey();
			TableMessageHeader header = new TableMessageHeader(6);

			header.setCaption(levelString + ". " + student.getFname() + " " + student.getLname()
					+ " grade summary (" + student.getLoginName().toUpperCase() + "). ");
			header.setText(0, "Course",
					TableMessageHeader.TableMessageContent.TEXT);
			header.setText(1, "Score" + InetConstants.PERCENT_SIGN,
					TableMessageHeader.TableMessageContent.NUMBER);
			header.setText(2, InetConstants.GRADE_HEADER,
					TableMessageHeader.TableMessageContent.TEXT);
			header.setText(3, "Points",
					TableMessageHeader.TableMessageContent.NUMBER);
			header.setText(4, "Units",
					TableMessageHeader.TableMessageContent.NUMBER);
			header.setText(5, "Title",
					TableMessageHeader.TableMessageContent.TEXT);

			data.add(header);
		}

		ArrayList<TableMessage> getStudentData(CGPAStruct cgpa, boolean isGradYear, boolean printMode) {
			if (data.size() > 1) {
				TableMessageHeader h = (TableMessageHeader) data.get(0);
				TableMessage sampleData = data.get(1);
				String suffix = "Total Units: " + gpaInfo.totalUnits;
				TableMessageFooter semFooter = new TableMessageFooter(sampleData.getNumberOfTextFields(), sampleData.getNumberOfDoubleFields(), sampleData.getNumberOfDateFields());
				TableMessageFooter cumFooter, gradFooter;
				cumFooter = gradFooter = null;
				semFooter.setText(0, "Semester") ;
				semFooter.setNumber(1,gpaInfo.totalCoursePoints);
				semFooter.setNumber(2, gpaInfo.totalUnits);
				if (gpaInfo.totalUnits != 0 && gpaInfo.totalCoursePoints != 0.0) {
					gpaInfo.cgpa  = gpaInfo.totalCoursePoints / gpaInfo.totalUnits;
					suffix += ". Total Course Points: " + gpaInfo.totalCoursePoints
							+ " Semester GPA: "
							+ InetConstants.NUMBER_FORMAT.format(gpaInfo.cgpa);
					semFooter.setText(2, "Semester GPA: " + roundTwoDecimals(gpaInfo.cgpa));
					if(isGradYear)
					{
						suffix += "<div style='background-color:#000084;color:white'>";
						String gradStr = "";
						if(gpaInfo.hasNoFailures() && cgpa.hasNoFailures())
						{
							double gradGPA = (cgpa.cgpa + gpaInfo.cgpa)/2;
							gradGPA = roundTwoDecimals(gradGPA);
							gradStr = " " + gradGPA + " (" + getGraduationInfo(gradGPA) + ")";
						}
						else
							gradStr += " (NOT ELIGIBLE FOR GRADUATION)";
						suffix += " Graduation GPA: " + gradStr + "</div>";
						gradFooter = new TableMessageFooter(sampleData.getNumberOfTextFields(), sampleData.getNumberOfDoubleFields(), sampleData.getNumberOfDateFields());
						gradFooter.setText(0, "Graduation");
						gradFooter.setText(2, "Graduation GPA: " + gradStr);
					}
					cgpa.totalCoursePoints += gpaInfo.totalCoursePoints;
					cgpa.totalUnits += gpaInfo.totalUnits;
					cgpa.cgpa = cgpa.totalCoursePoints / cgpa.totalUnits;
					suffix += " Cumulative GPA: " + InetConstants.NUMBER_FORMAT.format(cgpa.cgpa);
					cumFooter = new TableMessageFooter(sampleData.getNumberOfTextFields(), sampleData.getNumberOfDoubleFields(), sampleData.getNumberOfDateFields());
					cumFooter.setText(0, "Cumulative");
					cumFooter.setNumber(1, cgpa.totalCoursePoints);
					cumFooter.setNumber(2, cgpa.totalUnits);
					cumFooter.setText(2, "Cumulative GPA: " + roundTwoDecimals(cgpa.cgpa));
				}
				
				if(printMode)
				{
					data.add(semFooter);
					if(cumFooter != null)
						data.add(cumFooter);
					if(gradFooter!=null)
						data.add(gradFooter);
				}
				else
					h.setCaption(h.getCaption() + suffix);
			}
			
			return data;
		}
	}

	public static ArrayList<TableMessage>[] getClassCummulativeGPAGradesViaSession(
			HttpSession sess, String timeStamp) {
		Object o = sess.getAttribute(SESSION_MULTI_SHEET_PREFIX + timeStamp);
		if (o == null)
			return null;
		else
			return (ArrayList<TableMessage>[]) o;
	}

	private static class InetGradesSingleClassGPAPopulator implements
			InetObjectDataPopulator, InetPopulatorInitializer {
		// int count = -2;
		HashMap<Key<InetStudent>, CGPAStruct> currentCGPA;
		ArrayList<TableMessage> data;
		Map<Key<InetCumulativeScores>, InetCumulativeScores> testScores;
		Collection<InetCourse> courseList;
		private InetGradeDescriptions gradeDesc;
		int numOfTextFields;
		int numOfDoubleFields;
		int textOffset = 0;
		int doubleOffset = 0;
		// indexes for static headers
		final static int LNAME_IDX = 0;
		final static int FNAME_IDX = 1;
		final static int LOGIN_IDX = 2;

		// start idx for generated test name headers
		final static int DYN_HEADER_START_IDX = 3;

		InetGradesSingleClassGPAPopulator() {
			data = new ArrayList<TableMessage>(
					InetConstants.DEFAULT_NUM_OF_STUDENTS_PER_COURSE);
			currentCGPA = new HashMap<Key<InetStudent>, GPAHelper.CGPAStruct>();
		}

		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper) {
			InetStudent student = (InetStudent) studentWrapper[0];
			//
			TableMessage m = new TableMessage(numOfTextFields,
					numOfDoubleFields, 0);

			m.setText(LNAME_IDX, student.getLname());
			m.setText(FNAME_IDX, student.getFname());
			m.setText(LOGIN_IDX, student.getKey().getName());
			int gradeIndex = DYN_HEADER_START_IDX;
			int scoreIndex = 0;
			int totalNumberOfUnits = 0;
			double totalCoursePoints = 0;
			StringBuilder remarks = new StringBuilder();
			String lowestLetterGrade = gradeDesc.getLowestLetterGrade();
			CGPAStruct gpa = new CGPAStruct();
			for (InetCourse course : courseList) {
				String grade = "";
				InetCumulativeScores ts = (InetCumulativeScores) testScores
						.get(course.getCumulativeScores());
				Double score = ts.getTestScore(student.getKey());
				m.setNumber(scoreIndex++, score);
				if (gradeDesc != null && score != null) {
					totalNumberOfUnits += course.getNumberOfUnits();
					grade = gradeDesc.getLetterGrade(score);
					totalCoursePoints += gradeDesc.getGpa(score)
							* course.getNumberOfUnits();
					if (grade.equals(lowestLetterGrade)) {
						if (remarks.length() > 0)
							remarks.append(", ");
						remarks.append(course.getCourseName());
						if (gpa.failedCourses == null)
							gpa.failedCourses = new HashSet<String>();
						gpa.failedCourses.add(course.getCourseName()
								.toUpperCase().trim());
					}
				}
				m.setText(gradeIndex++, grade);
			}
			gpa.totalCoursePoints = totalCoursePoints;
			gpa.totalUnits = totalNumberOfUnits;
			gpa.cgpa = totalCoursePoints / totalNumberOfUnits;
			currentCGPA.put(student.getKey(), gpa);
			m.setNumber(scoreIndex++, gpa.totalCoursePoints);
			m.setNumber(scoreIndex++, gpa.totalUnits);
			m.setNumber(scoreIndex++, gpa.cgpa);
			if (remarks.length() > 0)
				remarks.append(" failed!");
			if (gpa.cgpa < GPA_FAIL_THRESHOLD)
				remarks.append("Low GPA");
			m.setText(gradeIndex++, remarks.toString());
			data.add(m);
			/*
			 * if(count++ < 2) { log.warning("Inside gpa populate: " + count);
			 * log.warning("Totla Units: " + totalNumberOfUnits + " Total CP: "
			 * + totalCoursePoints + " for " + student.getLname()); }
			 */

		}

		ArrayList<TableMessage> getStudentData() {
			return data;
		}

		@Override
		public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
				throws MissingEntitiesException {
			// intialize grade description
			gradeDesc = InetDAO4CommonReads.getSchool(ofy)
					.getDefaultGradingScheme();

			// fetch test scores and their header names
			InetLevel level = (InetLevel) dataContainer[0];
			courseList = InetDAO4CommonReads.getEntities(level.getCourseList(),
					ofy, true).values();
			numOfTextFields = DYN_HEADER_START_IDX + courseList.size() + 1
					+ textOffset;

			ArrayList<Key<InetCumulativeScores>> testKeys = new ArrayList<Key<InetCumulativeScores>>();
			for (InetCourse course : courseList)
				testKeys.add(course.getCumulativeScores());

			testScores = InetDAO4CommonReads.getEntities(testKeys, ofy, true);
			numOfDoubleFields = testScores.size() + 3 + doubleOffset;

			TableMessageHeader m = new TableMessageHeader(numOfTextFields
					+ numOfDoubleFields); // header message

			m.setCaption(InetDAO4CommonReads.keyToPrettyString(level.getKey()));

			String groupName = InetConstants.BIO_GROUP_HEADER;
			m.setText(LNAME_IDX, InetConstants.GRD_LNAME_HEADER,
					TableMessageHeader.TableMessageContent.TEXT);
			m.setGroupName(LNAME_IDX, groupName);
			m.setText(FNAME_IDX, InetConstants.GRD_FNAME_HEADER,
					TableMessageHeader.TableMessageContent.TEXT);
			m.setGroupName(FNAME_IDX, groupName);
			m.setText(LOGIN_IDX, InetConstants.GRD_LOGIN_HEADER,
					TableMessageHeader.TableMessageContent.TEXT);
			m.setGroupName(LOGIN_IDX, groupName);

			int msgIndex = DYN_HEADER_START_IDX;
			for (InetCourse course : courseList) {
				groupName = course.getCourseName();
				InetTestScores ts = testScores
						.get(course.getCumulativeScores());
				m.setText(msgIndex, ts.getTestName()
						+ InetConstants.PERCENT_SIGN,
						TableMessageHeader.TableMessageContent.NUMBER);
				m.setGroupName(msgIndex++, groupName);
				m.setText(msgIndex, InetConstants.GRADE_HEADER,
						TableMessageHeader.TableMessageContent.TEXT);
				m.setGroupName(msgIndex++, groupName);
			}
			groupName = "GPA Info";
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "TCP", TableMessageContent.NUMBER);
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "TNU", TableMessageContent.NUMBER);
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "GPA", TableMessageContent.NUMBER);
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "Remarks", TableMessageContent.TEXT);
			data.add(m);
		}
	}

	private static class CumulativeGPAPopulator extends
			InetGradesSingleClassGPAPopulator {
		HashMap<Key<InetStudent>, CGPAStruct> cumulativeGPA;
		HashMap<Key<InetStudent>, Integer> semesterCounts;
		final int gradThreshold;
		boolean cumulativeProvided = false;
		boolean isGradYear = false;

		public CumulativeGPAPopulator(
				HashMap<Key<InetStudent>, CGPAStruct> priorCGPA, boolean gradYear, int gradThreshold) {
			super();
			this.textOffset += 2;
			this.doubleOffset += 4;
			this.isGradYear = gradYear;
			semesterCounts = new HashMap<Key<InetStudent>, Integer>();
			this.gradThreshold = gradThreshold;
			if(gradYear)
			{
				textOffset++;
				doubleOffset++;
			}
			
			if (priorCGPA != null) {
				cumulativeProvided = true;
				this.cumulativeGPA = priorCGPA;
			} else
				this.cumulativeGPA = new HashMap<Key<InetStudent>, GPAHelper.CGPAStruct>();
		}
		
		void setSemesterCounts(HashMap<Key<InetStudent>, Integer> counts)
		{
			if(counts == null) return;
			semesterCounts = counts;
		}		

		@Override
		public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
				throws MissingEntitiesException {
			super.init(dataContainer, ofy);
			TableMessageHeader m = (TableMessageHeader) data
					.get(data.size() - 1);
			String groupName = "Cumulative GPA Info";
			int msgIndex = numOfTextFields + numOfDoubleFields - textOffset - doubleOffset;
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "C-TCP", TableMessageContent.NUMBER);
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "C-TNU", TableMessageContent.NUMBER);
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "C-GPA", TableMessageContent.NUMBER);
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "Remarks", TableMessageContent.TEXT);
			groupName = "Prior/Outstanding Info";
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "Old C-GPA", TableMessageContent.NUMBER);
			m.setGroupName(msgIndex, groupName);
			m.setText(msgIndex++, "Carry Over", TableMessageContent.TEXT);
			if(isGradYear)
			{
				groupName = "Graduation Point Average";
				m.setGroupName(msgIndex, groupName);
				m.setText(msgIndex++, "Gr-PA.", TableMessageContent.NUMBER);
				m.setGroupName(msgIndex, groupName);
				m.setText(msgIndex++, "Remarks", TableMessageContent.TEXT);							
			}
		}

		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper) {
			super.populateInfo(studentWrapper);
			InetStudent student = (InetStudent) studentWrapper[0];
			TableMessage m = data.get(data.size() - 1);
			CGPAStruct gpa = currentCGPA.get(student.getKey());
			Integer count = semesterCounts.get(student.getKey());
			if(count == null)
				count = 0;
			CGPAStruct cumGPA = new CGPAStruct();

			if (cumulativeProvided) {
				cumGPA = cumulativeGPA.get(student.getKey());

				if (cumGPA == null) {
					m.setText(numOfTextFields - 1, "Incomplete gpa history");
					return; // TODO?
				}
			} else
				cumulativeGPA.put(student.getKey(), cumGPA);
			semesterCounts.put(student.getKey(), ++count);
			
			/*
			 * if(count < 2) { log.warning("Cumulative Part 1: " + count);
			 * log.warning("Cum Total Units: " + cumGPA.totalUnits +
			 * " Total CP: " + cumGPA.totalCoursePoints + " for " +
			 * student.getLname()); log.warning("Cur Total Units: " +
			 * gpa.totalUnits + " Total CP: " + gpa.totalCoursePoints + " for "
			 * + student.getLname());
			 * 
			 * }
			 */
			Double oldGPA = cumGPA.totalUnits > 0 ? cumGPA.cgpa : null;
			cumGPA.totalUnits += gpa.totalUnits;
			cumGPA.totalCoursePoints += gpa.totalCoursePoints;
			cumGPA.cgpa = cumGPA.totalCoursePoints / cumGPA.totalUnits;
			StringBuilder remarks = new StringBuilder();
			StringBuilder missedRemarks = new StringBuilder();
			StringBuilder failedRemarks = new StringBuilder();
			StringBuilder finalRemarks = new StringBuilder();

			/*
			 * if(count < 2) { log.warning("Cumulative Part 2: " + count);
			 * log.warning("Cum Total Units: " + cumGPA.totalUnits +
			 * " Total CP: " + cumGPA.totalCoursePoints + " for " +
			 * student.getLname()); log.warning("Cur Total Units: " +
			 * gpa.totalUnits + " Total CP: " + gpa.totalCoursePoints + " for "
			 * + student.getLname());
			 * 
			 * }
			 */
			// first we check whether it's 2 consecutive semesters of cumulative
			// gpa being low
			if (oldGPA != null && oldGPA < GPA_FAIL_THRESHOLD
					&& cumGPA.cgpa < GPA_FAIL_THRESHOLD)
				finalRemarks.append("WITHDRAWAL! ");
			else if (cumGPA.cgpa < GPA_FAIL_THRESHOLD)
				finalRemarks.append("PROBATION! ");

			if (cumGPA.failedCourses == null) {
				if (gpa.failedCourses != null)
					cumGPA.failedCourses = new HashSet<String>();
			} else {
				HashSet<String> currentFailures = gpa.failedCourses;
				for (InetCourse course : courseList) {
					String courseName = course.getCourseName().toUpperCase()
							.trim();
					if (cumGPA.failedCourses.contains(courseName)) //
					{
						if (currentFailures == null
								|| !currentFailures.contains(courseName)) {
							if (student.getCourseKeys(course.getInetLevelKey())
									.contains(course.getKey())) {
								cumGPA.failedCourses.remove(courseName);
								if (remarks.length() > 0)
									remarks.append(", ");
								remarks.append(courseName);
							} else {
								if (missedRemarks.length() > 0)
									missedRemarks.append(", ");
								missedRemarks.append(courseName);
							}
						} else {
							if (failedRemarks.length() > 0)
								failedRemarks.append(", ");
							failedRemarks.append(courseName);
						}
					}
				}
			}

			if (remarks.length() > 0)
				finalRemarks.append(remarks.toString()).append(
						" retakes passed! ");
			if (missedRemarks.length() > 0)
				finalRemarks.append(missedRemarks.toString()).append(
						" failed before yet not retaken? ");
			if (failedRemarks.length() > 0)
				finalRemarks.append(failedRemarks.toString()).append(
						" failed AGAIN! ");

			StringBuilder outstandingCarryOver = new StringBuilder(
					" Outstanding: ");
			StringBuilder newCarryOver = new StringBuilder("Repeat: ");
			updateOutstandingInfo(cumGPA, gpa, outstandingCarryOver,
					newCarryOver);
			newCarryOver.append(outstandingCarryOver);
			int doubleOffset = isGradYear? 5: 4;
			int scoreIndex = numOfDoubleFields - doubleOffset;
			m.setNumber(scoreIndex++, cumGPA.totalCoursePoints);
			m.setNumber(scoreIndex++, cumGPA.totalUnits);
			m.setNumber(scoreIndex++, cumGPA.cgpa);
			m.setNumber(scoreIndex++, oldGPA);
			int offset = 0;
			if(isGradYear)
			{
				//log.warning(student.getKey().toString() + " GrCount: " + count);
				offset = 1;
				Double gradGPA = null;
				String comment = count < gradThreshold? "Delayed Graduation/Defered Semester":"Repeat/Outstanding courses";
				if(gpa.hasNoFailures() && cumGPA.hasNoFailures() && count >= gradThreshold)
				{
					gradGPA = (oldGPA + gpa.cgpa)/2;
					gradGPA = roundTwoDecimals(gradGPA);
					comment = getGraduationInfo(gradGPA);
				}
				m.setText(numOfTextFields - 1, comment);
				m.setNumber(scoreIndex++, gradGPA);
			}
			m.setText(numOfTextFields - 2 - offset, finalRemarks.toString());
			m.setText(numOfTextFields - 1 - offset, newCarryOver.toString());
			/*
			 * if(count < 2) { log.warning("Cumulative Part 3: " + count);
			 * log.warning("Cum Total Units: " + cumGPA.totalUnits +
			 * " Total CP: " + cumGPA.totalCoursePoints + " for " +
			 * student.getLname()); log.warning("Cur Total Units: " +
			 * gpa.totalUnits + " Total CP: " + gpa.totalCoursePoints + " for "
			 * + student.getLname());
			 * 
			 * }
			 */
		}
	}

	private static String getGraduationInfo(double gradGPA)
	{
		//double threshold = .00001;
		//gradGPA += threshold;
		if(gradGPA >= 3.5)
			return "Distinction";
		else if(gradGPA >= 3)
			return "Upper Credit";
		else if(gradGPA >= 2.5)
			return "Lower Credit";
		else if(gradGPA >= 2.0)
			return "Pass";
		else
			return "Repeat";
	}
	
	private static void updateOutstandingInfo(CGPAStruct cumGPA,
			CGPAStruct gpa, StringBuilder outstandingCarryOver,
			StringBuilder newCarryOver) {

		if (cumGPA.failedCourses != null) {
			int repeatCount = 0;

			if (gpa.failedCourses != null) {
				if (gpa.failedCourses.size() == 0)
					newCarryOver.append("none");
				else {
					for (String s : gpa.failedCourses)
						newCarryOver.append(s).append(", ");
					cumGPA.failedCourses.addAll(gpa.failedCourses);
				}
			} else
				newCarryOver.append("none");

			for (String s : cumGPA.failedCourses) {
				if (gpa.failedCourses == null || !gpa.failedCourses.contains(s))
					outstandingCarryOver.append(s).append(", ");
				else
					repeatCount++;
			}
			if (cumGPA.failedCourses.size() == 0)
				outstandingCarryOver.append("none");
			else if (cumGPA.failedCourses.size() == repeatCount)
				outstandingCarryOver.append("none"); // these will show up in
														// repeat bucket for
														// current semester and
														// outstanding bucket if
														// course not retaken
														// next semester (a
														// likely scenario since
														// most nigerian schools
														// offer courses 1ce a
														// year)

		} else {
			outstandingCarryOver.append("none");
			newCarryOver.append("none"); // if there were repeats, cumulative
											// failures would have been
											// initialized to a new hashset
											// earlier in the fn, ie wouldnt be
											// null
		}
	}

	// gpa summary report
	private static class PassFailPopulator extends
			InetGradesSingleClassGPAPopulator {
		HashMap<Key<InetStudent>, CGPAStruct> cumulativeGPA;
		HashMap<Key<InetStudent>, Integer> semesterCounts;
		final static int GRAD_SUMMARY_IDX = 6;
		final static int SUMMARY_IDX = 5;
		final static int PASS_IDX = 4;
		final static int REPEAT_IDX = 3;
		final static int PROBATION_IDX = 2;
		final static int WITHDRAW_IDX = 1;
		final static int EXCEPTION_IDX = 0;
		boolean isGradYear;

		boolean cumulativeProvided = false;
		ArrayList<TableMessage>[] summary;
		int[] gradCounts = new int[6];
		final static int GRAD_DISTINTCTION = 0;
		final static int GRAD_UPPER = 1;
		final static int GRAD_LOWER = 2;
		final static int GRAD_PASS = 3;
		final static int GRAD_FAIL = 4;
		final static int GRAD_EXCEPTION = 5;
		final int gradThreshold;

		public PassFailPopulator(HashMap<Key<InetStudent>, CGPAStruct> priorCGPA, boolean gradYear, int gradThreshold) {
			super();
			int numOfReports = gradYear? 7 : 6;
			summary = new ArrayList[numOfReports];
			this.gradThreshold = gradThreshold;
			semesterCounts = new HashMap<Key<InetStudent>, Integer>();
			this.isGradYear = gradYear;
			if (priorCGPA != null) {
				cumulativeProvided = true;
				this.cumulativeGPA = priorCGPA;
			} else
				this.cumulativeGPA = new HashMap<Key<InetStudent>, GPAHelper.CGPAStruct>();
		}
		
		void setSemesterCounts(HashMap<Key<InetStudent>, Integer> counts)
		{
			if(counts == null) return;
			semesterCounts = counts;
		}

		private void updateStudentInfo(TableMessage m, InetStudent s) {
			m.setText(0, s.getLoginName().toUpperCase());
			m.setText(1, s.getLname());
			m.setText(2, s.getFname());
		}

		@Override
		public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
				throws MissingEntitiesException {
			super.init(dataContainer, ofy);
			TableMessageHeader classHeader = (TableMessageHeader) data.get(0);
			
			String gradSuffix = "";
			if(isGradYear)
			{
				gradSuffix = "/Graduation";
				TableMessageHeader summary = new TableMessageHeader(3);
				summary.setText(0, "Description", TableMessageContent.TEXT);
				summary.setText(1, "Frequency", TableMessageContent.NUMBER);
				summary.setText(2, "Percentage", TableMessageContent.NUMBER);
				summary.setCaption("Graduation Summary (" + classHeader.getCaption() + ")");
				this.summary[GRAD_SUMMARY_IDX] = new ArrayList<TableMessage>();
				this.summary[GRAD_SUMMARY_IDX].add(summary);				
			}
			
			TableMessageHeader summary = new TableMessageHeader(3);
			summary.setText(0, "Description", TableMessageContent.TEXT);
			summary.setText(1, "Frequency", TableMessageContent.NUMBER);
			summary.setText(2, "Percentage", TableMessageContent.NUMBER);
			summary.setCaption("SUMMARY (" + classHeader.getCaption() + ")");
			this.summary[SUMMARY_IDX] = new ArrayList<TableMessage>();
			this.summary[SUMMARY_IDX].add(summary);

			TableMessageHeader exception = new TableMessageHeader(4);
			exception.setText(0, "Matric No", TableMessageContent.TEXT);
			exception.setText(1, "Surname", TableMessageContent.TEXT);
			exception.setText(2, "Other Names", TableMessageContent.TEXT);
			exception.setText(3, "System Message", TableMessageContent.TEXT);
			exception.setCaption("Exception List (" + classHeader.getCaption() + ")");
			this.summary[EXCEPTION_IDX] = new ArrayList<TableMessage>();
			this.summary[EXCEPTION_IDX].add(exception);

			
			TableMessageHeader pass = getSummaryBucketHeader("Pass" + gradSuffix + " List ("
					+ classHeader.getCaption() + ")");
			this.summary[PASS_IDX] = new ArrayList<TableMessage>();
			this.summary[PASS_IDX].add(pass);

			TableMessageHeader repeat = getSummaryBucketHeader("Repeat/Outstanding List ("
					+ classHeader.getCaption() + ")");
			this.summary[REPEAT_IDX] = new ArrayList<TableMessage>();
			this.summary[REPEAT_IDX].add(repeat);

			TableMessageHeader probation = getSummaryBucketHeader("Probation List ("
					+ classHeader.getCaption() + ")");
			this.summary[PROBATION_IDX] = new ArrayList<TableMessage>();
			this.summary[PROBATION_IDX].add(probation);

			TableMessageHeader withdrawal = getSummaryBucketHeader("Withdrawal List ("
					+ classHeader.getCaption() + ")");
			this.summary[WITHDRAW_IDX] = new ArrayList<TableMessage>();
			this.summary[WITHDRAW_IDX].add(withdrawal);
		}

		private TableMessageHeader getSummaryBucketHeader(String caption) {
			int numHeaders = isGradYear ? 10 : 7; //TODO 2 many magic numbers
			TableMessageHeader defaultHeader = new TableMessageHeader(numHeaders);
			defaultHeader.setText(0, "Matric No", TableMessageContent.TEXT);
			defaultHeader.setText(1, "Surname", TableMessageContent.TEXT);
			defaultHeader.setText(2, "Other Names", TableMessageContent.TEXT);
			defaultHeader.setText(3, "Repeat", TableMessageContent.TEXT);
			defaultHeader.setText(4, "Outstanding", TableMessageContent.TEXT);
			defaultHeader
					.setText(5, "Current CGPA", TableMessageContent.NUMBER);
			defaultHeader.setText(6, "Prior CGPA", TableMessageContent.NUMBER);
			
			if(isGradYear)
			{
				defaultHeader.setText(7, "GPA", TableMessageContent.NUMBER);
				defaultHeader.setText(8, "Gr-PA", TableMessageContent.NUMBER);
				defaultHeader.setText(9, "Gr. Remarks", TableMessageContent.TEXT);
			}
			
			defaultHeader.setCaption(caption);
			return defaultHeader;
		}

		private TableMessage getSummaryBucketRow(CGPAStruct cumGPA,
				CGPAStruct gpa, Double oldGPA, InetStudent student) {
			StringBuilder outstandingCarryOver = new StringBuilder();
			StringBuilder newCarryOver = new StringBuilder();
			updateOutstandingInfo(cumGPA, gpa, outstandingCarryOver,
					newCarryOver);

			// set row values
			int textFields = isGradYear?6:5;
			int numFields = isGradYear?4:2;
			TableMessage row = new TableMessage(textFields, numFields, 0);
			row.setNumber(0, cumGPA.cgpa);
			row.setNumber(1, oldGPA);
			updateStudentInfo(row, student);
			row.setText(3, newCarryOver.toString());
			row.setText(4, outstandingCarryOver.toString());

			Integer semesterCount = semesterCounts.get(student.getKey());
			if(isGradYear)
			{
				//log.warning(student.getKey().toString() + " threshold: " + gradThreshold + " " + " COUNT: " + semesterCount);
				Double gradGPA = null;
				String comment = semesterCount < gradThreshold ? "Graduation Delayed/Deferred Semester" : "Not eligible for graduation";
				if(gpa.hasNoFailures() && cumGPA.hasNoFailures() && semesterCount >= gradThreshold)
				{
					gradGPA = (oldGPA + gpa.cgpa)/2;
					gradGPA = roundTwoDecimals(gradGPA);
					comment = getGraduationInfo(gradGPA);
				}
				updateGradCounts(gradGPA==null?0:gradGPA);
				row.setText(5, comment);
				row.setNumber(2, gpa.cgpa);
				row.setNumber(3, gradGPA);
			}
			
			
			return row;
		}

		void updateGradCounts(double gradGPA)
		{
			//double threshold = .0001;
			//gradGPA += threshold;
			if(gradGPA >= 3.5)
				gradCounts[GRAD_DISTINTCTION]++;
			else if(gradGPA >= 3)
				gradCounts[GRAD_UPPER]++;
			else if(gradGPA >= 2.5)
				gradCounts[GRAD_LOWER]++;
			else if(gradGPA >= 2.0)
				gradCounts[GRAD_PASS]++;
			else
				gradCounts[GRAD_FAIL]++;			
		}
		
		
		@Override
		public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper) {
			super.populateInfo(studentWrapper);
			InetStudent student = (InetStudent) studentWrapper[0];
			Integer count = semesterCounts.get(student.getKey());
			if(count == null)
				count = 0;
			CGPAStruct gpa = currentCGPA.get(student.getKey());
			CGPAStruct cumGPA = new CGPAStruct();

			if (cumulativeProvided) {
				cumGPA = cumulativeGPA.get(student.getKey());

				if (cumGPA == null) {
					TableMessage m = new TableMessage(4, 0, 0);
					updateStudentInfo(m, student);
					m.setText(3,
							"Incomplete GPA history. Unable to generate summary recap for this student");
					summary[EXCEPTION_IDX].add(m);
					if(isGradYear) gradCounts[GRAD_EXCEPTION]++;
					return;
				}
			} else
				cumulativeGPA.put(student.getKey(), cumGPA);
			semesterCounts.put(student.getKey(), ++count);
			Double oldGPA = cumGPA.totalUnits > 0 ? cumGPA.cgpa : null;
			cumGPA.totalUnits += gpa.totalUnits;
			cumGPA.totalCoursePoints += gpa.totalCoursePoints;
			cumGPA.cgpa = cumGPA.totalCoursePoints / cumGPA.totalUnits;

			if (cumGPA.failedCourses == null) {
				if (gpa.failedCourses != null)
					cumGPA.failedCourses = new HashSet<String>();
			} else {
				HashSet<String> currentFailures = gpa.failedCourses;
				for (InetCourse course : courseList) {
					String courseName = course.getCourseName().toUpperCase()
							.trim();
					if (cumGPA.failedCourses.contains(courseName)) //
					{
						if (currentFailures == null
								|| !currentFailures.contains(courseName)) {
							if (student.getCourseKeys(course.getInetLevelKey())
									.contains(course.getKey())) {
								cumGPA.failedCourses.remove(courseName);
							}
						}
					}
				}
			}

			TableMessage row = getSummaryBucketRow(cumGPA, gpa, oldGPA, student);
			// classify the row into a particular bucket
			int msgIdx = -1;
			if (oldGPA != null && oldGPA < GPA_FAIL_THRESHOLD
					&& cumGPA.cgpa < GPA_FAIL_THRESHOLD)
				msgIdx = WITHDRAW_IDX;
			else if (cumGPA.cgpa < GPA_FAIL_THRESHOLD)
				msgIdx = PROBATION_IDX;
			else if ((cumGPA.failedCourses != null && cumGPA.failedCourses
					.size() > 0)
					|| (gpa.failedCourses != null && gpa.failedCourses.size() > 0))
				msgIdx = REPEAT_IDX;
			else
				msgIdx = PASS_IDX;
			summary[msgIdx].add(row);
			// if(msgIdx == PASS_IDX) return;
		}
	}

	private static class CGPAStruct {
		double cgpa = 0.0;
		int totalUnits = 0;
		double totalCoursePoints = 0.0;
		HashSet<String> failedCourses;
		boolean hasNoFailures(){ return failedCourses == null || failedCourses.size() == 0; }
	}
	
	public static Double roundTwoDecimals(Double val)
	{
		return (val == null? null : (Math.round(val * 100)/100.0));
	}
}