package j9educationgwtgui.client.classpanels;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.LevelStudentRosterBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import java.util.Collection;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DeleteRosterPanel extends Composite implements ClickHandler{
	
	@UiField
	LevelStudentRosterBox studentSelectBox;
	
	@UiField
	Button deleteStudents;
	
	@UiField
	Label status;
	
	DialogBox alertBox;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> deleteCallback =
            new AsyncCallback<Void>() {
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
            deleteStudents.setEnabled(true);
        }

        public void onSuccess(Void result) {
            WidgetHelper.setSuccessStatus(status, "Successfully removed students from class", true);
            deleteStudents.setEnabled(true);
        }
    };	
	
	private static DeleteRosterPanelUiBinder uiBinder = GWT
			.create(DeleteRosterPanelUiBinder.class);

	public DeleteRosterPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		studentSelectBox.setDisplayHeight(GUIConstants.DEFAULT_LB_VISIBLE_COUNT);
		deleteStudents.addClickHandler(this);
		alertBox = new DialogBox(true);
		alertBox.setHTML("<b>Confirm removal from class</b>");
        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);		
	}

	interface DeleteRosterPanelUiBinder extends
	UiBinder<Widget, DeleteRosterPanel> {
}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		HashMap<String, String> selectedStudents = studentSelectBox.getAllSelectedStudents();
		Collection<String> selectedNames = selectedStudents.values();
		String classKey = studentSelectBox.getSelectedClass();
		String className = studentSelectBox.getSelectedClassName();
		studentSelectBox.removeCacheEntry(classKey);
		
		if(selectedNames.size() == 0)
			return;
		
		VerticalPanel contentPanel = new VerticalPanel();
		
		String msg = "Are you sure you want to remove the " + selectedNames.size() + 
			" students below from class?";
		contentPanel.add(new Label(msg));
		HorizontalPanel buttonPanel = getAlertBoxButtonPanel(selectedStudents.keySet(), 
				classKey, className);
		contentPanel.add(buttonPanel);
		contentPanel.setCellHorizontalAlignment(buttonPanel, HorizontalPanel.ALIGN_CENTER);
		for(String s : selectedNames)
			contentPanel.add(new Label(s));
        
		//display popup box
		alertBox.clear();
		alertBox.setWidget(contentPanel);
        alertBox.show();
        alertBox.center();
        GWT.log(msg);
	}
	
	private HorizontalPanel getAlertBoxButtonPanel(final Collection<String> studKeys, final String classKey, 
			final String className)
	{
		HorizontalPanel panel = new HorizontalPanel();
		Button yes = new Button("  Yes  ");
		Button no = new Button("  No  ");
		yes.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
				WidgetHelper.setNormalStatus(status, "Attempting to remove " + studKeys.size() + " students from class "
						+ className);
				deleteStudents.setEnabled(false);
				studentSelectBox.getClassService().removeStudentsFromLevel(classKey, 
						studKeys.toArray(new String[studKeys.size()]), deleteCallback);
			}
		});
		
		no.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();				
			}
		});
		
		panel.add(yes);
		panel.add(new Label("     "));
		panel.add(no);
		return panel;
	}
	
}
