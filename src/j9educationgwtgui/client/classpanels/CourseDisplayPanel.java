/**
 * 
 */
package j9educationgwtgui.client.classpanels;

import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.custom.widgets.YesNoDialog;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class CourseDisplayPanel extends Composite implements ClickHandler, DoneLoadingWidget {
	@UiField
	Button display;
	
	@UiField
	Button update;
	
	@UiField
	ScrollPanel tableSlot;
	
	@UiField
	Label status;
	
	@UiField
	LevelBox classList;
	
	private PsTable table;
	
	HashMap<String, Double> cachedUnits = new HashMap<String, Double>();
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<ArrayList<TableMessage>> courseCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
        	WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
        	display.setEnabled(true);
        	update.setEnabled(true);
        }

        @Override
        public void onSuccess(ArrayList<TableMessage> result) {
            displayTable(result);
            WidgetHelper.setSuccessStatus(status, "loaded subjects successfully", false);
            display.setEnabled(true);
            updateCache(result);
            update.setEnabled(true);
        }
    };

    private void displayTable(ArrayList<TableMessage> result)
    {
        TableMessageHeader header = (TableMessageHeader) result.remove(0);

        if(table == null)
        	table = new PsTable(header);
        	
        table.showMessages(result, header.getCaption() + " subjects");
        tableSlot.add(table);
    }
    
    private void updateCache(ArrayList<TableMessage> result)
    {
    	for(TableMessage m : result)
    		cachedUnits.put(m.getMessageId(), m.getNumber(PanelServiceConstants.COURSE_UNIT_IDX));
    }
	
	private static CourseDisplayPanelUiBinder uiBinder = GWT
			.create(CourseDisplayPanelUiBinder.class);

	interface CourseDisplayPanelUiBinder extends
			UiBinder<Widget, CourseDisplayPanel> {
	}

	public CourseDisplayPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		tableSlot.setWidth("100%");
		display.addClickHandler(this);
		update.addClickHandler(this);
		classList.initData();
		update.setEnabled(true);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() == display)
		{
			tableSlot.clear();
			display.setEnabled(false);
			update.setEnabled(false);
			cachedUnits.clear();
			WidgetHelper.setNormalStatus(status, "Fetching subject list for " + 
					classList.getSelectedClassName() + ". Please wait ...");
			classList.getClassService().getCourseTable(classList.getSelectedClass(), 
					courseCallback);
		}
		else
		{
			TableMessage[] messages = table.getMessages();
			final HashMap<String, Integer> updateList = new HashMap<String, Integer>();
			for(TableMessage m : messages)
			{
				long courseUnit = Math.round(m.getNumber(PanelServiceConstants.COURSE_UNIT_IDX));
				long cachedUnit = Math.round(cachedUnits.get(m.getMessageId()));
				if(courseUnit != cachedUnit)
					updateList.put(m.getMessageId(), (int) courseUnit);
			}
			if(updateList.size() == 0)
				WidgetHelper.infoBox.show("Ignoring unit update request. No modifications found");
			else
			{
				final YesNoDialog confirmBox = new YesNoDialog("Confirm course unit update");
				confirmBox.setClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						tableSlot.clear();
						display.setEnabled(false);
						update.setEnabled(false);
						cachedUnits.clear();						
						classList.getClassService().updateCourseTable(updateList, courseCallback);
						confirmBox.hide();
					}
				});
				confirmBox.show("Update units for " + updateList.size() + " courses?");
			}
		}
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() 
	{
		return classList.doneLoading();
	}

}
