/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.classpanels;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.WebAppHelper;
import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.tables.TableMessageRowSelectionHandler;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class RosterDisplayPanel extends Composite implements 
        ClickHandler, TableMessageRowSelectionHandler, HasSelectionHandlers<TableMessage>, DoneLoadingWidget
{

    @UiField VerticalPanel contentPanel;
    @UiField HTML statusArea;
    @UiField Button display;
    @UiField LevelBox classList;
    
    private Anchor download;
    
    private PsTable studentTable;
    private String currentlyDisplayedClass;

    private static RosterDisplayPanelUiBinder uiBinder =
            GWT.create(RosterDisplayPanelUiBinder.class);

    private ClassRosterServiceAsync classService;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<ArrayList<TableMessage>> rosterCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
            WebAppHelper.checkForLoginError(caught);
            statusArea.setHTML("<b>Request failed: " +
                    caught.getMessage() + "</b>");
            display.setEnabled(true);
        }

        @Override
        public void onSuccess(ArrayList<TableMessage> result) {
            displayTable(result);
            statusArea.setText("Loaded roster successfully. " + (result.size() - 1) + " students loaded.");
            display.setEnabled(true);
        }
    };

    private void displayTable(ArrayList<TableMessage> result)
    {
        TableMessageHeader header = (TableMessageHeader) result.remove(0);
        //setup table if it hasn't been setup yet
        if(studentTable == null)
        {        	
            studentTable = new PsTable(header);            
            studentTable.addMessageSelectionHandler(this);
            
            download = new Anchor();
            download.setText("Download Table");
            contentPanel.add(download);
            contentPanel.setCellHorizontalAlignment(download, HorizontalPanel.ALIGN_RIGHT);
            contentPanel.add(studentTable);
        }
        studentTable.showMessages(result, header.getCaption() + " roster");
        contentPanel.setVisible(true);
        
        String url = GWT.getHostPageBaseURL() + "studentdownload?id="
        + getCurrentlyDisplayedClass() + "&type=" +
        PanelServiceConstants.DOWNLOAD_ROSTER;
        
        download.setHref(url);
    }
    
    private String getCurrentlyDisplayedClass()
    {
        if(currentlyDisplayedClass != null)
            return currentlyDisplayedClass;
        else 
            return classList.getSelectedClass();
    }

    @Override
    public void onRowSelected(TableMessage m) {
        TableMessage studGradeMessage = new TableMessage(3,0,0);
        studGradeMessage.setText(0, m.getText(0)); //login id
        studGradeMessage.setText(1, getCurrentlyDisplayedClass());
        studGradeMessage.setText(2, m.getText(1) + ", " + m.getText(2)); //lname, fname
        SelectionEvent.fire(this, studGradeMessage);
        /*scoreService.getStudentScoreSummary(m.getText(0), rosterCallback);
        status.setText("Fetching grades for: " + m.getText(1) + ", " +
                m.getText(2));
        display();*/
    }

    @Override
    public void onClick(ClickEvent event) {
        String key = classList.getSelectedClass();
        statusArea.setHTML("Fetching class roster, please wait ...");
        contentPanel.setVisible(false);
        display.setEnabled(false);
        classService.getClassRosterTable(key, rosterCallback);
        currentlyDisplayedClass = key;
    }

    @Override
    public HandlerRegistration addSelectionHandler(SelectionHandler<TableMessage> handler) {
        return addHandler(handler, SelectionEvent.getType());
    }


    interface RosterDisplayPanelUiBinder extends UiBinder<Widget, RosterDisplayPanel> {
    }

    public RosterDisplayPanel() {
        initWidget(uiBinder.createAndBindUi(this));
        classService = classList.getClassService();
        classList.initData();
        contentPanel.setWidth("100%");
        contentPanel.setVisible(false);
        //handlers
        display.addClickHandler(this);
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classList.doneLoading();
	}
}
