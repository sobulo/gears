/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.classpanels;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox.LB2LBAdd;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceConstants;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class ClassRosterMovePanel extends Composite implements ClickHandler, ValueChangeHandler<String>, 
	LB2LBAdd, DoneLoadingWidget
{

    @UiField LevelBox sourceClass;
    @UiField LevelBox targetClass;
    @UiField ListBox2ListBox studentBox;
    @UiField Button saveStudents;
    @UiField Label status;

    private final static int MAX_DISPLAY_ITEMS = 15;
    private SimpleDialog errorBox;
    
    private boolean academicTermInfoLoaded = false;
    private HashMap<String, String> academicTermInfo;

    private ClassRosterServiceAsync classService;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> classRosterCallBack =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
            studentBox.initValues(result);
            WidgetHelper.setSuccessStatus(status, "Showing " + result.size() + 
            		" students for source class " + sourceClass.getSelectedClassName(), false);
            saveStudents.setEnabled(true);
        }
    };

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> academicTermCallBack =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) 
        {
        	academicTermInfoLoaded = true;
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
            academicTermInfo = result;
            academicTermInfoLoaded = true;
        }
    };    
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> addStudentsCallBack =
            new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
            WidgetHelper.setErrorStatus(status, "Addition of student failed, error was: " +
                    caught.getMessage(), true);
            saveStudents.setEnabled(true);
        }

        @Override
        public void onSuccess(Void result) {
            WidgetHelper.setSuccessStatus(status, "Added students to class roster", true);
            saveStudents.setEnabled(true);
        }
    };

    private static ClassRosterMovePanelUiBinder uiBinder = GWT.create(ClassRosterMovePanelUiBinder.class);

    public ClassRosterMovePanel() {
        initWidget(uiBinder.createAndBindUi(this));
        classService = sourceClass.getClassService();
        sourceClass.addValueChangeHandler(this);
        targetClass.addValueChangeHandler(this);
        sourceClass.initData();
        targetClass.initData();
        saveStudents.addClickHandler(this);
        studentBox.initAppearance(MAX_DISPLAY_ITEMS, GUIConstants.DEFAULT_LB_WIDTH, "Source Class List", "Target Copy List");
        studentBox.setAddConstraint(this);
        sourceClass.setClassLabel("Select Source Class");
        targetClass.setClassLabel("Select Target Class");
        errorBox = new SimpleDialog("An error occurred");
        classService.getAcademicTermInfo4Levels(academicTermCallBack);
    }

    @Override
    public void onClick(ClickEvent event) {
        HashSet<String> chosen = studentBox.getValues();
        String targetKey = targetClass.getSelectedClass();
        String sourceKey = sourceClass.getSelectedClass();
        String className = targetClass.getSelectedClassName();
        
        //perform validation
        if(chosen.size() == 0)
        {
        	errorBox.show("You must add at least 1 student to target box!");
        	return;
        }
        
        if(academicTermInfo == null)
        {
        	errorBox.show("Still loading class information, if problem persists try reloading browser");
        	return;
        }
        
        if(!academicTermInfo.containsKey(targetKey) || !academicTermInfo.containsKey(sourceKey))
        {
        	errorBox.show("Class information not consistent, try refreshing browser");
        	return;
        }
        
        if(academicTermInfo.get(targetKey).equals(academicTermInfo.get(sourceKey)))
        {
        	String[] yearTerm = academicTermInfo.get(targetKey).split(PanelServiceConstants.DATA_SEPERATOR_REGEX);
        	errorBox.show("Copying from source belonging to the same academic year (" + 
        			yearTerm[0] + ") and term (" + yearTerm[1] + ") is not allowed");
        	return;
        }
        
        //send list to server to save
        String[] studentKeys = new String[chosen.size()];
        WidgetHelper.setNormalStatus(status, "Attempting to copy " + chosen.size() + 
        		" students to " + className + ". Please wait ...");
        saveStudents.setEnabled(false);
        classService.addStudentsToLevel(targetKey,
                chosen.toArray(studentKeys), addStudentsCallBack);
    }

    interface ClassRosterMovePanelUiBinder extends UiBinder<Widget, ClassRosterMovePanel> {
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		if(event.getSource() == sourceClass)
		{
			String selected = sourceClass.getSelectedClass();
			studentBox.initValues(null);
			saveStudents.setEnabled(false);
			WidgetHelper.setNormalStatus(status, "Fetching list of students for class: " + 
					sourceClass.getSelectedClassName() + ". Please wait");
	        classService.getClassRosterMap(selected,
	                classRosterCallBack);
		}
		else
		{
			studentBox.reset();
			WidgetHelper.setNormalStatus(status, "Select students to copy to " + 
					targetClass.getSelectedClassName());
		}
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.ListBox2ListBox.LB2LBAdd#rejectAdd()
	 */
	@Override
	public boolean rejectAdd() 
	{
        String targetKey = targetClass.getSelectedClass();
        String sourceKey = sourceClass.getSelectedClass();		
        if(academicTermInfo.get(targetKey).equals(academicTermInfo.get(sourceKey)))
        {
        	String[] yearTerm = academicTermInfo.get(targetKey).split(PanelServiceConstants.DATA_SEPERATOR_REGEX);
        	errorBox.show("Copying from source belonging to the same academic year (" + 
        			yearTerm[0] + ") and term (" + yearTerm[1] + ") is not allowed");
        	return true;
        }
		return false;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return academicTermInfoLoaded && sourceClass.doneLoading() && targetClass.doneLoading();
	}
}