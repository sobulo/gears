/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.classpanels;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.client.SchoolServiceAsync;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceConstants;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class AddCoursePanel extends Composite implements ValueChangeHandler<String>,
        ClickHandler, DoneLoadingWidget
{

    @UiField
    LevelBox classList;
    
    @UiField
    SuggestBox subjectName;
    
    @UiField
    ListBox courseDisplay;
    
    @UiField
    Button saveCourse;

    @UiField
    Label status;

    private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);
    private ClassRosterServiceAsync classService;
    private static AddCoursePanelUiBinder uiBinder = GWT.create(AddCoursePanelUiBinder.class);
    private SimpleDialog errorBox;

    private boolean initOptsLoaded = false;
    private boolean subjectsLoadedForAClass = false;
    
    final AsyncCallback<HashMap<String, HashSet<String>>> initOptionsCallback = new AsyncCallback<HashMap<String,HashSet<String>>>() {
        public void onSuccess(HashMap<String,HashSet<String>> result) {
            HashSet<String> initVals = result.get(PanelServiceConstants.DEFAULT_SUBJECTS_LABEL);
            if(initVals != null)
            {
                Iterator<String> i = initVals.iterator();
                MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) subjectName.getSuggestOracle();

                while(i.hasNext())
                    oracle.add(i.next());

                WidgetHelper.setSuccessStatus(status, "Populated suggestion box with " +
                        initVals.size() + " subjects", false);
            }
            initOptsLoaded = true;
        }

        public void onFailure(Throwable caught) 
        {
        	initOptsLoaded = true;
        	WidgetHelper.checkForLoginError(caught);
            String message = "Error getting initial subject values from server. " +
                    "Try refreshing browser to fix";
            WidgetHelper.setErrorStatus(status, message, false);
        }
    };

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String[]> courseNamesCallback =
            new AsyncCallback<String[]>() 
    {
        public void onFailure(Throwable caught) 
        {
        	subjectsLoadedForAClass = true;
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
        }

        public void onSuccess(String[] result) 
        {
            for(String val : result)
                courseDisplay.addItem(val.toUpperCase());
            WidgetHelper.setSuccessStatus(status, "Found: " + result.length + " subjects in " + 
            		classList.getSelectedClassName(), false);
            subjectsLoadedForAClass = true;
            saveCourse.setEnabled(true);
        }
    };

    final AsyncCallback<String> saveCourseCallback =
            new AsyncCallback<String>() {
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
            saveCourse.setEnabled(true);
        }

        public void onSuccess(String result) {
        	subjectName.setValue(null);
            courseDisplay.addItem(result.toUpperCase());
            WidgetHelper.setSuccessStatus(status, "Added " + result + " successfully", true);
            saveCourse.setEnabled(true);
        }
    };

    @Override
    public void onClick(ClickEvent event)
    {
        String courseName = subjectName.getText().trim();
        if(courseName.length() == 0)
        {
            errorBox.show("Please enter a value in subject field");
            return;
        }
        String classKey = classList.getSelectedClass();
        WidgetHelper.setNormalStatus(status, "Attempting to add subject " + courseName 
        		+ " to class " + classList.getSelectedClassName() );
        saveCourse.setEnabled(false);
        subjectName.setValue(null);
        classService.addCourse(courseName, classKey, saveCourseCallback);
    }

    interface AddCoursePanelUiBinder extends UiBinder<Widget, AddCoursePanel> {
    }

    public AddCoursePanel() {
        initWidget(uiBinder.createAndBindUi(this));
        classService = classList.getClassService();
        courseDisplay.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        //courseDisplay.addStyleName(GUIConstants.STYLE_CAPITALIZE);
        courseDisplay.addStyleName(GUIConstants.STYLE_GREP_BLUE);
        courseDisplay.setVisibleItemCount(GUIConstants.VISIBLE_CLASSES);
        String[] optionNames = {PanelServiceConstants.DEFAULT_SUBJECTS_LABEL};
        
        //populate subjects suggestion box
        schoolService.getInitialOptions(optionNames, initOptionsCallback);

        //register handlers
        saveCourse.addClickHandler(this);
        classList.addValueChangeHandler(this);
        classList.initData();
        
        errorBox = new SimpleDialog("An error occured");
    }


	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
        String classKey = classList.getSelectedClass();
        WidgetHelper.setNormalStatus(status, "Loading names of subjects already added to " + classList.getSelectedClassName());
        saveCourse.setEnabled(false);
        courseDisplay.clear();
        subjectName.setValue(null);
        classService.getSimpleCourseNames(classKey, courseNamesCallback);		
	}


	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return initOptsLoaded && subjectsLoadedForAClass;
	}
}
