/**
 * 
 */
package j9educationgwtgui.client.classpanels;

import j9educationgwtgui.client.custom.widgets.LevelAndCourseBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class DeleteCoursePanel extends Composite implements ClickHandler {
	
	@UiField
	Button deleteCourse;
	
	@UiField
	Label status;
	
	@UiField
	LevelAndCourseBox courseList;
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> deleteCallback =
            new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
            deleteCourse.setEnabled(true);
        }

        @Override
        public void onSuccess(Void result)
        {
        	WidgetHelper.setSuccessStatus(status, "Successfully deleted subject", true);
        	deleteCourse.setEnabled(true);
        }
    };	

	private static DeleteCoursePanelUiBinder uiBinder = GWT
			.create(DeleteCoursePanelUiBinder.class);

	interface DeleteCoursePanelUiBinder extends
			UiBinder<Widget, DeleteCoursePanel> {
	}

	public DeleteCoursePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		courseList.initData();
		deleteCourse.addClickHandler(this);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		deleteCourse.setEnabled(false);
		WidgetHelper.setNormalStatus(status, "Attempting to delete subject: " + 
				courseList.getCourseName() + " (" + courseList.getClassName() + ")");
		courseList.removeCachEntry(courseList.getClassValue() + ". Please wait");
		courseList.getClassService().deleteCourse(courseList.getCourseValue(), deleteCallback);	
	}

}
