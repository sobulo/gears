package j9educationgwtgui.client.classpanels;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class CourseCopyPanel extends Composite implements ValueChangeHandler<String>, ClickHandler, DoneLoadingWidget{
	
    @UiField LevelBox sourceClass;
    @UiField LevelBox targetClass;
    @UiField ListBox2ListBox courseBox;
    @UiField Button saveCourses;
    @UiField Label status;
    @UiField CheckBox copyStudents;

    private final static int MAX_DISPLAY_ITEMS = 10;
    private SimpleDialog errorBox;
    private SimpleDialog infoBox;

    private ClassRosterServiceAsync classService;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> courseInfoCallBack =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	HashMap<String, String> upperCaseResult = new HashMap<String, String>(result.size());
        	for(String key : result.keySet())
        		upperCaseResult.put(key.toUpperCase(), result.get(key));
        		
        	
            courseBox.initValues(upperCaseResult);
            WidgetHelper.setSuccessStatus(status, "Showing " + result.size() + 
            		" subjects for source class " + sourceClass.getSelectedClassName(), false);
            saveCourses.setEnabled(true);
        }
    };    
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> addCoursesCallBack =
            new AsyncCallback<String>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Subject copy failed, error was: " +
                    caught.getMessage(), true);
            saveCourses.setEnabled(true);
        }

        @Override
        public void onSuccess(String result) {
            WidgetHelper.setSuccessStatus(status, result, true);
            saveCourses.setEnabled(true);
        }
    };
	private static CourseCopyPanelUiBinder uiBinder = GWT
			.create(CourseCopyPanelUiBinder.class);

	interface CourseCopyPanelUiBinder extends UiBinder<Widget, CourseCopyPanel> {
	}

	public CourseCopyPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		classService = sourceClass.getClassService();
        sourceClass.addValueChangeHandler(this);
        targetClass.addValueChangeHandler(this);
        sourceClass.initData();
        targetClass.initData();
        saveCourses.addClickHandler(this);
        copyStudents.addClickHandler(this);
        courseBox.initAppearance(MAX_DISPLAY_ITEMS, GUIConstants.DEFAULT_LB_WIDTH, "Source Class List", "Target Copy List");
        //studentBox.setAddConstraint(this); //might be useful if init is changed to take target into account, this fn would then stop same name course but diff key from being added
        sourceClass.setClassLabel("Select Source Class");
        targetClass.setClassLabel("Select Target Class");
        errorBox = new SimpleDialog("An error occurred");
        infoBox = new SimpleDialog("Warning!");
        //courseBox.addStyleName(GUIConstants.STYLE_CAPITALIZE);
        courseBox.addStyleName(GUIConstants.STYLE_GREP_BLUE);
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
        String selected = event.getValue();
		String selectedName = "your last choice";
		if(event.getSource() == sourceClass)
		{
			if(selected.equals(sourceClass.getSelectedClass()))
				selectedName = sourceClass.getSelectedClassName();
			WidgetHelper.setNormalStatus(status, "Fetching subject list for: " + selectedName + 
					". Please wait ...");
			courseBox.initValues(null);
			saveCourses.setEnabled(false);
			classService.getCourseKeys(selected, courseInfoCallBack);
		}
		else if(event.getSource() == targetClass)
		{
			if(selected.equals(targetClass.getSelectedClass()))
					selectedName = targetClass.getSelectedClassName();
			WidgetHelper.setNormalStatus(status, "Add subjects to the target list then click copy subjects button to copy to: " + selectedName);
			courseBox.reset();
		}
	}
	
    @Override
    public void onClick(ClickEvent event) {
    	if(event.getSource() == copyStudents)
    	{
    		if(copyStudents.getValue())
    		{	
	    		infoBox.show("By checking 'copy students' option, you're requesting that in addition to copying over the selected subjects, " +
	    				"the students registered for those source subjects will also be registered for the newly created copies. Provided those students have" +
	    				" already been registered for the target class");
    		}
    		return;
    	}
        HashSet<String> chosen = courseBox.getValues();
        String targetKey = targetClass.getSelectedClass();
        String sourceKey = sourceClass.getSelectedClass();
        String targetName = targetClass.getSelectedClassName();
        
        //perform validation
        if(chosen.size() == 0)
        {
        	errorBox.show("You must add at least 1 subject to target box!");
        	return;
        }
        
        if(targetKey.equals(sourceKey))
        {
        	errorBox.show("Source and Target are the same, ignoring copy request");
        	return;
        }
        
        //send list to server to save
        String[] courseKeys = new String[chosen.size()];
        courseKeys = chosen.toArray(courseKeys);
        String appendMsg = copyStudents.getValue()?". Subject rosters will also be copied over.": ""; 
        WidgetHelper.setNormalStatus(status, "Attempting to copy " + courseKeys.length + " subjects to class " + targetName + appendMsg);
        saveCourses.setEnabled(false);
        classService.copyCourses(courseKeys, targetKey, copyStudents.getValue(), addCoursesCallBack);
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() 
	{
		return sourceClass.doneLoading() && targetClass.doneLoading();
	}	

}
