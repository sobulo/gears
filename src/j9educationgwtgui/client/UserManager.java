/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 *
 * @author Administrator
 */
@RemoteServiceRelativePath("restricted/usermanager")
public interface UserManager extends RemoteService {
    public HashMap<String, String> getStudentLevels()
            throws LoginValidationException, MissingEntitiesException;

    public HashMap<String, String> getStudentLevels(String studKeyStr)
            throws MissingEntitiesException, LoginValidationException;

    public HashMap<String, String> getGuardianChildren()
            throws LoginValidationException;
    
    public TableMessage getStudentInfo(String studKeyStr) throws MissingEntitiesException, LoginValidationException;
    
    public TableMessage getStudentInfo() 
    		throws MissingEntitiesException, LoginValidationException;
    
    public HashMap<String, String> getAllStudentIDs(boolean useLogins) throws LoginValidationException;

    public HashMap<String, String> getAllGuardianIDs(boolean useLogins) throws LoginValidationException;
    
    public HashMap<String, String> getAllTeacherIDs() throws LoginValidationException;
    
    public TableMessage getUserInfo(String loginName, PanelServiceLoginRoles role) 
    	throws MissingEntitiesException, LoginValidationException;

    public String getDetailedUserInfo(String loginName, PanelServiceLoginRoles role) 
	throws MissingEntitiesException, LoginValidationException;    
    
    public TableMessage getUserInfo() 
    		throws MissingEntitiesException, LoginValidationException;
    
    public void updateStudent(String fname, String lname, Date dob, String email, 
    		HashSet<String> phoneNums, String studKey) throws MissingEntitiesException, LoginValidationException;
    
    public void updateStudent(String fname, String lname, Date dob, String email, 
    		HashSet<String> phoneNums) throws MissingEntitiesException, LoginValidationException;
    
    public void updateUser(String fname, String lname, String email, 
    		HashSet<String> phoneNums, String loginName, PanelServiceLoginRoles role) 
    	throws MissingEntitiesException, LoginValidationException; 

    public void updateUser(String fname, String lname, String email, 
    		HashSet<String> phoneNums) throws MissingEntitiesException, LoginValidationException;    
    
    public HashMap<String, String> getGuardianChildren(String guardKeyStr) throws LoginValidationException;
    
    public HashMap<String, String> getStudentParents(String studKeyStr) 
    	throws MissingEntitiesException, LoginValidationException;
    
    public void addGuardianToStudent(String guardKeyStr, String studKeyStr) 
    	throws MissingEntitiesException, DuplicateEntitiesException, LoginValidationException;
    
    public String resetPassword(String userKeyStr, PanelServiceLoginRoles role) 
    	throws MissingEntitiesException, LoginValidationException;
    
    public String changePassword(String oldPassword, String newPassword) 
    	throws MissingEntitiesException, LoginValidationException;
    
    public String deleteStudent(String studKeyStr) 
    	throws MissingEntitiesException, ManualVerificationException, LoginValidationException;
    
    public String deleteGuardian(String loginName) 
    	throws MissingEntitiesException, LoginValidationException;
    
    public String[] getHoldTypes() throws LoginValidationException;
    
    public String createStudentHold(String levelKeyStr, String studKeyStr, String holdType, 
    		String message, String note) throws MissingEntitiesException, DuplicateEntitiesException, LoginValidationException;
    
    public String editStudentHold(String holdKeyStr, String holdType, String message, String note, boolean resolutionStatus) 
    	throws MissingEntitiesException, LoginValidationException;
    
    public ArrayList<TableMessage> getStudentHolds(String levelKeyStr, boolean unresolvedOnly) 
    	throws MissingEntitiesException, LoginValidationException;
    
	public String createUser(String fname, String lname,
			String email, HashSet<String> phoneNums, String loginName, String password, PanelServiceLoginRoles role)
			throws LoginValidationException, DuplicateEntitiesException;
	
	public HashMap<String, String> getCourseTeacherMappings(String levelKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	public String setCourseTeacherMapping(String courseKeyStr, String teacherKeyStr, 
			boolean isAdd) throws MissingEntitiesException, DuplicateEntitiesException, LoginValidationException;
	
	public HashMap<String, String> getTeacherCourses() throws LoginValidationException;
}


