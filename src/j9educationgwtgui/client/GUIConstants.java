/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class GUIConstants {
    public static final int MIN_ACADEMIC_YEAR = 2000;
    public static final int OFFSET_ACADEMIC_YEAR = 1;

    //gwt doesn't support Calendar class, base yr below used for deprecated date fns;
    public static final int BASE_YEAR = 1900;
    public static final int DOB_YEAR_OFFSET = 14;

    public static final int VISIBLE_YEARS = 3;
    public static final int VISIBLE_TERMS = 3;
    public static final int VISIBLE_CLASSES = 6;
    public static final int VISIBLE_SUB_CLASSES = 5;

    
    public final static String TERM_INPUT_LABEL = "Term:";
    public final static String CLASS_NAME_INPUT_LABEL = "Class Name:";
    public final static String SUB_CLASS_NAME_INPUT_LABEL = "Sub-Class Name:";
    public final static String PHONE_NUMBER_INPUT_LABEL = "Phone Number";
    public final static int DEFAULT_PHONE_PER_USER = 3;
    

    public final static String DEFAULT_LB_WIDTH = "275px";
    public final static int DEFAULT_LB_VISIBLE_COUNT = 10;
    
    public final static String DEFAULT_STACK_WIDTH = "100%";
    public final static String DEFAULT_STACK_HEIGHT = "100%";  
    public final static Double DEFAULT_STACK_HEADER = 30.0;
    
    public final static String STYLE_PS_TABLE_CAPTION = "scrollTableCaption";
    public final static String STYLE_PS_TABLE_SPACER = "scrollTableSpacer";;
    public final static String STYLE_STATUS_SUCCESS = "statusSuccessIndicator";
    public final static String STYLE_STATUS_BOX = "statusBox";
    public final static String STYLE_STATUS_FAIL = "statusErrorIndicator";
    public final static String STYLE_STND_TABLE_HEADER = "standardTableHeader";
    public final static String STYLE_STND_TABLE = "standardTable";
    public final static String STYLE_HORIZ_PRIM_CLR = "horizontalPrimaryColor";
    public final static String STYLE_HORIZ_SCND_CLR = "horizontalSecondaryColor";
    public final static String STYLE_HORIZ_HEADER = "horizontalHeader";
    public final static String STYLE_FORM_SHIFT = "formShiftRight";
    public final static String STYLE_FORM_LABEL = "formLabel";
    public final static String STYLE_PADDED_BORDER = "themePaddedBorder";
    public final static String SELECTED_STACK_COLOR = "grepGreen";
    public final static String STYLE_CAPITALIZE = "capitalizeText";
    public final static String STYLE_GREP_GREEN = "grepGreen";
    public final static String STYLE_GREP_BLUE = "grepBlue";
    public final static String STYLE_GREP_RED = "grepRed";
    
    //Images Folder
    public final static String MODULE_IMAGE_FOLDER = GWT.getModuleBaseURL() + "images/";
    public final static String SCHOOL_IMAGE_FOLDER = MODULE_IMAGE_FOLDER + "school/";
    public final static String IMG_DEFAULT_EXT = ".gif"; //TODO, upload school logs to blob-store once billing enabled
    public final static String[] GREP_LOGOS = {"greplogosmall.jpg", "appengine.gif"}; 
    
    public final static DefaultFormat DEFAULT_DATEBOX_FORMAT = new DateBox.DefaultFormat(DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT));
	public final static NumberFormat DEFAULT_NUMBER_FORMAT = NumberFormat.getFormat("#,###,###,##0.00");
	public final static DateTimeFormat DEFAULT_DATE_FORMAT = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM);    
}
