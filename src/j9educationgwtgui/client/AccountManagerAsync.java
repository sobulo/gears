/**
 * 
 */
package j9educationgwtgui.client;

import j9educationgwtgui.shared.BillDescriptionItem;
import j9educationgwtgui.shared.TableMessage;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface AccountManagerAsync {

	void createBillTemplate(String name, String year, String term,
			Date dueDate, LinkedHashSet<BillDescriptionItem> itemizedBill,
			AsyncCallback<String> callback);

	void getBillTemplateList(AsyncCallback<HashMap<String, String>> callback);

	void createUserBill(String billTemplate, String[] users,
			AsyncCallback<String> callback);

	void getStudentBills(String studentID,
			AsyncCallback<List<TableMessage>> callback);

	void savePayment(String billKey, double amount, Date payDate,
			String referenceID, String comments, AsyncCallback<String> callback);

	void getBillTemplateInfo(String billTemplateKeyStr,
			AsyncCallback<List<TableMessage>> callback);

	void getStudentBillKeys(String studentID, boolean isLogin,
			AsyncCallback<HashMap<String, String>> callback);

	void getStudentBillKeys(AsyncCallback<HashMap<String, String>> callback);	
	
	void getStudentBillAndPayment(String billKeyStr,
			AsyncCallback<List<TableMessage>> callback);

	void getBillsForTemplate(String billDescKeyStr,
			AsyncCallback<List<TableMessage>> callback);

	void getBillPayments(String billKeyStr,
			AsyncCallback<List<TableMessage>> callback);

	void getStudentBillKeysFromTemplate(String templateKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void getInvoiceDownloadLink(String billTemplateKeyStr,
			String[] billKeyStrs, AsyncCallback<String> callback);

}
