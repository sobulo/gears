/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.TableMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author Administrator
 */
public interface ClassRosterServiceAsync {

    public void getClassKeys(AsyncCallback<HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>>> asyncCallback);

    public void addStudent(String classKey, String fname, String lname, Date dob, 
            String email, HashSet<String> phoneNumbers, String loginName,
            String password, AsyncCallback<String> asyncCallback);

    public void getSimpleCourseNames(String classKey, AsyncCallback<String[]> asyncCallback);

    public void addCourse(String subjectName, String classKey, AsyncCallback<String> asyncCallback);

    public void getClassRosterMap(String classKey, AsyncCallback<HashMap<String, String>> asyncCallback);

    public void getClassRosterTable(String classKey, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getCourseRosterTable(String courseKey, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getCourseKeys(String classKey, AsyncCallback<HashMap<String, String>> asyncCallback);

    public void addStudentsToCourse(String levelKeyStr, String courseKey, java.lang.String[] studentKeys, AsyncCallback<Void> asyncCallback);

    public void addStudentsToLevel(String levelKeyStr, java.lang.String[] studentKeys, AsyncCallback<Void> asyncCallback);

    public void addGuardian(String studKey, String fname, String lname, 
            String email, HashSet<String> phoneNumbers, String loginName,
            String password, AsyncCallback<String> asyncCallback);

	void removeStudentsFromLevel(String levelKeyStr, String[] studentKeys,
			AsyncCallback<Void> callback);

	void getCourseTable(String classKey,
			AsyncCallback<ArrayList<TableMessage>> callback);
	
	void updateCourseTable(HashMap<String, Integer> courseUnits,
			AsyncCallback<ArrayList<TableMessage>> callback);
	
	void deleteCourse(String courseKey, AsyncCallback<Void> callback);

	void removeStudentsFromCourse(String courseKeyStr, String[] studentKeys,
			AsyncCallback<Void> callback);

	void getCourseRosterMap(String courseKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void getAcademicTermInfo4Levels(
			AsyncCallback<HashMap<String, String>> callback);

	void copyCourses(String[] courseKeyStrs, String classKeyStr, boolean copyStudents,
			AsyncCallback<String> callback);

	void requestReportCards(String classKey, String[] studentLogins,
			AsyncCallback<String> callback);

	void getCoursePublishStatusTable(String levelKeyStr,
			AsyncCallback<ArrayList<TableMessage>> callback);

	void updateCourseTestStatus(String levelKeyStr,
			HashMap<String, Boolean> testStatusInfo,
			AsyncCallback<String> callback);
}
