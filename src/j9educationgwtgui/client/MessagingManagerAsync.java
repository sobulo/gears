/**
 * 
 */
package j9educationgwtgui.client;

import j9educationgwtgui.shared.CustomMessageTypes;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface MessagingManagerAsync {

	void getStudentParentContact(CustomMessageTypes msgType, String keyStr, boolean includeParents,
			AsyncCallback<List<TableMessage>> callback);

	void sendMessage(CustomMessageTypes msgType, String[] addresses, String message[],
			boolean isEmail, AsyncCallback<Void> callback);

	void getMessagingControllerNames(AsyncCallback<HashMap<String, String>> callback);

	void getControllerDetails(String controllerName,
			AsyncCallback<List<TableMessage>> callback);

	void fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header, AsyncCallback<String> callback);

}
