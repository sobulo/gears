/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client;

import j9educationgwtgui.client.accounting.AddBillTemplate;
import j9educationgwtgui.client.accounting.AddStudentBill;
import j9educationgwtgui.client.accounting.AddStudentPayment;
import j9educationgwtgui.client.accounting.GenerateBillingInvoice;
import j9educationgwtgui.client.accounting.ViewBillBlotter;
import j9educationgwtgui.client.accounting.ViewBillTemplate;
import j9educationgwtgui.client.accounting.ViewStudentBill;
import j9educationgwtgui.client.classpanels.AddCoursePanel;
import j9educationgwtgui.client.classpanels.ClassRosterMovePanel;
import j9educationgwtgui.client.classpanels.CourseCopyPanel;
import j9educationgwtgui.client.classpanels.CourseDisplayPanel;
import j9educationgwtgui.client.classpanels.DeleteCoursePanel;
import j9educationgwtgui.client.classpanels.DeleteRosterPanel;
import j9educationgwtgui.client.classpanels.RosterDisplayPanel;
import j9educationgwtgui.client.coursepanels.AddTestsPanel;
import j9educationgwtgui.client.coursepanels.CourseEnrollmentPanel;
import j9educationgwtgui.client.coursepanels.CourseRosterDisplayPanel;
import j9educationgwtgui.client.coursepanels.CourseTestsDisplayPanel;
import j9educationgwtgui.client.coursepanels.DeleteCourseRosterPanel;
import j9educationgwtgui.client.coursepanels.DeleteTestsPanel;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.gradepanels.ClassGradesPanel;
import j9educationgwtgui.client.gradepanels.ClassGradesUploadPanel;
import j9educationgwtgui.client.gradepanels.CourseGradesPanel;
import j9educationgwtgui.client.gradepanels.CumulativeStudentGradesPanel;
import j9educationgwtgui.client.gradepanels.GradeUploadPanel;
import j9educationgwtgui.client.gradepanels.StudentGradesPanel;
import j9educationgwtgui.client.gradepanels.TestGradesPanel;
import j9educationgwtgui.client.messaging.MessagingStatisticsPanel;
import j9educationgwtgui.client.messaging.SendGenericMessagePanel;
import j9educationgwtgui.client.schoolpanels.BulkCourseNameUploadPanel;
import j9educationgwtgui.client.schoolpanels.DeleteLevelPanel;
import j9educationgwtgui.client.schoolpanels.EditAppParameters;
import j9educationgwtgui.client.schoolpanels.ImageUpload;
import j9educationgwtgui.client.schoolpanels.SchoolCreationPanel;
import j9educationgwtgui.client.schoolpanels.SchoolUpdatePanel;
import j9educationgwtgui.client.userpanels.AddGuardianPanel;
import j9educationgwtgui.client.userpanels.AddStudentPanel;
import j9educationgwtgui.client.userpanels.AddUserPanel;
import j9educationgwtgui.client.userpanels.BulkStudentUploadPanel;
import j9educationgwtgui.client.userpanels.CourseTeacherMappings;
import j9educationgwtgui.client.userpanels.DeleteUserPanel;
import j9educationgwtgui.client.userpanels.SearchUsersPanel;
import j9educationgwtgui.client.userpanels.StudentGuardianPanel;
import j9educationgwtgui.client.userpanels.UpdateAdminPanel;
import j9educationgwtgui.client.userpanels.UpdateGuardianPanel;
import j9educationgwtgui.client.userpanels.UpdateStudentPanel;
import j9educationgwtgui.client.userpanels.UpdateTeacherPanel;
import j9educationgwtgui.client.utilities.AddHoldPanel;
import j9educationgwtgui.client.utilities.ListHoldsPanel;
import j9educationgwtgui.client.utilities.NoticeBoardPanel;
import j9educationgwtgui.client.utilities.RequestReportCardPanel;
import j9educationgwtgui.client.utilities.TestPublishStatusListPanel;
import j9educationgwtgui.shared.DTOConstants;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceConstants.ClassGradesGPAType;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;

import java.util.Arrays;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class WebAppHelper {

    private static int indexCounter = 0;
    private final static int WELCOME_IDX = indexCounter++;
    private final static int SCHOOL_CREATION_IDX = indexCounter++;
    private final static int SCHOOL_UPDATE_IDX = indexCounter++;
    private final static int EDIT_STUDENT_IDX = indexCounter++;
    private final static int BULK_STUDENT_IDX = indexCounter++;
    private final static int BULK_COURSE_NAME_IDX = indexCounter++;
    private final static int CLASS_ROSTER_IDX = indexCounter++;
    private final static int CLASS_COURSE_IDX = indexCounter++;
    private final static int COURSE_TEST_IDX = indexCounter++;
    private final static int COURSE_ROSTER_IDX = indexCounter++;
    private final static int COURSE_GRADES_IDX = indexCounter++;
    private final static int COURSE_NAME_MAP_IDX = indexCounter++;
    private final static int TEST_GRADES_IDX = indexCounter++;
    private final static int BULK_GRADES_IDX = indexCounter++;
    private final static int BULK_CLASS_GRADES_IDX = indexCounter++;
    private final static int STUDENT_GRADE_IDX = indexCounter++;
    private final static int STUDENT_CUMULATIVE_GRADE_IDX = indexCounter++;
    private final static int EDIT_GUARDIAN_IDX = indexCounter++;
    private final static int EDIT_TEACHER_IDX = indexCounter++;
    private final static int CLASS_GRADES_IDX = indexCounter++;
    private final static int CLASS_GRADES_GPA_IDX = indexCounter++;
    private final static int CLASS_GRADES_CUM_IDX = indexCounter++;
    private final static int CLASS_GRADES_SUM_IDX = indexCounter++;
    private final static int MY_ACCT_IDX = indexCounter++;
    private final static int HOLDS_IDX = indexCounter++;
    private final static int DELETE_CLASS_IDX = indexCounter++;
    private final static int SCHOOL_LOGO_IDX = indexCounter++;    
    private final static int REQUEST_REPORT_CARDS_IDX = indexCounter++;
    private final static int SEARCH_USERS_IDX = indexCounter++;
    private final static int NOTICE_BOARD_IDX = indexCounter++;
    private final static int PUBLISH_STATUS_IDX = indexCounter++;
    private final static int BILLING_EDIT_IDX = indexCounter++;
    private final static int BILLING_VIEW_IDX = indexCounter++;
    private final static int BILL_INVOICE_IDX = indexCounter++;
    private final static int SEND_MSG_IDX = indexCounter++;
    private final static int INFO_MSG_IDX = indexCounter++;    
    private static StudentGradesPanel studentGradesPanel;
    private static Hyperlink studentGradesLink;
    
    private static CumulativeStudentGradesPanel cumulativeStudentGradesPanel;
    private static Hyperlink cumulativeStudentGradesLink;
    
    private static HyperlinkedPanel[] appPanels = new HyperlinkedPanel[indexCounter];

    //TODO static management of login state seems tacky, might want to replace
    //with gwt event bus logic
    private static LoginInfo loginInfo;
    
    static Image getSchoolImage()
    {
    	return new Image(GUIConstants.SCHOOL_IMAGE_FOLDER + 
            		loginInfo.getSchoolAccronym().toLowerCase().trim() + 
            		GUIConstants.IMG_DEFAULT_EXT);    		
    }
    
    static String getSchoolAccronym()
    {
    	return loginInfo.getSchoolAccronym();
    }

    static HyperlinkedPanel[] initializePanels() {
        GWT.log("loading static block");
        //welcome panel
        appPanels[DELETE_CLASS_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget welcomePanel;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_SUPER};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Delete Class", "delete");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (welcomePanel == null) {
                    welcomePanel = new DeleteLevelPanel();
                }
                return welcomePanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				// TODO Auto-generated method stub
				return true;
			}          
        };

        appPanels[NOTICE_BOARD_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private NoticeBoardPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Notice Board", "notice_board");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new NoticeBoardPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget.doneLoading();
			}          
        };

        appPanels[SEND_MSG_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private SendGenericMessagePanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Send Message", "send_msg");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new SendGenericMessagePanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget.doneLoading();
			}          
        };
        
        appPanels[INFO_MSG_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private MessagingStatisticsPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Messaging Stats", "stats_msg");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new MessagingStatisticsPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget.doneLoading();
			}          
        };        
        
        appPanels[PUBLISH_STATUS_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private TestPublishStatusListPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Publish Tests/Exams", "publish_test");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new TestPublishStatusListPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget.doneLoading();
			}          
        }; 
        
        appPanels[COURSE_NAME_MAP_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private EditAppParameters widget;
            //private Widget widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Course Names", "course-map");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                	widget = new EditAppParameters(DTOConstants.APP_PARAM_COURSE_NAMES, 
                    		true, true, null, DTOConstants.MAX_COURSE_NAME_LENGTH);
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget.doneLoading();
			}          
        };       
        
        //welcome panel
        appPanels[WELCOME_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private WelcomePanel welcomePanel;
            private PanelServiceLoginRoles[] eligibleRoles = PanelServiceLoginRoles.values();

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Welcome", "welcome");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (welcomePanel == null) {
                    welcomePanel = new WelcomePanel();
                }
                return welcomePanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return welcomePanel != null && welcomePanel.doneLoading();
			}
            
            
        };

        //school creation panel
        appPanels[SCHOOL_CREATION_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private SchoolCreationPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_SUPER};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Create School", "addschool");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new SchoolCreationPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return true;
			}
            
            
        };
        
        //school creation panel
        appPanels[SCHOOL_LOGO_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_SUPER};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Upload Logo", "schoollogo");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new ImageUpload();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return true;
			}  
        };        

        //school update panel
        appPanels[SCHOOL_UPDATE_IDX] = new HyperlinkedPanel() {
            private Hyperlink link;
            private SchoolUpdatePanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_SUPER};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Update School", "updateschool");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new SchoolUpdatePanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return true;
			}
        };

        //add student panel
        appPanels[EDIT_STUDENT_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
            AddStudentPanel studentPanel;
            
            @Override
            public Hyperlink getLink() 
            {
                if (link == null) {
                    link = new Hyperlink("Student", "editstudent");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
            	if (widget == null) 
                {
            		PanelServiceLoginRoles role = loginInfo.getRole();
        			UpdateStudentPanel usp = new UpdateStudentPanel(true);
        			int numOfWidgets = 3;
        			Widget[] wdgs = new Widget[numOfWidgets];
        			String[] headers = new String[numOfWidgets];
        			
        			studentPanel = new AddStudentPanel();
        			wdgs[0] = studentPanel;
        			headers[0] = "Add New Student";
        			
        			wdgs[1] = usp;
        			headers[1] = "Update Existing Student";

        			//wdgs[2] = new PasswordResetPanel(PanelServiceLoginRoles.ROLE_STUDENT);
        			//headers[2] = "Reset Password";
        			
        			wdgs[2] = new DeleteUserPanel(PanelServiceLoginRoles.ROLE_STUDENT);
        			headers[2] = "Delete Student";        			
        			
        			widget = WidgetHelper.getStackLayout(wdgs, headers);
                    
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {		
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return studentPanel != null && studentPanel.doneLoading();
			}
        };
        
        //panel for generating report cards
        appPanels[REQUEST_REPORT_CARDS_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private RequestReportCardPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Transcripts", "transcripts");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new RequestReportCardPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };        

        //bulk upload panel for new students
        appPanels[BULK_STUDENT_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private BulkStudentUploadPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("New Student Upload", "studentupload");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new BulkStudentUploadPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };

        //bulk upload panel for new students
        appPanels[BULK_COURSE_NAME_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private BulkCourseNameUploadPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Course Name Upload", "coursemapupload");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new BulkCourseNameUploadPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };
        
        
        //display class roster panel
        appPanels[CLASS_ROSTER_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private StackLayoutPanel classRosterPanel;
			private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
			RosterDisplayPanel displayPanel;
			
            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Class Roster", "classroster");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (classRosterPanel == null) {
                	int numOfWidgets = 3;
                	Widget[] widgets = new Widget[numOfWidgets];
                	String[] headers = new String[numOfWidgets];
                	
                    //note relationship exists between this panel and student
                    //grades panel. when a student is clicked on this panel, the
                    //grades for that student are displayed on the studeent
                    //grades panel
                    displayPanel = new RosterDisplayPanel();
                    displayPanel.addSelectionHandler(getStudentGradesPanel());
                    widgets[0] = displayPanel;
                    headers[0] = "View Class Roster";
                    
                    widgets[1] = new ClassRosterMovePanel();
                    headers[1] = "Add/Copy to Class Roster";
                    
                    widgets[2] = new DeleteRosterPanel();
                    headers[2] = "Remove from Class Roster";
                    
                    classRosterPanel = WidgetHelper.getStackLayout(widgets, headers);
                }
                return classRosterPanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return displayPanel != null && displayPanel.doneLoading();
			}
        };

        //add new course panel
        appPanels[CLASS_COURSE_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private StackLayoutPanel widgetWrapper;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};            
            private CourseDisplayPanel courseDisplay;
            
            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Subjects", "addcourse");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widgetWrapper == null) {
                	int numOfWidgets = 4;
                	Widget widgets[] = new Widget[numOfWidgets];
                	String headers[] = new String[numOfWidgets];
                    
                	courseDisplay = new CourseDisplayPanel();
                	widgets[0] = courseDisplay;
                	headers[0] = "View Subjects";
                	
                	widgets[1] = new AddCoursePanel();
                	headers[1] = "Add Subject";
                	
                	widgets[2] = new CourseCopyPanel();
                	headers[2] = "Copy Subjects";
                	
                	widgets[3] = new DeleteCoursePanel();
                	headers[3] = "Delete Subject";
                	
                	widgetWrapper = WidgetHelper.getStackLayout(widgets, headers);
                }
                return widgetWrapper;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() 
			{
				return courseDisplay != null && courseDisplay.doneLoading();
			}
        };

        //add a new test
        appPanels[BILLING_EDIT_IDX] = new HyperlinkedPanel() {
            private Hyperlink link;
            private StackLayoutPanel billingPanel;
            private AddBillTemplate firstStack;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Edit Bills/Payment", "create-bill");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (firstStack == null) {
                	int numOfWidgets =  3;
                    Widget[] widgets = new Widget[numOfWidgets];
                    String[] headers = new String[numOfWidgets];

                    firstStack = new AddBillTemplate();
                    widgets[0] = firstStack;
                    headers[0] = "Add New Bill Template";
                    
                    widgets[1] = new AddStudentBill();
                    headers[1] = "Generate Student Bills from Template";
                    
                    widgets[2] = new AddStudentPayment();
                    headers[2] = "Add Student Payment";
                    
   
                    billingPanel = WidgetHelper.getStackLayout(widgets, headers);
                }
                return billingPanel;
            }

            
            
			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return firstStack != null && firstStack.doneLoading();
			}
        };        

        appPanels[BILLING_VIEW_IDX] = new HyperlinkedPanel() {
            private Hyperlink link;
            private Widget billingPanel;
            private DoneLoadingWidget doneChecker;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN, 
            		PanelServiceLoginRoles.ROLE_STUDENT, PanelServiceLoginRoles.ROLE_GUARDIAN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("View Bills/Payment", "view-bill");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (billingPanel == null) 
                {
                	ViewStudentBill studBill = new ViewStudentBill(loginInfo.getRole());
                	if(loginInfo.getRole().equals(PanelServiceLoginRoles.ROLE_ADMIN))
                	{
	                	int numOfWidgets =  3;
	                    Widget[] widgets = new Widget[numOfWidgets];
	                    String[] headers = new String[numOfWidgets];
	
	                    ViewBillTemplate firstWidget = new ViewBillTemplate();
	                    doneChecker = firstWidget;
	                    widgets[0] = firstWidget;
	                    headers[0] = "View Bill Templates";
	                    
	                    widgets[1] = studBill;
	                    headers[1] = "View Student Bills/Payments";
	                    
	                    widgets[2] = new ViewBillBlotter();
	                    headers[2] = "Bill Blotter";                                       
	   
	                    billingPanel = WidgetHelper.getStackLayout(widgets, headers);
                	}
                	else
                	{
                		billingPanel = studBill;
                		doneChecker = studBill;
                	}
                }
                return billingPanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return doneChecker != null && doneChecker.doneLoading();
			}
        };        
        

        appPanels[BILL_INVOICE_IDX] = new HyperlinkedPanel() {
            private Hyperlink link;
            private GenerateBillingInvoice widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Print Bill/Invoice", "print-bill");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) 
                	widget = new GenerateBillingInvoice();
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };        
        
        
        //add a new test
        appPanels[COURSE_TEST_IDX] = new HyperlinkedPanel() {
            private Hyperlink link;
            private StackLayoutPanel courseTestPanel;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
            private CourseTestsDisplayPanel testDisplay;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Tests/Exams", "newtest");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (courseTestPanel == null) {
                	int numOfWidgets =  3;
                    Widget[] widgets = new Widget[numOfWidgets];
                    String[] headers = new String[numOfWidgets];

                    testDisplay = new CourseTestsDisplayPanel();
                    
                    widgets[0] = testDisplay;
                    headers[0] = "View Tests";
                    
                    widgets[1] = new AddTestsPanel();
                    headers[1] = "Add Tests";
                    
                    widgets[2] = new DeleteTestsPanel();
                    headers[2] = "Delete Tests";
                    
                    courseTestPanel = WidgetHelper.getStackLayout(widgets, headers);
                }
                return courseTestPanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return testDisplay != null && testDisplay.doneLoading();
			}
        };

        //enroll students in a particular subject/course
        appPanels[COURSE_ROSTER_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private StackLayoutPanel courseRoster;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
            private CourseRosterDisplayPanel rosterDisplay;
            
            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Subject Roster", "subjectenroll");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (courseRoster == null) {
                	int numOfWidgets = 3;
                	Widget[] widgets = new Widget[numOfWidgets];
                	String[] headers = new String[numOfWidgets];
                	
                	rosterDisplay = new CourseRosterDisplayPanel();
                	widgets[0] = rosterDisplay;
                	headers[0] = "View Students Registered for Subject";
                	
                    widgets[1] = new CourseEnrollmentPanel();
                    headers[1] = "Add Students to Subject";
                    
                    widgets[2] = new DeleteCourseRosterPanel();
                    headers[2] = "Remove Students from Subject";
                    
                    courseRoster = WidgetHelper.getStackLayout(widgets, headers);
                }
                return courseRoster;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return rosterDisplay != null && rosterDisplay.doneLoading();
			}
        };

        //course grades display panel
        appPanels[COURSE_GRADES_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private CourseGradesPanel widget;
            PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("View Subject Grades", "coursegrades");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() 
            {
                if (widget == null) 
                    widget = new CourseGradesPanel(loginInfo.getRole());
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };

        //test grades display panel
        appPanels[TEST_GRADES_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private TestGradesPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("View/Edit Test Grades", "testgrades");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null)
                    widget = new TestGradesPanel(loginInfo.getRole());
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };
        
        //test grades display panel
        appPanels[CLASS_GRADES_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private ClassGradesPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("View Class Grades", "classgrades");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new ClassGradesPanel(ClassGradesGPAType.NONE);
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };
        
        appPanels[CLASS_GRADES_GPA_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private ClassGradesPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Class GPA", "classgpa");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new ClassGradesPanel(ClassGradesGPAType.GPA);
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };        

        appPanels[CLASS_GRADES_CUM_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private ClassGradesPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Cumulative GPA", "cumulativegpa");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new ClassGradesPanel(ClassGradesGPAType.CUMULATIVE);
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };        
        
        appPanels[CLASS_GRADES_SUM_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private ClassGradesPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("GPA Summary", "sumgpa");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new ClassGradesPanel(ClassGradesGPAType.SUMMARY);
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };   
        
        //student test grades bulk upload panel
        appPanels[BULK_GRADES_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private GradeUploadPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Upload Test Grades", "bulkscoreLoad");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new GradeUploadPanel(loginInfo.getRole());
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };
        
        //student test grades bulk upload panel
        appPanels[BULK_CLASS_GRADES_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private ClassGradesUploadPanel widget;
            private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Upload Class Grades", "bulkclassscoreLoad");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (widget == null) {
                    widget = new ClassGradesUploadPanel();
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };        

        //display grades for a particular student
        appPanels[STUDENT_GRADE_IDX] = new HyperlinkedPanel() {
			private PanelServiceLoginRoles[] eligibleRoles = 
			{PanelServiceLoginRoles.ROLE_GUARDIAN, PanelServiceLoginRoles.ROLE_STUDENT, 
			 PanelServiceLoginRoles.ROLE_ADMIN};
			private StudentGradesPanel studPanel;
            //note: these call helper static fns outside the inner class because
            //of dependency with class roster panel
            @Override
            public Hyperlink getLink() {
                return getStudentGradesLink();
            }

            @Override
            public Widget getPanelWidget() {
            	studPanel = getStudentGradesPanel();
                return studPanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return studPanel != null && studPanel.doneLoading();
			}
			
			
        };
        
        //display grades for a particular student
        appPanels[STUDENT_CUMULATIVE_GRADE_IDX] = new HyperlinkedPanel() {
			private PanelServiceLoginRoles[] eligibleRoles = 
			{PanelServiceLoginRoles.ROLE_GUARDIAN, PanelServiceLoginRoles.ROLE_STUDENT, 
			 PanelServiceLoginRoles.ROLE_ADMIN};
			private CumulativeStudentGradesPanel studPanel;
            //note: these call helper static fns outside the inner class because
            //of dependency with class roster panel
            @Override
            public Hyperlink getLink() {
                return getCumulativeStudentGradesLink();
            }

            @Override
            public Widget getPanelWidget() {
            	studPanel = getCumulativeStudentGradesPanel();
                return studPanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return studPanel != null && studPanel.doneLoading();
			}
			
			
        };
        
        
        appPanels[MY_ACCT_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget myacctpanel;
			PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_GUARDIAN,
					PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_STUDENT, PanelServiceLoginRoles.ROLE_TEACHER};            

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("My Account", "myaccount");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (myacctpanel == null) 
                {
                	int numOfWidgets = 2;
                	Widget[] widgets = new Widget[numOfWidgets];
                	String[] headers = new String[numOfWidgets];
                	PanelServiceLoginRoles role = loginInfo.getRole();
                	if(role.equals(PanelServiceLoginRoles.ROLE_STUDENT))
                		widgets[0] = new UpdateStudentPanel(false);
                	else if(role.equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
                		widgets[0] = new UpdateGuardianPanel(false);
                	else if(role.equals(PanelServiceLoginRoles.ROLE_ADMIN))
                		widgets[0] = new UpdateAdminPanel();
                	else if(role.equals(PanelServiceLoginRoles.ROLE_TEACHER))
                		widgets[0] = new UpdateTeacherPanel(false); 
                    headers[0] = "Information";
                    //widgets[1] = new PasswordChangePanel();
                    //headers[1] = "Change Password";
                    myacctpanel = widgets[0];
                }
                return myacctpanel;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return true;
			}
        };   

        appPanels[EDIT_GUARDIAN_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget widget;
            private AddGuardianPanel guardPanel;
            PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Guardian", "editguardian");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() 
            {
            	if (widget == null) 
                {
        			int numOfWidgets = 4;
        			Widget[] wdgs = new Widget[numOfWidgets];
        			String[] headers = new String[numOfWidgets];
        			
        			guardPanel = new AddGuardianPanel();
        			wdgs[0] = guardPanel;
        			headers[0] = "Add New Parent/Guardian";
        			
        			wdgs[1] = new UpdateGuardianPanel(true);
        			headers[1] = "Update Existing Parent/Guardian";
        			
        			wdgs[2] = new StudentGuardianPanel();
        			headers[2] = "Update Children";
        			
        			//wdgs[3] = new PasswordResetPanel(PanelServiceLoginRoles.ROLE_GUARDIAN);
        			//headers[3] = "Reset Password";
        			
        			wdgs[3] = new DeleteUserPanel(PanelServiceLoginRoles.ROLE_GUARDIAN);
        			headers[3] = "Delete Parent/Guardian";
        			
        			widget = WidgetHelper.getStackLayout(wdgs, headers);                    
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return guardPanel != null && guardPanel.doneLoading();
			}
        };
        
        appPanels[EDIT_TEACHER_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget widget;
            PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
            AddUserPanel userPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Teacher", "editteacher");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() 
            {
            	if (widget == null) 
                {
        			int numOfWidgets = 3;
        			Widget[] wdgs = new Widget[numOfWidgets];
        			String[] headers = new String[numOfWidgets];
        			
        			userPanel = new AddUserPanel(PanelServiceLoginRoles.ROLE_TEACHER);
        			wdgs[0] = userPanel;
        			headers[0] = "Add New Teacher";
        			
        			wdgs[1] = new UpdateTeacherPanel(true);
        			headers[1] = "Update Existing Teacher";
        			
        			wdgs[2] = new CourseTeacherMappings();
        			headers[2] = "Select Subject Teacher";
        			
        			//wdgs[3] = new PasswordResetPanel(PanelServiceLoginRoles.ROLE_TEACHER);
        			//headers[3] = "Reset Password";        			

        			//TODO delete teacher option ...
        			
        			widget = WidgetHelper.getStackLayout(wdgs, headers);                    
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return userPanel != null && userPanel.doneLoading();
			}
        };        
        
        appPanels[HOLDS_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget widget;
            PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
            private AddHoldPanel holdPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Student Holds", "studentholds");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() 
            {
            	if (widget == null) 
                {
        			int numOfWidgets = 2;
        			Widget[] wdgs = new Widget[numOfWidgets];
        			String[] headers = new String[numOfWidgets];
        			
        			holdPanel = new AddHoldPanel();
        			wdgs[0] = holdPanel;
        			headers[0] = "Add Student Hold";
        			
        			wdgs[1] = new ListHoldsPanel();
        			headers[1] = "List/Edit Holds";
        			
        			widget = WidgetHelper.getTabLayout(wdgs, headers);                    
                }
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return holdPanel != null && holdPanel.doneLoading();
			}
        };
        
        appPanels[SEARCH_USERS_IDX] = new HyperlinkedPanel() {
			private PanelServiceLoginRoles[] eligibleRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
			private Hyperlink link;
			private SearchUsersPanel widget;
			
            @Override
            public Hyperlink getLink() {
                if(link == null)
                	link = new Hyperlink("Search", "search_users");
                return link;
            }

            @Override
            public Widget getPanelWidget() {
            	if(widget == null)
            		widget = new SearchUsersPanel();
                return widget;
            }

			@Override
			public PanelServiceLoginRoles[] getEligibleRoles() {
				return eligibleRoles;
			}

			@Override
			public boolean doneLoading() {
				return widget != null && widget.doneLoading();
			}
        };        
        
        return appPanels;
    }
    
    static Anchor getLogoutUrl()
    {
    	Anchor loginLink = new Anchor("Logout", loginInfo.getLogoutUrl());
    	return loginLink;
    }

    static Tree getMenuTree(PanelServiceLoginRoles role) {
        GWT.log("calling gettree");

        
        //logout menu item
        Tree navtree = new Tree();
        navtree.add(getLogoutUrl());
        
        //welcome menuitem
        TreeItem welcomenav = new TreeItem(appPanels[WELCOME_IDX].getLink());
        navtree.addItem(welcomenav);
        

        switch (role) {
            case ROLE_SUPER: {
                navtree.addItem(buildSchoolItems());
                navtree.addItem(appPanels[DELETE_CLASS_IDX].getLink());
                break;
            }
            case ROLE_ADMIN: {
                navtree.addItem(buildClassItems());
                navtree.addItem(buildGradeItems());
                navtree.addItem(buildUserItems());
                navtree.addItem(buildBillingItems());
                navtree.addItem(buildMessaggingItems());
                navtree.addItem(buildUtilityItems());
                navtree.addItem(appPanels[MY_ACCT_IDX].getLink());
                break;
            }
            case ROLE_STUDENT: 
            {
            	navtree.addItem(appPanels[STUDENT_GRADE_IDX].getLink());
            	navtree.addItem(appPanels[STUDENT_CUMULATIVE_GRADE_IDX].getLink());
            	//navtree.addItem(appPanels[BILLING_VIEW_IDX].getLink());
            	navtree.addItem(appPanels[MY_ACCT_IDX].getLink());
            	break;
            }
            case ROLE_GUARDIAN:
            {
                navtree.addItem(appPanels[STUDENT_GRADE_IDX].getLink());
                navtree.addItem(appPanels[STUDENT_CUMULATIVE_GRADE_IDX].getLink());
                //navtree.addItem(appPanels[BILLING_VIEW_IDX].getLink());
                navtree.addItem(appPanels[MY_ACCT_IDX].getLink());
                break;
            }
            case ROLE_TEACHER:
            {
                navtree.addItem(appPanels[MY_ACCT_IDX].getLink());
                navtree.addItem(appPanels[COURSE_GRADES_IDX].getLink());
                navtree.addItem(appPanels[TEST_GRADES_IDX].getLink());
                navtree.addItem(appPanels[BULK_GRADES_IDX].getLink());
                break;           	
            }
        }
        
        return navtree;
    }

    private static TreeItem buildSchoolItems() {
        //school submenu
        TreeItem schoolnav = new TreeItem(new Label("Schools"));
        schoolnav.addItem(appPanels[SCHOOL_CREATION_IDX].getLink());
        schoolnav.addItem(appPanels[SCHOOL_UPDATE_IDX].getLink());
        schoolnav.addItem(appPanels[SCHOOL_LOGO_IDX].getLink());
        return schoolnav;
    }
    
    private static TreeItem buildBillingItems() {
        //school submenu
        TreeItem billingnav = new TreeItem(new Label("Billing/Payments"));
        billingnav.addItem(appPanels[BILLING_VIEW_IDX].getLink());
        billingnav.addItem(appPanels[BILLING_EDIT_IDX].getLink());
        billingnav.addItem(appPanels[BILL_INVOICE_IDX].getLink());
        return billingnav;
    }    

    private static TreeItem buildClassItems() {
        //class submenu
        TreeItem classNav = new TreeItem(new Label("Setup School"));
        classNav.addItem(appPanels[CLASS_COURSE_IDX].getLink());
        classNav.addItem(appPanels[COURSE_TEST_IDX].getLink());
        classNav.addItem(appPanels[CLASS_ROSTER_IDX].getLink());
        classNav.addItem(appPanels[COURSE_ROSTER_IDX].getLink());
        classNav.addItem(appPanels[COURSE_NAME_MAP_IDX].getLink());
        classNav.addItem(appPanels[BULK_COURSE_NAME_IDX].getLink());
        return classNav;
    }

    private static TreeItem buildGradeItems() {
        //grades submenu
        TreeItem gradeNav = new TreeItem(new Label("Grades"));
        TreeItem uploadNav = new TreeItem(new Label("Uploads"));
        TreeItem gpaNav = new TreeItem(new Label("GPAs"));
        gradeNav.addItem(appPanels[CLASS_GRADES_IDX].getLink());
        gradeNav.addItem(appPanels[COURSE_GRADES_IDX].getLink());
        gradeNav.addItem(appPanels[TEST_GRADES_IDX].getLink());
        gradeNav.addItem(appPanels[STUDENT_GRADE_IDX].getLink());
        gradeNav.addItem(appPanels[STUDENT_CUMULATIVE_GRADE_IDX].getLink());
        gradeNav.addItem(appPanels[REQUEST_REPORT_CARDS_IDX].getLink());
        gradeNav.addItem(gpaNav);
        gradeNav.addItem(uploadNav);
        gpaNav.addItem(appPanels[CLASS_GRADES_GPA_IDX].getLink());
        gpaNav.addItem(appPanels[CLASS_GRADES_CUM_IDX].getLink());
        gpaNav.addItem(appPanels[CLASS_GRADES_SUM_IDX].getLink());
        uploadNav.addItem(appPanels[BULK_CLASS_GRADES_IDX].getLink());
        uploadNav.addItem(appPanels[BULK_GRADES_IDX].getLink());
        return gradeNav;
    }    
    
    private static TreeItem buildUserItems(){
    	TreeItem userNav = new TreeItem(new Label("User Accounts"));
    	TreeItem uploadNav = new TreeItem(new Label("Uploads"));
    	userNav.addItem(appPanels[SEARCH_USERS_IDX].getLink());
        userNav.addItem(appPanels[EDIT_STUDENT_IDX].getLink());
        userNav.addItem(appPanels[EDIT_GUARDIAN_IDX].getLink());
        userNav.addItem(appPanels[EDIT_TEACHER_IDX].getLink());
        userNav.addItem(uploadNav);
        uploadNav.addItem(appPanels[BULK_STUDENT_IDX].getLink());
        return userNav;
    }
    
    private static TreeItem buildUtilityItems(){
    	TreeItem utilNav = new TreeItem(new Label("Tools"));
    	utilNav.addItem(appPanels[PUBLISH_STATUS_IDX].getLink());
    	utilNav.addItem(appPanels[HOLDS_IDX].getLink());
    	utilNav.addItem(appPanels[NOTICE_BOARD_IDX].getLink());
    	return utilNav;
    }
    
    private static TreeItem buildMessaggingItems()
    {
    	TreeItem messagingNav = new TreeItem(new Label("Messaging"));
    	messagingNav.addItem(appPanels[SEND_MSG_IDX].getLink());
    	messagingNav.addItem(appPanels[INFO_MSG_IDX].getLink());
    	return messagingNav;
    }

    static int getWelcomeScreenIndex() {
        return WELCOME_IDX;
    }

    private static Hyperlink getStudentGradesLink() {
        if (studentGradesLink == null) {
            studentGradesLink = new Hyperlink("Student Grades", "studentgrades");
        }
        return studentGradesLink;
    }
    
    private static Hyperlink getCumulativeStudentGradesLink() {
        if (cumulativeStudentGradesLink == null) {
            cumulativeStudentGradesLink = new Hyperlink("Student C-GPA", "studentcgpa");
        }
        return cumulativeStudentGradesLink;
    }    

    private static StudentGradesPanel getStudentGradesPanel() {
        if (studentGradesPanel == null)
        {
            studentGradesPanel = new StudentGradesPanel();
            studentGradesPanel.setHyperlink(getStudentGradesLink());
            if(loginInfo.getRole().equals(PanelServiceLoginRoles.ROLE_STUDENT))
                studentGradesPanel.initStudentMode();
            else if(loginInfo.getRole().equals(PanelServiceLoginRoles.ROLE_ADMIN))
                studentGradesPanel.initAdminMode();
            else if(loginInfo.getRole().equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
                studentGradesPanel.initGuardianMode();
        }
        return studentGradesPanel;
    }
    
    private static CumulativeStudentGradesPanel getCumulativeStudentGradesPanel() {
        if (cumulativeStudentGradesPanel == null)
        {
            cumulativeStudentGradesPanel = new CumulativeStudentGradesPanel();
            cumulativeStudentGradesPanel.setHyperlink(getCumulativeStudentGradesLink());
            if(loginInfo.getRole().equals(PanelServiceLoginRoles.ROLE_STUDENT))
                cumulativeStudentGradesPanel.initStudentMode();
            else if(loginInfo.getRole().equals(PanelServiceLoginRoles.ROLE_ADMIN))
                cumulativeStudentGradesPanel.initAdminMode();
            else if(loginInfo.getRole().equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
                cumulativeStudentGradesPanel.initGuardianMode();
        }
        return cumulativeStudentGradesPanel;
    }    

    public static void setLoginInfo(LoginInfo info) {
        loginInfo = info;
    }

    public static void checkForLoginError(Throwable error) {
        if (error instanceof LoginValidationException)
        {
            Window.alert(error.getMessage());
            String url = GWT.getHostPageBaseURL();
            if(loginInfo != null && loginInfo.isLoggedIn())
                url = loginInfo.getLogoutUrl();
            Window.Location.replace(url);
        }
    }
    
    static boolean checkLoginEligibility(HyperlinkedPanel p)
    {
    	PanelServiceLoginRoles[] allowedRoles = p.getEligibleRoles();
    	Arrays.sort(allowedRoles);
    	GWT.log("Login Info: " + loginInfo.getRole());
    	return(Arrays.binarySearch(allowedRoles, loginInfo.getRole()) >= 0);
    }
    
    static String getNoticeId(PanelServiceLoginRoles role)
    {
    	switch (role)
    	{
    		case ROLE_ADMIN:
    			return PanelServiceConstants.ADMIN_NOTE_ID;
    		case ROLE_GUARDIAN:
    			return PanelServiceConstants.GUARDIAN_NOTE_ID;
    		case ROLE_STUDENT:
    			return PanelServiceConstants.STUDENT_NOTE_ID;
    		case ROLE_TEACHER:
    			return PanelServiceConstants.TEACHER_NOTE_ID;
    		default:
    			return PanelServiceConstants.PUBLIC_NOTE_ID;
    	}
    }
}
