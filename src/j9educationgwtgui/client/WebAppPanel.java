package j9educationgwtgui.client;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.widgetideas.client.ProgressBar;

/**
 *
 * @author Administrator
 */
public class WebAppPanel extends ResizeComposite implements ValueChangeHandler<String>, DoneLoadingWidget 
{
    @UiField
    ScrollPanel navcontent;
    @UiField
    LayoutPanel content;
    @UiField
    VerticalPanel grepLogos;
    @UiField
    SimplePanel schoolLogo;
    
    Timer loadingTimer;
    private TextProgressBar loadingIndicator;
   
    DialogBox loadingPanel;
    
    private static WebAppPanelUiBinder uiBinder = GWT.create(WebAppPanelUiBinder.class);
    private String newToken;
    private HTML unAuthorizedLabel = new HTML("<center><b><font color='red'>You're not authorized to view " +
    		"this panel</font></b><center>");

    //different views
    HyperlinkedPanel currentContent; //keeps track of view currently displayed
    HyperlinkedPanel[] appPanels;
    WelcomePanel welcomePanel;
    private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);    
    private final int LOAD_INDICATOR_STEP_SIZE = 500; //milliseconds
    private final int MAX_LOAD_INDICATOR_TIME = 20000; //milliseconds
    private static class TextProgressBar extends ProgressBar
    {
    	private String displayText;
    	public TextProgressBar(String displayText, double max)
    	{
    		super(0, max);
    		this.displayText = displayText;
    	}
    	
    	protected String generateText(double curProgress)
    	{
    		return displayText; 
    	};
    	
    	public void setProgressText(String text)
    	{
            //progress = percentageProgress * getMaxProgress();
            displayText = text;
    	}
    }
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<LoginInfo> loginCallback = new AsyncCallback<LoginInfo>() {

        @Override
        public void onSuccess(LoginInfo result) {
            WebAppHelper.setLoginInfo(result);
            WidgetHelper.setLoginInfo(result);
            loadingIndicator.setProgressText("Building menu options");
            Image m = WebAppHelper.getSchoolImage();
            schoolLogo.add(m);
            welcomePanel.setSchoolHeader(result.getSchoolName());
            GWT.log("School name: " + result.getSchoolName());
            if (result.isLoggedIn()) {
                welcomePanel.setLoginLabel("Welcome: " + result.getName() + ". Click on link below to signout.");
                welcomePanel.setAnchor("Sign Out", result.getLogoutUrl(), result.getRole());
                setupMenu(result.getRole());
            } else {
                welcomePanel.setLoginLabel(result.getMessage());
                welcomePanel.setAnchor(result.getLoginUrl());
            }
            loadingIndicator.setProgressText("Finalizing loading ...");
        }

        @Override
        public void onFailure(Throwable caught) {
        	if(caught instanceof LoginValidationException)
        	{
                welcomePanel.setLoginLabel("Login attempt failed, click on link" +
                        " to retry. Error was: [" + caught.getMessage() + "]");        		
        	}
            welcomePanel.setLoginLabel("Unable to reach server, click on link" +
                    " to retry. Error was: [" + caught.getMessage() + "]");
            welcomePanel.setAnchor("Retry", GWT.getHostPageBaseURL(), PanelServiceLoginRoles.ROLE_PUBLIC);
        }
    };

/*    final AsyncCallback<LoginInfo> ensureLoginCallback = new AsyncCallback<LoginInfo>() {

        @Override
        public void onSuccess(LoginInfo result) {
            WebAppHelper.setLoginInfo(result);
            if (!result.isLoggedIn()) {
            	cancelProgress(); //called here as we never make it out of the if block  due to window.location call
                Window.alert("Seems you got signed out. For security/privacy reasons, the system automatically logs " +
                		"you out after 30 minutes of inactivity");
                //since logged out on server, immediately clear client state
                Window.Location.replace(GWT.getHostPageBaseURL());
            }
            else
                loadNewPanel();
            cancelProgress();
            
        }

        @Override
        public void onFailure(Throwable caught) {
        	cancelProgress();
            welcomePanel.setLoginLabel("Unable to reach server, click on link" +
                    " to retry. Error was: " + caught.getMessage() + "]");
            welcomePanel.setAnchor("Retry", GWT.getHostPageBaseURL());
        }
    };*/

    interface WebAppPanelUiBinder extends UiBinder<Widget, WebAppPanel> {
    }

    public void setupMenu(PanelServiceLoginRoles role) {
        GWT.log("adding menu");
        Tree navtree = WebAppHelper.getMenuTree(role);
        navcontent.add(navtree);
        //setup hyperlink handler
        int welcomeIndex = WebAppHelper.getWelcomeScreenIndex();
        History.addValueChangeHandler(this);
        String initToken = History.getToken();
        if (initToken.length() == 0) {
            History.newItem(appPanels[welcomeIndex].getLink().
                    getTargetHistoryToken());
        }
        //setup init state
        History.fireCurrentHistoryState();
    }

    public WebAppPanel() {
        //GWT.log("initializing webapp panel");
        initWidget(uiBinder.createAndBindUi(this));
  
        //setup loading panel
        loadingIndicator = new TextProgressBar("Initializing GREP", MAX_LOAD_INDICATOR_TIME);
        simulateProgress();
        loadingPanel = new DialogBox(false, false);
        loadingPanel.setHTML("<b>Loading, please wait</b>");
        loadingPanel.setAnimationEnabled(true);
        loadingPanel.setGlassEnabled(true);
        SimplePanel temp = new SimplePanel();
        temp.setWidth("350px");
        temp.add(loadingIndicator);
        loadingPanel.setWidget(temp);
        loadingPanel.show();
        loadingPanel.center();
        GWT.log("Calling sim progress");
        
        
        //setup grep logos
        for(String logoUrl : GUIConstants.GREP_LOGOS)
        	grepLogos.add(new Image(GUIConstants.MODULE_IMAGE_FOLDER + logoUrl));
        
        loadingIndicator.setProgressText("Generating panel ...");
        
        //setup welcome screen
        int welcomeIndex = WebAppHelper.getWelcomeScreenIndex();
        appPanels = WebAppHelper.initializePanels();
        
        loadingIndicator.setProgressText("Building welcome page");
        
        //GWT.log("initialized panels");
        currentContent = appPanels[welcomeIndex];
        
        //GWT.log("retrieved current content");
        welcomePanel = (WelcomePanel) currentContent.getPanelWidget();
        content.add(currentContent.getPanelWidget());
        
        //GWT.log("referenced welcome panel");
         
        
        //welcomePanel = (WelcomePanel) appPanels[welcomeIndex].getPanelWidget();
        //History.addValueChangeHandler(this);
        loadingIndicator.setProgressText("Fetching session data");
        schoolService.login(GWT.getHostPageBaseURL(), loginCallback);
    }
    
    public void simulateProgress()
    {
    	loadingTimer = new Timer() {
			
			@Override
			public void run() 
			{
				double newProgress = loadingIndicator.getProgress() + LOAD_INDICATOR_STEP_SIZE;
				GWT.log("Checking panel:  " + currentContent.getLink().getTargetHistoryToken() + " postion: " + loadingIndicator.getProgress());
				
				if(doneLoading() || newProgress >= loadingIndicator.getMaxProgress())
				{
			    	this.cancel();
			    	loadingPanel.hide();
			    	GWT.log("Called cancel @: " + loadingIndicator.getProgress() + "");
			    	
				}
					
				loadingIndicator.setProgress(newProgress);	
			}
		};
		
		//schedule timer to repeat
		loadingIndicator.setProgress(0);
		loadingTimer.scheduleRepeating(400);
    };
    

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
        newToken = event.getValue();
        loadingIndicator.setProgressText("Fetching panel for " + newToken);
        loadingPanel.show();
        simulateProgress();
        loadNewPanel();       
        //schoolService.login(GWT.getHostPageBaseURL(), ensureLoginCallback);
    }

    private void loadNewPanel()
    {
        HyperlinkedPanel newContent = null;
        //determine what widget user link points to
        for(HyperlinkedPanel cursorPanel : appPanels) 
        {
            if (newToken.equals(cursorPanel.getLink().getTargetHistoryToken())) {
            	GWT.log("Cursor Panel: " + cursorPanel.getLink().getTargetHistoryToken());
            	if(WebAppHelper.checkLoginEligibility(cursorPanel))
            		newContent = cursorPanel;
            	else
            		newContent = null;
                break;
            }
        }
        
        //update current widget to widget specified by user above
        if(newContent != null) 
        {
        	if(currentContent != newContent)
        	{
	            content.clear();
	            content.add(newContent.getPanelWidget());
	            currentContent = newContent;
        	}
        }
        else
        {
        	content.clear();
        	content.add(unAuthorizedLabel);
        	currentContent = null;
        }
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() 
	{
		GWT.log("current Content: " + (currentContent==null?null:currentContent.getLink().getTargetHistoryToken()) + "  done: " + currentContent.doneLoading());
		return (currentContent == null || currentContent.doneLoading());
	}
}
