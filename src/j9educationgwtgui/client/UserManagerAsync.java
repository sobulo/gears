/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author Administrator
 */
public interface UserManagerAsync 
{
    public void getStudentLevels(AsyncCallback<HashMap<String, String>> asyncCallback);

    public void getStudentLevels(String studKeyStr, AsyncCallback<HashMap<String, String>> asyncCallback);

    public void getGuardianChildren(AsyncCallback<HashMap<String, String>> asyncCallback);

	void getStudentInfo(String studKeyStr, AsyncCallback<TableMessage> callback);

	void getStudentInfo(AsyncCallback<TableMessage> callback);

	void getAllStudentIDs(boolean useLogins,
			AsyncCallback<HashMap<String, String>> callback);

	void getAllGuardianIDs(boolean useLogins,
			AsyncCallback<HashMap<String, String>> callback);

	void getUserInfo(String loginName, PanelServiceLoginRoles role, AsyncCallback<TableMessage> callback);

	void getUserInfo(AsyncCallback<TableMessage> callback);

	void updateStudent(String fname, String lname, Date dob, String email,
			HashSet<String> phoneNums, String studKey,
			AsyncCallback<Void> callback);

	void updateStudent(String fname, String lname, Date dob, String email,
			HashSet<String> phoneNums, AsyncCallback<Void> callback);

	void updateUser(String fname, String lname, String email,
			HashSet<String> phoneNums, String loginName, PanelServiceLoginRoles role,
			AsyncCallback<Void> callback);

	void getGuardianChildren(String guardKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void getStudentParents(String studKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void addGuardianToStudent(String guardKeyStr, String studKeyStr,
			AsyncCallback<Void> callback);

	void resetPassword(String userKeyStr, PanelServiceLoginRoles role, AsyncCallback<String> callback);

	void changePassword(String oldPassword, String newPassword,
			AsyncCallback<String> callback);

	void deleteStudent(String studKeyStr, AsyncCallback<String> callback);

	void deleteGuardian(String loginName, AsyncCallback<String> callback);

	void updateUser(String fname, String lname, String email,
			HashSet<String> phoneNums, AsyncCallback<Void> callback);

	void createStudentHold(String levelKeyStr, String studKeyStr, String holdType,
			String message, String note, AsyncCallback<String> callback);

	void getHoldTypes(AsyncCallback<String[]> callback);

	void getStudentHolds(String levelKeyStr, boolean unresolvedOnly,
			AsyncCallback<ArrayList<TableMessage>> callback);

	void editStudentHold(String holdKeyStr, String holdType, String message,
			String note, boolean resolutionStatus,
			AsyncCallback<String> callback);

	void createUser(String fname, String lname, String email,
			HashSet<String> phoneNums, String loginName, String password,
			PanelServiceLoginRoles role, AsyncCallback<String> callback);

	void getAllTeacherIDs(AsyncCallback<HashMap<String, String>> callback);

	void getCourseTeacherMappings(String levelKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void setCourseTeacherMapping(String courseKeyStr, String teacherKeyStr,
			boolean isAdd, AsyncCallback<String> callback);

	void getTeacherCourses(AsyncCallback<HashMap<String, String>> callback);

	void getDetailedUserInfo(String loginName, PanelServiceLoginRoles role,
			AsyncCallback<String> callback);
}
