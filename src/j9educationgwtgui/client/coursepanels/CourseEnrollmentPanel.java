/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.coursepanels;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.LevelAndCourseBox;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class CourseEnrollmentPanel extends Composite implements ClickHandler, ValueChangeHandler<String>
{

    @UiField LevelAndCourseBox classAndSubject;
    @UiField ListBox2ListBox studentBox;
    @UiField Button saveStudents;
    @UiField Label status;

    private final static int MAX_DISPLAY_ITEMS = 15;

    private ClassRosterServiceAsync classService;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> classRosterCallBack =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
            studentBox.initValues(result);
            WidgetHelper.setSuccessStatus(status, "Retrieved " + result.size() +
                    " students from " + classAndSubject.getClassName(), false);
        }
    };

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> addStudentsCallBack =
            new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
        }

        @Override
        public void onSuccess(Void result) {
            WidgetHelper.setSuccessStatus(status, "Added students to subject roster", true);
        }
    };

    private static CourseEnrollmentPanelUiBinder uiBinder = GWT.create(CourseEnrollmentPanelUiBinder.class);

    @Override
    public void onClick(ClickEvent event)
    {
        HashSet<String> chosen = studentBox.getValues();
        if(chosen.size() == 0)
        {
        	WidgetHelper.setErrorStatus(status, "Please select students to add to subject roster", true);
        	return;
        }
        String[] studentKeys = new String[chosen.size()];
        WidgetHelper.setNormalStatus(status, "Requesting registration of " + chosen.size() +
                " students for subject " + classAndSubject.getCourseName() + " (" + classAndSubject.getClassName() + "). Please wait ...");
        classService.addStudentsToCourse(classAndSubject.getClassValue(), classAndSubject.getCourseValue(),
                chosen.toArray(studentKeys), addStudentsCallBack);
    }

    interface CourseEnrollmentPanelUiBinder extends UiBinder<Widget, CourseEnrollmentPanel> {
    }

    public CourseEnrollmentPanel()
    {
        initWidget(uiBinder.createAndBindUi(this));
        studentBox.initAppearance(MAX_DISPLAY_ITEMS, GUIConstants.DEFAULT_LB_WIDTH, "Class List", "Subject Registration List");
        classService = classAndSubject.getClassService();
        //register handlers
        classAndSubject.addValueChangeHandler(this);
        classAndSubject.addValueChangeHandlerForTopLevel(this);
        classAndSubject.initData();
        saveStudents.addClickHandler(this);
        
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event)
	{
		String selected = event.getValue();
		String selectedName = "your last choice";
		if(classAndSubject.isTopLevelEvent(event))
		{
			if(selected.equals(classAndSubject.getClassValue()))
				selectedName = classAndSubject.getClassName();
			WidgetHelper.setNormalStatus(status, "Fetching students for class: " + selectedName + ". Please wait ...");
			studentBox.initValues(null);
			classService.getClassRosterMap(selected,
                classRosterCallBack);
		}
		else if(studentBox.getValues().size() > 0)
		{
			if(selected.equals(classAndSubject.getCourseValue()))
				selectedName = classAndSubject.getCourseName();
			WidgetHelper.setNormalStatus(status, "Please select students to add to subject " + selectedName);
			studentBox.reset();
		}
	}
}