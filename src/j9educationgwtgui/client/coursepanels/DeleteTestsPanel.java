/**
 * 
 */
package j9educationgwtgui.client.coursepanels;

import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.custom.widgets.LevelCourseTestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class DeleteTestsPanel extends Composite implements ClickHandler {

	@UiField
	Label status;

	@UiField
	Button delete;

	@UiField
	LevelCourseTestBox testPicker;

	private ScoreManagerAsync testScoreService;
	private DialogBox alertBox;

	final AsyncCallback<Void> deleteCallback = new AsyncCallback<Void>() {
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
		}

		public void onSuccess(Void result) {
			WidgetHelper.setSuccessStatus(status, "Successfully deleted test/exam", true);
		}
	};

	private static DeleteTestsPanelUiBinder uiBinder = GWT
			.create(DeleteTestsPanelUiBinder.class);

	interface DeleteTestsPanelUiBinder extends
			UiBinder<Widget, DeleteTestsPanel> {
	}


	public DeleteTestsPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		testScoreService = testPicker.getScoreManager();
		testPicker.initData();
		delete.addClickHandler(this);
		
		//setup alert box
		alertBox = new DialogBox(true);
		alertBox.setHTML("<b>Confirm Delete</b>");
        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);			
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) 
	{
		String selectedTest = testPicker.getSelectedTest();
		String selectedName = testPicker.getSelectedTestName() + "(" + 
		testPicker.getClassName() + " / " + testPicker.getCourseName() + ")";
		
		Label msg = new Label("Are you sure you want to delete: " + selectedName);
		VerticalPanel contentPanel = new VerticalPanel();
		contentPanel.add(msg);
		HorizontalPanel buttonPanel = getAlertBoxButtonPanel(selectedTest, selectedName);
		contentPanel.add(buttonPanel);
		contentPanel.setCellHorizontalAlignment(buttonPanel, HorizontalPanel.ALIGN_CENTER);
	    
		//display popup box
		alertBox.clear();
		alertBox.setWidget(contentPanel);
	    alertBox.show();
	    alertBox.center();	
	}

	private HorizontalPanel getAlertBoxButtonPanel(final String testKey, final String testName)
	{
		HorizontalPanel panel = new HorizontalPanel();
		Button yes = new Button("  Yes  ");
		Button no = new Button("  No  ");
		yes.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
				WidgetHelper.setNormalStatus(status, "Requesting delete of " + testName + ". Please wait ...");
				testScoreService.deleteTest(testKey, deleteCallback);
			}
		});
		
		no.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();				
			}
		});
		
		panel.add(yes);
		panel.add(new Label("     "));
		panel.add(no);
		return panel;
	}	
}
