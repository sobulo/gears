/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.coursepanels;

import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.custom.widgets.LevelAndCourseBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class AddTestsPanel extends Composite implements ClickHandler ,ValueChangeHandler<String>
{
    private static AddTestsPanelUiBinder uiBinder = GWT.create(AddTestsPanelUiBinder.class);

    private ScoreManagerAsync scoreManager = GWT.create(ScoreManager.class);

    @UiField LevelAndCourseBox classAndSubject;
    @UiField TextBox testName;
    @UiField TextBox maxScore;
    @UiField TextBox weight;
    @UiField Button saveTest;
    @UiField HTML status;

    //private static HashMap<String, Double> cachedTotalScores;
    private SimpleDialog errorBox;


    final AsyncCallback<String> saveTestCallback =
            new AsyncCallback<String>() {
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
        }

        public void onSuccess(String result) {
            WidgetHelper.setSuccessStatus(status, "Added " + result + " successfully", true);
            //scoreManager.totalTestWeight(classAndSubject.getCourseValue(), weightCallback);     
        }
    };

    @Override
    public void onClick(ClickEvent event) {
        String courseKey = classAndSubject.getCourseValue();
        String courseName = classAndSubject.getCourseName() + "(" + classAndSubject.getClassName() +")";
       
        /*if(!cachedTotalScores.containsKey(courseKey))
        {
        	errorBox.show("Unable to calculate current test weights." +
        			" Please try refreshing the browser");
        	return;
        }*/
        
        
        String tName = testName.getText().trim();
        if(tName.length() == 0)
        {
        	errorBox.show("Please enter a value for field test name");
        	return;
        }
        if(courseKey.length() == 0)
        {
        	errorBox.show("Please select a valid value for subject field");
        	return;
        }
        
        Double mScore = 0.0;
        Double wgt = 0.0;
        try
        {
        	mScore = Double.parseDouble(maxScore.getText().trim()); 
        	wgt = Double.parseDouble(weight.getText().trim());
        	
        }
        catch(NumberFormatException ex)
        {
        	errorBox.show("Max score and weight must be valid numbers");
        	return;
        }
        
        if(wgt < 0 || mScore < 0)
        {
        	errorBox.show("Max score and weight must be positive numbers");
        	return;
        }
        
        WidgetHelper.setNormalStatus(status, "Attemting to add " + tName + 
        		" to " + courseName + ". Please wait ...");
        scoreManager.addTest(courseKey, tName, mScore, wgt, saveTestCallback);
    }


    interface AddTestsPanelUiBinder extends UiBinder<Widget, AddTestsPanel> {
    }

    public AddTestsPanel() {
        initWidget(uiBinder.createAndBindUi(this));

        //instantiate helper objs
        errorBox = new SimpleDialog("An error occured");
        //cachedTotalScores = new HashMap<String, Double>();
        //add handlers
        saveTest.addClickHandler(this);
        classAndSubject.addValueChangeHandler(this);
        classAndSubject.addValueChangeHandlerForTopLevel(this);
        classAndSubject.initData();
        //classAndSubject.addValueChangeHandler(this);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		maxScore.setValue(null);
		weight.setValue(null);
		testName.setValue(null);
	}
}