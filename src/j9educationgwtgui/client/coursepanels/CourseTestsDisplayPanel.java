package j9educationgwtgui.client.coursepanels;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelAndCourseBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class CourseTestsDisplayPanel extends Composite implements ValueChangeHandler<String>, DoneLoadingWidget
{
    private ScoreManagerAsync scoreService =
        GWT.create(ScoreManager.class); 
    
	@UiField
	LevelAndCourseBox classAndSubject;
	
	@UiField
	Label status;
	
	@UiField
	VerticalPanel content;

    final AsyncCallback<ArrayList<TableMessage>> testCallback =
        new AsyncCallback<ArrayList<TableMessage>>() 
    {
	    @Override
	    public void onFailure(Throwable caught) {
	    	WidgetHelper.checkForLoginError(caught);
	        WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
	    }
	
	    @Override
	    public void onSuccess(ArrayList<TableMessage> result) {
	        TableMessageHeader header = (TableMessageHeader) result.remove(0);
	        Label title = new Label(header.getCaption());
	        title.addStyleName(GUIConstants.STYLE_PS_TABLE_CAPTION);
	        //title.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
	        content.add(title);
	        Iterator<TableMessage> testInfoItr = result.iterator();
	        while(testInfoItr.hasNext())
	        	content.add(getTestTable(testInfoItr.next(), header));
	        WidgetHelper.setSuccessStatus(status, "Displaying " + result.size() + " tests", false);
	        content.setVisible(true);
	    }
    };	
	
	private static CourseTestsDisplayPanelUiBinder uiBinder = GWT
			.create(CourseTestsDisplayPanelUiBinder.class);

	interface CourseTestsDisplayPanelUiBinder extends
			UiBinder<Widget, CourseTestsDisplayPanel> {
	}

	public CourseTestsDisplayPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		classAndSubject.addValueChangeHandler(this);
		content.setVisible(false);
		content.setWidth("100%");
		content.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);	
		classAndSubject.initData();

	}

	public FlexTable getTestTable(TableMessage m, TableMessageHeader h)
	{
		FlexTable table = new FlexTable();
		table.setWidth("60%");
		boolean isPrimaryColor = false;
		table.addStyleName(GUIConstants.STYLE_STND_TABLE);
		//table.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
		//table.setCellSpacing(0);
		//table.setBorderWidth(1);
		//table.setBorderWidth(1);
		//table.setCellPadding(10);
		table.getColumnFormatter().setWidth(0, "50%");
		for(int i = 0; i < h.getNumberOfTextFields(); i++)
		{
			isPrimaryColor = !isPrimaryColor;
			if(isPrimaryColor)
				table.getRowFormatter().addStyleName(i, GUIConstants.STYLE_HORIZ_PRIM_CLR);
			else
				table.getRowFormatter().addStyleName(i, GUIConstants.STYLE_HORIZ_SCND_CLR);
			table.setText(i, 0, h.getText(i));
			table.setText(i, 1, m.getText(i));
			table.getCellFormatter().addStyleName(i, 0, GUIConstants.STYLE_HORIZ_HEADER);
		}
		return table;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		content.clear();
		WidgetHelper.setNormalStatus(status, "Please wait, loading test/exam information for selected subject");
		scoreService.getCourseTestsTable(event.getValue(), testCallback);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classAndSubject.doneLoading();
	}
	
}
