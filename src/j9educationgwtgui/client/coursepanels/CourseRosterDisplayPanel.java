package j9educationgwtgui.client.coursepanels;

import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelAndCourseBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class CourseRosterDisplayPanel extends Composite implements ClickHandler, DoneLoadingWidget{
	
	@UiField 
	Label status;
	
	@UiField
	SimplePanel tableSlot;
	
	@UiField
	LevelAndCourseBox classAndSubject;
	
	@UiField
	Button display;
	
	private PsTable table;
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<ArrayList<TableMessage>> rosterCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
        }

        @Override
        public void onSuccess(ArrayList<TableMessage> result) {
            displayTable(result);
            WidgetHelper.setSuccessStatus(status, "loaded roster successfully", false);
        }
    };

    private void displayTable(ArrayList<TableMessage> result)
    {
        TableMessageHeader header = (TableMessageHeader) result.remove(0);
        //setup table if it hasn't been setup yet
        if(table == null)
        {
            table = new PsTable(header);
            tableSlot.add(table);
        }
        tableSlot.setVisible(true);
        table.showMessages(result, header.getCaption() + " roster");
    }	

	private static CourseRosterDisplayPanelUiBinder uiBinder = GWT
			.create(CourseRosterDisplayPanelUiBinder.class);

	interface CourseRosterDisplayPanelUiBinder extends
			UiBinder<Widget, CourseRosterDisplayPanel> {
	}

	public CourseRosterDisplayPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		tableSlot.setWidth("100%");
		display.addClickHandler(this);
		classAndSubject.initData();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		WidgetHelper.setNormalStatus(status, "Fetching data for " + classAndSubject.getCourseName() + " - " + 
				classAndSubject.getClassName() + ". Please wait ...");
		tableSlot.setVisible(false);
		classAndSubject.getClassService().
			getCourseRosterTable(classAndSubject.getCourseValue(), rosterCallback);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classAndSubject.doneLoading();
	}
}
