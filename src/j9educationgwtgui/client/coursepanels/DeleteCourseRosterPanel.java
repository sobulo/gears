/**
 * 
 */
package j9educationgwtgui.client.coursepanels;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.LevelAndCourseStudentRosterBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import java.util.Collection;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class DeleteCourseRosterPanel extends Composite 
	implements ClickHandler
{
	
	@UiField
	Label status;
	
	@UiField
	Button delete;
	
	@UiField
	LevelAndCourseStudentRosterBox studentBox;
	
	DialogBox alertBox;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> deleteCallback =
            new AsyncCallback<Void>() {
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
        }

        public void onSuccess(Void result) {
            WidgetHelper.setSuccessStatus(status, "Successfully removed students from subject", true);
        }
    };	

	private static DeleteCourseRosterPanelUiBinder uiBinder = GWT
			.create(DeleteCourseRosterPanelUiBinder.class);

	interface DeleteCourseRosterPanelUiBinder extends
			UiBinder<Widget, DeleteCourseRosterPanel> {
	}

	public DeleteCourseRosterPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		studentBox.setDisplayHeight(GUIConstants.DEFAULT_LB_VISIBLE_COUNT);
		delete.addClickHandler(this);
		alertBox = new DialogBox(true);
		alertBox.setHTML("<b>Confirm Removal from Subject</b>");
        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);			
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		HashMap<String, String> selectedStudents = studentBox.getAllSelectedStudents();
		Collection<String> selectedNames = selectedStudents.values();
		String courseKey = studentBox.getSelectedCourse();
		String courseName = studentBox.getSelectedCourseName() + " / " + studentBox.getSelectedClassName();
		studentBox.removeCacheEntry(courseKey);
		
		if(selectedNames.size() == 0)
			return;
		
		VerticalPanel contentPanel = new VerticalPanel();
		
		String msg = "Are you sure you want to remove the " + selectedNames.size() + 
			" students below from " + courseName + "?";
		contentPanel.add(new Label(msg));
		HorizontalPanel buttonPanel = getAlertBoxButtonPanel(selectedStudents.keySet(), 
				courseKey, courseName);
		contentPanel.add(buttonPanel);
		contentPanel.setCellHorizontalAlignment(buttonPanel, HorizontalPanel.ALIGN_CENTER);
		for(String s : selectedNames)
			contentPanel.add(new Label(s));
        
		//display popup box
		alertBox.clear();
		alertBox.setWidget(contentPanel);
        alertBox.show();
        alertBox.center();
        GWT.log(msg);
	}
	
	private HorizontalPanel getAlertBoxButtonPanel(final Collection<String> studKeys, 
			final String courseKey, final String courseName)
	{
		HorizontalPanel panel = new HorizontalPanel();
		Button yes = new Button("  Yes  ");
		Button no = new Button("  No  ");
		yes.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
				WidgetHelper.setNormalStatus(status, "Attempting to remove " + studKeys.size() + 
						" students from " + courseName + ". Please wait ...");
				studentBox.getClassService().removeStudentsFromCourse(courseKey, 
						studKeys.toArray(new String[studKeys.size()]), deleteCallback);
			}
		});
		
		no.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();				
			}
		});
		
		panel.add(yes);
		panel.add(new Label("     "));
		panel.add(no);
		return panel;
	}
}
