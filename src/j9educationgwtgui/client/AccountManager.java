/**
 * 
 */
package j9educationgwtgui.client;

import j9educationgwtgui.shared.BillDescriptionItem;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author Segun Razaq Sobulo
 *
 */
@RemoteServiceRelativePath("accountmanager")
public interface AccountManager extends RemoteService {
	public String createBillTemplate(String name, String year, String term, Date dueDate, 
			LinkedHashSet<BillDescriptionItem> itemizedBill) throws DuplicateEntitiesException, LoginValidationException;
	
	public HashMap<String, String> getBillTemplateList() throws LoginValidationException;
	
	public String createUserBill(String billTemplate, String[] users) throws LoginValidationException;
	
	public List<TableMessage> getStudentBills(String studentID) throws LoginValidationException;
	
	public HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin) throws LoginValidationException;
	
	public HashMap<String, String> getStudentBillKeys() throws LoginValidationException;
	
	public HashMap<String, String> getStudentBillKeysFromTemplate(String templateKeyStr) throws LoginValidationException, MissingEntitiesException;
	
	public String savePayment(String billKey, double amount, Date payDate, String referenceID, 
			String comments) throws MissingEntitiesException, ManualVerificationException, LoginValidationException;
	
	public List<TableMessage> getBillTemplateInfo(String billTemplateKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	public List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	public List<TableMessage> getBillsForTemplate(String billDescKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	public List<TableMessage> getBillPayments(String billKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	public String getInvoiceDownloadLink(String billTemplateKeyStr, String[] billKeyStrs) throws MissingEntitiesException, LoginValidationException;
}
