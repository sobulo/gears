package j9educationgwtgui.client;

import j9educationgwtgui.shared.CustomMessageTypes;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("MessagingManager")
public interface MessagingManager extends RemoteService {
	List<TableMessage> getStudentParentContact(CustomMessageTypes msgType, String keyStr, boolean includeParents) 
		throws MissingEntitiesException;
	
	public void sendMessage(CustomMessageTypes msgType, String[] addresses, String[] message, boolean isEmail)
		throws MissingEntitiesException, ManualVerificationException, LoginValidationException;
	
	public HashMap<String, String> getMessagingControllerNames();
	
	public List<TableMessage> getControllerDetails(String controllerName) throws MissingEntitiesException;
	
	String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header);	
}
