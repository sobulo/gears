package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.GuardianSuggestBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.custom.widgets.YesNoDialog;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class StudentGuardianPanel extends Composite implements
		ValueChangeHandler<String>, ClickHandler {
	@UiField
	GuardianSuggestBox guardianBox;

	@UiField
	StudentSuggestBox studentBox;

	@UiField
	ListBox guardianDisplay;

	@UiField
	ListBox studentDisplay;

	@UiField
	Button update;

	@UiField
	Label status;

	private SimpleDialog alertBox;
	private YesNoDialog confirmBox;
	private UserManagerAsync userService;

	private static HashMap<String, HashMap<String, String>> guardianCache = new HashMap<String, HashMap<String, String>>();
	private static HashMap<String, HashMap<String, String>> studentCache = new HashMap<String, HashMap<String, String>>();
	private String lastSelectedGuardian, lastSelectedStudent;
	
	private static StudentGuardianPanelUiBinder uiBinder = GWT
			.create(StudentGuardianPanelUiBinder.class);

	interface StudentGuardianPanelUiBinder extends
			UiBinder<Widget, StudentGuardianPanel> {
	}

	public StudentGuardianPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		alertBox = new SimpleDialog("An error occured");
		confirmBox = new YesNoDialog("Confirm Guardian/Student pairing");
		studentBox.addValueChangeHandler(this);
		guardianBox.addValueChangeHandler(this);
		guardianDisplay.setEnabled(false);
		guardianDisplay.setVisibleItemCount(6);
		guardianDisplay.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
		studentDisplay.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
		studentDisplay.setVisibleItemCount(3);
		studentDisplay.setEnabled(false);
		update.addClickHandler(this);
		userService = studentBox.getUserService();
	}
	
	final AsyncCallback<Void> updateCallback = new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			String errMsg = "Error loading: " + caught.getMessage();
			alertBox.show(errMsg);
			WidgetHelper.setErrorStatus(status, errMsg, false);
		}

		@Override
		public void onSuccess(Void result) {
			WidgetHelper.setSuccessStatus(status, "Guardian update successful", true);
			userService.getGuardianChildren(lastSelectedGuardian, childrenCallback);
			userService.getStudentParents(lastSelectedStudent, parentCallback);
			
		}
	};	

	final AsyncCallback<HashMap<String, String>> childrenCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			GWT.log("in guardian fail callback");
			guardianDisplay.clear();
			String errMsg = "Error loading: " + caught.getMessage();
			alertBox.show(errMsg);
			WidgetHelper.setErrorStatus(status, errMsg, false);
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			GWT.log("in guardian succs callback");
			populateGuardianBox(result);
			guardianCache.put(lastSelectedGuardian, result);
		}
	};

	final AsyncCallback<HashMap<String, String>> parentCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			GWT.log("in stud fail callback");
			studentDisplay.clear();
			String errMsg = "Error loading: " + caught.getMessage();
			alertBox.show(errMsg);
			WidgetHelper.setErrorStatus(status, errMsg, false);
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			GWT.log("in stud succs callback");
			populateStudentBox(result);
			studentCache.put(lastSelectedStudent, result);
		}
	};
	
	public void populateGuardianBox(HashMap<String, String> result)
	{
		guardianDisplay.clear();
		Iterator<Entry<String, String>> childrenVals = result.entrySet()
				.iterator();
		while (childrenVals.hasNext()) {
			Entry<String, String> val = childrenVals.next();
			guardianDisplay.addItem(val.getValue(), val.getKey());
		}		
	}
	
	public void populateStudentBox(HashMap<String, String> result)
	{
		studentDisplay.clear();
		Iterator<Entry<String, String>> parentVals = result.entrySet()
				.iterator();
		while (parentVals.hasNext()) {
			Entry<String, String> val = parentVals.next();
			studentDisplay.addItem(val.getValue(), val.getKey());
		}		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(
	 * com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) 
	{
		if (event.getSource().equals(guardianBox))
		{
			GWT.log("in guardan box call");
			lastSelectedGuardian = event.getValue();
			if(!guardianCache.containsKey(lastSelectedGuardian))
			{
				GWT.log("Cache Miss!");
				userService.getGuardianChildren(lastSelectedGuardian, childrenCallback);
			}
			else
			{
				GWT.log("Cache Hit!!");
				populateGuardianBox(guardianCache.get(lastSelectedGuardian));
			}
		}
		else if (event.getSource().equals(studentBox))
		{
			GWT.log("in student box call");
			lastSelectedStudent = event.getValue();
			if(!studentCache.containsKey(lastSelectedStudent))
			{
				GWT.log("Cache Miss!");
				userService.getStudentParents(lastSelectedStudent, parentCallback);
			}
			else
			{
				GWT.log("Cache Hit!!");
				populateStudentBox(studentCache.get(lastSelectedStudent));
			}			
		}
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		final String guardName = guardianBox.getSelectedUserDisplay();
		final String studName = studentBox.getSelectedUserDisplay();
		lastSelectedGuardian = guardianBox.getSelectedUser();
		lastSelectedStudent = studentBox.getSelectedUser();
		
		if(guardName == null)
		{
			alertBox.show("Please select a valid guardian/parent name");
			return;
		}
		
		if(studName == null)
		{
			alertBox.show("Please select a valid student name");
			return;
		}
		
		
		confirmBox.setClickHandler(new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				WidgetHelper.setNormalStatus(status, "Attempting to set " + guardName + " as " +
						"parent/guardian of " + studName + ". Please wait ...");
				userService.addGuardianToStudent(lastSelectedGuardian, lastSelectedStudent, updateCallback);
				confirmBox.hide();
			}
		});
		
		String message = "Are you sure you want to set " + guardName + " as " +
				"parent/guardian of " + studName;
		
		confirmBox.show(message);		
	}

}