/**
 * 
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.MultipleMessageDialog;
import j9educationgwtgui.client.custom.widgets.UserPanel;
import j9educationgwtgui.client.custom.widgets.UserSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;

import java.util.ArrayList;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public abstract class UpdateUserPanel extends Composite 
	implements ValueChangeHandler<String>, ClickHandler
{
	//abstract methods
	protected abstract UserSuggestBox getSuggestBox();
	
	@UiField
	SimplePanel userSlot;
	@UiField
	Button save;
	@UiField
	Label status;
	@UiField
	UserPanel userInfo;
	
	private MultipleMessageDialog errorBox;
	
	private static UpdateUserPanelUiBinder uiBinder = GWT
			.create(UpdateUserPanelUiBinder.class);

	interface UpdateUserPanelUiBinder extends
			UiBinder<Widget, UpdateUserPanel> {
	}

	private UserSuggestBox userBox;

	private UserManagerAsync userService = GWT.create(UserManager.class);

	final AsyncCallback<TableMessage> infoCallback = new AsyncCallback<TableMessage>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
		}

		@Override
		public void onSuccess(TableMessage result) {
			userInfo.setLoginName(result.getText(0));
			userInfo.setFirstName(result.getText(1));
			userInfo.setLastName(result.getText(2));
			userInfo.setEmail(result.getText(3));
			HashSet<String> phoneNumVals = new HashSet<String>();
			for(int i = 4; i < result.getNumberOfTextFields(); i++)
				phoneNumVals.add(result.getText(i));
			userInfo.setPhoneNumbers(phoneNumVals);
			WidgetHelper.setNormalStatus(status, "Retrieved information for user: " + result.getText(0));
		}
	};
	
	final AsyncCallback<Void> updateCallback = new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
		}

		@Override
		public void onSuccess(Void result) {
			WidgetHelper.setSuccessStatus(status, "Successfully updated user information", true);
		}
	};	
	
	public UpdateUserPanel(boolean isAdmin) {
		initWidget(uiBinder.createAndBindUi(this));
		userInfo.disableLoginGenerator();
		userInfo.disablePasswords();
		save.addClickHandler(this);
		setupPanel(isAdmin);
		errorBox = new MultipleMessageDialog("Invalid Input");
	}

	private void enableUserSelection() {	
		userBox = getSuggestBox();
		userBox.addValueChangeHandler(this);	
		userSlot.add(userBox);
	}

	private void setupPanel(boolean isAdmin) {
		if (isAdmin)
			enableUserSelection();
		else 
		{
			userSlot.setVisible(false);
			userInfo.disableNameEdits();
			userService.getUserInfo(infoCallback);
		}
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		userInfo.clear();
		WidgetHelper.setNormalStatus(status, "Fetching user information. Please wait ...");
		userService.getUserInfo(event.getValue(), userBox.getRole(), infoCallback);		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
        //validate fields
        ArrayList<String> errorStrings = userInfo.validateFields();

        if(errorStrings.size() != 0)
        {
            errorBox.show("Please fix issues listed below: ", errorStrings);
            return;
        }

        //get values
        String fname = userInfo.getFirstName();
        String lname = userInfo.getLastName();
        String eml = userInfo.getEmail();
        String lgName = userInfo.getLoginName();

        HashSet<String> phNums = userInfo.getPhoneNumbers();

        //persist values on server
        WidgetHelper.setNormalStatus(status, "Contacting server to update user info. Please wait ...");
        if(userBox != null)
        	userService.updateUser(fname, lname, eml, phNums, lgName, userBox.getRole(), 
        			updateCallback);
        else
        	userService.updateUser(fname, lname, eml, phNums, updateCallback);
		
	}
}
