/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.MultipleMessageDialog;
import j9educationgwtgui.client.custom.widgets.UserPanel;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 *
 * @author Administrator
 */
public class AddStudentPanel extends Composite implements ClickHandler, DoneLoadingWidget
{

    private static AddStudentPanelUiBinder uiBinder = GWT.create(AddStudentPanelUiBinder.class);

    private ClassRosterServiceAsync classService;

    @UiField DateBox dateOfBirth;
    @UiField LevelBox classList;
    @UiField Button saveStudent;
    @UiField Label status;
    @UiField UserPanel userInfo;

    private MultipleMessageDialog errorBox;
    private int duplicateCount = 0;
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> studentCallback = new AsyncCallback<String>() {
        @Override
        public void onSuccess(String result) {
            userInfo.clear();
            dateOfBirth.setValue(null);
            duplicateCount = 0;
            WidgetHelper.setSuccessStatus(status, "created student [" + result + "] succesfully", true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
        	String msg = "Request failed: " + caught.getMessage();
        	if(caught instanceof DuplicateEntitiesException)
        	{
        		userInfo.setLoginName(userInfo.getLoginName() + (++duplicateCount));
        		msg = "Duplicate exception. A number has been added to the login-name. " +
        				"Please try resaving if you're sure this is a different student you're trying to add." + msg;
        		errorBox.show(msg);
        		WidgetHelper.setErrorStatus(status, msg, false);
        	}
        	else
        		WidgetHelper.setErrorStatus(status, msg, true);
            
        }
    };

    public void onClick(ClickEvent event) {
        //validate fields
        ArrayList<String> errorStrings = userInfo.validateFields();

        if(errorStrings.size() != 0)
        {
            errorBox.show("Please fix the following issues: ", errorStrings);
            return;
        }

        //get values
        Date dob = dateOfBirth.getValue();
        String fname = userInfo.getFirstName();
        String lname = userInfo.getLastName();
        String eml = userInfo.getEmail();
        String lgName = userInfo.getLoginName();
        String pword = userInfo.getPassword();

        HashSet<String> phNums = userInfo.getPhoneNumbers();

        //todo, handle index out of bound issue
        String classKey = classList.getSelectedClass();

        //persist values on server
        WidgetHelper.setNormalStatus(status, "Contacting server to save student information. Please wait ...");
        classService.addStudent(classKey, fname, lname, dob, eml, phNums, lgName,
                pword, studentCallback);
        
    }

    interface AddStudentPanelUiBinder extends UiBinder<Widget, AddStudentPanel> {
    }

    public AddStudentPanel() {
        initWidget(uiBinder.createAndBindUi(this));
        classService = classList.getClassService();
        classList.initData();
        dateOfBirth.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
        dateOfBirth.setValue(null);
        saveStudent.addClickHandler(this);
        errorBox = new MultipleMessageDialog("Invalid input");
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classList.doneLoading();
	}
}