/**
 * 
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.custom.widgets.YesNoDialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PasswordChangePanel extends Composite implements ClickHandler
{	
	@UiField
	PasswordTextBox oldPassword;
	
	@UiField
	PasswordTextBox newPassword;
	
	@UiField
	PasswordTextBox confirmPassword;
	
	@UiField
	Button change;
	
	@UiField
	Label status;
	
	private YesNoDialog confirmBox;
	//private SimpleDialog errorBox, infoBox;
    private UserManagerAsync userService =
        GWT.create(UserManager.class);
    
    private final static int MIN_PASSWORD_LENGTH = 4;
    
	final AsyncCallback<String> changeCallback = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			String errMsg = "Request failed: " + caught.getMessage();
			WidgetHelper.setErrorStatus(status, errMsg, true);
		}

		@Override
		public void onSuccess(String result) {
			String message = "Password change completed for: " + result;
			WidgetHelper.setSuccessStatus(status, message, true);
		}
	};    
    
	private static PasswordChangePanelUiBinder uiBinder = GWT
			.create(PasswordChangePanelUiBinder.class);

	interface PasswordChangePanelUiBinder extends
			UiBinder<Widget, PasswordChangePanel> {
	}

	public PasswordChangePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		confirmBox = new YesNoDialog("Confirm password change");
		change.addClickHandler(this);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) 
	{
		boolean success = validateInputLength(oldPassword.getValue(), "Old password") &&
						  validateInputLength(newPassword.getValue(), "New password") &&
						  validateInputLength(confirmPassword.getValue(), "Confirm password") &&
						  validateNewPassword() && validateDifferent();
		
		if(success)
		{
			confirmBox.setClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					WidgetHelper.setNormalStatus(status, "Requesting password change. Please wait ...");
					userService.changePassword(oldPassword.getValue(), 
							newPassword.getValue(), changeCallback);
					confirmBox.hide();
				}
			});		
			confirmBox.show("Are you sure you want to change your password?");
		}
	}
	
	public boolean validateInputLength(String input, String fieldName)
	{
		if(input.length() < MIN_PASSWORD_LENGTH)
		{
			WidgetHelper.setErrorStatus(status, fieldName + " field must be at least " + MIN_PASSWORD_LENGTH + " characters long", true);
			return false;
		}
		return true;
	}
	
	public boolean validateNewPassword()
	{
		if(newPassword.getValue().equals(confirmPassword.getValue()))
			return true;
		else
		{
			WidgetHelper.setErrorStatus(status, "New/Confirm password fields do not match!", true);
			return false;
		}
	}
	
	public boolean validateDifferent()
	{
		if(!newPassword.getValue().equals(oldPassword.getValue()))
			return true;
		else
		{
			WidgetHelper.setErrorStatus(status, "Ignoring change request, new password same as the old one", true);
			return false;
		}		
	}

}
