/**
 * 
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.custom.widgets.LevelCourseTeacherBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.TeacherSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.custom.widgets.YesNoDialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class CourseTeacherMappings extends Composite implements ClickHandler{
	@UiField TeacherSuggestBox teacherPicker;
	@UiField LevelCourseTeacherBox courseTeacher;
	@UiField Button act;
	@UiField CheckBox isAdd;
	@UiField Label status;
	
	private SimpleDialog errorBox;
	private YesNoDialog confirmAction;
	private final static String ADD_LABEL = "Add Teacher to Subject";
	private final static String REMOVE_LABEL = "Remove Teacher From Subject";
	
	private static CourseTeacherMappingsUiBinder uiBinder = GWT
			.create(CourseTeacherMappingsUiBinder.class);

	interface CourseTeacherMappingsUiBinder extends
			UiBinder<Widget, CourseTeacherMappings> {
	}

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> actCallback =
            new AsyncCallback<String>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            errorBox.show(caught.getMessage());
            WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
        }

        @Override
        public void onSuccess(String result)
        {
        	WidgetHelper.setSuccessStatus(status, result, true);
        }
    };  
    
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public CourseTeacherMappings() {
		initWidget(uiBinder.createAndBindUi(this));
		act.setWidth("150px");
		act.setText(ADD_LABEL);
		isAdd.setValue(true);
		isAdd.addClickHandler(this);
		act.addClickHandler(this);
		errorBox = new SimpleDialog("Error, save ignored!");
		confirmAction = new YesNoDialog("Confirm Teacher-Subject Update");
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() == isAdd)
		{
			teacherPicker.setEnabled(isAdd.getValue());
			act.setText(isAdd.getValue()?ADD_LABEL:REMOVE_LABEL);
		}
		else
		{
			final String teacherLogin;
			if(isAdd.getValue())
			{
				if(!courseTeacher.getDisplayedTeacher().equals(""))
				{
					errorBox.show("Can't add teacher since one already exists for this subject. Clear the checkbox above and remove current teacher first");
					return;
				}
				else if(teacherPicker.getSelectedUser() == null)
				{
					errorBox.show("Please select a valid teacher to add to subject");
					return;
				}
				teacherLogin = teacherPicker.getSelectedUser();
			}
			else
			{
				if(courseTeacher.getDisplayedTeacher().equals(""))
				{
					errorBox.show("Ignoring request to remove teacher from subject because subject currently has no teacher specified");
					return;
				}
				teacherLogin = courseTeacher.getDisplayedTeacher();
			}
			confirmAction.setClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					WidgetHelper.setNormalStatus(status, "Requesting update of teacher, " + teacherLogin + 
							", information for subject " + courseTeacher.getSelectedCourseName() + ". Please wait ...");
					teacherPicker.getUserService().setCourseTeacherMapping(courseTeacher.getSelectedCourse(), teacherLogin, isAdd.getValue(), actCallback);
					confirmAction.hide();
				}
			});
			confirmAction.show("Are you sure you want to " + (isAdd.getValue()?"set":"remove") + 
					" teacher " + teacherLogin + " for subject " + courseTeacher.getSelectedCourseName());
			
		}
	}

}
