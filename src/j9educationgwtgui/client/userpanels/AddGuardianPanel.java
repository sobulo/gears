/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelStudentRosterBox;
import j9educationgwtgui.client.custom.widgets.MultipleMessageDialog;
import j9educationgwtgui.client.custom.widgets.UserPanel;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;

import java.util.ArrayList;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class AddGuardianPanel extends Composite implements DoneLoadingWidget,
        ClickHandler
{
    @UiField LevelStudentRosterBox studentListBox;
    @UiField Label status;
    @UiField Button save;
    @UiField UserPanel userInfo;
    
    private MultipleMessageDialog errorBox;
    private int duplicateCount = 0;
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> saveCallback =
            new AsyncCallback<String>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
        	String msg = "Request failed: " + caught.getMessage();
        	if(caught instanceof DuplicateEntitiesException)
        	{
        		userInfo.setLoginName(userInfo.getLoginName() + (++duplicateCount));
        		msg = "Duplicate exception. A number has been added to the login-name. " +
        				"Please try resaving if you're sure this is a different guardian/parent you're trying to add." + msg;
        		errorBox.show(msg);
        		WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
        	}
        	else
        		WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
        }

        @Override
        public void onSuccess(String result) {
            WidgetHelper.setSuccessStatus(status, "Added: " + result, true);
        }
    };


    private static AddGuardianPanelUiBinder uiBinder = GWT.create(AddGuardianPanelUiBinder.class);

    public AddGuardianPanel() {
        initWidget(uiBinder.createAndBindUi(this));
        save.addClickHandler(this);
        errorBox = new MultipleMessageDialog("Invalid Input");
    }


    @Override
    public void onClick(ClickEvent event) {
    	ArrayList<String> errors = userInfo.validateFields();
    	if(errors.size() != 0)
    	{
    		errorBox.show("Please fix issues below: ", errors);
    		return;
    	}
        String fname = userInfo.getFirstName();
        String lname = userInfo.getLastName();
        String email = userInfo.getEmail();
        HashSet<String> phoneNum = userInfo.getPhoneNumbers();
        String password = userInfo.getPassword();
        String loginName = userInfo.getLoginName();
        String studKey = studentListBox.getSelectedStudent();
        WidgetHelper.setNormalStatus(status, "Requesting addition of new parent/guardian. Please wait ...");
        studentListBox.getClassService().addGuardian(studKey, fname, lname, email, phoneNum,
                loginName, password, saveCallback);
    }

    interface AddGuardianPanelUiBinder extends UiBinder<Widget, AddGuardianPanel> {
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return studentListBox.doneLoading();
	}
}