package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.GuardianSuggestBox;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.UserSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.custom.widgets.YesNoDialog;
import j9educationgwtgui.shared.PanelServiceLoginRoles;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class DeleteUserPanel extends Composite implements ClickHandler{

	@UiField
	Label status;
	
	@UiField
	SimplePanel userSlot;
	
	@UiField
	Button delete;
	
	private YesNoDialog confirmBox;
	private UserManagerAsync userService;
	private UserSuggestBox userBox;
	
	private static DeleteUserPanelUiBinder uiBinder = GWT
			.create(DeleteUserPanelUiBinder.class);

	interface DeleteUserPanelUiBinder extends
			UiBinder<Widget, DeleteUserPanel> {
	}

	final AsyncCallback<String> deleteCallback = new AsyncCallback<String>() {
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			String message = "Request failed: " + caught.getMessage();
			WidgetHelper.setErrorStatus(status, message, true);
		}

		public void onSuccess(String result) {
			WidgetHelper.setSuccessStatus(status, "Successfully deleted user records for: " + result, true);
		}
	};	
	
	public DeleteUserPanel(PanelServiceLoginRoles userType) {
		initWidget(uiBinder.createAndBindUi(this));		
		
		//setup dialog boxes
		confirmBox = new YesNoDialog("Confirm deletion");

		//setup userbox
		if(PanelServiceLoginRoles.ROLE_STUDENT.equals(userType))
			userBox = new StudentSuggestBox();
		else if(PanelServiceLoginRoles.ROLE_GUARDIAN.equals(userType))
			userBox = new GuardianSuggestBox();
		else
			throw new UnsupportedOperationException("Unsupported type: " + userType);
		
		userService = userBox.getUserService();
		
		userSlot.add(userBox);
		
		//setup button and service
		delete.addClickHandler(this);
		
	}
	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) 
	{
		final String userName = userBox.getSelectedUserDisplay();
		final String userKey = userBox.getSelectedUser();
		
		if(userKey == null)
		{
			WidgetHelper.setErrorStatus(status,"Please select a valid user value", true);
			return;
		}
		
		confirmBox.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				WidgetHelper.setNormalStatus(status, "Requesting deletion of " + userName + ". Please wait ...");
				if(userBox.getRole().equals(PanelServiceLoginRoles.ROLE_STUDENT))
					userService.deleteStudent(userKey, deleteCallback);
				else if(userBox.getRole().equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
					userService.deleteGuardian(userKey, deleteCallback);
				confirmBox.hide();
			}
		});
		
		confirmBox.show("Are you sure you want to delete: " + userName);
	}

}
