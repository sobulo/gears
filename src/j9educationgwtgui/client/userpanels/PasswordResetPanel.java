/**
 * 
 */
package j9educationgwtgui.client.userpanels;


import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.GuardianSuggestBox;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.TeacherSuggestBox;
import j9educationgwtgui.client.custom.widgets.UserSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.custom.widgets.YesNoDialog;
import j9educationgwtgui.shared.PanelServiceLoginRoles;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PasswordResetPanel extends Composite implements ClickHandler
{
	private UserSuggestBox userBox;
	private Label status;
	private YesNoDialog confirmBox;
	private Button reset;
	//private SimpleDialog errorBox;
	//private SimpleDialog infoBox;
	private UserManagerAsync userService;
	
	final AsyncCallback<String> resetCallback = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			String errMsg = "Request failed: " + caught.getMessage();
			WidgetHelper.setErrorStatus(status, errMsg, true);
		}

		@Override
		public void onSuccess(String result) {
			String message = "Password reset comlpeted. New password is: " + result;
			WidgetHelper.setSuccessStatus(status, message, true);
		}
	};		
	
	public PasswordResetPanel(PanelServiceLoginRoles userType)
	{
		VerticalPanel contentPanel = new VerticalPanel();
		initWidget(contentPanel);
		
		//initialize suggest box based on user type
		if(userType.equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
			userBox = new GuardianSuggestBox();
		else if(userType.equals(PanelServiceLoginRoles.ROLE_STUDENT))
			userBox = new StudentSuggestBox();
		else if(userType.equals(PanelServiceLoginRoles.ROLE_TEACHER))
			userBox = new TeacherSuggestBox();
		else
			throw new IllegalArgumentException("Invalid user type: " + userType);
		
		//setup user service
		userService = userBox.getUserService();		
		
		//initialize other widgets
		SimplePanel statusContainer = new SimplePanel();
		VerticalPanel userContainer = new VerticalPanel();
		status = new Label("Click button above to reset password");
		reset = new Button("Reset Password");
		reset.addClickHandler(this);
		
		
		//set style
		statusContainer.addStyleName(GUIConstants.STYLE_STATUS_BOX);
		userContainer.addStyleName(GUIConstants.STYLE_PADDED_BORDER);
		reset.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
		contentPanel.setWidth("100%");
		userContainer.setWidth("100%");
		statusContainer.setWidth("100%");
		
		//add widgets to content area
		statusContainer.add(status);
		userContainer.add(userBox);		
		userContainer.add(reset);
		contentPanel.add(userContainer);
		contentPanel.add(statusContainer);		
		
		//initialize dialog boxes
		confirmBox = new YesNoDialog("Confirm Password Reset");
	}
	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		final String userID = userBox.getSelectedUser();
		final String userName = userBox.getSelectedUserDisplay();
		final PanelServiceLoginRoles role = userBox.getRole();
		
		if(userID == null)
		{
			WidgetHelper.setErrorStatus(status, "Please select a valid user", true);
			return;
		}
			
		confirmBox.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				WidgetHelper.setNormalStatus(status, "Requesting password reset for user: " + userName + ". Please wait");
				userService.resetPassword(userID, role, resetCallback);
				confirmBox.hide();
			}
		});
		
		confirmBox.show("Are you sure you want to reset password for user: " + userName);
	}

}
