/**
 * 
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.MultipleMessageDialog;
import j9educationgwtgui.client.custom.widgets.UserPanel;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;

import java.util.ArrayList;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class AddUserPanel extends Composite implements ClickHandler, DoneLoadingWidget{

    @UiField Button saveUser;
    @UiField Label status;
    @UiField UserPanel userInfo;

    private MultipleMessageDialog errorBox;
    private int duplicateCount = 0;
    
    private PanelServiceLoginRoles role;
    private UserManagerAsync userService = GWT.create(UserManager.class);
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> userCallback = new AsyncCallback<String>() {
        @Override
        public void onSuccess(String result) {
            userInfo.clear();
            duplicateCount = 0;
            WidgetHelper.setSuccessStatus(status, "created user id [" + result + "] succesfully", true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
        	
        	String msg = "Request failed: " + caught.getMessage();
        	if(caught instanceof DuplicateEntitiesException)
        	{
        		userInfo.setLoginName(userInfo.getLoginName() + (++duplicateCount));
        		msg = "Duplicate exception. A number has been added to the login-name. " +
        				"Please try resaving if you're sure this is a different user you're trying to add." + msg;
        		errorBox.show(msg);
        		WidgetHelper.setErrorStatus(status, msg, false);
        	}
        	WidgetHelper.setErrorStatus(status, msg, true);
            
        }
    };

    public void onClick(ClickEvent event) {
        //validate fields
        ArrayList<String> errorStrings = userInfo.validateFields();

        if(errorStrings.size() != 0)
        {
            errorBox.show("Please fix the following issues: ", errorStrings);
            return;
        }

        //get values
        String fname = userInfo.getFirstName();
        String lname = userInfo.getLastName();
        String eml = userInfo.getEmail();
        String lgName = userInfo.getLoginName();
        String pword = userInfo.getPassword();

        HashSet<String> phNums = userInfo.getPhoneNumbers();
        //persist values on server
        status.setText("Contacting server to save user info. Please wait ...");
        userService.createUser(fname, lname, eml, phNums, lgName, pword, role, userCallback);
    }
	private static AddUserPanelUiBinder uiBinder = GWT
			.create(AddUserPanelUiBinder.class);

	interface AddUserPanelUiBinder extends UiBinder<Widget, AddUserPanel> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public AddUserPanel(PanelServiceLoginRoles role) {
		initWidget(uiBinder.createAndBindUi(this));
        saveUser.addClickHandler(this);
        errorBox = new MultipleMessageDialog("Invalid input");
        this.role = role;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return true;
	}

}
