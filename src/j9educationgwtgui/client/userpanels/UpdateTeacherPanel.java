package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.custom.widgets.TeacherSuggestBox;
import j9educationgwtgui.client.custom.widgets.UserSuggestBox;

public class UpdateTeacherPanel extends UpdateUserPanel {
	private static TeacherSuggestBox teachSuggestBox;

	/**
	 * @param isAdmin
	 */
	public UpdateTeacherPanel(boolean isAdmin) {
		super(isAdmin);
		if(!isAdmin)
			userInfo.disableEmailEdits();		
	}

	@Override
	protected UserSuggestBox getSuggestBox() {
		if(teachSuggestBox == null)
			teachSuggestBox = new TeacherSuggestBox();
		return teachSuggestBox;
	}

}
