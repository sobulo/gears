/**
 * 
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.GuardianSuggestBox;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.TeacherSuggestBox;
import j9educationgwtgui.client.custom.widgets.UserSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SearchUsersPanel extends Composite implements ClickHandler, DoneLoadingWidget{

	@UiField SimplePanel suggestPanel;
	@UiField SimplePanel modePanel;
	@UiField Button search;
	@UiField HTML display;
	
	private RadioButton teachMode, guardMode, studMode;
	private UserSuggestBox teachBox, guardBox, studBox;
	
	private static SearchUsersPanelUiBinder uiBinder = GWT
			.create(SearchUsersPanelUiBinder.class);

	interface SearchUsersPanelUiBinder extends
			UiBinder<Widget, SearchUsersPanel> {
	}

	private final AsyncCallback<String> searchCallback = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(display, "Unable to fetch user details due to: " + caught.getMessage(), true);
		}

		@Override
		public void onSuccess(String result) {
			display.setHTML(result);
		}
	};
	
	
	public SearchUsersPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		studBox = new StudentSuggestBox();
		setModePanel();
		studMode.setValue(true);
		setSuggestPanel(studBox);
	}

	private void setModePanel()
	{
		final String groupId = "mode";
		guardMode = new RadioButton(groupId, "Parent/Guardian");
		teachMode = new RadioButton(groupId, "Teacher");
		studMode = new RadioButton(groupId, "Student");
		guardMode.addClickHandler(this);
		teachMode.addClickHandler(this);
		studMode.addClickHandler(this);
		search.addClickHandler(this);
		HorizontalPanel modeWidgets = new HorizontalPanel();
		modeWidgets.add(studMode);
		modeWidgets.add(guardMode);
		modeWidgets.add(teachMode);
		modePanel.add(modeWidgets);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() == search)
		{
			UserSuggestBox currentBox = (UserSuggestBox) suggestPanel.getWidget();
			if(currentBox.getSelectedUser() == null)
			{
				WidgetHelper.setErrorStatus(display, "Please select a valid user name!!", true);
				return;
			}
			WidgetHelper.setNormalStatus(display, "Fetching data for " + currentBox.getSelectedUserDisplay() + ", please wait ...");
			currentBox.getUserService().getDetailedUserInfo(currentBox.getSelectedUser(), currentBox.getRole(), searchCallback);
		}
		else if(event.getSource() == studMode)
		{
			WidgetHelper.setNormalStatus(display, "Select a student then click search button");
			setSuggestPanel(studBox);
		}		
		else if(event.getSource() == guardMode)
		{
			if(guardBox == null)
			{
				WidgetHelper.setNormalStatus(display, "Fetching parent/guardian list, please wait ...");
				guardBox = new GuardianSuggestBox();
				final Timer t = new Timer() {
					final int MAX_ITERATIONS = 10;
					int currentItr = 0;
					@Override
					public void run() {
						if(++currentItr > MAX_ITERATIONS || guardBox.doneLoading())
						{
							WidgetHelper.setNormalStatus(display, "Select a parent/guardian then click search button");
							this.cancel();	
						}
					}
				};
				t.scheduleRepeating(1000);
			}
			else
				WidgetHelper.setNormalStatus(display, "Select a parent/guardian then click search button");
			
			setSuggestPanel(guardBox);
		}
		else if(event.getSource() == teachMode)
		{
			if(teachBox == null)
			{
				WidgetHelper.setNormalStatus(display, "Fetching teacher list, please wait ...");
				teachBox = new TeacherSuggestBox();
				final Timer t = new Timer() {
					final int MAX_ITERATIONS = 10;
					int currentItr = 0;
					@Override
					public void run() {
						if(++currentItr > MAX_ITERATIONS || teachBox.doneLoading())
						{
							WidgetHelper.setNormalStatus(display, "Select a teacher then click search button");
							this.cancel();	
						}
					}
				};
				t.scheduleRepeating(1000);
			}
			else
				WidgetHelper.setNormalStatus(display, "Select a teacher then click search button");
			
			setSuggestPanel(teachBox);
		}
	}
	
	private void setSuggestPanel(UserSuggestBox suggestBox)
	{
		suggestPanel.clear();
		suggestPanel.add(suggestBox);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return studBox.doneLoading();
	}
}
