package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.MultipleMessageDialog;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.UserPanel;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class UpdateStudentPanel extends Composite implements ValueChangeHandler<String>, ClickHandler{

	@UiField
	DateBox dateOfBirth;
	@UiField
	SimplePanel studentSlot;
	@UiField
	Button saveStudent;
	@UiField
	Label status;
	@UiField
	UserPanel userInfo;

	private StudentSuggestBox studentBox;
	private MultipleMessageDialog errorBox;
	private UserManagerAsync userService = GWT.create(UserManager.class);

	final AsyncCallback<TableMessage> infoCallback = new AsyncCallback<TableMessage>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
		}

		@Override
		public void onSuccess(TableMessage result) {
			userInfo.setLoginName(result.getText(0));
			userInfo.setFirstName(result.getText(1));
			userInfo.setLastName(result.getText(2));
			userInfo.setEmail(result.getText(3));
			HashSet<String> phoneNumVals = new HashSet<String>();
			for(int i = 4; i < result.getNumberOfTextFields(); i++)
				phoneNumVals.add(result.getText(i));
			userInfo.setPhoneNumbers(phoneNumVals);
			dateOfBirth.setValue(result.getDate(0));
			WidgetHelper.setSuccessStatus(status, "Successfully retrieved information for " + result.getText(0), false);
		}
	};

	final AsyncCallback<Void> updateCallback = new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
		}

		@Override
		public void onSuccess(Void result) {
			WidgetHelper.setSuccessStatus(status, "Successfully updated information", true);
		}
	};	
	
	private static UpdateStudentPanelUiBinder uiBinder = GWT
			.create(UpdateStudentPanelUiBinder.class);

	interface UpdateStudentPanelUiBinder extends
			UiBinder<Widget, UpdateStudentPanel> {
	}

	public UpdateStudentPanel(boolean isAdmin) {
		initWidget(uiBinder.createAndBindUi(this));
		userInfo.disableLoginGenerator();
		userInfo.disablePasswords();
		saveStudent.addClickHandler(this);
		setupPanel(isAdmin);
		dateOfBirth.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT)));
		errorBox = new MultipleMessageDialog("Invalid input");
	}

	private void enableStudentSelection() {
		studentBox = new StudentSuggestBox();
		studentBox.addValueChangeHandler(this);
		studentSlot.add(studentBox);
	}

	private void setupPanel(boolean isAdmin) {
		if (isAdmin)
			enableStudentSelection();
		else 
		{
			studentSlot.setVisible(false);
			userInfo.disableNameEdits();
			userInfo.disableEmailEdits();
			dateOfBirth.setEnabled(false);			
			userService.getStudentInfo(infoCallback);
		}
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		userInfo.clear();
		dateOfBirth.setValue(null);
		WidgetHelper.setNormalStatus(status, "Fetching information for selected student. Please wait ...");
		userService.getStudentInfo(event.getValue(), infoCallback);		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
        //validate fields
        ArrayList<String> errorStrings = userInfo.validateFields();

        if(errorStrings.size() != 0)
        {
            errorBox.show("Please fix issues shown below: ", errorStrings);
            return;
        }

        //get values
        Date dob = dateOfBirth.getValue();
        String fname = userInfo.getFirstName();
        String lname = userInfo.getLastName();
        String eml = userInfo.getEmail();
        String lgName = userInfo.getLoginName();

        HashSet<String> phNums = userInfo.getPhoneNumbers();

        //persist values on server
        WidgetHelper.setNormalStatus(status, "Contacting server to update student info. Please wait ...");
        if(studentBox != null)
        	userService.updateStudent(fname, lname, dob, eml, phNums, lgName, updateCallback);
        else
        	userService.updateStudent(fname, lname, dob, eml, phNums, updateCallback);
	}

}
