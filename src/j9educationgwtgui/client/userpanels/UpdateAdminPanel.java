/**
 * 
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.custom.widgets.UserSuggestBox;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class UpdateAdminPanel extends UpdateUserPanel{
	
	public UpdateAdminPanel()
	{
		super(false);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.userpanels.UpdateUserPanel#getSuggestBox()
	 */
	@Override
	protected UserSuggestBox getSuggestBox() {
		throw new UnsupportedOperationException("Editing other admins not currently supported");
	}
}
