/**
 * 
 */
package j9educationgwtgui.client.userpanels;

import j9educationgwtgui.client.custom.widgets.GuardianSuggestBox;
import j9educationgwtgui.client.custom.widgets.UserSuggestBox;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class UpdateGuardianPanel extends UpdateUserPanel{

	/**
	 * @param isAdmin
	 */
	public UpdateGuardianPanel(boolean isAdmin) {
		super(isAdmin);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.userpanels.UpdateUserPanel#getSuggestBox()
	 */
	@Override
	protected UserSuggestBox getSuggestBox() {
		return new GuardianSuggestBox();
	}
}
