/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.GradeDescription;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 *
 * @author Administrator
 */
@RemoteServiceRelativePath("schoolpanelservice")
public interface SchoolService extends RemoteService {
    public String createSchool(String name, String email, String accr, String addr,
                             String url, String phoneNumString, boolean isUpdate) 
    throws DuplicateEntitiesException, LoginValidationException, MissingEntitiesException;

    public Boolean updateAcademicYears(HashSet<String> years) throws
            MissingEntitiesException, LoginValidationException;

    public Boolean updateAcademicTerms(HashSet<String> terms) throws
            MissingEntitiesException, LoginValidationException;

    public Boolean updateClassNames(HashSet<String> classNames) throws
            MissingEntitiesException, LoginValidationException;

    public Boolean updateSubClassNames(HashSet<String> subClassNames) throws
            MissingEntitiesException, LoginValidationException;

    public Boolean updateDefaultSubjects(HashSet<String> subjectNames) throws
            MissingEntitiesException, LoginValidationException;

    public HashMap<String,HashSet<String>> getInitialOptions() throws 
            MissingEntitiesException, LoginValidationException;

    public HashMap<String,HashSet<String>> getInitialOptions(String[] 
            optionNames) throws MissingEntitiesException, LoginValidationException;

    public String createClass(String className, String subClassName,
            String year, String term) throws MissingEntitiesException,
            DuplicateEntitiesException, LoginValidationException;
    
    public String[] getSchoolInfo() throws MissingEntitiesException, LoginValidationException;

    public String[] getSimpleClassNames() throws MissingEntitiesException, LoginValidationException;

    public LoginInfo login(String requestUri) throws MissingEntitiesException,
            LoginValidationException;

    public void updateDefaultGradingScheme(LinkedHashMap<Double, GradeDescription> scheme) throws LoginValidationException;

    public String[] getDefaultGradingSchemeDescription() throws LoginValidationException;
    
    public void deleteLevel(String levelKeyStr) throws MissingEntitiesException, 
	ManualVerificationException, LoginValidationException; 
    public String[] getNotice(String id) throws MissingEntitiesException, LoginValidationException;
    public void updateNotice(String id, String title, String content) throws MissingEntitiesException, LoginValidationException;
    
	List<TableMessage> getApplicationParameter(String ID);
	String saveApplicationParameter(String id, HashMap<String, String> val)throws MissingEntitiesException;
    
}
