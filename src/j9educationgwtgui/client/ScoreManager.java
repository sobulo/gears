/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 *
 * @author Administrator
 */
@RemoteServiceRelativePath("scoremanager")
public interface ScoreManager extends RemoteService
{
    public String[] getSimpleTestNames(String courseKeyStr) throws 
            MissingEntitiesException, LoginValidationException;

    public String addTest(String courseKeyStr, String testName, Double maxScore,
            Double weight) throws MissingEntitiesException,
            DuplicateEntitiesException, LoginValidationException;

    public ArrayList<TableMessage> getCourseScoresTable(String courseKeyStr) 
            throws MissingEntitiesException, LoginValidationException;

    public ArrayList<TableMessage> getTestScoresTable(String testKeyStr) throws
            MissingEntitiesException, LoginValidationException;

    public HashMap<String, String> getTestKeys(String courseKeyStr) throws
            MissingEntitiesException, LoginValidationException;

    public ArrayList<TableMessage> getStudentScoreSummary(String loginName, String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException;

    public ArrayList<TableMessage> getStudentScoreSummary(String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException;

    public ArrayList<TableMessage> getStudentScoreDetails(String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException;

     public ArrayList<TableMessage> getStudentScoreDetails(String loginName,
             String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException;

     public ArrayList<TableMessage> getStudentScoreSummaryWithKey(String studentKeyStr, String levelKeyStr)
        throws MissingEntitiesException, LoginValidationException;

     public ArrayList<TableMessage> getStudentScoreDetailsWithKey(String studentKeyStr,
             String levelKeyStr)
            throws MissingEntitiesException, LoginValidationException;
     
     public void saveTestScores(String testKeyStr, HashMap<String, Double> testScores)
     		throws MissingEntitiesException, LoginValidationException;
     
     public ArrayList<TableMessage> getCourseTestsTable(String courseKeyStr) 
     		throws MissingEntitiesException, LoginValidationException;    
     
     public void deleteTest(String testKeyStr) throws MissingEntitiesException, LoginValidationException;
     
     public double totalTestWeight(String courseKeyStr) throws MissingEntitiesException, LoginValidationException;
     
     public ArrayList<TableMessage> getClassScores(String levelKeyStr, boolean cumulativeOnly)
     	throws MissingEntitiesException, LoginValidationException;
     
     public ArrayList<TableMessage> getClassScoresWithGPA(String levelKeyStr)
    	     	throws MissingEntitiesException, LoginValidationException;
     
     public ArrayList<TableMessage>[] getClassScoresWithCumulativeGPA(String[] levelStrs)
 	     	throws MissingEntitiesException, LoginValidationException;      
     
     public ArrayList<TableMessage>[] getCumulativeSummary(String[] levelStrs)
  	     	throws MissingEntitiesException, LoginValidationException;      
     
     public ArrayList<TableMessage>[] getCumulativeStudentGrades() throws MissingEntitiesException, LoginValidationException;
     public ArrayList<TableMessage>[] getCumulativeStudentGrades(String studentId, boolean isKey) throws MissingEntitiesException, LoginValidationException;
}
