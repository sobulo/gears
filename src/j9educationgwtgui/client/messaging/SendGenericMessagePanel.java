/**
 * 
 */
package j9educationgwtgui.client.messaging;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.MessagingManager;
import j9educationgwtgui.client.MessagingManagerAsync;
import j9educationgwtgui.client.custom.richtext.RichTextToolbar;
import j9educationgwtgui.client.custom.widgets.BillTemplateBox;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.DropDownSelector;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.CustomMessageTypes;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.TableMessage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SendGenericMessagePanel extends Composite implements ValueChangeHandler<String>, 
	ClickHandler, DoneLoadingWidget, ChangeHandler 
{

	private LevelBox classSelector; //used by both generic and grade msg types
	private BillTemplateBox billSelector;
	@UiField ListBox messageType;
	@UiField RadioButton sendParent;
	@UiField RadioButton sendStudent;
	@UiField RadioButton sendEmail;
	@UiField RadioButton sendSMS;
	@UiField ListBox2ListBox studentBox;
	@UiField SimplePanel messageSlot;
	@UiField SimplePanel selectionSlot;
	@UiField Button sendMessage;
	@UiField Label status;
	
	private HashMap<CustomMessageTypes, DropDownSelector> typeSelectors;
	private HashMap<String, HashSet<String>> addressToLoginMap;
	private HashSet<CustomMessageTypes> initializedSelectors;
	private RichTextArea area;
	public final static HashMap<String, List<TableMessage>> cachedContacts = new HashMap<String, List<TableMessage>>();
	
	private final static int MAX_DISPLAY_ITEMS = 10;
	private SimpleDialog errorBox;
	
	private static SendGenericMessagePanelUiBinder uiBinder = GWT
			.create(SendGenericMessagePanelUiBinder.class);

	interface SendGenericMessagePanelUiBinder extends
			UiBinder<Widget, SendGenericMessagePanel> {
	}	
	
	MessagingManagerAsync messageService = GWT.create(MessagingManager.class);

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<List<TableMessage>> classRosterCallBack =
            new AsyncCallback<List<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
        }

        @Override
        public void onSuccess(List<TableMessage> result) {
        	DropDownSelector selector = typeSelectors.get(getSelectedMessageType());
        	cachedContacts.put(selector.getSelectedDropDownValue(), result);
            setupMessages(result);
            setEnableStatus4Clickables(true);
        }
    };
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> sendMessageCallBack =
            new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
            setEnableStatus4Clickables(true);
        }

        @Override
        public void onSuccess(Void result) {
            WidgetHelper.setSuccessStatus(status, "Scheduled sending of messages successfully", true);
            setEnableStatus4Clickables(true);
        }
    };	    
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SendGenericMessagePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		addressToLoginMap = new HashMap<String, HashSet<String>>();
		initializedSelectors = new HashSet<CustomMessageTypes>();
		//setup widget default appearance
        studentBox.initAppearance(MAX_DISPLAY_ITEMS, GUIConstants.DEFAULT_LB_WIDTH, 
        		"Choose Users", "Recipient List");
        messageType.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        errorBox = new SimpleDialog("WARNING!!");
        
        //setup msg type listbox
		populateMessageType();
		messageType.addChangeHandler(this);
		
		//default values for radio Buttons
		sendStudent.setValue(true);
		sendEmail.setValue(true);
		
		//initialize click handlers for radioButtons and send message button
		sendStudent.addClickHandler(this);
		sendEmail.addClickHandler(this);
		sendParent.addClickHandler(this);
		sendSMS.addClickHandler(this);
		sendMessage.addClickHandler(this);
		
		//initialize the different selectors
		classSelector = new LevelBox();
		billSelector = new BillTemplateBox();
        classSelector.addValueChangeHandler(this);
        billSelector.addValueChangeHandler(this);
        
        //map selectors to message types
        typeSelectors = new HashMap<CustomMessageTypes, DropDownSelector>(3);
        typeSelectors.put(CustomMessageTypes.GENERIC_MESSAGE, classSelector);
        typeSelectors.put(CustomMessageTypes.GRADING_MESSAGE, classSelector);
        typeSelectors.put(CustomMessageTypes.BILLING_MESSAGE, billSelector);
        refreshSelector();
        
        //setup text area for generic messages
        messageSlot.add(getRichTextArea());
		
	}
	
	private void refreshSelector()
	{
		CustomMessageTypes msgType = getSelectedMessageType();
		DropDownSelector selector = typeSelectors.get(msgType);
		if(initializedSelectors.contains(msgType))
			setupMessages(cachedContacts.get(selector.getSelectedDropDownValue()));
		else
		{
			setSelectorAsInitialized(selector); //we rely on this function call instead of directly setting initializedSelectors to ensure initData only called once, i.e. due to 1-many relationship
			WidgetHelper.setNormalStatus(status, "Initializing drop down selector, please wait ...");
			selector.initData();
		}
		selectionSlot.clear();
		selectionSlot.add(selector.getWidgetDisplay());
	}
	
	private void setSelectorAsInitialized(DropDownSelector selector)
	{
		for(CustomMessageTypes type : typeSelectors.keySet())
			if(typeSelectors.get(type) == selector)
				initializedSelectors.add(type);
	}

	private void populateMessageType()
	{
		int genericIndex = 0;
		int count = 0;
		CustomMessageTypes returnVal = CustomMessageTypes.GENERIC_MESSAGE;
		for(CustomMessageTypes type : CustomMessageTypes.values())
		{
			messageType.addItem(type.toString());
			if(type.equals(returnVal))
				genericIndex = count;
			else
				count++;
		}
		messageType.setSelectedIndex(genericIndex);
	}
	
	public void setEnableStatus4Clickables(boolean status)
	{
		sendMessage.setEnabled(status);
		sendParent.setEnabled(status);
		sendStudent.setEnabled(status);
		sendSMS.setEnabled(status);
		sendEmail.setEnabled(status);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		DropDownSelector selector = (DropDownSelector) event.getSource();
		String selected = selector.getSelectedDropDownValue();

		List<TableMessage> contacts = cachedContacts.get(selected);
		if(contacts == null)
		{
			studentBox.initValues(null);
			setEnableStatus4Clickables(false);			
			WidgetHelper.setNormalStatus(status, "Fetching list of students for: " + 
					selector.getSelectedDropDownDisplayName() + ". Please wait");
			messageService.getStudentParentContact(getSelectedMessageType(), selected, true, classRosterCallBack);
		}
		else
			setupMessages(contacts);
	}
	
	public void setupMessages(List<TableMessage> messages)
	{
		if(messages == null)
		{
			//sanity check
			WidgetHelper.setErrorStatus(status, "Event handler error, try refereshing your browser", true);
			return;
		}
		addressToLoginMap.clear();
		
		GWT.log("Total messages received: " + messages.size());
		String display = "";
		String value = "";
		String msgType = "";
		//String studentName = "";
		int eligibleCount = 0;
		boolean isEmailMode = sendEmail.getValue();
		boolean isStudentMode = sendStudent.getValue();
		HashMap<String, String> result = new HashMap<String, String>(messages.size());
		for(TableMessage msg : messages)
		{
			msgType = msg.getText(0);
			if(isEmailMode)
				value = msg.getText(3);
			else
				value = msg.getText(4);
			
			if(isStudentMode)
			{
				if(msgType.equals(PanelServiceConstants.STUDENT_DATA_INDICATOR))
				{
					display = msg.getText(1) + ", " + msg.getText(2) + "(" + value + ")";
					eligibleCount++;
				}
				else
					continue;
			}
			else
			{
				if(msgType.equals(PanelServiceConstants.GUARDIAN_DATA_INDICATOR))
				{
					display = msg.getText(1) + ", " + msg.getText(2) + "(" + value + ")";
					eligibleCount++;
				}
				else
					continue;
			}
			
			if(value.length() > 0)
			{
				HashSet<String> loginList = null;
				if(addressToLoginMap.containsKey(value))
					loginList = addressToLoginMap.get(value);
				else
				{
					loginList = new HashSet<String>(2);
					addressToLoginMap.put(value, loginList);
				}
				loginList.add(msg.getMessageId());
				result.put(value, display);
			}
		}
		
		if(result.size() != eligibleCount)
		{
			String warnMsg = "Warning: Found " + eligibleCount + (isStudentMode?" students":" parents") + 
			" but only " + result.size() + " have " + (isEmailMode?"emails":"phone numbers") +
			" (thus available for selection)";
			errorBox.show(warnMsg);
			WidgetHelper.setNormalStatus(status, warnMsg);
		}
			
		studentBox.initValues(result, false);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		/**
		 * Noteworthy, addresses are hacky. studentBox gives you a unique list of addresses
		 * but then there are cases where we want to email different reports to the same addresses
		 * in which case we rely on addressToLoginMap to help identify the different students that 
		 * we'll be generating custom reports for. And oh did we mention messaging for custom reports
		 * is slightly hacky as well but that's partly because of the problem defination. Only thing to know
		 * on that front is d messages are built on the server. This widget simply goes, hey i need a report emailed
		 * for segun and then on server side we figure out how to build a report 4 segun.
		 */
		HashSet<String> addresses = studentBox.getValues();
		boolean isEmail = sendEmail.getValue();
		CustomMessageTypes selectedType = getSelectedMessageType();
		if(event.getSource() == sendMessage)
		{
			//do some validation
			if(addresses.size() == 0)
			{
				WidgetHelper.setErrorStatus(status, "At least 1 recipient must me selected", true);
				return;
			}			
			String[] addyList, msgParts;
			addyList = msgParts = null;
			
			if(selectedType.equals(CustomMessageTypes.GENERIC_MESSAGE))
			{
				msgParts = new String[2];
				msgParts[0] = area.getText();
				if(isEmail)
					msgParts[1] = area.getHTML();
				
				if(msgParts[0].length() == 0)
				{
					WidgetHelper.setErrorStatus(status, "Nothing to send, message is empty", true);
					return;
				}
				
				//build addresses
				addyList = new String[addresses.size()];
				int i = 0;
				for(String s : addresses)
					addyList[i++] = s;
			}
			else 
			{
				int numOfReports = 0;
				for(String addr : addresses)
					numOfReports += addressToLoginMap.get(addr).size();
				msgParts = new String[numOfReports + 1];
				addyList = new String[numOfReports];
				int i = 0;
				msgParts[0] = typeSelectors.get(selectedType).getSelectedDropDownValue();
				for(String addr : addresses)
				{
					HashSet<String> loginIDs = addressToLoginMap.get(addr);
					for(String id : loginIDs)
					{
						addyList[i++] = addr;
						msgParts[i] = id;
					}
				}
			}
			//send message
			setEnableStatus4Clickables(false);
			WidgetHelper.setNormalStatus(status, "Making server request to send messages, please wait ...");
			messageService.sendMessage(selectedType, addyList, msgParts, isEmail, sendMessageCallBack);
		}
		else
		{
			DropDownSelector selector = typeSelectors.get(selectedType);
			setupMessages(cachedContacts.get(selector.getSelectedDropDownValue()));
		}
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		CustomMessageTypes selectedType = getSelectedMessageType();
		DropDownSelector selector = typeSelectors.get(selectedType);		
		if(selector instanceof DoneLoadingWidget)
			return ((DoneLoadingWidget) selector).doneLoading();
		else
			return true;
	}
	
	public Widget getRichTextArea()
	{
		area = new RichTextArea();
	    RichTextToolbar tb = new RichTextToolbar(area, "Enter Message below");
	    VerticalPanel p = new VerticalPanel();
	    p.add(tb);
	    p.add(area);
	    
	    area.setHeight("7em");
	    area.setWidth("99%");
	    tb.setWidth("100%");
	    p.setWidth("100%");		
	    return p;	
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		refreshSelector();
		CustomMessageTypes selectedType = getSelectedMessageType();
		if(selectedType.equals(CustomMessageTypes.GENERIC_MESSAGE))
		{
			messageSlot.setVisible(true);
			return;
		}
		else
			messageSlot.setVisible(false);
	}
	
	private CustomMessageTypes getSelectedMessageType()
	{
		String selectedTypeStr = messageType.getValue(messageType.getSelectedIndex());
		CustomMessageTypes selectedType = CustomMessageTypes.valueOf(selectedTypeStr);
		return selectedType;
	}

}
