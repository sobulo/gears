/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.shared.PanelServiceLoginRoles;

import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public interface HyperlinkedPanel extends DoneLoadingWidget{
    public Hyperlink getLink();
    public Widget getPanelWidget();
    public PanelServiceLoginRoles[] getEligibleRoles();
}
