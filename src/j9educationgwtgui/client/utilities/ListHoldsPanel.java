/**
 * 
 */
package j9educationgwtgui.client.utilities;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.tables.TableMessageRowSelectionHandler;
import j9educationgwtgui.client.custom.widgets.HoldInfoPanel;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ListHoldsPanel extends Composite implements ClickHandler, TableMessageRowSelectionHandler{
	
	@UiField LevelBox classList;
	@UiField Button fetchHold;
	@UiField SimplePanel tablePanel;
	@UiField Label status;
	@UiField CheckBox openOnly;
	
	private DialogBox alertBox;
	private Button editHold;
	private HoldInfoPanel infoPanel;
	private String holdId;
	
    private UserManagerAsync userService =
        GWT.create(UserManager.class);	

	private static ListHoldsPanelUiBinder uiBinder = GWT
			.create(ListHoldsPanelUiBinder.class);

	interface ListHoldsPanelUiBinder extends UiBinder<Widget, ListHoldsPanel> {
	}

    final AsyncCallback<String> editHoldCallback =
        new AsyncCallback<String>() {
	    @Override
	    public void onFailure(Throwable caught) {
	    	WidgetHelper.checkForLoginError(caught);
	        WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
	    }
	
	    @Override
	    public void onSuccess(String result) {
	    	WidgetHelper.setSuccessStatus(status, "Saved hold for: " + result, true);
	    }
    };	
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<ArrayList<TableMessage>> displayHoldCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {

                @Override
                public void onFailure(Throwable caught) {
                	WidgetHelper.checkForLoginError(caught);
                    WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
                }

                @Override
                public void onSuccess(ArrayList<TableMessage> result) {
                    tablePanel.clear(); //clear current grades

                    if (result.size() < 2) {
                        WidgetHelper.setSuccessStatus(status, "No student in selected class has a hold set on their account", false);
                        return;
                    }
                    setUpTable(result);
                    WidgetHelper.setSuccessStatus(status, "Displaying " + result.size() + " holds. Click on any to edit.", false);

                }
            };	

            
	public ListHoldsPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		classList.initData();
		fetchHold.addClickHandler(this);
		tablePanel.setVisible(false);
		
		//setup alert box
		alertBox = new DialogBox(true);
		alertBox.setHTML("<b>Edit Hold</b>");
        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);
        VerticalPanel editPanel = new VerticalPanel();
        infoPanel = new HoldInfoPanel();
        HorizontalPanel buttonPanel = new HorizontalPanel();
        editHold = new Button("Save Hold");
        editHold.addClickHandler(this);
        buttonPanel.add(editHold);
        Button cancel = new Button("Cancel");
        cancel.addClickHandler(new ClickHandler() {	
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();	
			}
		});
        buttonPanel.add(cancel);
        buttonPanel.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
        editPanel.add(infoPanel);
        editPanel.add(buttonPanel);
        alertBox.setWidget(editPanel);
        //alertBox.setWidth("500px");
	}
	
	public void setUpTable(ArrayList<TableMessage> result)
	{
        tablePanel.setVisible(true);
        
        //setup table headers
        TableMessageHeader header = (TableMessageHeader) result.remove(0);

        //display grades
        PsTable holdsTable = new PsTable(header);
        holdsTable.addMessageSelectionHandler(this);
        holdsTable.showMessages(result, header.getCaption());
        tablePanel.add(holdsTable);		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event)
	{
		if(event.getSource().equals(fetchHold))
		{
			tablePanel.clear();
			tablePanel.setVisible(false);
			WidgetHelper.setNormalStatus(status, "Fetching hold information for students in class " + classList.getSelectedClassName() + ". Please wait ...");
			userService.getStudentHolds(classList.getSelectedClass(), openOnly.getValue(), displayHoldCallback);
		}
		else if(event.getSource().equals(editHold))
		{
			String type = infoPanel.getSelectedType();
			String note = infoPanel.getNote();
			String message = infoPanel.getMessage();
			boolean resolutionStatus = infoPanel.getResolutionStatus();
			WidgetHelper.setNormalStatus(status, "Requested update of account hold for student " + infoPanel.getStudentName() + ". Please wait ...");
			userService.editStudentHold(holdId, type, message, note, resolutionStatus, editHoldCallback);
			alertBox.hide();
		}
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.tables.TableMessageRowSelectionHandler#onRowSelected(j9educationgwtgui.shared.TableMessage)
	 */
	@Override
	public void onRowSelected(TableMessage m) 
	{
		GWT.log("Received Click!!");
		//setup alert box
		infoPanel.setStudentName(m.getText(0));
		infoPanel.setLevelName(m.getText(1));
		infoPanel.setType(m.getText(2));
		infoPanel.setResolutionStatus(m.getText(3).equals(PanelServiceConstants.HOLD_RESOLVED_TRUE)? true : false);
		infoPanel.setMessage(m.getText(4));
		infoPanel.setNote(m.getText(5));
		holdId = m.getMessageId();
		
		alertBox.show();
	}
}
