/**
 * 
 */
package j9educationgwtgui.client.utilities;

import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.client.SchoolServiceAsync;
import j9educationgwtgui.client.custom.richtext.RichTextToolbar;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceConstants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class NoticeBoardPanel extends Composite implements ClickHandler, ChangeHandler, DoneLoadingWidget{
	
	@UiField ListBox noticeId;
	@UiField SimplePanel contentSlot;
	@UiField TextBox title;
	@UiField Label status;
	@UiField Button saveNotice;
	

	private boolean loadingCompleted = false;
	private RichTextArea area;
	private static NoticeBoardPanelUiBinder uiBinder = GWT
			.create(NoticeBoardPanelUiBinder.class);
	
	private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);

	interface NoticeBoardPanelUiBinder extends
			UiBinder<Widget, NoticeBoardPanel> {
	}

	final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, "Unable to save notice info: " + caught.getMessage(), true);
			enableWidgets();
		}

		@Override
		public void onSuccess(Void result) {
			enableWidgets();
			WidgetHelper.setSuccessStatus(status, "Saved notice successfully", true);
		}
	};	
	
	final AsyncCallback<String[]> noticeCallback = new AsyncCallback<String[]>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, "Unable to load notice info: " + caught.getMessage(), true);
			enableWidgets();
			loadingCompleted = true;
		}

		@Override
		public void onSuccess(String[] result) {
			enableWidgets();
			if(result.length != 2)
			{
				WidgetHelper.setErrorStatus(status, "Badly formed notice, expected 2 fields (for title, content) but found " + result.length, true);
				return;
			}
			title.setText(result[0]);
			area.setHTML(result[1]);
			loadingCompleted = true;
		}
	};	
	 	
	
	public NoticeBoardPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		contentSlot.add(getRichTextArea());
		noticeId.addChangeHandler(this);
		saveNotice.addClickHandler(this);
		buildNoticeIDs();
		disableWidgets();
	}
	
	public void buildNoticeIDs()
	{
		for(String id : PanelServiceConstants.ROLE_NOTES)
			noticeId.addItem(id);
		
		noticeId.setSelectedIndex(0);
		schoolService.getNotice(noticeId.getValue(0), noticeCallback);		
	}
	
	public Widget getRichTextArea()
	{
		area = new RichTextArea();
	    RichTextToolbar tb = new RichTextToolbar(area, "Enter notice details below");
	    VerticalPanel p = new VerticalPanel();
	    p.add(tb);
	    p.add(area);
	    
	    area.setHeight("14em");
	    area.setWidth("99%");
	    tb.setWidth("100%");
	    p.setWidth("100%");		
	    return p;	
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		title.setValue(null);
		area.setHTML("");
		disableWidgets();
		schoolService.getNotice(noticeId.getValue(noticeId.getSelectedIndex()), noticeCallback);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		String selectedId = noticeId.getValue(noticeId.getSelectedIndex());
		String title = this.title.getValue();
		String content = area.getHTML();
		disableWidgets();
		schoolService.updateNotice(selectedId, title, content, saveCallback);
	}
	
	private void disableWidgets()
	{
		saveNotice.setEnabled(false);
		area.setEnabled(false);
		title.setEnabled(false);
	}

	private void enableWidgets()
	{
		saveNotice.setEnabled(true);
		area.setEnabled(true);
		title.setEnabled(true);		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return loadingCompleted;
	}
}
