package j9educationgwtgui.client.utilities;

import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.HoldInfoPanel;
import j9educationgwtgui.client.custom.widgets.LevelStudentRosterBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class AddHoldPanel extends Composite implements ClickHandler, DoneLoadingWidget
{
	
	@UiField LevelStudentRosterBox studentBox;
	@UiField Button addHold;
	@UiField HoldInfoPanel holdInfo;
	@UiField Label status;	

	private UserManagerAsync userService;

	
	private static AddHoldPanelUiBinder uiBinder = GWT
			.create(AddHoldPanelUiBinder.class);

	interface AddHoldPanelUiBinder extends UiBinder<Widget, AddHoldPanel> {
	}
	
    final AsyncCallback<String> addHoldCallback =
        new AsyncCallback<String>() {
	    @Override
	    public void onFailure(Throwable caught) {
	    	WidgetHelper.checkForLoginError(caught);
	        WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
	    }
	
	    @Override
	    public void onSuccess(String result) {
	    	WidgetHelper.setSuccessStatus(status, "Succesfully added hold for: " + result, true);
	    }
    };	

	public AddHoldPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		holdInfo.setIdPanelVisibility(false);
		holdInfo.setResolutionPanelVisibility(false);
		userService = holdInfo.getUserService();
		addHold.addClickHandler(this);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		String studentId = studentBox.getSelectedStudent();
		String studentName = studentBox.getSelectedStudentName();
		
		if(studentId.equals(""))
			return;
		
		String levelKey = studentBox.getSelectedClass();		
		String message = holdInfo.getMessage();
		String note = holdInfo.getNote();
		String type = holdInfo.getSelectedType();
			
		WidgetHelper.setNormalStatus(status, "Requesting addition of account hold for student: " + studentName + ". Please wait ...");
		userService.createStudentHold(levelKey, studentId, type, message, note, addHoldCallback);
		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return studentBox.doneLoading();
	}

}
