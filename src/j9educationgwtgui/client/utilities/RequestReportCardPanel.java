/**
 * 
 */
package j9educationgwtgui.client.utilities;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class RequestReportCardPanel extends Composite implements ClickHandler, ValueChangeHandler<String>, DoneLoadingWidget
{
    @UiField LevelBox classList;
    @UiField ListBox2ListBox studentBox;
    @UiField Button requestCards;
    @UiField HTML status;

    private final static int MAX_DISPLAY_ITEMS = 15;
    private SimpleDialog errorBox;
    private SimpleDialog messageBox;

    private ClassRosterServiceAsync classService;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> classRosterCallBack =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
            studentBox.initValues(result);
            WidgetHelper.setSuccessStatus(status, "Showing " + result.size() + 
            		" students for source class " + classList.getSelectedClassName(), false);
        }
    };
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> cardsCallBack =
            new AsyncCallback<String>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
        }

        @Override
        public void onSuccess(String result) {
        	messageBox.show("Setup for report card generation completed. Please click on link in status box to start the process");
        	GWT.log(result);
        	status.setHTML(result);
        }
    };    	

	private static RequestReportCardPanelUiBinder uiBinder = GWT
			.create(RequestReportCardPanelUiBinder.class);

	interface RequestReportCardPanelUiBinder extends
			UiBinder<Widget, RequestReportCardPanel> {
	}


	public RequestReportCardPanel() {
		initWidget(uiBinder.createAndBindUi(this));
        classService = classList.getClassService();
        classList.addValueChangeHandler(this);
        classList.initData();
        requestCards.addClickHandler(this);
        studentBox.initAppearance(MAX_DISPLAY_ITEMS, GUIConstants.DEFAULT_LB_WIDTH, "Class List", "Selected Students List");
        errorBox = new SimpleDialog("An error occurred");
        messageBox = new SimpleDialog("Info");		
	}

    @Override
    public void onClick(ClickEvent event) {
        HashSet<String> chosen = studentBox.getValues();
        String sourceKey = classList.getSelectedClass();
        String className = classList.getSelectedClassName();
        
        //perform validation
        if(chosen.size() == 0)
        {
        	errorBox.show("You must add at least 1 student to target box!");
        	return;
        }
        
        WidgetHelper.setNormalStatus(status, "Requesting " + className + " report card generation for " 
        		+ chosen.size() + " students. Please wait ...");
        //send list to server to save
        String[] studentKeys = new String[chosen.size()];
        classService.requestReportCards(sourceKey,
                chosen.toArray(studentKeys), cardsCallBack);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String selected = classList.getSelectedClass();
		String className = classList.getSelectedClassName();
		studentBox.initValues(null);
		WidgetHelper.setNormalStatus(status, "Fetching student list for: " + className + ". Please wait ...");
        classService.getClassRosterMap(selected,
                classRosterCallBack);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classList.doneLoading();
	}	
}
