/**
 * 
 */
package j9educationgwtgui.client.utilities;

import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.tables.TableMessageRowSelectionHandler;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.PublishedTestsInfoPanel;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class TestPublishStatusListPanel extends Composite implements ClickHandler, TableMessageRowSelectionHandler, DoneLoadingWidget 
{
	@UiField LevelBox classList;
	@UiField Button fetchPublishStatus;
	@UiField SimplePanel tablePanel;
	@UiField Label status;

	private DialogBox alertBox;
	private Button editStatus;
	private PublishedTestsInfoPanel testPanel;
	private String lastSelectedClass;
	private TableMessage lastSelectedMessage;
	private HashMap<String, Boolean> lastSavedStatusInfo;

	private static TestPublishStatusListPanelUiBinder uiBinder = GWT
	.create(TestPublishStatusListPanelUiBinder.class);

	interface TestPublishStatusListPanelUiBinder extends
	UiBinder<Widget, TestPublishStatusListPanel> {
	}
	
	private boolean loadingCompleted = false;

	// Create an asynchronous callback to handle the result.
	final AsyncCallback<ArrayList<TableMessage>> displayStatusCallback =
		new AsyncCallback<ArrayList<TableMessage>>() 
		{

			@Override
			public void onFailure(Throwable caught) {
				WidgetHelper.checkForLoginError(caught);
				WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
			}
	
			@Override
			public void onSuccess(ArrayList<TableMessage> result) {
				tablePanel.clear(); //clear current grades
	
				if (result.size() < 2) {
					WidgetHelper.setErrorStatus(status, "No subjects found for selected level", false);
					return;
				}
				setUpTable(result);
				WidgetHelper.setSuccessStatus(status, "Displaying " + result.size() + " subjects. Click on any to edit publish status.", false);
	
			}
		};
		
		final AsyncCallback<String> updateStatusCallback =
			new AsyncCallback<String>() 
			{

				@Override
				public void onFailure(Throwable caught) {
					WidgetHelper.checkForLoginError(caught);
					WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
				}
		
				@Override
				public void onSuccess(String result) 
				{
					String append = " .Refresh browser to view updated table";
					int lastPublishCount = 0;
					if(lastSelectedMessage.getMessageId().equals(result))
					{
						int nameIdx = 3;
						for(int i = 2; i < lastSelectedMessage.getNumberOfDoubleFields(); i++)
						{
							String tk = lastSelectedMessage.getText(nameIdx);
							nameIdx += 2;
							if(lastSavedStatusInfo.get(tk))
							{
								lastPublishCount++;
								lastSelectedMessage.setNumber(i, 1);
							}
							else
								lastSelectedMessage.setNumber(i, 0);
							
						}
						if(lastPublishCount == lastSelectedMessage.getNumber(0))
							lastSelectedMessage.setText(1, "Yes?");
						else
							lastSelectedMessage.setText(1, "No?");
						lastSelectedMessage.setNumber(1, lastPublishCount);
						PsTable table = (PsTable) tablePanel.getWidget();
						table.refresh();
						append = " for " + lastSelectedMessage.getText(0); 
					}
					WidgetHelper.setSuccessStatus(status, "Successfully updated status" + append + ". If subject publish status shows a '?', " +
							"it means status values are based on estimates. To confirm this is the actual value: click the 'Display Publish Status' button", false);
		
				}
			};		

	public TestPublishStatusListPanel() 
	{
		initWidget(uiBinder.createAndBindUi(this));
		classList.initData();
		fetchPublishStatus.addClickHandler(this);
		tablePanel.setVisible(false);
		
		//setup alert box
		alertBox = new DialogBox(true);
		alertBox.setHTML("<b>Edit Status</b>");
        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);
        //VerticalPanel editPanel = new VerticalPanel();
        testPanel = new PublishedTestsInfoPanel();
        HorizontalPanel buttonPanel = new HorizontalPanel();
        editStatus = new Button("Save Publish Status");
        editStatus.addClickHandler(this);
        buttonPanel.add(editStatus);
        Button cancel = new Button("Cancel");
        cancel.addClickHandler(new ClickHandler() {	
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();	
			}
		});
        buttonPanel.add(cancel);
        //buttonPanel.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
        testPanel.setAdditionalSlot(buttonPanel);
        //editPanel.add(testPanel);
        //editPanel.add(buttonPanel);
        alertBox.setWidget(testPanel);
		
	}
	
	public void setUpTable(ArrayList<TableMessage> result)
	{
        tablePanel.setVisible(true);
        
        //setup table headers
        TableMessageHeader header = (TableMessageHeader) result.remove(0);
        lastSelectedClass = header.getCaption();
        header.setCaption(lastSelectedClass + " publish status");
        
        //display grades
        PsTable statusTable = new PsTable(header);
        statusTable.addMessageSelectionHandler(this);
        statusTable.showMessages(result, header.getCaption());
        tablePanel.add(statusTable);		
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event)
	{
		if(event.getSource().equals(fetchPublishStatus))
		{
			tablePanel.clear();
			tablePanel.setVisible(false);
			WidgetHelper.setNormalStatus(status, "Fetching publish status for subjects in " + classList.getSelectedClassName() + ". Please wait ...");
			classList.getClassService().getCoursePublishStatusTable(classList.getSelectedClass(), displayStatusCallback);
		}
		else if(event.getSource().equals(editStatus))
		{
			String subjectId = testPanel.getSubjectID();
			HashMap<String, Boolean> testStatusInfo = testPanel.getPublishStatus();
			lastSavedStatusInfo = testStatusInfo;
			WidgetHelper.setNormalStatus(status, "Requested update of publish status for tests/exams in " + testPanel.getSubjectName() + ". Please wait ...");
			classList.getClassService().updateCourseTestStatus(subjectId, testStatusInfo, updateStatusCallback);
			alertBox.hide();
		}
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.tables.TableMessageRowSelectionHandler#onRowSelected(j9educationgwtgui.shared.TableMessage)
	 */
	@Override
	public void onRowSelected(TableMessage m) 
	{
		GWT.log("Received Click!!");
		lastSelectedMessage = m;
		testPanel.setClassName(lastSelectedClass);
		testPanel.setSubjectName(m.getText(0), m.getMessageId());
		int numOfTests = m.getNumberOfDoubleFields() - 2;
		if(numOfTests <= 0)
		{
			WidgetHelper.setErrorStatus(status, "Selected subject, " + testPanel.getSubjectName() + 
					", has no tests/exams. Nothing to do", true);
			return;
		}
		int nameIdx = 2;
		int statusIdx = 2;
		String[] testIDs = new String[numOfTests];
		String[] testNames = new String[numOfTests];
		Boolean[] testValues = new Boolean[numOfTests];
		for(int i = 0; i < numOfTests; i++)
		{
			testNames[i] = m.getText(nameIdx++);
			testIDs[i] = m.getText(nameIdx++);
			testValues[i] = Math.abs((1-m.getNumber(statusIdx++))) < 0.0001;
		}
		testPanel.setPublishStatus(testIDs, testNames, testValues);
		alertBox.center();
		alertBox.show();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classList.doneLoading();
	}	
	
}