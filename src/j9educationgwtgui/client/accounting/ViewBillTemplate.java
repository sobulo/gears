/**
 * 
 */
package j9educationgwtgui.client.accounting;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.tables.MessageListToGrid;
import j9educationgwtgui.client.custom.widgets.BillTemplateBox;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ViewBillTemplate extends Composite implements DoneLoadingWidget, ClickHandler {
	@UiField BillTemplateBox billList;
	@UiField Button viewTemplate;
	@UiField TextBox billName;
	@UiField TextBox billYear;
	@UiField TextBox billTerm;
	@UiField TextBox billAmount;
	@UiField TextBox billDate;
	@UiField MessageListToGrid itemizedSlot;
	@UiField Label status;
	
	private final static HashMap<String, List<TableMessage>> cachedBillInfo = new HashMap<String, List<TableMessage>>();

	private static ViewBillTemplateUiBinder uiBinder = GWT
			.create(ViewBillTemplateUiBinder.class);

	interface ViewBillTemplateUiBinder extends
			UiBinder<Widget, ViewBillTemplate> {
	}
	
	final AsyncCallback<List<TableMessage>> viewBillCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Fetched bill template info successfully", true);
				cachedBillInfo.put(billList.getSelectedBillTemplate(), result);
				setupBillInfo(result);
			}
			else
			{
				WidgetHelper.setErrorStatus(status, "No data found for selected bill template!!", true);				
			}
		}
	};
	

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ViewBillTemplate() {
		initWidget(uiBinder.createAndBindUi(this));
		viewTemplate.addClickHandler(this);
		billList.initData();
		billName.setEnabled(false);
		billYear.setEnabled(false);
		billTerm.setEnabled(false);
		billAmount.setEnabled(false);
		billDate.setEnabled(false);
	}
	
	private void setupBillInfo(List<TableMessage> info)
	{
		ArrayList<TableMessage> infoCopy = new ArrayList<TableMessage>(info);
		setupSummaryInfo(infoCopy.remove(0));
		GWT.log("Calling itemized with: " + infoCopy.size());
		itemizedSlot.populateTable(infoCopy);
	}
	
	private void setupSummaryInfo(TableMessage m)
	{
		billName.setText(m.getText(0));
		billYear.setText(m.getText(1));
		billTerm.setText(m.getText(2));
		billAmount.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0)));
		billDate.setText(m.getDate(0) == null?"":GUIConstants.DEFAULT_DATE_FORMAT.format(m.getDate(0)));
	}
	
	private void clearFields()
	{
		billName.setText("");
		billYear.setText("");
		billTerm.setText("");
		billAmount.setText("");
		billDate.setValue(null);
		itemizedSlot.clear();
	}		

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return billList.doneLoading();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		clearFields();
		String selectedBill = billList.getSelectedBillTemplate();
		if(selectedBill.length() == 0)
		{
			WidgetHelper.setErrorStatus(status, "No bill templates selected, save aborted", true);
			return;
		}
		
		List<TableMessage> billInfo = cachedBillInfo.get(selectedBill);
		if(billInfo == null)
			billList.getAccountService().getBillTemplateInfo(selectedBill, viewBillCallBack);
		else
			setupBillInfo(billInfo);
	}
	
	
}
