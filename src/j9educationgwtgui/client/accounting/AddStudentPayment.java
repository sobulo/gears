/**
 * 
 */
package j9educationgwtgui.client.accounting;

import j9educationgwtgui.client.AccountManager;
import j9educationgwtgui.client.AccountManagerAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class AddStudentPayment extends Composite implements ValueChangeHandler<String>, 
	DoneLoadingWidget, ChangeHandler, ClickHandler
{
	@UiField StudentSuggestBox studentList;
	@UiField ListBox billList;
	
	@UiField TextBox totalAmount;
	@UiField TextBox priorPayment;
	@UiField TextBox balance;
	
	@UiField TextBox paymentAmount;
	@UiField TextBox comments;
	@UiField TextBox referenceID;
	@UiField DateBox paymentDate;
	
	@UiField Label status;
	@UiField Label description;
	
	@UiField Button savePayment;
	
	private final static String DEFAULT_TITLE = "Please select a student bill to pay";
	private final static NumberFormat NUMBER_FORMAT = GUIConstants.DEFAULT_NUMBER_FORMAT;
	
	private static AccountManagerAsync accountService = GWT.create(AccountManager.class);

	private static HashMap<String, List<TableMessage>> cachedBillInfo = new HashMap<String, List<TableMessage>>();
	
	private static AddStudentPaymentUiBinder uiBinder = GWT
			.create(AddStudentPaymentUiBinder.class);

	interface AddStudentPaymentUiBinder extends
			UiBinder<Widget, AddStudentPayment> {
	}
	
	final AsyncCallback<List<TableMessage>> billListCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
			WidgetHelper.setErrorStatus(description, "Unable to retrieve billing data for " + studentList.getSelectedUserDisplay(), false);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Fetched " + result.size() + " student bills." + DEFAULT_TITLE, true);
				cachedBillInfo.put(studentList.getSelectedUser(), result);
				populateBillBox(result);
				displaySelectedBillInfo();				
			}
			else
			{
				WidgetHelper.setErrorStatus(description, "No bills found for this student!!", false);
				WidgetHelper.setErrorStatus(status, "No bills found for student. Please generate a bill before attempting to enter payment.", true);				
			}
		}
	};
	
	final AsyncCallback<String> saveCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(String result) {
			WidgetHelper.setSuccessStatus(status, result, true);
			cachedBillInfo.remove(studentList.getSelectedUser());
			clearFields();
			studentList.clear();
			billList.clear();
		}
	};	

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public AddStudentPayment() {
		initWidget(uiBinder.createAndBindUi(this));
		totalAmount.setEnabled(false);
		priorPayment.setEnabled(false);
		balance.setEnabled(false);
		paymentDate.setValue(new Date());
		savePayment.setEnabled(false);
		studentList.addValueChangeHandler(this);
		billList.addChangeHandler(this);
		savePayment.addClickHandler(this);
	}

	private void clearFields()
	{
		totalAmount.setText("");
		priorPayment.setText("");
		balance.setText("");
		description.setText(DEFAULT_TITLE);
		
		paymentAmount.setText("");
		paymentDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		paymentDate.setValue(new Date());
		comments.setText("");
		referenceID.setValue("");
		
		savePayment.setEnabled(false);
	}
	
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return studentList.doneLoading();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		clearFields();
		billList.clear();
		String selectedStudent = event.getValue();
		savePayment.setEnabled(false);
		List<TableMessage> billInfoList = cachedBillInfo.get(selectedStudent);
		if(billInfoList == null)
		{
			WidgetHelper.setNormalStatus(status, "Fetching list of student bills. Please wait ...");
			accountService.getStudentBills(selectedStudent, billListCallBack);
		}
		else
		{
			GWT.log("BillPayment cache hit!!!");
			populateBillBox(billInfoList);
		}
	}
	
	private void populateBillBox(List<TableMessage> billInfo)
	{
		for(TableMessage m : billInfo)
			billList.addItem(m.getText(0), m.getText(1));
	}
	
	private TableMessage getBillInfoFromCache(String studentID, String billID)
	{
		List<TableMessage> billInfoList = cachedBillInfo.get(studentID);
		for(TableMessage m : billInfoList)
			if(m.getText(1).equals(billID))
				return m;
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		clearFields();
		savePayment.setEnabled(false);
		displaySelectedBillInfo();
	}
	
	private void displaySelectedBillInfo()
	{	
		TableMessage billInfo = getBillInfoFromCache(studentList.getSelectedUser(), billList.getValue(billList.getSelectedIndex()));
		double total = billInfo.getNumber(0);
		double paid = billInfo.getNumber(1);
		totalAmount.setText(NUMBER_FORMAT.format(total));
		priorPayment.setText(NUMBER_FORMAT.format(paid));
		balance.setText(NUMBER_FORMAT.format(total-paid));
		
		if(billInfo.getText(2).equals("True")) //fully paid already
		{
			WidgetHelper.setErrorStatus(description, "Bill fully paid already", false);
			WidgetHelper.setErrorStatus(status, "Save payment button is disabled because bill has already been paid", false);
		}
		else
		{
			WidgetHelper.setSuccessStatus(description, "Enter a payment amount to pay all or part of outstanding balance", false);
			WidgetHelper.setNormalStatus(status, "Enter a payment amount and then click save button");
			savePayment.setEnabled(true);
		}		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Date payDate = paymentDate.getValue();
		double amount = 0;
		String description = comments.getText();
		String id = referenceID.getText();
		String billKey = billList.getValue(billList.getSelectedIndex());
		
		try
		{
			amount = Double.parseDouble(paymentAmount.getValue());
		}
		catch(NumberFormatException ex)
		{
			WidgetHelper.setErrorStatus(status, "Enter a valid number for payment amount", true);
			return;
		}
		
		accountService.savePayment(billKey, amount, payDate, id, description, saveCallBack);
		WidgetHelper.setNormalStatus(status, "Saving payment. Please wait ...");		
	}

}
