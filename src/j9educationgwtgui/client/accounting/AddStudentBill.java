/**
 * 
 */
package j9educationgwtgui.client.accounting;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.BillTemplateBox;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class AddStudentBill extends Composite implements DoneLoadingWidget, ValueChangeHandler<String>, ClickHandler{
	@UiField
	LevelBox classList;
	@UiField
	ListBox2ListBox studentBox;
	@UiField
	BillTemplateBox billList;
	@UiField
	Label status;
	@UiField
	Button requestBills;
	
    private final static int MAX_DISPLAY_ITEMS = 15;


	private static AddStudentBillUiBinder uiBinder = GWT
			.create(AddStudentBillUiBinder.class);

	interface AddStudentBillUiBinder extends UiBinder<Widget, AddStudentBill> {
	}

	final AsyncCallback<HashMap<String, String>> classRosterCallBack = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			studentBox.initValues(result);
			WidgetHelper.setSuccessStatus(status,
					"Showing " + result.size() + " students for source class "
							+ classList.getSelectedClassName(), false);
		}
	};
	
	final AsyncCallback<String> saveCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(String result) {
			WidgetHelper.setSuccessStatus(status, result, true);
		}
	};	

	/**
	 * Because this class has a default constructor, it can be used as a binder
	 * template. In other words, it can be used in other *.ui.xml files as
	 * follows: <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 * xmlns:g="urn:import:**user's package**">
	 * <g:**UserClassName**>Hello!</g:**UserClassName> </ui:UiBinder> Note that
	 * depending on the widget that is used, it may be necessary to implement
	 * HasHTML instead of HasText.
	 */
	public AddStudentBill() {
		initWidget(uiBinder.createAndBindUi(this));
        classList.addValueChangeHandler(this);
        classList.initData();
        billList.initData();
        studentBox.initAppearance(MAX_DISPLAY_ITEMS, GUIConstants.DEFAULT_LB_WIDTH, "Class List", "Selected Students List");
        requestBills.addClickHandler(this);
	}
	
    @Override
    public void onClick(ClickEvent event) {
        HashSet<String> chosen = studentBox.getValues();
        String className = classList.getSelectedClassName();
        String billTemplateKey = billList.getSelectedBillTemplate();
        
        //perform validation
        if(chosen.size() == 0)
        {
        	WidgetHelper.setErrorStatus(status, "You must add at least 1 student to target box!", true);
        	return;
        }
        
        if(billTemplateKey.length() == 0)
        {
        	WidgetHelper.setErrorStatus(status, "No bill template selected. Please create a" +
        			" bill template if none exists, then refresh your browser.", true);
        	return;
        }
        
        WidgetHelper.setNormalStatus(status, "Generating " + billList.getSelectedBillTemplateName() + " bills for " 
        		+ chosen.size() + " students in " + className +  ". Please wait ...");
        
        //send list to server to save
        String[] studentKeys = new String[chosen.size()];
        billList.getAccountService().createUserBill(billTemplateKey, chosen.toArray(studentKeys), saveCallBack);

    }	
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String selected = classList.getSelectedClass();
		String className = classList.getSelectedClassName();
		studentBox.initValues(null);
		WidgetHelper.setNormalStatus(status, "Fetching student list for: " + className + ". Please wait ...");
        classList.getClassService().getClassRosterMap(selected,
                classRosterCallBack);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classList.doneLoading() && billList.doneLoading();
	}	
}