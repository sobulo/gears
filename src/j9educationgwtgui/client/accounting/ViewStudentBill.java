/**
 * 
 */
package j9educationgwtgui.client.accounting;

import j9educationgwtgui.client.AccountManager;
import j9educationgwtgui.client.AccountManagerAsync;
import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.tables.MessageListToGrid;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.GuardianStudentPicker;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ViewStudentBill extends Composite implements DoneLoadingWidget, ValueChangeHandler<String>, ClickHandler{
	
	@UiField SimplePanel studentListSlot;
	@UiField ListBox billList;
	
	@UiField TextBox billName;
	@UiField TextBox billYear;
	@UiField TextBox billTerm;
	@UiField TextBox billAmount;
	@UiField TextBox billDate;	
	@UiField MessageListToGrid itemizedSlot;
	
	@UiField TextBox priorPayments;
	@UiField TextBox balance;
	@UiField MessageListToGrid paySlot;
	
	@UiField Button viewBill;
	
	@UiField Label status;
	
	private static HashMap<String, HashMap<String, String>> cachedBillKeys = new HashMap<String, HashMap<String, String>>();
	private static HashMap<String, List<TableMessage>> cachedBillData = new HashMap<String, List<TableMessage>>();
	private static AccountManagerAsync accountService = GWT.create(AccountManager.class);
	
	private StudentSuggestBox studentList;
	private GuardianStudentPicker guardianList;
	private static boolean doneLoading = false;
	private boolean useLogins = true;
	private static ViewStudentBillUiBinder uiBinder = GWT
			.create(ViewStudentBillUiBinder.class);

	interface ViewStudentBillUiBinder extends UiBinder<Widget, ViewStudentBill> {
	}

	final AsyncCallback<HashMap<String, String>> billListCallBack = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
			doneLoading = true;
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			doneLoading = true;
			result.remove(0); //discard header as its not needed here
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Fetched " + result.size() + " student bills.", false);
				if(studentList != null )
					cachedBillKeys.put(studentList.getSelectedUser(), result);
				else if(guardianList != null)
					cachedBillKeys.put(guardianList.getSelectedStudent(), result);
				populateBillBox(result);
			}
			else
			{
				WidgetHelper.setErrorStatus(status, "No bills found for student. Please generate a bill before attempting to view it.", true);				
			}
		}
	};
	
	final AsyncCallback<List<TableMessage>> viewBillCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Fetched bill template info successfully", true);
				cachedBillData.put(billList.getValue(billList.getSelectedIndex()), result);
				setupDetails(result);
			}
			else
			{
				WidgetHelper.setErrorStatus(status, "No data found for selected student bill!!", true);				
			}
		}
	};	
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ViewStudentBill(PanelServiceLoginRoles role) {
		initWidget(uiBinder.createAndBindUi(this));
		if(role.equals(PanelServiceLoginRoles.ROLE_ADMIN))
			setupAdmin();
		else if(role.equals(PanelServiceLoginRoles.ROLE_STUDENT))
			setupStudent();
		else if(role.equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
			setupGuardian();
		viewBill.addClickHandler(this);
		billName.setEnabled(false);
		billYear.setEnabled(false);
		billTerm.setEnabled(false);
		billAmount.setEnabled(false);
		billDate.setEnabled(false);
		priorPayments.setEnabled(false);
		balance.setEnabled(false);
	}
	
	private void setupAdmin()
	{
		studentList = new StudentSuggestBox();
		studentListSlot.add(studentList);
		studentList.addValueChangeHandler(this);
	}
	
	private void setupStudent()
	{
		accountService.getStudentBillKeys(billListCallBack);
	}
	
	private void setupGuardian()
	{
		guardianList = new GuardianStudentPicker();
		studentListSlot.add(guardianList);
		guardianList.addValueChangeHandler(this);
		useLogins = false;
	}
	
	private void setupDetails(List<TableMessage> info)
	{
		ArrayList<TableMessage> itemizedCopy = new ArrayList<TableMessage>(info.size());
		ArrayList<TableMessage> historyCopy = new ArrayList<TableMessage>(info.size());
	    TableMessage studBillInfo = info.get(0); //settlement info
	    double totalCheck = studBillInfo.getNumber(0);
	    double priorPayAmount = studBillInfo.getNumber(1);
		setupSummaryInfo(info.get(1), priorPayAmount, totalCheck); //bill template info
		itemizedCopy.add(info.get(2));
		boolean switchToHistory = false;
		for(int i = 3; i < info.size(); i++)
		{
			TableMessage m = info.get(i);
			if(m instanceof TableMessageHeader)
				switchToHistory = true;
			if(switchToHistory)
				historyCopy.add(m);
			else
				itemizedCopy.add(m);
		}
		itemizedSlot.populateTable(itemizedCopy);
		paySlot.populateTable(historyCopy);
	}
	
	private void setupSummaryInfo(TableMessage m, double priorPayAmount, double totalCheck)
	{
		billName.setText(m.getText(0));
		billYear.setText(m.getText(1));
		billTerm.setText(m.getText(2));
		billAmount.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0)));
		billDate.setText(m.getDate(0)==null?"":GUIConstants.DEFAULT_DATE_FORMAT.format(m.getDate(0)));
		priorPayments.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(priorPayAmount));
		balance.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0) - priorPayAmount));
		if(Math.abs(m.getNumber(0) - totalCheck) > 0.000001 )
		{
			WidgetHelper.setErrorStatus(status, "Total for bill doesn't match its template. " +
					"This should not be the case, please alert your administrator about the discrepancy", true);
		}
	}	
	
	private void clearFields()
	{
		billName.setText("");
		billYear.setText("");
		billTerm.setText("");
		billAmount.setText("");
		billDate.setValue(null);
		itemizedSlot.clear();
		priorPayments.setText("");
		balance.setText("");
		paySlot.clear();
		status.setText("");
	}
	
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		if(studentList != null)
			return studentList.doneLoading();
		return doneLoading;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		GWT.log("RECEIVED change event");
		clearFields();
		billList.clear();
		String selectedStudent = event.getValue();
		WidgetHelper.setNormalStatus(status, "Fetching list of student bills. Please wait ...");
		HashMap<String, String> billKeys = cachedBillKeys.get(selectedStudent);
		GWT.log("Selected Student: " + selectedStudent);
		if(billKeys == null)
			accountService.getStudentBillKeys(selectedStudent, useLogins, billListCallBack);
		else
		{
			GWT.log("BillPayment cache hit!!!");
			populateBillBox(billKeys);
		}
	}
	
	private void populateBillBox(HashMap<String, String> billKeys)
	{
		for(String bk : billKeys.keySet())
			billList.addItem(billKeys.get(bk), bk);
	}	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		int idx = billList.getSelectedIndex();
		if(idx < 0)
		{
			WidgetHelper.setErrorStatus(status, "No bill found for student. A bill must " +
					"be generated for the student before you can view it. If one was recently generated, try refreshing your browser", true);
			return;
		}
		
		WidgetHelper.setNormalStatus(status, "Fetching billing details for " + billList.getItemText(idx) + ". Please wait ...");
		List<TableMessage> cachedVal = cachedBillData.get(billList.getValue(idx));
		if(cachedVal == null)
			accountService.getStudentBillAndPayment(billList.getValue(billList.getSelectedIndex()), viewBillCallBack);
		else
			setupDetails(cachedVal);
	}

}
