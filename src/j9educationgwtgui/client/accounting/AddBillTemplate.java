package j9educationgwtgui.client.accounting;

import j9educationgwtgui.client.AccountManager;
import j9educationgwtgui.client.AccountManagerAsync;
import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.client.SchoolServiceAsync;
import j9educationgwtgui.client.custom.widgets.BillingItemWrapper;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.MultipleMessageDialog;
import j9educationgwtgui.client.custom.widgets.Text2ListBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.BillDescriptionItem;
import j9educationgwtgui.shared.PanelServiceConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class AddBillTemplate extends Composite implements DoneLoadingWidget, ClickHandler{

	@UiField DateBox dueDate;
	@UiField ListBox termList;
	@UiField ListBox yearList;
	@UiField TextBox billTemplateName;
	@UiField Text2ListBox billItems;
	@UiField Button saveBill;
	@UiField Label status;
	
	private MultipleMessageDialog errorBox;
	private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);
	private AccountManagerAsync accountService = GWT.create(AccountManager.class);
	private boolean doneLoading = false;
	
	private final static int VISIBLE_BILLABLE_ITEMS = 4;
	private final static String VISIBLE_BILLABLE_WIDTH = "300px";
	private final static String DEFAULT_BOX_WIDTH = "150px";
	
	private static AddBillTemplateUiBinder uiBinder = GWT
			.create(AddBillTemplateUiBinder.class);

	interface AddBillTemplateUiBinder extends UiBinder<Widget, AddBillTemplate> {
	}
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> billTemplateCallback = new AsyncCallback<String>() {
        @Override
        public void onSuccess(String result) {
        	billTemplateName.setText("");
        	dueDate.setValue(null);
        	billItems.clear();
            WidgetHelper.setSuccessStatus(status, "Created bill template [" + result + "]" +
            		" succesfully. Click on 'create bills' on naviagation menu to generate student bills from this template", true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
        	String msg = "Request failed: " + caught.getMessage();
        	WidgetHelper.setErrorStatus(status, msg, true);
            
        }
    };	
	
    final AsyncCallback<HashMap<String, HashSet<String>>> initOptionsCallback = new AsyncCallback<HashMap<String,HashSet<String>>>() {
        public void onSuccess(HashMap<String,HashSet<String>> result) {
        	doneLoading = true;
            HashMap<String, ListBox> listMap = new HashMap<String, ListBox>(result.size());
            listMap.put(PanelServiceConstants.ACADEMIC_YEAR_LABEL, yearList);
            listMap.put(PanelServiceConstants.TERM_LABEL, termList);
            
            for(String optMode : result.keySet())
            {
            	ListBox targetList = listMap.get(optMode);
            	for(String optModeVals : result.get(optMode))
            		targetList.addItem(optModeVals);
            }
            WidgetHelper.setSuccessStatus(status, "Successfully setup academic term/year" +
            		" lists. Populate form and click [Save Bill Template] button to procede", false);
        }
        
        public void onFailure(Throwable caught) {
            String message = "Error getting initial academic (year/term) option values from server. " +
                    "Save feature disabled. Try refreshing browser to fix";
            WidgetHelper.setErrorStatus(status, message, true);
            saveBill.setEnabled(false);
            doneLoading = true;
        }
    };        

	public AddBillTemplate() {
		initWidget(uiBinder.createAndBindUi(this));
		BillingItemWrapper biw = new BillingItemWrapper();
		billItems.initInput(biw);
        billItems.setVisbileArea(VISIBLE_BILLABLE_ITEMS, VISIBLE_BILLABLE_WIDTH);
        billItems.enableBox();
        billItems.removeAdditionalFeatures();
        termList.setWidth(DEFAULT_BOX_WIDTH);
        yearList.setWidth(DEFAULT_BOX_WIDTH);
        billTemplateName.setWidth(DEFAULT_BOX_WIDTH);
        dueDate.setWidth(DEFAULT_BOX_WIDTH);
		dueDate.setValue(null);
		errorBox = new MultipleMessageDialog("Error!!");
		String[] termYearOptionNames = {PanelServiceConstants.ACADEMIC_YEAR_LABEL, PanelServiceConstants.TERM_LABEL};
		WidgetHelper.setNormalStatus(status, "Fetching term/year option values. Please wait ...");
		schoolService.getInitialOptions(termYearOptionNames, initOptionsCallback);
		saveBill.addClickHandler(this);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return doneLoading;
	}
	
	private String getSelectedItem(ListBox lb)
	{
		return lb.getValue(lb.getSelectedIndex());
	}
	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		ArrayList<String> errors = new ArrayList<String>();
		if(billTemplateName.getText().trim().length() == 0)
			errors.add("Must enter a value for 'bill template name' field");
		
		LinkedHashSet<BillDescriptionItem> billDescs = null;
		
		HashSet<String> itemStrings = billItems.getList();
		if(itemStrings.size() == 0)
			errors.add("Bill template must have at least 1 billable ITEM added");
		else
		{
			billDescs = new LinkedHashSet<BillDescriptionItem>(itemStrings.size());
			for(String item : itemStrings)
			{
				String[] parts = item.split(BillingItemWrapper.SEPERATOR_REGEX);
				if(parts.length != 3)
				{
					errors.add("Unable to parse billing item information. Please ensure only alphanumeric characters are used.");
					continue;
				}
				billDescs.add(new BillDescriptionItem(parts[BillingItemWrapper.NAME_IDX], 
						parts[BillingItemWrapper.COMMENT_IDX], Double.parseDouble(parts[BillingItemWrapper.AMOUNT_IDX])));
			}
		}
		
		if(errors.size() > 0)
		{
			errorBox.show("Save request aborted because:", errors);
			return;
		}
		
		//rpc call to persist bill
		WidgetHelper.setNormalStatus(status, "Sending save request to server, please wait ...");
		accountService.createBillTemplate(billTemplateName.getText().trim(), getSelectedItem(yearList), 
				getSelectedItem(termList), dueDate.getValue(), billDescs, billTemplateCallback);
	}

}
