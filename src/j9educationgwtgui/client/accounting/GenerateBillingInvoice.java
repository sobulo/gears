/**
 * 
 */
package j9educationgwtgui.client.accounting;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.widgets.BillTemplateBox;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.ListBox2ListBox;
import j9educationgwtgui.client.custom.widgets.SimpleDialog;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class GenerateBillingInvoice extends Composite implements ClickHandler, ValueChangeHandler<String>, DoneLoadingWidget {
	@UiField
	BillTemplateBox btList;
	@UiField
	ListBox2ListBox studentBox;
	@UiField
	Button requestInvoice;
	@UiField
	HTML status;

	private final static int MAX_DISPLAY_ITEMS = 15;
	private SimpleDialog errorBox;
	private SimpleDialog messageBox;

	private static GenerateBillingInvoiceUiBinder uiBinder = GWT
			.create(GenerateBillingInvoiceUiBinder.class);

	interface GenerateBillingInvoiceUiBinder extends
			UiBinder<Widget, GenerateBillingInvoice> {
	}

	// Create an asynchronous callback to handle the result.
	final AsyncCallback<HashMap<String, String>> classRosterCallBack = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), false);
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			studentBox.initValues(result, false);
			WidgetHelper.setSuccessStatus(status,
					"Showing " + result.size() + " students for source class "
							+ btList.getSelectedBillTemplateName(), false);
		}
	};

	final AsyncCallback<String> invoiceCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.checkForLoginError(caught);
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(String result) {
			messageBox.show("Setup for invoice/bill generation completed. " +
					"Please click on link in status box to start download");
			GWT.log(result);
			status.setHTML(result);
		}
	};

	/**
	 * Because this class has a default constructor, it can be used as a binder
	 * template. In other words, it can be used in other *.ui.xml files as
	 * follows: <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 * xmlns:g="urn:import:**user's package**">
	 * <g:**UserClassName**>Hello!</g:**UserClassName> </ui:UiBinder> Note that
	 * depending on the widget that is used, it may be necessary to implement
	 * HasHTML instead of HasText.
	 */
	public GenerateBillingInvoice() {
		initWidget(uiBinder.createAndBindUi(this));
        btList.addValueChangeHandler(this);
        btList.initData();
        requestInvoice.addClickHandler(this);
        studentBox.initAppearance(MAX_DISPLAY_ITEMS, GUIConstants.DEFAULT_LB_WIDTH, "Class List", "Selected Students List");
        errorBox = new SimpleDialog("An error occurred");
        messageBox = new SimpleDialog("Info");			
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return btList.doneLoading();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String selected = btList.getSelectedBillTemplate();
		String name = btList.getSelectedBillTemplateName();
		studentBox.initValues(null);
		WidgetHelper.setNormalStatus(status, "Fetching student list for: " + name + ". Please wait ...");
		btList.getAccountService().getStudentBillKeysFromTemplate(selected, classRosterCallBack);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
        HashSet<String> chosen = studentBox.getValues();
        String sourceKey = btList.getSelectedBillTemplate();
        String sourceName = btList.getSelectedBillTemplateName();
        
        //perform validation
        if(chosen.size() == 0)
        {
        	errorBox.show("You must add at least 1 student to target box!");
        	return;
        }
        
        WidgetHelper.setNormalStatus(status, "Requesting " + sourceName + " invoice/bill generation for " 
        		+ chosen.size() + " students. Please wait ...");
        //send list to server to save
        String[] studentKeys = new String[chosen.size()];
        btList.getAccountService().getInvoiceDownloadLink(sourceKey,
                chosen.toArray(studentKeys), invoiceCallBack);
		
	}

}
