/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.schoolpanels;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.client.SchoolServiceAsync;
import j9educationgwtgui.client.custom.widgets.Text2ListBox;
import j9educationgwtgui.client.custom.widgets.TextBoxWrapper;
import j9educationgwtgui.client.custom.widgets.YearRangeWrapper;
import j9educationgwtgui.shared.PanelServiceConstants;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class SchoolUpdatePanel extends ResizeComposite implements 
        ClickHandler, SelectionHandler<Integer>
{
    private static SchoolUpdatePanelUiBinder uiBinder = GWT.create(SchoolUpdatePanelUiBinder.class);

    interface SchoolUpdatePanelUiBinder extends UiBinder<Widget, SchoolUpdatePanel> {
    }

    private final static String DEFAULT_WIDTH = GUIConstants.DEFAULT_STACK_WIDTH;
    private final static String DEFAULT_HEIGHT = GUIConstants.DEFAULT_STACK_HEIGHT;

    private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);

    //layout panel
    @UiField StackLayoutPanel stackContainer;
    @UiField HTMLPanel classPanel;

    //year gui fields
    @UiField Text2ListBox yearText2ListBox;

    //term gui fields
    @UiField Text2ListBox termText2ListBox;

    //class name gui fields
    @UiField Text2ListBox classNameText2ListBox;

    //sub-class name gui fields
    @UiField Text2ListBox subClassNameText2ListBox;

    //default subjects gui fields
    @UiField Text2ListBox defaultSubjectsText2ListBox;

    //class gui fields
    @UiField ListBox classOpt;
    @UiField ListBox subClassOpt;
    @UiField ListBox classYearOpt;
    @UiField ListBox classTermOpt;
    @UiField ListBox classDisplay;
    @UiField Label classStatus;
    @UiField Button saveClass;


    // Create asynchronous callbacks to handle result of service calls.
    final AsyncCallback<Boolean> yearCallback = new AsyncCallback<Boolean>() {
        public void onSuccess(Boolean result) {
            if(result)
                yearText2ListBox.setStatus("Academic years information succesfully saved");
            else
                yearText2ListBox.setStatus("Data not persisted but no exception info available");
        }

        public void onFailure(Throwable caught) {
            yearText2ListBox.setStatus("Request failed: " + caught.getMessage());
        }
    };

    final AsyncCallback<Boolean> termCallback = new AsyncCallback<Boolean>() {
        public void onSuccess(Boolean result) {
            if(result)
                termText2ListBox.setStatus("Term information succesfully saved");
            else
                termText2ListBox.setStatus("Data not persisted but no exception info available");
        }

        public void onFailure(Throwable caught) {
            termText2ListBox.setStatus("Request failed: " + caught.getMessage());
        }
    };
    
    final AsyncCallback<Boolean> classNameCallback = new AsyncCallback<Boolean>() {
        public void onSuccess(Boolean result) {
            if(result)
              classNameText2ListBox.setStatus("Class name information succesfully saved");
            else
                classNameText2ListBox.setStatus("Data not persisted but no exception info available");
        }

        public void onFailure(Throwable caught) {
            classNameText2ListBox.setStatus("Request failed: " + caught.getMessage());
        }
    };
    
    final AsyncCallback<Boolean> subClassNameCallback = new AsyncCallback<Boolean>() {
        public void onSuccess(Boolean result) {
            if(result)
                subClassNameText2ListBox.setStatus("Term information succesfully saved");
            else
                subClassNameText2ListBox.setStatus("Data not persisted but no exception info available");
        }

        public void onFailure(Throwable caught) {
            subClassNameText2ListBox.setStatus("Request failed: " + caught.getMessage());
        }
    };

    final AsyncCallback<String> saveClassCallback = new AsyncCallback<String>() {
        public void onSuccess(String result) {
            classDisplay.addItem(result);
            classStatus.setText("successfully created class: " + result);
        }

        public void onFailure(Throwable caught) {
            classStatus.setText("Request failed: " + caught.getMessage());
        }
    };

    final AsyncCallback<Boolean> defaultSubjectsCallback = new AsyncCallback<Boolean>() {
        public void onSuccess(Boolean result) {
            if(result)
                defaultSubjectsText2ListBox.setStatus("Default subject list succesfully saved");
            else
                defaultSubjectsText2ListBox.setStatus("Data not persisted but no exception info available");
        }

        public void onFailure(Throwable caught) {
            defaultSubjectsText2ListBox.setStatus("Request failed: " + caught.getMessage());
        }
    };

    final AsyncCallback<String[]> initClassListCallback = new AsyncCallback<String[]>() {
        public void onSuccess(String[] result) {
            for(int i = 0; i < result.length; i++)
                classDisplay.addItem(result[i]);
            classStatus.setText("Num of classes loaded from server: " + result.length);
        }

        public void onFailure(Throwable caught) {
            classStatus.setText("Request failed: " + caught.getMessage());
        }
    };

    final AsyncCallback<HashMap<String, HashSet<String>>> initOptionsCallback = new AsyncCallback<HashMap<String,HashSet<String>>>() {
        public void onSuccess(HashMap<String,HashSet<String>> result) {
            HashMap<String, Text2ListBox> listMap = new HashMap<String, Text2ListBox>(result.size());

            listMap.put(PanelServiceConstants.ACADEMIC_YEAR_LABEL, yearText2ListBox);
            listMap.put(PanelServiceConstants.TERM_LABEL, termText2ListBox);
            listMap.put(PanelServiceConstants.CLASS_NAME_LABEL, classNameText2ListBox);
            listMap.put(PanelServiceConstants.SUB_CLASS_NAME_LABEL, subClassNameText2ListBox);
            listMap.put(PanelServiceConstants.DEFAULT_SUBJECTS_LABEL, defaultSubjectsText2ListBox);
       
            HashSet<String> initVals;
            for( Entry<String, HashSet<String>> e: result.entrySet())
            {
                if((initVals = e.getValue()) != null)
                {
                    Iterator<String> i = initVals.iterator();
                    Text2ListBox disp = listMap.get(e.getKey());
                    String val;
                    while(i.hasNext())
                    {
                        val = i.next();
                        disp.addItemToList(val);
                    }
                    disp.enableBox();
                }
            }
        }

        public void onFailure(Throwable caught) {
            String message = "Error getting initial option values from server. " +
                    "Save feature disabled. Try refreshing browser to fix";
            yearText2ListBox.setStatus(message);
            termText2ListBox.setStatus(message);
            classNameText2ListBox.setStatus(message);
            subClassNameText2ListBox.setStatus(message);
            classStatus.setText(message);
            yearText2ListBox.disableBox();
            termText2ListBox.disableBox();
            classNameText2ListBox.disableBox();
            subClassNameText2ListBox.disableBox();
            defaultSubjectsText2ListBox.disableBox();
            saveClass.setEnabled(false);
        }
    };


    @Override
    public void onLoad()
    {
        schoolService.getInitialOptions(initOptionsCallback);
        schoolService.getSimpleClassNames(initClassListCallback);
    }

    public SchoolUpdatePanel() {
        initWidget(uiBinder.createAndBindUi(this));
        this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        //setup year fields;
        YearRangeWrapper yrw = new YearRangeWrapper();
        yearText2ListBox.initInput(yrw);
        yearText2ListBox.setVisbileArea(GUIConstants.VISIBLE_YEARS,
                GUIConstants.DEFAULT_LB_WIDTH);
        yearText2ListBox.registerSaveButtonHandler(this);


        //setup term fields
        TextBoxWrapper termWrapper = new TextBoxWrapper();
        termWrapper.setLabel(GUIConstants.TERM_INPUT_LABEL);
        termText2ListBox.initInput(termWrapper);
        termText2ListBox.setVisbileArea(GUIConstants.VISIBLE_TERMS,
                GUIConstants.DEFAULT_LB_WIDTH);
        termText2ListBox.registerSaveButtonHandler(this);

        //setup class name fields
        TextBoxWrapper tbw = new TextBoxWrapper();
        tbw.setLabel(GUIConstants.CLASS_NAME_INPUT_LABEL);
        classNameText2ListBox.initInput(tbw);
        classNameText2ListBox.setVisbileArea(GUIConstants.VISIBLE_CLASSES,
                GUIConstants.DEFAULT_LB_WIDTH);
        classNameText2ListBox.registerSaveButtonHandler(this);

        //setup sub-class name fields
        tbw = new TextBoxWrapper();
        tbw.setLabel(GUIConstants.SUB_CLASS_NAME_INPUT_LABEL);
        subClassNameText2ListBox.initInput(tbw);
        subClassNameText2ListBox.setVisbileArea(GUIConstants.VISIBLE_SUB_CLASSES,
                GUIConstants.DEFAULT_LB_WIDTH);
        subClassNameText2ListBox.registerSaveButtonHandler(this);

        //setup default subject list fields
        tbw = new TextBoxWrapper();
        tbw.setLabel(PanelServiceConstants.DEFAULT_SUBJECTS_LABEL);
        defaultSubjectsText2ListBox.initInput(tbw);
        defaultSubjectsText2ListBox.setVisbileArea(GUIConstants.VISIBLE_SUB_CLASSES,
                GUIConstants.DEFAULT_LB_WIDTH);
        defaultSubjectsText2ListBox.registerSaveButtonHandler(this);

        //setup class fields
        classOpt.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        subClassOpt.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        classYearOpt.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        classTermOpt.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        saveClass.addClickHandler(this);

        stackContainer.addSelectionHandler(this);

        //TODO setup school info update stack
    }

    //ClickHandler interface fns 
    public void onClick(ClickEvent event) {
        String saveMessage = "sending save request to server...";
        Object src = event.getSource();
        if(yearText2ListBox.isSaveButton(src))
        {
            yearText2ListBox.setStatus(saveMessage);
            schoolService.updateAcademicYears(yearText2ListBox.getList(),
                    yearCallback);
        }
        else if(termText2ListBox.isSaveButton(src))
        {
            termText2ListBox.setStatus(saveMessage);
            schoolService.updateAcademicTerms(termText2ListBox.getList(),
                    termCallback);
        }
        else if(classNameText2ListBox.isSaveButton(src))
        {
            classNameText2ListBox.setStatus(saveMessage);
            schoolService.updateClassNames(classNameText2ListBox.getList(),
                    classNameCallback);
        }
        else if(subClassNameText2ListBox.isSaveButton(src))
        {
            subClassNameText2ListBox.setStatus(saveMessage);
            schoolService.updateSubClassNames(subClassNameText2ListBox.getList(),
                    subClassNameCallback);
        }
        else if(defaultSubjectsText2ListBox.isSaveButton(src))
        {
            defaultSubjectsText2ListBox.setStatus(saveMessage);
            schoolService.updateDefaultSubjects(defaultSubjectsText2ListBox.getList(),
                    defaultSubjectsCallback);
        }
        else if(src.equals(saveClass))
        {
            classStatus.setText("contacting server to save class...");
            String classArg = classOpt.getValue(classOpt.getSelectedIndex());
            String subClassArg = subClassOpt.getValue(subClassOpt.getSelectedIndex());
            String yearArg = classYearOpt.getValue(classYearOpt.getSelectedIndex());
            String termArg = classTermOpt.getValue(classTermOpt.getSelectedIndex());
            schoolService.createClass(classArg, subClassArg, yearArg, termArg, saveClassCallback);
        }
    }

    @Override
    public void onSelection(SelectionEvent<Integer> event) {
        if(event.getSelectedItem() == stackContainer.getWidgetIndex(classPanel))
        {
            HashMap<String, HashSet<String>> listMap = new HashMap<String, HashSet<String>>(4);
            HashMap<String, ListBox> classOptMap = new HashMap<String, ListBox>(4);

            listMap.put(PanelServiceConstants.ACADEMIC_YEAR_LABEL, yearText2ListBox.getList());
            listMap.put(PanelServiceConstants.TERM_LABEL, termText2ListBox.getList());
            listMap.put(PanelServiceConstants.CLASS_NAME_LABEL, classNameText2ListBox.getList());
            listMap.put(PanelServiceConstants.SUB_CLASS_NAME_LABEL, subClassNameText2ListBox.getList());
            classOptMap.put(PanelServiceConstants.ACADEMIC_YEAR_LABEL, classYearOpt);
            classOptMap.put(PanelServiceConstants.TERM_LABEL, classTermOpt);
            classOptMap.put(PanelServiceConstants.CLASS_NAME_LABEL, classOpt);
            classOptMap.put(PanelServiceConstants.SUB_CLASS_NAME_LABEL, subClassOpt);

            HashSet<String> initVals;
            for( Entry<String, HashSet<String>> e: listMap.entrySet())
            {
                if((initVals = e.getValue()) != null)
                {
                    Iterator<String> i = initVals.iterator();
                    ListBox classOptDisp = classOptMap.get(e.getKey());
                    String val;
                    classOptDisp.clear();
                    while(i.hasNext())
                    {
                        val = i.next();
                        classOptDisp.addItem(val);
                    }
                }
            }
        }
    }
}