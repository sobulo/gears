/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.schoolpanels;

import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.client.SchoolServiceAsync;
import j9educationgwtgui.shared.PanelServiceConstants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class SchoolCreationPanel extends Composite{

    private static SchoolCreationPanelUiBinder uiBinder = GWT.create(SchoolCreationPanelUiBinder.class);

    interface SchoolCreationPanelUiBinder extends UiBinder<Widget, SchoolCreationPanel> {
    }

    @UiField TextBox name;
    @UiField TextBox accronym;
    @UiField TextBox email;
    @UiField TextBox url;
    @UiField TextBox address;
    @UiField TextBox phoneNumbers;
    @UiField Button submit;
    @UiField Label status;
    @UiField CheckBox updateMode;
    
 
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> schoolCallback = new AsyncCallback<String>() {
        public void onSuccess(String result) {
            status.setText("updated school [" + result + "] succesfully");
            reset();
        }

        public void onFailure(Throwable caught) {
            status.setText("Request failed: " + caught.getMessage());
        }
    };    

    final AsyncCallback<String[]> infoCallback = new AsyncCallback<String[]>() {
        public void onSuccess(String[] result) {
        	status.setText("Retrieved " + result.length + " entries");
            name.setText(result[PanelServiceConstants.SCHOOL_INFO_NAME_IDX]);
            email.setText(result[PanelServiceConstants.SCHOOL_INFO_EMAIL_IDX]);
            url.setText(result[PanelServiceConstants.SCHOOL_INFO_WEB_IDX]);
            address.setText(result[PanelServiceConstants.SCHOOL_INFO_ADDR_IDX]);
            phoneNumbers.setText(result[PanelServiceConstants.SCHOOL_INFO_NUMS_IDX]);
            accronym.setText(result[PanelServiceConstants.SCHOOL_INFO_ACCR_IDX]);  
        }

        public void onFailure(Throwable caught) {
            status.setText("Request failed: " + caught.getMessage());
        }
    };    

    private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);
    
    private void reset()
    {
        name.setText("");
        email.setText("");
        url.setText("");
        address.setText("");
        phoneNumbers.setText("");
        accronym.setText("");    	
    }

    public SchoolCreationPanel() {
        initWidget(uiBinder.createAndBindUi(this));
        
        updateMode.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				reset();
				if(updateMode.getValue())
				{
					status.setText("fetching school info ...");
					schoolService.getSchoolInfo(infoCallback);
				}
			}
		});

        submit.addClickHandler(new ClickHandler(){
            public void onClick(ClickEvent event) {
                // Make remote call. Control flow will continue immediately and later
                // 'callback' will be invoked when the RPC completes.
                status.setText("making request to server to persist school information");
                schoolService.createSchool(name.getText(), email.getText(), accronym.getText(), 
                		address.getText(), url.getText(), phoneNumbers.getText(), updateMode.getValue(), schoolCallback);
            }
        });
    }
}