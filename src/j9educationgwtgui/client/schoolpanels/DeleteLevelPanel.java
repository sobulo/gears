/**
 * 
 */
package j9educationgwtgui.client.schoolpanels;

import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.client.SchoolServiceAsync;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class DeleteLevelPanel extends Composite implements ClickHandler{
	
	@UiField
	Button deleteLevel;
	
	@UiField
	Label status;
	
	@UiField
	LevelBox classList;
	

    private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);	
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> deleteCallback =
            new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), true);
        }

        @Override
        public void onSuccess(Void result)
        {
        	WidgetHelper.setSuccessStatus(status, "Successfully deleted class", true);
        }
    };	
	

	private static DeleteLevelPanelUiBinder uiBinder = GWT
			.create(DeleteLevelPanelUiBinder.class);

	interface DeleteLevelPanelUiBinder extends
			UiBinder<Widget, DeleteLevelPanel> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public DeleteLevelPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		classList.initData();
		deleteLevel.addClickHandler(this);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		schoolService.deleteLevel(classList.getSelectedClass(), deleteCallback);	
	}

}
