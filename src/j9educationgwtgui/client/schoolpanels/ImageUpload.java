/**
 * 
 */
package j9educationgwtgui.client.schoolpanels;



import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ImageUpload extends Composite implements ClickHandler, FormPanel.SubmitCompleteHandler{

	@UiField Button submit;
	@UiField HTML status;
	@UiField FileUpload selectFile;
	@UiField FormPanel imageForm;
	
	private static ImageUploadUiBinder uiBinder = GWT
			.create(ImageUploadUiBinder.class);

	interface ImageUploadUiBinder extends UiBinder<Widget, ImageUpload> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ImageUpload() {
		initWidget(uiBinder.createAndBindUi(this));
        submit.addClickHandler(this);
        imageForm.setAction("/imageupload");
        imageForm.addSubmitCompleteHandler(this);
        imageForm.setMethod(FormPanel.METHOD_POST);
        imageForm.setEncoding(FormPanel.ENCODING_MULTIPART);
        selectFile.setName("chooseFile");
	}
	
    @Override
    public void onSubmitComplete(FormPanel.SubmitCompleteEvent event)
    {
        String results = event.getResults();
        status.setHTML("Received " + results.length() + " chars: " + results);
        GWT.log(results);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		status.setText("Sending file to server...");
		imageForm.submit();
	}	

}
