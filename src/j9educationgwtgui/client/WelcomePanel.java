/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client;

import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class WelcomePanel extends Composite implements DoneLoadingWidget{

    @UiField HTML loginLabel;
    @UiField HorizontalPanel linkPanel;
    @UiField Label header;
    @UiField HTML noticePanel;
    @UiField SimplePanel roleSlot;
    
    private boolean loadingCompleted;
    private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);
    private static WelcomePanelUiBinder uiBinder = GWT.create(WelcomePanelUiBinder.class);
    
    private RadioButton[] cachedRoleOptions = null;

    interface WelcomePanelUiBinder extends UiBinder<Widget, WelcomePanel> {
    }

    final AsyncCallback<String[]> noticeCallback = new AsyncCallback<String[]>() {

        @Override
        public void onSuccess(String[] result) {
        	noticePanel.setHTML("<b><i>" + result[0] + "</i></b>" + result[1]);
        	loadingCompleted = true;
        }

        @Override
        public void onFailure(Throwable caught) {
        	String output = "<b>Error loading notice board: </b><br/>" + caught.getMessage();
        	noticePanel.setHTML(output);
        	loadingCompleted = true;
        }
    };    
    
    public WelcomePanel() {
        initWidget(uiBinder.createAndBindUi(this));
        linkPanel.setSpacing(10);
    }

    public void setLoginLabel(String label)
    {
        loginLabel.setHTML(label);
    }

    public void setAnchor(String label, String url, PanelServiceLoginRoles role)
    {
        linkPanel.clear();
        Anchor logoutLink = new Anchor(label, url);
        linkPanel.add(logoutLink);
        schoolService.getNotice(WebAppHelper.getNoticeId(role), noticeCallback);
    }

    public void setAnchor(HashMap<String, String> linkInfo)
    {
        linkPanel.clear();
        roleSlot.clear();
        roleSlot.add(getRoleOptionsPanel());
        ArrayList<String> sortedKeys = new ArrayList<String>(linkInfo.size());
        sortedKeys.addAll(linkInfo.keySet());
        Collections.sort(sortedKeys);
        for(String key : sortedKeys)
        {
        	if(key.equals(PanelServiceConstants.PROVIDER_GREP))
        		continue;
            VerticalPanel loginLogo = getLoginProviderPanel(key, linkInfo.get(key));
            linkPanel.add(loginLogo);
            linkPanel.setCellVerticalAlignment(loginLogo, VerticalPanel.ALIGN_MIDDLE);
        }
        schoolService.getNotice(PanelServiceConstants.PUBLIC_NOTE_ID, noticeCallback);
    }
    
    private VerticalPanel getLoginProviderPanel(final String provider, final String providerUrl)
    {
    	VerticalPanel contentPanel = new VerticalPanel();
    	Image logo;
    	Anchor link;
    	if(provider.equals(PanelServiceConstants.PROVIDER_GREP))
    	{
    		//for grep provider, use the school logo
    		logo = new Image(GUIConstants.MODULE_IMAGE_FOLDER + "keys_icon.png");
    		link = new Anchor(WebAppHelper.getSchoolAccronym().toUpperCase() + 
    				" Login", providerUrl);
    	}
    	else
    	{
    		logo = new Image(GUIConstants.MODULE_IMAGE_FOLDER + provider + 
    				GUIConstants.IMG_DEFAULT_EXT);
    		link = new Anchor(provider.toUpperCase() + " Login", providerUrl);  		
    	}
    	
    	ClickHandler handler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String appendArg = null;
				for(int i = 0; i < cachedRoleOptions.length; i++)
					if(cachedRoleOptions[i].getValue())
						appendArg = "&" + PanelServiceConstants.ROLE_TYPE_REQ + "=" + cachedRoleOptions[i].getFormValue();
				if(event.getSource() instanceof Anchor)
					((Anchor) event.getSource()).setHref(providerUrl + appendArg);
				Window.Location.replace(providerUrl + appendArg);
			}
		};
		
		logo.addClickHandler(handler);
		link.addClickHandler(handler);
		
		contentPanel.add(logo);
		contentPanel.add(link);
		contentPanel.setCellHorizontalAlignment(link, HorizontalPanel.ALIGN_CENTER);		
    	return contentPanel;
    }
    
    public void setSchoolHeader(String schoolName)
    {
    	header.setText(schoolName);
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		// TODO Auto-generated method stub
		return loadingCompleted;
	}

	public HorizontalPanel getRoleOptionsPanel() 
	{
		HorizontalPanel options = new HorizontalPanel();
        PanelServiceLoginRoles[] loginRoles =
        {
            PanelServiceLoginRoles.ROLE_STUDENT,
            PanelServiceLoginRoles.ROLE_GUARDIAN,
            PanelServiceLoginRoles.ROLE_TEACHER,
            PanelServiceLoginRoles.ROLE_ADMIN
        };
		
        String[] roleNames =
        {
            "Student",
            "Parent/Guardian",
            "Teacher",
            "Admin"
        };
        
        options.setSpacing(10);
        cachedRoleOptions = new RadioButton[loginRoles.length];
        for(int i = 0; i < loginRoles.length; i++)
        {
        	RadioButton rb = new RadioButton("role", roleNames[i]);
        	rb.setFormValue(loginRoles[i].toString().toLowerCase());
        	options.add(rb);
        	cachedRoleOptions[i] = rb;
        }
        cachedRoleOptions[0].setValue(true);
        return options;
	}
}