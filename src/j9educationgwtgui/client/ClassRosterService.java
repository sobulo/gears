/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 *
 * @author Administrator
 */
@RemoteServiceRelativePath("classrosterservice")
public interface ClassRosterService extends RemoteService {
    public HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> getClassKeys() throws 
            MissingEntitiesException, LoginValidationException;
    
    public String addStudent(String classKey, String fname, String lname,
            Date dob, String email, HashSet<String> phoneNumbers, String loginName, 
            String password) throws MissingEntitiesException,
            DuplicateEntitiesException, LoginValidationException;

    public String addGuardian(String studKey, String fname, String lname,
            String email, HashSet<String> phoneNumbers, String loginName,
            String password) throws MissingEntitiesException,
            DuplicateEntitiesException, LoginValidationException;

    public ArrayList<TableMessage> getClassRosterTable(String classKey) throws
            MissingEntitiesException, LoginValidationException;
    
    public String[] getSimpleCourseNames(String classKey) throws MissingEntitiesException, LoginValidationException;

    public String addCourse(String subjectName, String classKey) throws
            MissingEntitiesException, DuplicateEntitiesException, LoginValidationException;

    public String copyCourses(String[] courseKeyStrs, String classKeyStr, boolean copyStudents) throws
    MissingEntitiesException, DuplicateEntitiesException, LoginValidationException;    
    
    public HashMap<String, String> getClassRosterMap(String classKey) throws
            MissingEntitiesException, LoginValidationException;

    public void addStudentsToCourse(String levelKeyStr, String courseKey, String[] studentKeys)
            throws MissingEntitiesException, DuplicateEntitiesException, LoginValidationException;

    public ArrayList<TableMessage> getCourseRosterTable(String courseKey) throws
            MissingEntitiesException, LoginValidationException;

    public HashMap<String, String> getCourseKeys(String classKey) throws
            MissingEntitiesException, LoginValidationException;

    public void addStudentsToLevel(String levelKeyStr, String[] studentKeys)
            throws DuplicateEntitiesException, MissingEntitiesException, LoginValidationException;
    
    public void removeStudentsFromLevel(String levelKeyStr, String[] studentKeys)
    		throws MissingEntitiesException, ManualVerificationException, LoginValidationException;
    
    public ArrayList<TableMessage> getCourseTable(String classKey) 
    	throws MissingEntitiesException, LoginValidationException;
    
    public void deleteCourse(String courseKey) throws MissingEntitiesException, 
    	ManualVerificationException, LoginValidationException;
    
    public void removeStudentsFromCourse(String courseKeyStr, String[] studentKeys)
		throws MissingEntitiesException, LoginValidationException;
    
    public HashMap<String, String> getCourseRosterMap(String courseKeyStr) throws 
    	MissingEntitiesException, LoginValidationException; 
    
    public HashMap<String, String> getAcademicTermInfo4Levels() throws MissingEntitiesException, LoginValidationException;
    
    public String requestReportCards(String classKey, String[] studentLogins) throws MissingEntitiesException, LoginValidationException;
    
    public ArrayList<TableMessage> getCoursePublishStatusTable(String levelKeyStr) throws MissingEntitiesException, LoginValidationException;
    
    public String updateCourseTestStatus(String levelKeyStr, HashMap<String, Boolean> testStatusInfo) throws MissingEntitiesException, LoginValidationException;

	ArrayList<TableMessage> updateCourseTable(
			HashMap<String, Integer> courseUnits) throws LoginValidationException, MissingEntitiesException;
}
