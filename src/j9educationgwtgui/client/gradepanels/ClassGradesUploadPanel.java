/**
 * 
 */
package j9educationgwtgui.client.gradepanels;

import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceConstants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ClassGradesUploadPanel extends Composite implements ClickHandler, FormPanel.SubmitCompleteHandler, DoneLoadingWidget
{
    public final static String INIT_DISPLAY = "Please choose a spreadsheet " +
            "and hit submit to upload";
    
    @UiField LevelBox classList;
    @UiField FileUpload selectFile;
    @UiField Button submitFile;
    @UiField FormPanel studentUploadForm;
    @UiField HTML formResponse;
    @UiField SimplePanel topLevelPanel;
    @UiField Hidden classParam;

    FormPanel confirmationForm;
    Button confirmData;
    Button resetData;

	
	private static ClassGradesUploadPanelUiBinder uiBinder = GWT
			.create(ClassGradesUploadPanelUiBinder.class);

	interface ClassGradesUploadPanelUiBinder extends
			UiBinder<Widget, ClassGradesUploadPanel> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ClassGradesUploadPanel() {
        initWidget(uiBinder.createAndBindUi(this));
        classList.initData();
        String actionName = "/levelscoreupload";
        selectFile.setName("chooseFile");
        classParam.setName(PanelServiceConstants.CLASSKEY_PARAM_NAME);        
        submitFile.addClickHandler(this);
        studentUploadForm.setAction(actionName);
        studentUploadForm.addSubmitCompleteHandler(this);
        studentUploadForm.setMethod(FormPanel.METHOD_POST);
        studentUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);	

        confirmationForm = new FormPanel();
        FlowPanel formComponents = new FlowPanel();
        confirmationForm.setAction(actionName);
        confirmationForm.setMethod("post");
        confirmationForm.addSubmitCompleteHandler(this);
        confirmData = new Button("Save Loaded Data");
        resetData = new Button("Load new File");
        confirmData.addClickHandler(this);
        resetData.addClickHandler(this);
        formComponents.add(new Hidden(PanelServiceConstants.LOAD_PARAM_NAME));
        HorizontalPanel buttonPanel = new HorizontalPanel();
        buttonPanel.setSpacing(20);
        buttonPanel.add(confirmData);
        buttonPanel.add(resetData);
        formComponents.add(buttonPanel);
        confirmationForm.add(formComponents);
	}

    public void resetPanel()
    {
        studentUploadForm.reset();
        confirmationForm.reset();
        formResponse.setHTML(INIT_DISPLAY);
        topLevelPanel.clear();
        topLevelPanel.add(studentUploadForm);
    }

    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(submitFile))
        {
        	String fileName = selectFile.getFilename();
        	if(!fileName.endsWith("xls"))
        	{
        		WidgetHelper.setErrorStatus(formResponse, "Uploaded file must be excel 97-2003 format (xls extension)", true);
        		return;
        	}
        	
            formResponse.setHTML("<b>Sending file to server. Please wait ...</b>");
            classParam.setValue(classList.getSelectedClass());
            studentUploadForm.submit();
        }
        else if(event.getSource().equals(confirmData))
        {
            formResponse.setHTML("<b>Saving confirmed data ...</b>");
            confirmationForm.submit();
        }
        else if(event.getSource().equals(resetData))
        {
            resetPanel();
        }
    }

    @Override
    public void onSubmitComplete(FormPanel.SubmitCompleteEvent event)
    {
        String results = event.getResults();
        formResponse.setHTML(event.getResults());
        if(results.startsWith(PanelServiceConstants.GOOD_DATA_PREFIX)) //hacky way 2 determine state
            confirmData.setVisible(true);
        else
            confirmData.setVisible(false);
        if(topLevelPanel.getWidget().equals(studentUploadForm))
        {
            topLevelPanel.remove(studentUploadForm);
            topLevelPanel.add(confirmationForm);
        }
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classList.doneLoading();
	}	
}
