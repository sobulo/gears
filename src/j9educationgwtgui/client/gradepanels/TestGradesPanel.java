/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.gradepanels;

import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelCourseTestBox;
import j9educationgwtgui.client.custom.widgets.TeacherCourseTestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.client.custom.widgets.YesNoDialog;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class TestGradesPanel extends Composite implements ClickHandler, ValueChangeHandler<String>, DoneLoadingWidget
{
    @UiField
    VerticalPanel gradesPanel;
    @UiField
    Label status;
    @UiField
    Button saveButton;
    @UiField
    SimplePanel selectionPanel;
    
    private LevelCourseTestBox levelTestPicker;
    private TeacherCourseTestBox teacherTestPicker;

    private PsTable testGrades;

    private String currentlyDisplayedTest, currentlyDisplayedTestName;
    private ScoreManagerAsync testScoreService;
    
    private double currentMaxScore;
    
    private YesNoDialog confirmSave;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<ArrayList<TableMessage>> rosterCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {
                @Override
                public void onFailure(Throwable caught) {
                	WidgetHelper.checkForLoginError(caught);
                	gradesPanel.clear();
                	gradesPanel.add(new Label("Unable to load grades. See error below."));
                    WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
                }

                @Override
                public void onSuccess(ArrayList<TableMessage> result) {
                    if (result.size() < 2) {
                    	gradesPanel.add(new Label("No students have been added/registered for this subject!"));
                        WidgetHelper.setSuccessStatus(status, "Displaying 0 students' grades.", false);
                        return;
                    } 
                    gradesPanel.clear();                    
                    //setup a download link     
                    String downloadName = "Download " + currentlyDisplayedTestName + " grades";
                    String url = GWT.getHostPageBaseURL() + "studentdownload?id="
                    	+ currentlyDisplayedTest + "&type=" +
                    	PanelServiceConstants.DOWNLOAD_GRADES;
                    Anchor downloadLink = new Anchor(downloadName, url);                   

                    //setup table headers
                    TableMessageHeader header = (TableMessageHeader) result.remove(0);
                    String[] headerParts = header.getCaption().split(PanelServiceConstants.DATA_SEPERATOR_REGEX);
                    currentMaxScore = Double.valueOf(headerParts[1]);
                    
                    //display grades
                    testGrades = new PsTable(header);
                    testGrades.showMessages(result, headerParts[0] + " grades");
                    gradesPanel.add(downloadLink);
                    gradesPanel.setCellHorizontalAlignment(downloadLink, HorizontalPanel.ALIGN_RIGHT);
                    gradesPanel.add(testGrades);
                    gradesPanel.setVisible(true);
                    enableSaveButton();
                    //report success
                    WidgetHelper.setSuccessStatus(status, "Displaying " + result.size() + 
                    		" students' grades. Click on a score to edit, once you're done editing. Hit button above to save.", false);

                }
            };

            final AsyncCallback<Void> saveCallback =
                    new AsyncCallback<Void>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            WidgetHelper.setErrorStatus(status, "Request failed: " + 
                            		caught.getMessage(), true);
                        }

                        @Override
                        public void onSuccess(Void result) {                            
                            WidgetHelper.setSuccessStatus(status, "Saved grades for " + 
                            		currentlyDisplayedTestName, true);
                        }
                    };            

    private static TestGradesPanelUiBinder uiBinder = GWT.create(TestGradesPanelUiBinder.class);

    public TestGradesPanel(PanelServiceLoginRoles role) {
        initWidget(uiBinder.createAndBindUi(this));
        saveButton.addClickHandler(this);
        saveButton.setEnabled(false);
        confirmSave = new YesNoDialog("Confirm Edit Grades");
        gradesPanel.setWidth("100%");
        gradesPanel.setVisible(false);        
        if(role.equals(PanelServiceLoginRoles.ROLE_ADMIN))
        {
        	levelTestPicker = new LevelCourseTestBox();
        	testScoreService = levelTestPicker.getScoreManager();
        	levelTestPicker.addValueChangeHandler(this);
        	levelTestPicker.initData();
        	selectionPanel.add(levelTestPicker);
        }
        else if(role.equals(PanelServiceLoginRoles.ROLE_TEACHER))
        {
        	teacherTestPicker = new TeacherCourseTestBox();
        	testScoreService = teacherTestPicker.getScoreManager();
        	teacherTestPicker.addValueChangeHandler(this);
        	teacherTestPicker.initData();
        	selectionPanel.add(teacherTestPicker);
        }
    }
    
    void enableSaveButton()
    {
    	saveButton.setEnabled(true);
    }

    @Override
    public void onClick(ClickEvent event) {
    	confirmSave.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				confirmSave.hide();	
				TableMessage[] messages = testGrades.getMessages();
	        	HashMap<String, Double> testResults = new HashMap<String, Double>(messages.length);
	        	for(TableMessage m : messages)
	        	{
	        		double score = m.getNumber(0);
	        		String id = m.getText(2);
	        		if(score < 0 || score > currentMaxScore)
	        		{
	        			String msg ="Aborted save request. " + id + 
	        			" has a score entered (" + score + ") that is not between 0 and " + currentMaxScore;
	        			WidgetHelper.setErrorStatus(status, msg, true);
	        			return;
	        		}
	        		testResults.put(m.getText(2), m.getNumber(0));
	        	}
	        	WidgetHelper.setNormalStatus(status, "Attempting to save " + currentlyDisplayedTestName + 
	        			". Please wait ...");
	        	testScoreService.saveTestScores(currentlyDisplayedTest, testResults, saveCallback);					
						
			}
		});
    	
    	confirmSave.show("Are you sure you want to save changes to: " + 
    			currentlyDisplayedTestName);
    }

    interface TestGradesPanelUiBinder extends UiBinder<Widget, TestGradesPanel> {
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		saveButton.setEnabled(false);
		gradesPanel.clear(); //clear current grades
		status.setText("Requesting grades ...");
		currentlyDisplayedTest = "";
		if(levelTestPicker != null)
		{
			currentlyDisplayedTest = levelTestPicker.getSelectedTest();
			currentlyDisplayedTestName = levelTestPicker.getClassName() + "/" + levelTestPicker.getCourseName() + " - " +
        	levelTestPicker.getSelectedTestName();
		}
		else if(teacherTestPicker != null)
		{
			currentlyDisplayedTest = teacherTestPicker.getSelectedTest();
			currentlyDisplayedTestName = teacherTestPicker.getCourseName() +"/" + teacherTestPicker.getSelectedTestName();
		}
        
        if(currentlyDisplayedTest.equals(""))
        {
        	gradesPanel.add(new Label("Unable to load grades as no tests/exam have been" +
        			" setup for the subject. Try refreshing browser if test/exam was added recently."));
        	WidgetHelper.setErrorStatus(status, "Grades request aborted", false);
        }
        else
        {
        	gradesPanel.add(new Label("Loading grades. Please wait ..."));
        	testScoreService.getTestScoresTable(currentlyDisplayedTest, rosterCallback); 
        }
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return (levelTestPicker == null || levelTestPicker.doneLoading()) && 
			(teacherTestPicker == null || teacherTestPicker.doneLoading());
	}
}