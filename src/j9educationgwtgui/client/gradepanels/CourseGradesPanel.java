/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.gradepanels;

import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelAndCourseBox;
import j9educationgwtgui.client.custom.widgets.TeacherCoursesBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class CourseGradesPanel extends Composite implements ClickHandler, DoneLoadingWidget
{
    @UiField
    FlowPanel gradesPanel;
    @UiField
    SimplePanel selectionPanel;
    @UiField
    Label status;
    @UiField
    Button display;
    
    private LevelAndCourseBox classAndSubject;
    private TeacherCoursesBox teacherCourses;
    private ScoreManagerAsync testScoreService =
            GWT.create(ScoreManager.class);
    

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<ArrayList<TableMessage>> rosterCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {

                @Override
                public void onFailure(Throwable caught) {
                	WidgetHelper.checkForLoginError(caught);
                    WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
                }

                @Override
                public void onSuccess(ArrayList<TableMessage> result) {
                    gradesPanel.clear(); //clear current grades

                    if (result.size() < 2) {
                        WidgetHelper.setSuccessStatus(status, "No students have been added to this subject", false);
                        return;
                    }

                    //setup table headers
                    TableMessageHeader header = (TableMessageHeader) result.remove(0);

                    //display grades
                    PsTable courseGrades = new PsTable(header);
                    courseGrades.showMessages(result, header.getCaption() + " grades");
                    gradesPanel.add(courseGrades);
                    WidgetHelper.setSuccessStatus(status, "Displaying " + result.size() + " students' grades", false);

                }
            };

    private static CourseGradesPanelUiBinder uiBinder =
            GWT.create(CourseGradesPanelUiBinder.class);

    private void fetchCourseGrades(String courseKeyStr) {
        testScoreService.getCourseScoresTable(courseKeyStr, rosterCallback);
    }


    interface CourseGradesPanelUiBinder extends UiBinder<Widget, CourseGradesPanel> {
    }

    public CourseGradesPanel(PanelServiceLoginRoles role) {
        initWidget(uiBinder.createAndBindUi(this));
        display.addClickHandler(this);
        if(role.equals(PanelServiceLoginRoles.ROLE_ADMIN))
        {
        	classAndSubject = new LevelAndCourseBox();
        	classAndSubject.initData();
        	selectionPanel.add(classAndSubject);
        }
        else if(role.equals(PanelServiceLoginRoles.ROLE_TEACHER))
        {
        	GWT.log("instantiating teacher course selection");
        	teacherCourses = new TeacherCoursesBox();
        	teacherCourses.initData();
        	selectionPanel.add(teacherCourses);
        }
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) 
	{
		gradesPanel.clear();
		status.setText("Grades requested ...");
		String ckStr = "";
		if(classAndSubject != null)
		{
			ckStr = classAndSubject.getCourseValue();
			WidgetHelper.setNormalStatus(status, "Fetching grades for " + classAndSubject.getCourseName() + " / " + 
					classAndSubject.getClassName());			
		}
		else if(teacherCourses != null)
		{
			ckStr = teacherCourses.getSelectedCourse();
			WidgetHelper.setNormalStatus(status, "Fetching grades for " + teacherCourses.getSelectedCourseName());			
		}
		
		if(ckStr.equals(""))
		{
			gradesPanel.add(new Label("Request ignored because no subject selected. Please confirm subjects have been setup for this class and you have been granted access"));
			return;
		}
		fetchCourseGrades(ckStr);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		if(classAndSubject != null)
			return classAndSubject.doneLoading();
		else
			return teacherCourses.doneLoading();
	}
}
