/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.gradepanels;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.client.WebAppHelper;
import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.GuardianStudentPicker;
import j9educationgwtgui.client.custom.widgets.LevelStudentRosterBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class StudentGradesPanel extends Composite implements ClickHandler,
        SelectionHandler, DoneLoadingWidget
{
    @UiField SimplePanel summaryContentPanel;
    @UiField Label summaryStatus;
    @UiField VerticalPanel classSelectionPanel;
    @UiField VerticalPanel detailContentPanel;
    @UiField Label detailStatus;
    @UiField StackLayoutPanel stackContainer;
    @UiField ScrollPanel detailContainer;

    private PsTable studentTable;
    private Hyperlink link;
    private ListBox classList;
    private Button fetchGradesStudentMode, fetchGradesAdminMode, 
            fetchGradesGuardianMode;

    private LevelStudentRosterBox studentRosterBox;
    private String studentKeyDisplayed; //null value indicates running in student mode
    private String classKeyDisplayed;
    private boolean receivedClassHomeEvent = false;
    private GuardianStudentPicker guardianStudentBox;

    private static StudentGradesPanelUiBinder uiBinder = GWT.create(StudentGradesPanelUiBinder.class);

    private UserManagerAsync userService =
            GWT.create(UserManager.class);

    private ScoreManagerAsync scoreService =
            GWT.create(ScoreManager.class); 

    final AsyncCallback<ArrayList<TableMessage>> gradeSummaryCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(summaryStatus, "Request failed: " + caught.getMessage(), false);
        }

        @Override
        public void onSuccess(ArrayList<TableMessage> result) {
            TableMessageHeader header = (TableMessageHeader) result.remove(0);
            //setup table if it hasn't been setup yet
            if(studentTable == null)
            {
                studentTable = new PsTable(header);
                //studentTable.setPageSize(10);
                summaryContentPanel.add(studentTable);
            }
            //no need to clear panel since studentTable constructed once
            summaryContentPanel.setVisible(true);
            studentTable.showMessages(result, header.getCaption());
            WidgetHelper.setSuccessStatus(summaryStatus, "loaded grades successfully", false);
        }
    };

    final AsyncCallback<ArrayList<TableMessage>> gradeDetailCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(detailStatus, "Request failed: " + caught.getMessage(), false);
        }

        @Override
        public void onSuccess(ArrayList<TableMessage> result) {
            refreshGradeDetailsLayout(result);
            classKeyDisplayed = null; //prevents refetching of grade details unless summary refreshed
        }
    };

    final AsyncCallback<HashMap<String, String>> classCallback =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
            WebAppHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(summaryStatus, "Request failed: " +
                    caught.getMessage(), false);
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
            Iterator<Entry<String, String>> classVals = result.entrySet().iterator();
            while(classVals.hasNext())
            {
                Entry<String, String> val = classVals.next();
                classList.addItem(val.getValue(), val.getKey());
            }
            WidgetHelper.setSuccessStatus(summaryStatus, "Found: " + result.size() +
                    " classes, select one to display grades.", false);
            fetchGradesStudentMode.setEnabled(true);
        }
    };

    public StudentGradesPanel() {
        initWidget(uiBinder.createAndBindUi(this));
        this.setSize(GUIConstants.DEFAULT_STACK_WIDTH, GUIConstants.DEFAULT_STACK_HEIGHT);
        classSelectionPanel.setWidth("100%");
        classSelectionPanel.setVisible(false);
        stackContainer.addSelectionHandler(this);
        summaryContentPanel.setWidth("100%");
        summaryContentPanel.setVisible(false);
        detailContentPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        stackContainer.getHeaderWidget(0).addStyleName(GUIConstants.SELECTED_STACK_COLOR);
    }

    public void initStudentMode()
    {
        fetchGradesStudentMode = new Button("Fetch Grades");
        fetchGradesStudentMode.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
        fetchGradesStudentMode.setEnabled(false);
        classList = new ListBox();
        classList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);

        //add handlers
        fetchGradesStudentMode.addClickHandler(this);

        //set up class selection panel
        FlowPanel classHolder = new FlowPanel();
        Label classLabel = new Label("Select Class");
        classLabel.addStyleName("formLabel");
        classHolder.add(classLabel);
        classHolder.add(classList);
        classSelectionPanel.add(classHolder);
        classSelectionPanel.add(fetchGradesStudentMode);
        classSelectionPanel.setVisible(true);

        //remote call to populate dropdown
        userService.getStudentLevels(classCallback);
    }

    public void initAdminMode()
    {
        fetchGradesAdminMode = new Button("Fetch Grades");
        fetchGradesAdminMode.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
        studentRosterBox = new LevelStudentRosterBox();
        fetchGradesAdminMode.addClickHandler(this);

        classSelectionPanel.add(studentRosterBox);
        classSelectionPanel.add(fetchGradesAdminMode);
        classSelectionPanel.setVisible(true);
    }

    public void initGuardianMode()
    {
        fetchGradesGuardianMode = new Button("Fetch Grades");
        fetchGradesGuardianMode.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
        guardianStudentBox = new GuardianStudentPicker(true);
        fetchGradesGuardianMode.addClickHandler(this);

        classSelectionPanel.add(guardianStudentBox);
        classSelectionPanel.add(fetchGradesGuardianMode);
        classSelectionPanel.setVisible(true);
    }


    private void display()
    {
        //notify webapp that this panel should be displayed
        stackContainer.showWidget(0);
        History.newItem(link.getTargetHistoryToken(), true);        
    }

    private void fetchGradesSummaryForStudent(String classKey)
    {
        setClassKey(classKey);
        scoreService.getStudentScoreSummary(classKey, gradeSummaryCallback);
        WidgetHelper.setNormalStatus(summaryStatus, "Please wait, fetching grades ....");
    }

    private void fetchGradesSummaryForStudent(String classKey, String studentKey)
    {
        setClassKey(classKey);
        studentKeyDisplayed = studentKey;
        scoreService.getStudentScoreSummary(studentKey, classKey,
                gradeSummaryCallback);
        WidgetHelper.setNormalStatus(summaryStatus, "Please wait, fetching grades ....");
    }

    private void fetchGradesSummaryForStudentWithKey(String classKey, String studentKey)
    {
        setClassKey(classKey);
        studentKeyDisplayed = studentKey;
        scoreService.getStudentScoreSummaryWithKey(studentKey, classKey,
                gradeSummaryCallback);
        WidgetHelper.setNormalStatus(summaryStatus, "Please wait, fetching grades ....");
    }

    public void setHyperlink(Hyperlink link)
    {
        this.link = link;
    }

    private void setClassKey(String key)
    {
        classKeyDisplayed = key;

        //clear panels depending on classKeyDisplayed
        detailContentPanel.clear();
    }

    @Override
    public void onClick(ClickEvent event)
    {
    	summaryContentPanel.setVisible(false);
        if(event.getSource().equals(fetchGradesStudentMode))
        {
            fetchGradesSummaryForStudent(classList.getValue(classList.getSelectedIndex()));
        }
        else if(event.getSource().equals(fetchGradesAdminMode))
        {
            receivedClassHomeEvent = false;
            fetchGradesSummaryForStudentWithKey(studentRosterBox.getSelectedClass(),
                    studentRosterBox.getSelectedStudent());
        }
        else if(event.getSource().equals(fetchGradesGuardianMode))
        {
            fetchGradesSummaryForStudentWithKey(guardianStudentBox.getSelectedClass(),
                    guardianStudentBox.getSelectedStudent());
        }
    }

    //used for registering with classhomepanel in order to receive envents when
    //students are clicked on the class roster table
    @Override
    public void onSelection(SelectionEvent event) {
        Object eventObj = event.getSelectedItem();
        if(eventObj instanceof TableMessage)
        {
            TableMessage m = (TableMessage) eventObj;
            fetchGradesSummaryForStudent(m.getText(1), m.getText(0));
            display();
            receivedClassHomeEvent = true;
        }
        else if(eventObj instanceof Integer)
            handleStackSelection((Integer) eventObj);
    }

    public void handleStackSelection(int selectedStack)
    {
    	clearSelectedStack();
    	stackContainer.getHeaderWidget(selectedStack).addStyleName(GUIConstants.SELECTED_STACK_COLOR);    	
        if(stackContainer.getWidgetIndex(detailContainer) == selectedStack)
        {
            if(classKeyDisplayed == null)
                return;

            detailStatus.setText("Please wait. Fetching grade details...");

            if(studentKeyDisplayed == null)
                scoreService.getStudentScoreDetails(classKeyDisplayed, gradeDetailCallback);
            else
            {
                if(receivedClassHomeEvent)
                    scoreService.getStudentScoreDetails(studentKeyDisplayed, classKeyDisplayed, gradeDetailCallback);
                else
                    scoreService.getStudentScoreDetailsWithKey(studentKeyDisplayed, classKeyDisplayed, gradeDetailCallback);
            }
        }

    }
    
	public void clearSelectedStack()
	{
		for(int i = 0; i < stackContainer.getWidgetCount(); i++)
			stackContainer.getHeaderWidget(i).removeStyleName(GUIConstants.SELECTED_STACK_COLOR);
	}    

    private void refreshGradeDetailsLayout(ArrayList<TableMessage> details)
    {
        FlexTable courseTable = null;
        int rowCount = 0;
        int subjectCount = 0;
        for(TableMessage message : details)
        {
            if(message instanceof TableMessageHeader)
            {
                //found a new subject. create a table for it and add to layout
                courseTable = getGradeHeaderLayout((TableMessageHeader) message);
                courseTable.setWidth("70%");
                detailContentPanel.add(courseTable);
                rowCount = 2;
                subjectCount++;
            }
            else
            {
                //add test information as new row in subject table
                addTestInfoToTable(message, courseTable, rowCount++);
            }
        }
        detailStatus.setText("Displaying details for " + subjectCount + " subjects");
    }

    private FlexTable getGradeHeaderLayout(TableMessageHeader header)
    {
    	String total = header.getText(1);
    	try
    	{
    		 total = NumberFormat.getFormat("###.##").format(Double.valueOf(total));
    	}
    	catch(Exception ex){}
    	
    	HTML headerCaption;
    	if(total.equals(String.valueOf(Double.NaN)))
    		headerCaption = new HTML("<b>" + header.getText(0).toUpperCase() + ". Some exams/tests scores are still pending");
    	else
    		headerCaption = new HTML("<b>" + header.getText(0).toUpperCase() + ", Total Score: " +
                total + "%, Grade: " + header.getText(2) + "<b>");
        //headerCaption.addStyleName(GUIConstants.STYLE_CAPITALIZE);
        FlexTable table = new FlexTable();
        table.addStyleName(GUIConstants.STYLE_STND_TABLE);
        table.setBorderWidth(1);
        table.setWidget(0, 0, headerCaption);
        table.getFlexCellFormatter().setColSpan(0, 0, 4);
        String[] colHeaders = {"Test Name", "Your Score", "Max Score", "Weight"};

        for(int i = 0; i < colHeaders.length; i++)
        {
            table.setHTML(1, i, "<i>" + colHeaders[i] + "</i>");
            table.getFlexCellFormatter().addStyleName(1, i, GUIConstants.STYLE_STND_TABLE_HEADER);
        }
        return table;
    }

    private void addTestInfoToTable(TableMessage message, FlexTable table, int row)
    {
        for(int i = 0; i < message.getNumberOfTextFields(); i++)
            table.setText(row, i, message.getText(i));
    }

    interface StudentGradesPanelUiBinder extends UiBinder<Widget, StudentGradesPanel>
    {}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return (studentRosterBox == null || studentRosterBox.doneLoading()) &&
			(guardianStudentBox == null || guardianStudentBox.doneLoading());
	}
}