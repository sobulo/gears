/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.gradepanels;

import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelCourseTestBox;
import j9educationgwtgui.client.custom.widgets.TeacherCourseTestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class GradeUploadPanel extends Composite implements ClickHandler, FormPanel.SubmitCompleteHandler, DoneLoadingWidget
{
    public final static String INIT_DISPLAY = "Please choose a spreadsheet " +
            "and hit submit to upload";

    @UiField
    SimplePanel selectionPanel;

    @UiField FileUpload selectFile;
    @UiField Button submitFile;
    @UiField FormPanel studentUploadForm;
    @UiField HTML formResponse;
    @UiField SimplePanel topLevelPanel;
    @UiField Hidden testParam;

    FormPanel confirmationForm;
    Button confirmData;
    Button resetData;
    private static GradeUploadPanelUiBinder uiBinder = GWT.create(GradeUploadPanelUiBinder.class);
   
    private LevelCourseTestBox levelTestBox;
    private TeacherCourseTestBox teacherTestBox;

    public GradeUploadPanel(PanelServiceLoginRoles role) {
        initWidget(uiBinder.createAndBindUi(this));
        String actionName = "/testscoreupload";
        selectFile.setName("chooseFile");
        testParam.setName(PanelServiceConstants.TESTKEY_PARAM_NAME);        
        submitFile.addClickHandler(this);
        studentUploadForm.setAction(actionName);
        studentUploadForm.addSubmitCompleteHandler(this);
        studentUploadForm.setMethod(FormPanel.METHOD_POST);
        studentUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);	

        confirmationForm = new FormPanel();
        FlowPanel formComponents = new FlowPanel();
        confirmationForm.setAction(actionName);
        confirmationForm.setMethod("post");
        confirmationForm.addSubmitCompleteHandler(this);
        confirmData = new Button("Save Loaded Data");
        resetData = new Button("Load new File");
        confirmData.addClickHandler(this);
        resetData.addClickHandler(this);
        formComponents.add(new Hidden(PanelServiceConstants.LOAD_PARAM_NAME));
        HorizontalPanel buttonPanel = new HorizontalPanel();
        buttonPanel.setSpacing(20);
        buttonPanel.add(confirmData);
        buttonPanel.add(resetData);
        formComponents.add(buttonPanel);
        confirmationForm.add(formComponents);
        
        if(role.equals(PanelServiceLoginRoles.ROLE_ADMIN))
        {
        	levelTestBox = new LevelCourseTestBox();
        	levelTestBox.initData();
        	selectionPanel.add(levelTestBox);
        }
        else if(role.equals(PanelServiceLoginRoles.ROLE_TEACHER))
        {
        	teacherTestBox = new TeacherCourseTestBox();
        	teacherTestBox.initData();
        	selectionPanel.add(teacherTestBox);
        }
    }

    public void resetPanel()
    {
        studentUploadForm.reset();
        confirmationForm.reset();
        formResponse.setHTML(INIT_DISPLAY);
        topLevelPanel.clear();
        topLevelPanel.add(studentUploadForm);
    }

    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(submitFile))
        {
        	String fileName = selectFile.getFilename();
        	if(!fileName.endsWith("xls"))
        	{
        		WidgetHelper.setErrorStatus(formResponse, "Uploaded file must be excel 97-2003 format (xls extension)", true);
        		return;
        	}
        	
        	String testKey = "";
        	if(levelTestBox != null)
        		testKey = levelTestBox.getSelectedTest();
        	else if(teacherTestBox != null)
        		testKey = teacherTestBox.getSelectedTest();
        	
            formResponse.setHTML("<b>Sending file to server ...</b>");
            testParam.setValue(testKey);
            studentUploadForm.submit();
        }
        else if(event.getSource().equals(confirmData))
        {
            formResponse.setHTML("<b>Saving confirmed data ...</b>");
            confirmationForm.submit();
        }
        else if(event.getSource().equals(resetData))
        {
            resetPanel();
        }
    }

    @Override
    public void onSubmitComplete(FormPanel.SubmitCompleteEvent event)
    {
        String results = event.getResults();
        formResponse.setHTML(event.getResults());
        if(results.startsWith(PanelServiceConstants.GOOD_DATA_PREFIX)) //hacky way 2 determine state
            confirmData.setVisible(true);
        else
            confirmData.setVisible(false);
        if(topLevelPanel.getWidget().equals(studentUploadForm))
        {
            topLevelPanel.remove(studentUploadForm);
            topLevelPanel.add(confirmationForm);
        }
    }

    interface GradeUploadPanelUiBinder extends UiBinder<Widget, GradeUploadPanel> {
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		// TODO Auto-generated method stub
		return (levelTestBox == null || levelTestBox.doneLoading()) && 
			(teacherTestBox == null || teacherTestBox.doneLoading());
	}
}