/**
 * 
 */
package j9educationgwtgui.client.gradepanels;

import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.LevelBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceConstants.ClassGradesGPAType;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ClassGradesPanel extends Composite implements ClickHandler, DoneLoadingWidget{
	
	@UiField LevelBox classList;
	@UiField Button display;
	@UiField VerticalPanel contentPanel;
	@UiField Label status;
	@UiField ListBox selectedClasses;
	@UiField Button add;
	@UiField Button remove;
	@UiField Button up;
	@UiField Button down;
	@UiField Button clear;
	@UiField HTMLPanel controls;
	HashSet<String> selectedValues = new HashSet<String>();
	ClassGradesGPAType gpaType;
	String[] selectedClassList;

	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<ArrayList<TableMessage>> gradesCallback =
            new AsyncCallback<ArrayList<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
        }

        @Override
        public void onSuccess(ArrayList<TableMessage> result) {
            displayTable(result, true);
            WidgetHelper.setSuccessStatus(status, "loaded grades successfully", false);
        }
    };
    
	private AsyncCallback<ArrayList<TableMessage>[]> cumulativeCallback = new AsyncCallback<ArrayList<TableMessage>[]>() {

		@Override
		public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(status, "Request failed: " + caught.getMessage(), false);
		}

		@Override
		public void onSuccess(ArrayList<TableMessage>[] result) {
			displayTable(result);
            WidgetHelper.setSuccessStatus(status, "loaded cumulative GPAs successfully", false);
		}
	};
	
    private ScoreManagerAsync scoreService = GWT.create(ScoreManager.class);
    
	private static ClassGradesPanelUiBinder uiBinder = GWT
			.create(ClassGradesPanelUiBinder.class);

	interface ClassGradesPanelUiBinder extends
			UiBinder<Widget, ClassGradesPanel> {
	}

	public ClassGradesPanel(ClassGradesGPAType type) {
		initWidget(uiBinder.createAndBindUi(this));
		if(!(type.equals(ClassGradesGPAType.CUMULATIVE) || type.equals(ClassGradesGPAType.SUMMARY)))
			hideControls();
		else
			setupControls();
		this.gpaType = type;
		display.addClickHandler(this);
		classList.initData();
		contentPanel.setWidth("100%");
		contentPanel.setVisible(false);
	}
	
	private void hideControls()
	{
		selectedClasses.removeFromParent();
		controls.removeFromParent();
	}
	
	private void setupControls()
	{
		ClickHandler controlsHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(event.getSource() == add)
					addClass();
				else if(event.getSource() == remove && selectedClasses.getItemCount() > 0)
					removeClass(selectedClasses.getSelectedIndex());
				else if(event.getSource() == up)
					swapValues(true, selectedClasses.getSelectedIndex());
				else if(event.getSource() == down)
					swapValues(false, selectedClasses.getSelectedIndex());
				else
				{
					contentPanel.clear();
					selectedClasses.clear();
					selectedValues.clear();
				}
			}
		};
		
		add.addClickHandler(controlsHandler);
		remove.addClickHandler(controlsHandler);
		up.addClickHandler(controlsHandler);
		down.addClickHandler(controlsHandler);
		clear.addClickHandler(controlsHandler);
	}
		
	public void addClass()
	{
		if(!selectedValues.contains(classList.getSelectedClass()))
		{
			selectedClasses.addItem(classList.getSelectedClassName(), classList.getSelectedClass());
			selectedValues.add(classList.getSelectedClass());
			remove.setEnabled(true);
		}
	}
	
	public void removeClass(int idx)
	{
		selectedValues.remove(selectedClasses.getValue(idx));
		selectedClasses.removeItem(idx);
		if(selectedClasses.getItemCount() == 0)
		{
			remove.setEnabled(false);
			return;
		}
	}
	
	public void swapValues(boolean isUp, int idx)
	{
		String value = selectedClasses.getValue(idx);
		String name = selectedClasses.getItemText(idx);
		if(isUp)
		{
			if(idx == 0)
				return;
			
			String tempName = selectedClasses.getItemText(idx-1);
			String tempValue = selectedClasses.getValue(idx-1);
			selectedClasses.setItemText(idx, tempName);
			selectedClasses.setValue(idx, tempValue);
			selectedClasses.setItemText(idx-1, name);
			selectedClasses.setValue(idx-1, value);
			selectedClasses.setSelectedIndex(idx-1);
		}
		else
		{
			if(idx == selectedClasses.getItemCount() - 1)
				return;
			
			String tempName = selectedClasses.getItemText(idx+1);
			String tempValue = selectedClasses.getValue(idx+1);
			selectedClasses.setItemText(idx, tempName);
			selectedClasses.setValue(idx, tempValue);
			selectedClasses.setItemText(idx+1, name);
			selectedClasses.setValue(idx+1, value);
			selectedClasses.setSelectedIndex(idx+1);
		}
	}
	

	private void prepareDisplayTable(String name, String id)
	{
        //setup a download link     
    	String type = PanelServiceConstants.DOWNLOAD_CLASS_GRADES;
    	if(gpaType.equals(ClassGradesGPAType.GPA))
    		type = PanelServiceConstants.DOWNLOAD_CLASS_GRADES_WITH_GPA;
    	else if(gpaType.equals(ClassGradesGPAType.CUMULATIVE))
    		type = PanelServiceConstants.DOWNLOAD_CLASS_GRADES_WITH_CUM;
    	else if(gpaType.equals(ClassGradesGPAType.SUMMARY))
    		type = PanelServiceConstants.DOWNLOAD_CLASS_WITH_SUM;
    	
        String downloadName = "Download " + name;
        String url = GWT.getHostPageBaseURL() + "studentdownload?id="
        	+ id + "&type=" + type;
        
        Anchor downloadLink = new Anchor(downloadName, url);  
        
   
        
        //display table 
        contentPanel.clear();
        contentPanel.setVisible(true);               
        contentPanel.add(downloadLink);
        contentPanel.setCellHorizontalAlignment(downloadLink, HorizontalPanel.ALIGN_RIGHT);		
	}
	
    private void displayTable(ArrayList<TableMessage> result, boolean setupNeeded)
    {
    	if(setupNeeded)
    		prepareDisplayTable(classList.getSelectedClassName() + " grades",classList.getSelectedClass());
        
    	//setup table
        TableMessageHeader header = (TableMessageHeader) result.remove(0);
        PsTable studentTable = new PsTable(header);
        studentTable.showMessages(result, header.getCaption() + " Grades");
        contentPanel.add(studentTable);
        
    }
    
    private void displayTable(ArrayList<TableMessage>[] resultList)
    {
    	if(resultList.length == 0 || resultList[0].size() == 0)
    		return;
    	String timeStamp = resultList[0].get(0).getMessageId();
    	if(timeStamp == null || timeStamp.trim().length() == 0) //sanity check, shouldn't occur?
    	{
    		WidgetHelper.errorBox.show("Unable to process download. Badly formed data received from server");
    		return;
    	}
    	Date d = new Date(Long.valueOf(timeStamp));
    	String title = "Cumulative Grades Report generated on ";
    	if(gpaType.equals(ClassGradesGPAType.SUMMARY))
    		title = "Examination Summary Report generated on ";
        prepareDisplayTable(title + d, timeStamp);
        //setup table
        for(int i = resultList.length - 1; i >= 0; i--)
        	displayTable(resultList[i], false);        
    }    

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		contentPanel.setVisible(false);
		WidgetHelper.setNormalStatus(status, "Fetching grades for class: " + classList.getSelectedClassName() + 
				". Please wait ...");
		if(gpaType.equals(ClassGradesGPAType.NONE))
			scoreService.getClassScores(classList.getSelectedClass(), false, gradesCallback);
		else if(gpaType.equals(ClassGradesGPAType.GPA))
			scoreService.getClassScoresWithGPA(classList.getSelectedClass(), gradesCallback);
		else if(gpaType.equals(ClassGradesGPAType.CUMULATIVE))
		{
			String[] levelStrs = new String[selectedClasses.getItemCount()];
			for(int i = 0; i < selectedClasses.getItemCount(); i++)
				levelStrs[i] = selectedClasses.getValue(i);
			scoreService.getClassScoresWithCumulativeGPA(levelStrs, cumulativeCallback);
		}
		else if(gpaType.equals(ClassGradesGPAType.SUMMARY))
		{
			String[] levelStrs = new String[selectedClasses.getItemCount()];
			for(int i = 0; i < selectedClasses.getItemCount(); i++)
				levelStrs[i] = selectedClasses.getValue(i);
			scoreService.getCumulativeSummary(levelStrs, cumulativeCallback);
		}		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return classList.doneLoading();
	}

}
