/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.gradepanels;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;
import j9educationgwtgui.client.custom.tables.PsTable;
import j9educationgwtgui.client.custom.widgets.DoneLoadingWidget;
import j9educationgwtgui.client.custom.widgets.GuardianStudentPicker;
import j9educationgwtgui.client.custom.widgets.StudentSuggestBox;
import j9educationgwtgui.client.custom.widgets.WidgetHelper;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class CumulativeStudentGradesPanel extends Composite implements ClickHandler, DoneLoadingWidget
{
    @UiField VerticalPanel summaryContentPanel;
    @UiField Label summaryStatus;
    @UiField VerticalPanel studentSelectionPanel;

    private Hyperlink link;
    private Button fetchGradesAdminMode, 
            fetchGradesGuardianMode;

    private StudentSuggestBox studentRosterBox;
    private GuardianStudentPicker guardianStudentBox;

    private static CumulativeGradesPanelUiBinder uiBinder = GWT.create(CumulativeGradesPanelUiBinder.class);

    private ScoreManagerAsync scoreService =
            GWT.create(ScoreManager.class); 

    private final AsyncCallback<ArrayList<TableMessage>[]> gradeSummaryCallback =
            new AsyncCallback<ArrayList<TableMessage>[]>() {
        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            WidgetHelper.setErrorStatus(summaryStatus, "Request failed: " + caught.getMessage(), false);
        }

        @Override
        public void onSuccess(ArrayList<TableMessage>[] resultList) {
        	summaryContentPanel.clear();
        	for(ArrayList<TableMessage> result : resultList)
        	{
	            TableMessageHeader header = (TableMessageHeader) result.remove(0);
	            PsTable studentTable = new PsTable(header);
	            //studentTable.setPageSize(10);
	            summaryContentPanel.add(studentTable);
	            
	            //no need to clear panel since studentTable constructed once
	            summaryContentPanel.setVisible(true);
	            studentTable.showMessages(result, header.getCaption());
        	}
        	 WidgetHelper.setSuccessStatus(summaryStatus, "loaded grades successfully", false);
        }
    };


    public CumulativeStudentGradesPanel() {
        initWidget(uiBinder.createAndBindUi(this));
        this.setSize(GUIConstants.DEFAULT_STACK_WIDTH, GUIConstants.DEFAULT_STACK_HEIGHT);
        studentSelectionPanel.setWidth("100%");
        studentSelectionPanel.setVisible(false);
        summaryContentPanel.setWidth("100%");
        summaryContentPanel.setVisible(false);
    }

    public void initStudentMode()
    {
    	studentSelectionPanel.setVisible(false);

        //remote call to populate dropdown
        scoreService.getCumulativeStudentGrades(gradeSummaryCallback);
    }

    public void initAdminMode()
    {
        fetchGradesAdminMode = new Button("Fetch Grades");
        fetchGradesAdminMode.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
        studentRosterBox = new StudentSuggestBox();
        fetchGradesAdminMode.addClickHandler(this);

        studentSelectionPanel.add(studentRosterBox);
        studentSelectionPanel.add(fetchGradesAdminMode);
        studentSelectionPanel.setVisible(true);
    }

    public void initGuardianMode()
    {
        fetchGradesGuardianMode = new Button("Fetch Grades");
        fetchGradesGuardianMode.addStyleName(GUIConstants.STYLE_FORM_SHIFT);
        guardianStudentBox = new GuardianStudentPicker();
        fetchGradesGuardianMode.addClickHandler(this);

        studentSelectionPanel.add(guardianStudentBox);
        studentSelectionPanel.add(fetchGradesGuardianMode);
        studentSelectionPanel.setVisible(true);
    }


    private void display()
    {
        //notify webapp that this panel should be displayed
        History.newItem(link.getTargetHistoryToken(), true);        
    }



    public void setHyperlink(Hyperlink link)
    {
        this.link = link;
    }


    @Override
    public void onClick(ClickEvent event)
    {
    	summaryContentPanel.setVisible(false);
        if(event.getSource().equals(fetchGradesAdminMode))
        {
            scoreService.getCumulativeStudentGrades(studentRosterBox.getSelectedUser(), false, gradeSummaryCallback); 
        }
        else if(event.getSource().equals(fetchGradesGuardianMode))
        {
        	scoreService.getCumulativeStudentGrades(guardianStudentBox.getSelectedStudent(), true, gradeSummaryCallback);
        }        
    }

    interface CumulativeGradesPanelUiBinder extends UiBinder<Widget, CumulativeStudentGradesPanel>
    {}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return (studentRosterBox == null || studentRosterBox.doneLoading()) &&
			(guardianStudentBox == null || guardianStudentBox.doneLoading());
	}
}