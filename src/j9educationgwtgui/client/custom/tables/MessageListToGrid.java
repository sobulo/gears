/**
 * 
 */
package j9educationgwtgui.client.custom.tables;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;
import j9educationgwtgui.shared.TableMessageHeader.TableMessageContent;

import java.util.List;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class MessageListToGrid extends Composite{ 
	
	private SimplePanel contentArea;
	private NumberFormat numberFormat;
	private DateTimeFormat dateFormat;
	
	public MessageListToGrid()
	{
		contentArea = new SimplePanel();
		contentArea.setWidth("100%");
		numberFormat = GUIConstants.DEFAULT_NUMBER_FORMAT;
		dateFormat = GUIConstants.DEFAULT_DATE_FORMAT;
		initWidget(contentArea);

	}
	
	public MessageListToGrid(List<TableMessage> data)
	{
		this();
		populateTable(data);
	}
	
	public void populateTable(List<TableMessage> data)
	{
		contentArea.clear();
		int numOfRows = data.size();
		if(numOfRows == 0 || !(data.get(0) instanceof TableMessageHeader))
		{
			contentArea.add(new Label("No table header found"));
			return;
		}
		
		TableMessageHeader header = (TableMessageHeader) data.get(0);
		int numOfCols = header.getNumberOfHeaders();
		
		Grid table = new Grid(numOfRows, numOfCols);
		table.addStyleName(GUIConstants.STYLE_STND_TABLE);
		table.addStyleName(GUIConstants.STYLE_CAPITALIZE);
		table.setBorderWidth(1);
		table.setWidth("75%");
		
		//set up headers
		table.getRowFormatter().addStyleName(0, GUIConstants.STYLE_STND_TABLE_HEADER);
		for(int i = 0; i < numOfCols; i++)
			table.setText(0, i, header.getText(i));
		
		for(int row = 1; row < data.size(); row++)
		{
			TableMessage m = data.get(row);
			int dateCursor = 0;
			int numCursor = 0;
			int textCursor = 0;
			for(int i = 0; i < numOfCols; i++)
			{
				TableMessageContent headerType = header.getHeaderType(i);
				if(headerType.equals(TableMessageContent.TEXT))
				{
					table.setText(row, i, m.getText(textCursor));
					textCursor++;
				}
				else if(headerType.equals(TableMessageContent.NUMBER))
				{
					table.setText(row, i, m.getNumber(numCursor)==null?"":numberFormat.format(m.getNumber(numCursor)));
					numCursor++;
				}
				else if(headerType.equals(TableMessageContent.DATE))
				{
					table.setText(row, i, m.getDate(dateCursor)==null?"":dateFormat.format(m.getDate(dateCursor)));
					//table.setText(row, i, m.getDate(dateCursor)==null?"":m.getDate(dateCursor).toString());
					dateCursor++;
				}
			}
		}
		contentArea.add(table);
	}
	
	public void clear()
	{
		contentArea.clear();
	}
}
