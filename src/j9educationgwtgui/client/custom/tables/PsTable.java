package j9educationgwtgui.client.custom.tables;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.custom.tables.TableMessageSorter.DateComparator;
import j9educationgwtgui.client.custom.tables.TableMessageSorter.NumberComparator;
import j9educationgwtgui.client.custom.tables.TableMessageSorter.TextComparator;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.google.gwt.gen2.table.client.AbstractColumnDefinition;
import com.google.gwt.gen2.table.client.AbstractScrollTable.ColumnResizePolicy;
import com.google.gwt.gen2.table.client.AbstractScrollTable.ResizePolicy;
import com.google.gwt.gen2.table.client.AbstractScrollTable.SortPolicy;
import com.google.gwt.gen2.table.client.CachedTableModel;
import com.google.gwt.gen2.table.client.DefaultRowRenderer;
import com.google.gwt.gen2.table.client.DefaultTableDefinition;
import com.google.gwt.gen2.table.client.FixedWidthGridBulkRenderer;
import com.google.gwt.gen2.table.client.MutableTableModel;
import com.google.gwt.gen2.table.client.PagingOptions;
import com.google.gwt.gen2.table.client.PagingScrollTable;
import com.google.gwt.gen2.table.client.ScrollTable;
import com.google.gwt.gen2.table.client.SelectionGrid.SelectionPolicy;
import com.google.gwt.gen2.table.client.TableDefinition;
import com.google.gwt.gen2.table.client.TableModel;
import com.google.gwt.gen2.table.client.TableModelHelper.Request;
import com.google.gwt.gen2.table.client.TableModelHelper.Response;
import com.google.gwt.gen2.table.client.TextCellEditor;
import com.google.gwt.gen2.table.event.client.RowSelectionEvent;
import com.google.gwt.gen2.table.event.client.RowSelectionHandler;
import com.google.gwt.gen2.table.event.client.TableEvent.Row;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Simple example that shows the usage of GWT-incubator's {@link PagingScrollTable}
 * with a simple table model object.  The table also supports sorting via a 
 * custom set of {@link Comparator} I whipped up.  Nothing fancy or exciting.
 */
public class PsTable extends Composite {

    private CachedTableModel<TableMessage> cachedTableModel = null;
    private DefaultTableDefinition<TableMessage> tableDefinition = null;
    private PagingScrollTable<TableMessage> pagingScrollTable = null;
    private HTML countLabel = new HTML("Data displayed below");
    private DataSourceTableModel tableModel = null;
    private VerticalPanel vPanel = new VerticalPanel();
    private FlexTable flexTable = new FlexTable();
    private ArrayList<AbstractColumnDefinition<TableMessage, String>> colDefs;
    private TableMessageRowSelectionHandler tableMessageRowSelectionHandler;
    private final int DEFAULT_HEIGHT = 400;

    /**
     * Constructor
     * @param colDefs 
     */
    public PsTable(ArrayList<AbstractColumnDefinition<TableMessage, String>> colDefs) {
        super();
        this.colDefs = colDefs;
        setupTable();
    }
    
    public void setupTable()
    {
        pagingScrollTable = createScrollTable();
        pagingScrollTable.setHeight(DEFAULT_HEIGHT + "px");
        PagingOptions pagingOptions = new PagingOptions(pagingScrollTable);

        HorizontalPanel optionPanel = new HorizontalPanel();
        optionPanel.add(pagingOptions);
        HTML pagingDescription = new HTML("<div style='font-size:x-small; background-color:black; color:white;float:right'><ul><li>Use arrows on the left to go to next page,</li><li>" +
        		"Click on table headers to sort data</li><li>Drag border between headers to adjust column widths</li></ul>");
        pagingDescription.setWordWrap(true);
        optionPanel.add(pagingDescription);
        flexTable.setWidget(0, 0, pagingScrollTable);
        flexTable.getFlexCellFormatter().setColSpan(0, 0, 2);
        flexTable.setWidget(1, 0, optionPanel);

        countLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        vPanel.add(countLabel);
        vPanel.add(flexTable);

        pagingScrollTable.setWidth("99%");
        vPanel.setWidth("100%");
        flexTable.setWidth("100%");
        vPanel.addStyleName(GUIConstants.STYLE_PS_TABLE_SPACER);
        //countLabel.addStyleName(GUIConstants.STYLE_PS_TABLE_CAPTION);

        super.initWidget(vPanel);        
    }
    
    public PsTable()
    {
    	super();
    }
    
    public void initHeaders(TableMessageHeader header)
    {
        ArrayList<AbstractColumnDefinition<TableMessage, String>> tempColDefs = new ArrayList<AbstractColumnDefinition<TableMessage, String>>();

        int textIndex = 0;
        int dateIndex = 0;
        int numIndex = 0;
        for(int i = 0; i < header.getNumberOfHeaders(); i++)
        {
            TableMessageHeader.TableMessageContent type = header.getHeaderType(i);
            if(type == TableMessageHeader.TableMessageContent.TEXT)
            {
                tempColDefs.add(new PsTable.TextColumnDefinition(textIndex,
                        header.getText(i), true,
                        new TextComparator(true, textIndex), header.getGroupName(i)));
                textIndex++;
            }
            else if(type == TableMessageHeader.TableMessageContent.NUMBER)
            {
                tempColDefs.add(new PsTable.NumberColumnDefinition(numIndex,
                        header.getText(i), true,
                        new NumberComparator(true, numIndex), header.isEditable(i), header.getGroupName(i)));
                numIndex++;
            }
            else if(type == TableMessageHeader.TableMessageContent.DATE)
            {
                tempColDefs.add(new PsTable.DateColumnDefinition(dateIndex,
                        header.getText(i), true,
                        new DateComparator(true, dateIndex)));
                dateIndex++;
            }
        }
        this.colDefs = tempColDefs;    	
    }

    public PsTable(TableMessageHeader header)
    {
        super();
        initHeaders(header);
        setupTable();
    }
    
    public void setPageSize(int recordsPerPage)
    {
    	pagingScrollTable.setPageSize(recordsPerPage);
    	int height = recordsPerPage * 28;
    	height = Math.min(height, DEFAULT_HEIGHT);
    	pagingScrollTable.setHeight(height + "px");
    }


	public void showMessages(ArrayList<TableMessage> list, String caption)
    {
        // update the count
        countLabel.setHTML("<b>" + caption + "</b> (" + list.size() + " entries)");
        // reset the table model data
        tableModel.setData(list);
        // reset the table model row count
        tableModel.setRowCount(list.size());
        // clear the cache
        cachedTableModel.clearCache();
        // reset the cached model row count
        cachedTableModel.setRowCount(list.size());
        // force to page zero with a reload
        pagingScrollTable.gotoPage(0, true);
    }

    /**
     * Allows consumers of this class to stuff a new {@link ArrayList} of {@link Message}
     * into the table -- overwriting whatever was previously there.
     *
     * @param list the list of messages to show
     * @return the number of milliseconds it took to refresh the table
     */
    public void showMessages(ArrayList<TableMessage> list) {
        String caption = "Table Data";
        showMessages(list, caption);
    }
    
    public TableMessage[] getMessages()
    {
    	return tableModel.getAllMessages();
    }
    
    /**
     * The row selection handler when a user selects a row.
     */
    private RowSelectionHandler rowSelectionHandler = new RowSelectionHandler() {

        @Override
		public void onRowSelection(RowSelectionEvent event) {
            Set<Row> set = event.getSelectedRows();
            if (set.size() == 1)
            {
                int rowIdx = set.iterator().next().getRowIndex();
                TableMessage m = tableModel.getMessageById(rowIdx);               
                if(tableMessageRowSelectionHandler != null)
                    tableMessageRowSelectionHandler.onRowSelected(m);
            }
        }
    };
    
    public void refresh()
    {
    	cachedTableModel.clearCache();
    	pagingScrollTable.gotoPage(0, true);
    }

    public void addMessageSelectionHandler(TableMessageRowSelectionHandler tmrs)
    {
        tableMessageRowSelectionHandler = tmrs;
    }

    /**
     * Initializes the scroll table
     * @return
     */
    private PagingScrollTable<TableMessage> createScrollTable() {
        // create our own table model
        tableModel = new DataSourceTableModel();
        // add it to cached table model
        cachedTableModel = createCachedTableModel(tableModel);

        // create the table definition
        TableDefinition<TableMessage> tableDef = createTableDefinition();
        
        // create the paging scroll table
        PagingScrollTable<TableMessage> pagingScrollTable = new PagingScrollTable<TableMessage>(cachedTableModel, tableDef);
        pagingScrollTable.setPageSize(100);
        pagingScrollTable.setEmptyTableWidget(new HTML("There is no data to display"));
        pagingScrollTable.getDataTable().setSelectionPolicy(SelectionPolicy.ONE_ROW);

        // setup the bulk renderer
        FixedWidthGridBulkRenderer<TableMessage> bulkRenderer = new FixedWidthGridBulkRenderer<TableMessage>(pagingScrollTable.getDataTable(), pagingScrollTable);
       
        pagingScrollTable.setBulkRenderer(bulkRenderer);

        // setup the formatting
        pagingScrollTable.setCellPadding(3);
        pagingScrollTable.setCellSpacing(0);
        pagingScrollTable.setResizePolicy(ScrollTable.ResizePolicy.FILL_WIDTH);

        pagingScrollTable.setSortPolicy(SortPolicy.SINGLE_CELL);
        pagingScrollTable.setColumnResizePolicy(ColumnResizePolicy.SINGLE_CELL);
        pagingScrollTable.setResizePolicy(ResizePolicy.UNCONSTRAINED);
        pagingScrollTable.getDataTable().addRowSelectionHandler(rowSelectionHandler);     
        
        return pagingScrollTable;
    }

    /**
     * Create the {@link CachedTableModel}
     * @param tableModel
     * @return
     */
    private CachedTableModel<TableMessage> createCachedTableModel(DataSourceTableModel tableModel) {
        CachedTableModel<TableMessage> tm = new CachedTableModel<TableMessage>(tableModel);
        tm.setPreCachedRowCount(50);
        tm.setPostCachedRowCount(50);
        tm.setRowCount(1000);
        return tm;
    }

    private DefaultTableDefinition<TableMessage> createTableDefinition() {
        tableDefinition = new DefaultTableDefinition<TableMessage>();

        // set the row renderer
        final String[] rowColors = new String[]{"#FFFFDD", "EEEEEE"};
        tableDefinition.setRowRenderer(new DefaultRowRenderer<TableMessage>(rowColors));

        for (AbstractColumnDefinition<TableMessage, String> colDef : colDefs) {
            tableDefinition.addColumnDefinition(colDef);
        }

        return tableDefinition;
    }

    /**
     * Extension of {@link MutableTableModel} for our own {@link Message} type.
     */
    private class DataSourceTableModel extends MutableTableModel<TableMessage> {

        // knows how to sort messages
        private TableMessageSorter sorter = new TableMessageSorter();
        // we keep a map so we can index by id
        private Map<Integer, TableMessage> map;

        /**
         * Set the data on the model.  Overwrites prior data.
         * @param list
         */
        public void setData(ArrayList<TableMessage> list) {
            // toss the list, index by id in a map.
            int size = list.size();
            map = new HashMap<Integer, TableMessage>(size);
            for (int i = 0; i < size; i++) {
                map.put(i, list.get(i));
            }
        }
        
        public TableMessage[] getAllMessages()
        {
        	TableMessage[] result = new TableMessage[map.size()];
        	Iterator<TableMessage> msgItr = map.values().iterator();
        	int i = 0;
        	while(msgItr.hasNext())
        		result[i++] = msgItr.next();
        	return result;
        }

        /**
         * Fetch a {@link Message} by its id.
         * @param id
         * @return
         */
        public TableMessage getMessageById(int id) {
            return map.get(id);
        }

        @Override
        protected boolean onRowInserted(int beforeRow) {
            return true;
        }

        @Override
        protected boolean onRowRemoved(int row) {
            return true;
        }

        @Override
        protected boolean onSetRowValue(int row, TableMessage rowValue) {
            return true;
        }

        @Override
        public void requestRows(
                final Request request,
                TableModel.Callback<TableMessage> callback) {

            callback.onRowsReady(request, new Response<TableMessage>() {

                @Override
                public Iterator<TableMessage> getRowValues() {
                    int col = request.getColumnSortList().getPrimaryColumn();
                    col = (col < 0 ? 0 : col);
                    HasMessageComparator cmp = (HasMessageComparator) tableDefinition.getColumnDefinition(col);
                    final boolean ascending = request.getColumnSortList().isPrimaryAscending();
                    cmp.setSortOrder(ascending);
                    map = sorter.sort(map, cmp.getComparator());
                    pagingScrollTable.gotoFirstPage();
                    return map.values().iterator();
                }
            });
        }
    }

    /**
     * Defines the column for {@link Message#getId()}
     */
    private interface HasMessageComparator {

        Comparator<TableMessage> getComparator();

        void setSortOrder(boolean ascending);
    }

    public final static class NumberColumnDefinition extends AbstractColumnDefinition<TableMessage, String> implements HasMessageComparator {

        int messageField;
        NumberComparator comp;

        public NumberColumnDefinition(int messageField, String headerName,
                boolean columnSortable, NumberComparator comp, boolean isEditable, String groupName) {
            super();
            this.messageField = messageField;
            this.comp = comp;
            setColumnSortable(columnSortable);
            setColumnTruncatable(false);
            //columnDef.setPreferredColumnWidth(35);
            setHeader(0, new HTML(headerName, true));
            if(groupName != null)
            	setHeader(1, groupName);
            //setHeaderCount(1);
            setMinimumColumnWidth(40);
            setHeaderTruncatable(true);
            if(isEditable)
            	setCellEditor(new TextCellEditor());
        }

        @Override
        public String getCellValue(TableMessage rowValue) {
            Double val = rowValue.getNumber(messageField);
            return (val == null ? "" : NumberFormat.getDecimalFormat().format(val));
        }

        @Override
        public void setCellValue(TableMessage rowValue, String cellValue) {
        	try
        	{
        		Double number = NumberFormat.getDecimalFormat().parse(cellValue);
        		rowValue.setNumber(messageField, number);
        	}
        	catch(NumberFormatException ex)
        	{
        		//ignore edit request
        	}
        	
        }

        @Override
		public Comparator<TableMessage> getComparator() {
            return comp;
        }

        @Override
		public void setSortOrder(boolean ascending) {
            comp.setAscending(ascending);
        }
    }

    /**
     * Defines the column for {@link Message#getName()}
     */
    public final static class TextColumnDefinition extends AbstractColumnDefinition<TableMessage, String> implements HasMessageComparator {

        int messageField;
        TextComparator comp;

        public TextColumnDefinition(int messageField, String headerName,
                boolean columnSortable, TextComparator comp, String groupName) {
            super();
            this.messageField = messageField;
            this.comp = comp;
            setColumnSortable(columnSortable);
            setColumnTruncatable(true);
            if(headerName.equals(PanelServiceConstants.GRADE_HEADER))
            	setMinimumColumnWidth(40);
            else
            	setMinimumColumnWidth(75);
            setHeader(0, new HTML(headerName, true));
            if(groupName != null)
            	setHeader(1, groupName);
            //setHeaderCount(1);
            setHeaderTruncatable(true);
        }

        @Override
        public String getCellValue(final TableMessage rowValue) {
            return rowValue.getText(messageField);
        }

        @Override
        public void setCellValue(TableMessage rowValue, String cellValue) {
        }

        @Override
		public Comparator<TableMessage> getComparator() {
            return comp;
        }

        @Override
		public void setSortOrder(boolean ascending) {
            comp.setAscending(ascending);
        }
    }

    /**
     * Defines the column for {@link Message#getStartDate()}
     */
    public final static class DateColumnDefinition extends AbstractColumnDefinition<TableMessage, String> implements HasMessageComparator {

        int messageField;
        DateComparator comp;

        public DateColumnDefinition(int messageField, String headerName,
                boolean columnSortable, DateComparator comp) {
            super();
            this.messageField = messageField;
            this.comp = comp;
            setColumnSortable(columnSortable);
            setColumnTruncatable(false);
            //columnDef.setPreferredColumnWidth(90);
            setHeader(0, new HTML(headerName, true));
            setHeaderCount(1);
            setHeaderTruncatable(true);
        }

        @Override
        public String getCellValue(TableMessage rowValue) {
            Date val = rowValue.getDate(messageField);
            return (val == null ? "" : DateTimeFormat.getMediumDateFormat().format(val));
        }

        @Override
        public void setCellValue(TableMessage rowValue, String cellValue) {
        }

        @Override
		public Comparator<TableMessage> getComparator() {
            return comp;
        }

        @Override
		public void setSortOrder(boolean ascending) {
            comp.setAscending(ascending);
        }
    }
}
