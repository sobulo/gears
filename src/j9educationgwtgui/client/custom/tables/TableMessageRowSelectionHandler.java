/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client.custom.tables;

import j9educationgwtgui.shared.TableMessage;

/**
 *
 * @author Administrator
 */
public interface TableMessageRowSelectionHandler {
    public void onRowSelected(TableMessage m);
}
