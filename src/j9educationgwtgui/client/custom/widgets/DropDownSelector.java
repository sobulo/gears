/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface DropDownSelector {
	
	public String getSelectedDropDownValue();
	public String getSelectedDropDownDisplayName();
	public Widget getWidgetDisplay();
	public void initData();

}
