/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PublishedTestsInfoPanel extends Composite {
	
	@UiField TextBox className;
	@UiField TextBox subjectName;
	@UiField SimplePanel testSlot;
	@UiField SimplePanel additionalSlot;

	private String subjectId;
	private static PublishedTestsInfoPanelUiBinder uiBinder = GWT
			.create(PublishedTestsInfoPanelUiBinder.class);

	interface PublishedTestsInfoPanelUiBinder extends
			UiBinder<Widget, PublishedTestsInfoPanel> {
	}

	public PublishedTestsInfoPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		className.setEnabled(false);
		subjectName.setEnabled(false);
	}
	
	public void setClassName(String className)
	{
		this.className.setValue(className);
	}

	public void setSubjectName(String subject, String subjectId)
	{
		subjectName.setValue(subject);
		this.subjectId = subjectId;
	}
	
	public String getSubjectID()
	{
		return subjectId;
	}
	
	public void setPublishStatus(String[] ids, String[] names, Boolean[] values)
	{
		Grid table = new Grid(ids.length + 1, 2);
		table.addStyleName(GUIConstants.STYLE_STND_TABLE);
		table.getRowFormatter().addStyleName(0, GUIConstants.STYLE_STND_TABLE_HEADER);
		table.setBorderWidth(1);
		table.setText(0, 0, "Test Name");
		table.setText(0, 1, "Published");
		for(int i = 0; i < ids.length; i++)
		{
			CheckBox item = new CheckBox();
			item.setName(ids[i]);
			item.setValue(values[i]);
			table.setText(i+1, 0, names[i]);
			table.setWidget(i+1, 1, item);
			GWT.log("Name: " + names[i]);
			GWT.log("Id: " + ids[i]);
		}
		testSlot.clear();
		testSlot.add(table);
	}
	
	public HashMap<String, Boolean> getPublishStatus()
	{
		Grid table = (Grid) testSlot.getWidget();
		HashMap<String, Boolean> result = new HashMap<String, Boolean>(table.getRowCount() - 1);
		for(int i = 1; i < table.getRowCount(); i++) //start idx = 1 to skip header
		{
			CheckBox item = (CheckBox) table.getWidget(i, 1);
			result.put(item.getName(), item.getValue());
		}
		return result;
	}
	
	public String getSubjectName()
	{
		return subjectName.getValue() + " (" + className.getValue() + ")";
	}
	
	public void setAdditionalSlot(Widget w)
	{
		additionalSlot.add(w);
	}
}
