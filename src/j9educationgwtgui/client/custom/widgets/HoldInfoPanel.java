/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class HoldInfoPanel extends Composite implements DoneLoadingWidget {
	
	@UiField TextBox studentName;
	@UiField TextBox levelName;
	@UiField TextArea message;
	@UiField TextArea note;
	@UiField ListBox type;
	@UiField CheckBox resolved;
	@UiField HTMLPanel idPanel;
	@UiField FlowPanel resolutionPanel;
	
	private boolean loadingCompleted = false;
	private static UserManagerAsync userService = GWT.create(UserManager.class);
	
	private SimpleDialog errorBox;
	
	private static HoldInfoPanelUiBinder uiBinder = GWT
			.create(HoldInfoPanelUiBinder.class);

	interface HoldInfoPanelUiBinder extends UiBinder<Widget, HoldInfoPanel> {
	}
	
    final AsyncCallback<String[]> holdTypeCallBack =
        new AsyncCallback<String[]>() {
	    @Override
	    public void onFailure(Throwable caught) {
	    	loadingCompleted = true;
	    	WidgetHelper.checkForLoginError(caught);
	        errorBox.show("Request failed: " + caught.getMessage());
	    }
	
	    @Override
	    public void onSuccess(String[] result) {
	    	for(String holdType : result)
	    	{
	    		type.addItem(holdType);
	    	}
	    	loadingCompleted = true;
	    }
    };	

	public HoldInfoPanel() 
	{
		initWidget(uiBinder.createAndBindUi(this));
		studentName.setEnabled(false);
		levelName.setEnabled(false);
		userService.getHoldTypes(holdTypeCallBack);
		errorBox = new SimpleDialog("Error initializing hold panel");
	}
	
	public void setIdPanelVisibility(boolean visible)
	{
		idPanel.setVisible(visible);
	}
	
	public void setResolutionPanelVisibility(boolean visible)
	{
		resolutionPanel.setVisible(visible);
	}
	
	public void setMessage(String message)
	{
		this.message.setValue(message);
	}
	
	public String getMessage()
	{
		return message.getValue();
	}
	
	public void setNote(String note)
	{
		this.note.setValue(note);
	}
	
	public String getNote()
	{
		return note.getValue();
	}
	
	public boolean getResolutionStatus()
	{
		return resolved.getValue();
	}
	
	public void setResolutionStatus(boolean status)
	{
		resolved.setValue(status);
	}
	
	public String getStudentName()
	{
		return studentName.getValue();
	}
	
	public void setStudentName(String name)
	{
		studentName.setValue(name);
	}
	
	public void setLevelName(String name)
	{
		levelName.setValue(name);
	}
	
	public String getSelectedType()
	{
		if(type.getSelectedIndex() < 0)
			return "";
		else
			return type.getValue(type.getSelectedIndex());
	}
	
	public void setType(String type)
	{
		for(int i = 0; i < this.type.getItemCount(); i++)
			if(this.type.getValue(i).equalsIgnoreCase(type))
				this.type.setSelectedIndex(i);
	}
	
	public UserManagerAsync getUserService()
	{
		return userService;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return loadingCompleted;
	}
}
