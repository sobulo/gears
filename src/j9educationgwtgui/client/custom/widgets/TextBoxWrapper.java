/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class TextBoxWrapper extends Composite implements Text2ListBoxInput{

    @UiField TextBox input;
    @UiField Label inputLabel;

    private static TextBoxWrapperUiBinder uiBinder = GWT.create(TextBoxWrapperUiBinder.class);

    interface TextBoxWrapperUiBinder extends UiBinder<Widget, TextBoxWrapper> {
    }

    public TextBoxWrapper() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    public void setLabel(String text)
    {
        inputLabel.setText(text);
    }

    public String getCustomText() {
        return input.getText();
    }

    public Widget getInputWidget() {
        return this;
    }

    public void disableInput() {
        input.setEnabled(false);
    }

    public void enableInput() {
        input.setEnabled(true);
    }

    public void reset() {
        input.setText("");
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#getErrorMessage()
	 */
	@Override
	public String getErrorMessage() {
		return "";
	}
}