/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface DoneLoadingWidget {
	public boolean doneLoading();
}
