/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class Text2ListBox extends Composite implements ClickHandler{

    private static Text2ListBoxUiBinder uiBinder = GWT.create(Text2ListBoxUiBinder.class);

    interface Text2ListBoxUiBinder extends UiBinder<Widget, Text2ListBox> {
    }

    @UiField FlowPanel inputSlot;
    @UiField Label statusLabel;
    @UiField Button saveList, addInput, removeInput;
    @UiField ListBox targetList;
    @UiField HTMLPanel additionalFeatures;
    Text2ListBoxInput input;

    protected HashSet<String> targetSet;
    private final static int defaultSize = 4;

    public static int getDefaultSize()
    {
        return defaultSize;
    }

    public Text2ListBox() {
        initWidget(uiBinder.createAndBindUi(this));
        targetSet = new HashSet<String>(defaultSize);
        addInput.addClickHandler(this);
        removeInput.addClickHandler(this);
        disableBox();
    }

    //hack, ideally should have uitemplate specify a constructor
    //for now user is required to call init func below asap
    public void initInput(Text2ListBoxInput i)
    {
        input = i;
        inputSlot.add(i.getInputWidget());
        input.disableInput();
    }

    public void setStatus(String status)
    {
        statusLabel.setText(status);
    }

    public HandlerRegistration registerSaveButtonHandler(ClickHandler h)
    {
        return saveList.addClickHandler(h);
    }

    public void clear()
    {
        if(input != null)
            input.reset();
        if(statusLabel != null)
            statusLabel.setText("");
        targetList.clear();
        targetSet.clear();
    }

    public void setVisbileArea(int displaySize, String width)
    {
        targetList.setVisibleItemCount(displaySize);
        targetList.setWidth(width);
    }
    
    public void addItemToList(String value)
    {
    	String val = value.trim();
         if( val.length()!=0 && !targetSet.contains(val) && targetSet.add(val))
            targetList.addItem(val);
    }

    public void initializeList(String[] items)
    {
        clear();
        for(String s : items)
            addItemToList(s.trim());
        enableBox();
    }

    public void initializeList(HashSet<String> items)
    {
        clear();
        for(String s: items)
            addItemToList(s.trim());
        enableBox();
    }

    public void onClick(ClickEvent event) {
        if(event.getSource().equals(addInput))
        {
        	if(input.getErrorMessage().length() > 0)
        	{
        		statusLabel.setText("ERROR, unable to add because: " + input.getErrorMessage());
        		return;
        	}
        	
            String val = input.getCustomText();
            addItemToList(val);
            input.reset();
        }
        else if(event.getSource().equals(removeInput))
        {
            String val = targetList.getValue(targetList.getSelectedIndex());
            if(targetSet.contains(val) && targetSet.remove(val))
                targetList.removeItem(targetList.getSelectedIndex());
        }
    }

    public HashSet<String> getList()
    {
        return targetSet;
    }

    public void disableBox()
    {
        addInput.setEnabled(false);
        removeInput.setEnabled(false);
        if(saveList != null)
            saveList.setEnabled(false);
        if(input != null)
            input.disableInput();
    }

    public void enableBox()
    {
        addInput.setEnabled(true);
        removeInput.setEnabled(true);
        if(saveList != null)
            saveList.setEnabled(true);
        if(input != null)
            input.enableInput();
    }

    public boolean isSaveButton(Object b)
    {
        return (saveList.equals(b));
    }

    public void removeAdditionalFeatures()
    {
        additionalFeatures.remove(saveList);
        additionalFeatures.remove(statusLabel);
        saveList = null;
        statusLabel = null;
        additionalFeatures.removeFromParent();
    }
}