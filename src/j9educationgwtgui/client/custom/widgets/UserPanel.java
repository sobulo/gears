/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.shared.DefaultLoginNameGenerator;
import j9educationgwtgui.shared.PanelServiceConstants;

import java.util.ArrayList;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class UserPanel extends Composite implements ValueChangeHandler<String>
{
    @UiField TextBox firstName;
    @UiField TextBox lastName;
    @UiField TextBox email;
    @UiField Text2ListBox phoneNumbers;
    @UiField PasswordTextBox password;
    @UiField PasswordTextBox confirmPassword;
    @UiField TextBox loginName;
    @UiField Label passwordLabel;
    @UiField Label confirmPasswordLabel;

    private DefaultLoginNameGenerator loginNameGenerator;
    private static UserPanelUiBinder uiBinder = GWT.create(UserPanelUiBinder.class);
    private boolean passwordValidation = true;
    private static final RegExp EMAIL_MATCHER = RegExp.compile(PanelServiceConstants.REGEX_EMAIL, "i");
    private static final RegExp PHONE_NUM_MATCHER = RegExp.compile(PanelServiceConstants.REGEX_NUMS_ONLY);
    
    public UserPanel()
    {
        initWidget(uiBinder.createAndBindUi(this));
        TextBoxWrapper tbw = new TextBoxWrapper();
        //tbw.setLabel(GUIConstants.PHONE_NUMBER_INPUT_LABEL);
        phoneNumbers.initInput(tbw);
        phoneNumbers.setVisbileArea(GUIConstants.DEFAULT_PHONE_PER_USER, GUIConstants.DEFAULT_LB_WIDTH);
        phoneNumbers.enableBox();
        phoneNumbers.removeAdditionalFeatures();
        loginName.setEnabled(false);
        firstName.addValueChangeHandler(this);
        lastName.addValueChangeHandler(this);
        loginNameGenerator = new DefaultLoginNameGenerator();
    }
    
    public void disableLoginGenerator()
    {
    	loginNameGenerator = null;
    }
    
    public void disablePasswords()
    {
    	passwordValidation = false;
    	password.setVisible(false);
    	confirmPassword.setVisible(false);
    	passwordLabel.setVisible(false);
    	confirmPasswordLabel.setVisible(false);
    }
    
    public void disableNameEdits()
    {
    	firstName.setEnabled(false);
    	lastName.setEnabled(false);
    }
    
    public void disableEmailEdits()
    {
    	email.setEnabled(false);
    }

    public String getEmail() {
        return email.getValue().trim().toLowerCase();
    }

    public String getFirstName() {
        return firstName.getValue().trim();
    }

    public String getLastName() {
        return lastName.getValue().trim();
    }

    public String getLoginName() {
        return loginName.getValue().trim().toLowerCase();
    }

    public String getPassword() {
        return password.getValue();
    }

    public HashSet<String> getPhoneNumbers() {
        return phoneNumbers.getList();
    }
    
    public void setEmail(String eml) {
        email.setValue(eml);
    }

    public void setFirstName(String fnm) {
        firstName.setValue(fnm);
    }

    public void setLastName(String lnm) {
        lastName.setValue(lnm);
    }

    public void setLoginName(String lgnm) {
        loginName.setValue(lgnm);
    }

    public void setPassword(String pwd) {
        password.setValue(pwd);
    }

    public void setPhoneNumbers(HashSet<String> phnms) {
        phoneNumbers.initializeList(phnms);
    }
    
    public boolean isValidEmail(String email)
    {
    	return EMAIL_MATCHER.exec(email) != null;
    }
    
    public boolean isValidNum(String num)
    {
    	return PHONE_NUM_MATCHER.exec(num.replaceAll(PanelServiceConstants.REGEX_PHONE_REPLACE, "")) != null;
    }

    public void clear()
    {
        phoneNumbers.clear();
        firstName.setValue(null);
        lastName.setValue(null);
        email.setValue(null);
        password.setValue(null);
        confirmPassword.setValue(null);
        loginName.setValue(null);
    }

    public ArrayList<String> validateFields()
    {
        ArrayList<String> result = new ArrayList<String>();

        //validate fields
        if(passwordValidation && getPassword().length() == 0)
        	result.add("Password field is blank");
        
        if(getFirstName().length() == 0)
        	result.add("First name field is blank");
        
        if(getLastName().length() == 0)
        	result.add("Last name field is blank");
        
        if(getLoginName().length() == 0)
        	result.add("Login name field is blank");
        
        if(getEmail().length() != 0 && !isValidEmail(email.getValue()))
        	result.add("Email address is not valid");
        
        for(String num : getPhoneNumbers())
        	if(!isValidNum(num))
        		result.add("Phone number: " + num + " is not valid");
        
        if(passwordValidation && !password.getValue().equals(confirmPassword.getValue()))
            result.add("Password fields do not match");
        
        return result;
    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
    	if(loginNameGenerator == null)
    		return;
        String[] components = new String[2];
        components[0] = lastName.getValue();
        components[1] = firstName.getValue();
        loginName.setText(loginNameGenerator.generateLoginName(components));
    }

    interface UserPanelUiBinder extends UiBinder<Widget, UserPanel> {
    }
}