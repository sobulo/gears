/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.shared.PanelServiceLoginRoles;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class StudentSuggestBox extends UserSuggestBox {
	
	public StudentSuggestBox()
	{
		super();
		label.setText("Student");
	}

	@Override
	public void populateSuggestBox()
	{
		getUserService().getAllStudentIDs(true, getPopulateCallBack());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.UserSuggestBox#getRole()
	 */
	@Override
	public PanelServiceLoginRoles getRole() 
	{
		return PanelServiceLoginRoles.ROLE_STUDENT;
	}

}
