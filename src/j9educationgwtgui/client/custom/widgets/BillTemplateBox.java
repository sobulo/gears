/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.AccountManager;
import j9educationgwtgui.client.AccountManagerAsync;
import j9educationgwtgui.client.GUIConstants;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BillTemplateBox extends Composite implements DoneLoadingWidget, 
	HasValueChangeHandlers<String>, ChangeHandler, DropDownSelector{
	
	@UiField ListBox btList;

	private static BillTemplateBoxUiBinder uiBinder = GWT
			.create(BillTemplateBoxUiBinder.class);
	
	private static HashMap<String, String> cachedBillList = null;
	
	private static AccountManagerAsync accountService = GWT.create(AccountManager.class);

	interface BillTemplateBoxUiBinder extends UiBinder<Widget, BillTemplateBox> {
	}
	
	private SimpleDialog errorBox;
	private static boolean loadingCompleted = false;
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> btListCallback =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	loadingCompleted = true;
        	WidgetHelper.checkForLoginError(caught);
            errorBox.show(caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String, String> result)
        {
        	cachedBillList = result;
        	setupBillList(result);
        	loadingCompleted = true;
        }
    };

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public BillTemplateBox() {
		initWidget(uiBinder.createAndBindUi(this));
		errorBox = new SimpleDialog("Error!!");
		btList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
		btList.addChangeHandler(this);
	}
	
	public void setupBillList(HashMap<String, String> billTemplateMap)
	{
		for(String btName : billTemplateMap.keySet())
			btList.addItem(billTemplateMap.get(btName), btName);
		ValueChangeEvent.fire(this, getSelectedBillTemplate());
	}

    public void initData()
    {
        //fetch class list from server if an earlier instance hasn't done this already
        if(cachedBillList == null)
        	accountService.getBillTemplateList(btListCallback);
        else
        	setupBillList(cachedBillList);    	
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return loadingCompleted;
	}
	
	public String getSelectedBillTemplate()
	{
		int selIdx = btList.getSelectedIndex();
		if (selIdx < 0)
			return "";
		return btList.getValue(btList.getSelectedIndex());
	}
	
	public String getSelectedBillTemplateName()
	{
		int selIdx = btList.getSelectedIndex();
		if (selIdx < 0)
			return "";		
		return btList.getItemText(btList.getSelectedIndex());
	}
	
	public AccountManagerAsync getAccountService()
	{
		return accountService;
	}
	
	public int getNumberOfBillTemplates()
	{
		return btList.getItemCount();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		ValueChangeEvent.fire(this, getSelectedBillTemplate());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DropDownSelector#getSelectedDropDownValue()
	 */
	@Override
	public String getSelectedDropDownValue() {
		return getSelectedBillTemplate();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DropDownSelector#getSelectedDropDownDisplayName()
	 */
	@Override
	public String getSelectedDropDownDisplayName() {
		return getSelectedBillTemplateName();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DropDownSelector#getWidgetDisplay()
	 */
	@Override
	public Widget getWidgetDisplay() {
		return this;
	}
}
