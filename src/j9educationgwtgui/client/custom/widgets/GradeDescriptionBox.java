/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.SchoolService;
import j9educationgwtgui.client.SchoolServiceAsync;
import j9educationgwtgui.shared.GradeDescription;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class GradeDescriptionBox extends Composite implements ClickHandler{

    @UiField Label gradeLabel;
    @UiField TextBox grade;
    @UiField Label gpaLabel;
    @UiField TextBox gpa;
    @UiField Label scoreLabel;
    @UiField TextBox score;

    @UiField Button addInput;
    @UiField Button removeInput;

    @UiField ListBox display;

    @UiField Label status;
    @UiField Button save;
    @UiField Button view;

    private LinkedHashMap<Double, GradeDescription> gradeDescription;
    private SchoolServiceAsync schoolService = GWT.create(SchoolService.class);

    final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {
        @Override
        public void onSuccess(Void result) {
            status.setText("Saved grade descriptions. Click view to see updates");
        }

        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            status.setText("Request failed: " + caught.getMessage());
        }
    };

    final AsyncCallback<String[]> viewCallback = new AsyncCallback<String[]>() {
        @Override
        public void onSuccess(String[] result) {
            String msg = "";

            if(result.length == 0)
            {
                status.setText("There is currently no default grading scheme");
                return;
            }

            for(String label : result)
                msg += label + "\n";
            status.setText(msg);
        }

        @Override
        public void onFailure(Throwable caught) {
        	WidgetHelper.checkForLoginError(caught);
            status.setText("Request failed: " + caught.getMessage());
        }
    };

    private static GradeDescriptionBoxUiBinder uiBinder = GWT.create(GradeDescriptionBoxUiBinder.class);

    public GradeDescriptionBox() {
        initWidget(uiBinder.createAndBindUi(this));
        display.setVisibleItemCount(5);
        display.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        reset();
        save.addClickHandler(this);
        addInput.addClickHandler(this);
        removeInput.addClickHandler(this);
        view.addClickHandler(this);
    }

    @Override
    public void onClick(ClickEvent event) {
        try
        {
            if(event.getSource() == addInput)
                addGradeDesc();
            else if(event.getSource() == removeInput)
                removeGradeDesc();
            else if(event.getSource() == save)
                schoolService.updateDefaultGradingScheme(gradeDescription, saveCallback);
            else if(event.getSource() == view)
                schoolService.getDefaultGradingSchemeDescription(viewCallback);
            else
                status.setText("unsupported operation");
        }
        catch(RuntimeException ex)
        {
            Button b = (Button) event.getSource();
            String label = b.getText();
            status.setText(label + " request failed, error was: " +
                    ex.getMessage());
        }

    }
    
    private void reset()
    {
        gradeLabel.setText("Lowest Grade: ");
        gpaLabel.setText("Lowest GPA: ");
        scoreLabel.setText("Lowest Score: ");
        score.setValue("0.0");
        score.setEnabled(false);
        removeInput.setEnabled(false);
        gradeDescription = null;
    }

    private void addGradeDesc()
    {
        String gradeVal = grade.getValue();
        Double gpaVal = Double.valueOf(gpa.getValue());
        Double scoreVal = Double.valueOf(score.getValue());

        if(gradeDescription == null)
        {
            gradeDescription = new LinkedHashMap<Double, GradeDescription>();
            removeInput.setEnabled(true);
            score.setEnabled(true);
            gradeLabel.setText("Grade: ");
            gpaLabel.setText("GPA: ");
            scoreLabel.setText("Score: ");
            score.setValue("");
            gpa.setValue("");
            grade.setValue("");
        }
        gradeDescription.put(scoreVal, new GradeDescription(gradeVal, gpaVal));
        refreshDisplay();
    }

    private void removeGradeDesc()
    {
        double key = Double.valueOf(display.getValue(display.getSelectedIndex()));

        if(gradeDescription.remove(key) != null)
        {
            if(gradeDescription.size() == 0)
                reset();
            refreshDisplay();
        }
    }

    private void refreshDisplay()
    {
        display.clear();

        if(gradeDescription == null)
            return;

        Iterator<Map.Entry<Double, GradeDescription>> displayItr = gradeDescription.entrySet().iterator();
        while(displayItr.hasNext())
        {
            Map.Entry<Double, GradeDescription> item = displayItr.next();
            display.addItem("Grade: " + item.getValue().letterGrade + 
                    " Cutoff: " + item.getKey() + " GPA: " + item.getValue().gpa,
                    String.valueOf(item.getKey()));
        }
    }

    interface GradeDescriptionBoxUiBinder extends UiBinder<Widget, GradeDescriptionBox> {
    }
}