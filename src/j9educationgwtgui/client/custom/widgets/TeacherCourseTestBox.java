package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class TeacherCourseTestBox extends Composite implements ValueChangeHandler<String>, 
	HasValueChangeHandlers<String>, ChangeHandler, DoneLoadingWidget
{
	
	@UiField TeacherCoursesBox courseList;
	@UiField ListBox testList;
	
	private SimpleDialog errorBox;
	private ScoreManagerAsync scoreService = GWT.create(ScoreManager.class);
	
	private static TeacherCourseTestBoxUiBinder uiBinder = GWT
			.create(TeacherCourseTestBoxUiBinder.class);

	interface TeacherCourseTestBoxUiBinder extends
			UiBinder<Widget, TeacherCourseTestBox> {
	}
	
	private static boolean loadingCompleted;
	private static HashMap<String, HashMap<String, String>> cachedTests = new HashMap<String, HashMap<String, String>>();

    final AsyncCallback<HashMap<String, String>> testNamesCallback =
        new AsyncCallback<HashMap<String, String>>() {

            @Override
            public void onFailure(Throwable caught) {
            	loadingCompleted = true;
            	WidgetHelper.checkForLoginError(caught);
                errorBox.show("Try reloading browser. Request failed: " + 
                		caught.getMessage());
                testList.setVisible(false);
            }

            @Override
            public void onSuccess(HashMap<String, String> result) {
            	populateTests(result);
            	cachedTests.put(courseList.getSelectedCourse(), result);
            	loadingCompleted = true;
            }
        };	

	public TeacherCourseTestBox() {
		initWidget(uiBinder.createAndBindUi(this));
		courseList.addValueChangeHandler(this);
		testList.addChangeHandler(this);
		errorBox = new SimpleDialog("An error occured");
        testList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);			
	}
	
	public void initData()
	{
		courseList.initData();
	}

	private void populateTests(HashMap<String, String> result)
	{
        testList.clear();
        Iterator<Map.Entry<String,String>> resultItr =
                result.entrySet().iterator();
        while(resultItr.hasNext())
        {
            Map.Entry<String, String> item = resultItr.next();
            testList.addItem(item.getValue(), item.getKey());
        }
        
    	if(testList.getItemCount() > 0)
    	{
    		testList.setSelectedIndex(0);
    		ValueChangeEvent.fire(this, getSelectedTest());
    	}
	}
	
    private void fetchTestNames(String courseKeyStr) {
        scoreService.getTestKeys(courseKeyStr, testNamesCallback);
    }
    
    public ScoreManagerAsync getScoreManager()
    {
    	return scoreService;
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String courseKey = event.getValue();
		if(cachedTests.containsKey(courseKey))
		{
			GWT.log("test cache hit!!");
			populateTests(cachedTests.get(courseKey));
		}
		else
			fetchTestNames(event.getValue());
	}
	
	public String getSelectedTest()
	{
		if(testList.getSelectedIndex() < 0)
			return "";
		return testList.getValue(testList.getSelectedIndex());
	}
	
	public String getSelectedTestName()
	{
		if(testList.getSelectedIndex() < 0)
			return "";
		return testList.getItemText(testList.getSelectedIndex());
	}
		
	public String getCourseName()
	{
		return courseList.getSelectedCourseName();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		ValueChangeEvent.fire(this, getSelectedTest());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		// TODO Auto-generated method stub
		return loadingCompleted;
	}		
}
