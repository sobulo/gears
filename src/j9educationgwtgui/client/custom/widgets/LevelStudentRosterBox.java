/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class LevelStudentRosterBox extends Composite implements ValueChangeHandler<String>, DoneLoadingWidget
{
    @UiField LevelBox classListBox;
    @UiField Label studentListLabel;
    @UiField ListBox studentList;

    private static HashMap<String, HashMap<String, String>> cachedRosterResults;
    private SimpleDialog errorBox;
    
    static
    {
    	cachedRosterResults = new HashMap<String, HashMap<String, String>>();
    }
    
    private String lastSelectedClass = "";
    private static boolean loadingCompleted = false;
    private static LevelStudentRosterBoxUiBinder uiBinder = GWT.create(LevelStudentRosterBoxUiBinder.class);

    final AsyncCallback<HashMap<String, String>> classRosterCallBack =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	loadingCompleted = true;
        	WidgetHelper.checkForLoginError(caught);
            errorBox.show(caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
            populateStudentBox(result);
            cachedRosterResults.put(lastSelectedClass, result);
            loadingCompleted = true;
        }
    };

    interface LevelStudentRosterBoxUiBinder extends UiBinder<Widget, LevelStudentRosterBox> {
    }

    public LevelStudentRosterBox() {
        initWidget(uiBinder.createAndBindUi(this));  
        classListBox.addValueChangeHandler(this);
        classListBox.initData();
        studentList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
    }
    
    public void removeCacheEntry(String classKey)
    {
    	cachedRosterResults.remove(classKey);
    }


    private void populateStudentBox(HashMap<String, String> result)
    {
    	studentList.clear();
        Iterator<Entry<String, String>> studVals = result.entrySet().iterator();
        while(studVals.hasNext())
        {
            Entry<String, String> val = studVals.next();
            studentList.addItem(val.getKey(), val.getValue());
        }
    }

    public String getSelectedStudent()
    {
    	if(studentList.getSelectedIndex() < 0)
    		return "";
    	
        return studentList.getValue(studentList.getSelectedIndex());
    }
    
    public String getSelectedStudentName()
    {
    	if(studentList.getSelectedIndex() < 0)
    		return "";
    	
        return studentList.getItemText(studentList.getSelectedIndex());
    }    
    
    public HashMap<String, String> getAllSelectedStudents()
    {
    	HashMap<String, String> result = new HashMap<String, String>();
    	for(int i = 0; i < studentList.getItemCount(); i++)
    	{
    		if(studentList.isItemSelected(i))
    			result.put(studentList.getValue(i), studentList.getItemText(i));
    	}
    	return result;
    }

    public String getSelectedClass()
    {
        return classListBox.getSelectedClass();
    }
    
    public String getSelectedClassName()
    {
        return classListBox.getSelectedClassName();
    }    

    public ClassRosterServiceAsync getClassService()
    {
        return classListBox.getClassService();
    }
    
    public void setDisplayHeight(int visibleItemCount)
    {
    	studentList.setMultipleSelect(true);
    	studentList.setVisibleItemCount(visibleItemCount);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) 
	{
		studentList.clear();
        String selectedClass = event.getValue();
        if(cachedRosterResults.containsKey(selectedClass))
            populateStudentBox(cachedRosterResults.get(selectedClass));
        else
        {
            lastSelectedClass = selectedClass;
            classListBox.getClassService().getClassRosterMap(selectedClass, classRosterCallBack);
        }
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() 
	{
		//loading is considered completed when levelbox has fired at least 1 event that's triggered
		//an attempt to fetch students for that level
		return loadingCompleted;
	}
}