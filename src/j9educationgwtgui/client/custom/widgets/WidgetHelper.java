/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.exceptions.LoginValidationException;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class WidgetHelper{
	public final static String SELECTED_STACK_COLOR = GUIConstants.SELECTED_STACK_COLOR;
	public final static String SUCCESS_COLOR = "grepGreen";
	public final static String ERROR_COLOR = "grepRed";
	public final static String NORMAL_COLOR = "grepBlack";
	public final static SimpleDialog errorBox = new SimpleDialog("<center><b class='grepRed'>ERROR!</b></center>", true);
	public final static SimpleDialog infoBox = new SimpleDialog("<center><b class='grepGreen'>Info</b></center>", true);
	
	public static LoginInfo loginInfo = null;

	public static StackLayoutPanel getStackLayout(Widget[]widgets, String[] headers)
	{
		final StackLayoutPanel result = new StackLayoutPanel(Unit.PX);
		//either this or add to a simple panel with size set
		result.setSize(GUIConstants.DEFAULT_STACK_WIDTH, GUIConstants.DEFAULT_STACK_HEIGHT);
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), 
					headers[i], GUIConstants.DEFAULT_STACK_HEADER);
		result.addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				clearSelectedStack(result);
				result.getHeaderWidget(event.getSelectedItem()).addStyleName(SELECTED_STACK_COLOR);
			}
		});
		
		if(headers.length > 0)
			result.getHeaderWidget(0).addStyleName(SELECTED_STACK_COLOR);
		return result;
	}
	
	public static TabLayoutPanel getTabLayout(Widget[] widgets, String[]headers)
	{
		TabLayoutPanel result = new TabLayoutPanel(20, Unit.PX);
		result.setSize(GUIConstants.DEFAULT_STACK_WIDTH, GUIConstants.DEFAULT_STACK_HEIGHT);
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), headers[i]);
		return result;
	}
	
	public static void setErrorStatus(Label status, String msg, boolean showDialog)
	{
		status.setStyleName(ERROR_COLOR);
		status.setText(msg);
		if(showDialog)
			errorBox.show(msg);
	}
	
	public static void setSuccessStatus(Label status, String msg, boolean showDialog)
	{
		status.setStyleName(SUCCESS_COLOR);
		status.setText(msg);
		if(showDialog)
			infoBox.show(msg);
	}
	
	public static void setNormalStatus(Label status, String msg)
	{
		status.setStyleName(NORMAL_COLOR);
		status.setText(msg);
	}
		
	public static void clearSelectedStack(StackLayoutPanel stack)
	{
		for(int i = 0; i < stack.getWidgetCount(); i++)
			stack.getHeaderWidget(i).removeStyleName(SELECTED_STACK_COLOR);
	}
	
    public static void setLoginInfo(LoginInfo info) {
        loginInfo = info;
    }

    public static void checkForLoginError(Throwable error) {
        if (error instanceof LoginValidationException)
        {
        	String msgFmt = "You've either been logged out or you've tried accessing a resource not matching your system privilleges. For security/privacy reasons, the system automatically logs " +
    		"you out after 60 minutes of inactivity. The exact error returned by the server was: ";        	
            Window.alert(msgFmt + error.getMessage());
            String url = GWT.getHostPageBaseURL();
            if(loginInfo != null && loginInfo.isLoggedIn())
                url = loginInfo.getLogoutUrl();
            Window.Location.replace(url);
        }
    }	
}
