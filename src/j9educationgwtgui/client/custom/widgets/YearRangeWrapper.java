/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class YearRangeWrapper extends Composite implements ChangeHandler, Text2ListBoxInput{

    @UiField ListBox startYear;
    @UiField ListBox endYear;
    private static YearRangeWrapperUiBinder uiBinder = GWT.create(YearRangeWrapperUiBinder.class);

    public YearRangeWrapper() {
        initWidget(uiBinder.createAndBindUi(this));
      
        //setup year fields. yes we're using deprecated methods but gwt doesn't
        //seem to support Calendar class
        int currentYear = (new Date()).getYear() + GUIConstants.BASE_YEAR;
        for( int i = GUIConstants.MIN_ACADEMIC_YEAR; i <= currentYear +
                GUIConstants.OFFSET_ACADEMIC_YEAR; i++)
        {
            startYear.addItem(String.valueOf(i));
        }
        setUpEndYearList();
        startYear.addChangeHandler(this);
    }

    public void onChange(ChangeEvent event) {
        setUpEndYearList();
    }

    public void setUpEndYearList()
    {
        int currentIndex = startYear.getSelectedIndex();
        int maxIndex = startYear.getItemCount();
        endYear.clear();
        endYear.addItem(startYear.getValue(currentIndex++));
        if(currentIndex < maxIndex)
            endYear.addItem(startYear.getValue(currentIndex));
    }

    public String getCustomText() {
        String start = String.valueOf(startYear.getValue(startYear.getSelectedIndex()));
        String end = String.valueOf(endYear.getValue(endYear.getSelectedIndex()));
        String val;
        if(start.equals(end))
            val = start;
        else
            val = start + "/" + end;
        return val;
    }

    public Widget getInputWidget() {
        return this;
    }

    public void disableInput() {
        startYear.setEnabled(false);
        endYear.setEnabled(false);
    }

    public void enableInput() {
        startYear.setEnabled(true);
        endYear.setEnabled(true);
    }

    public void reset() {
        if(startYear.getItemCount() > 0)
            startYear.setSelectedIndex(0);
        if(endYear.getItemCount() > 0)
            endYear.setSelectedIndex(0);
    }

    interface YearRangeWrapperUiBinder extends UiBinder<Widget, YearRangeWrapper> {
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#getErrorMessage()
	 */
	@Override
	public String getErrorMessage() {
		return "";
	}
}