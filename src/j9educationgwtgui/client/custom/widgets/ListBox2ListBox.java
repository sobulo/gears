/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class ListBox2ListBox extends Composite implements ClickHandler{

    @UiField Button addInput;
    @UiField Button removeInput;
    @UiField Button sourceSort;
    @UiField Button targetSort;    
    @UiField SimplePanel sourcePanel;
    @UiField SimplePanel targetPanel;
    @UiField Label sourceLabel;
    @UiField Label targetLabel;
    @UiField CheckBox sourceSelectAll;
    @UiField CheckBox targetSelectAll;
    private ListBox targetList;
    private ListBox inputList;
    private HashSet<String> inputSet;
    private HashSet<String> targetSet;
    private LB2LBAdd addConstraint;
    private boolean targetSortFlag, sourceSortFlag;

    private static ListBox2ListBoxUiBinder uiBinder = GWT.create(ListBox2ListBoxUiBinder.class);

    interface ListBox2ListBoxUiBinder extends UiBinder<Widget, ListBox2ListBox> {
    }

    public ListBox2ListBox() {
        initWidget(uiBinder.createAndBindUi(this));
        inputList = new ListBox(true);
        targetList = new ListBox(true);
        inputSet = new HashSet<String>();
        targetSet = new HashSet<String>();
        sourcePanel.add(inputList);
        targetPanel.add(targetList);
        addInput.addClickHandler(this);
        removeInput.addClickHandler(this);
        sourceSelectAll.addClickHandler(this);
        targetSelectAll.addClickHandler(this);
        targetSortFlag = sourceSortFlag = true;
        sourceSort.addClickHandler(this);
        targetSort.addClickHandler(this);
    }

    public void initAppearance(int visibleItems, String width, String sourceCaption, String targetCaption)
    {
        inputList.setVisibleItemCount(visibleItems);
        targetList.setVisibleItemCount(visibleItems);
        inputList.setWidth(width);
        targetList.setWidth(width);    	
        sourceLabel.setText(sourceCaption);
        targetLabel.setText(targetCaption);
    }
    
    public void initValues(HashMap<String, String> inputItems, boolean useKeyAsDisplayedItem)
    {
    	inputList.clear();
    	inputSet.clear();
    	targetList.clear();
    	targetSet.clear();
    	
    	if(inputItems == null)
    		return;
    	
        Iterator<Entry<String, String>> mapItr = inputItems.entrySet().iterator();
        
        while(mapItr.hasNext())
        {
            Entry<String, String> entry = mapItr.next();
            if(useKeyAsDisplayedItem) 
                addItemToList(entry.getKey(), entry.getValue(), inputList, inputSet);
            else
            	addItemToList(entry.getValue(), entry.getKey(), inputList, inputSet);
        }
    }
    
    public void initValues(HashMap<String, String> inputItems)
    {
    	initValues(inputItems, true);
    }

    private void addItemToList(String name, String val, ListBox list2BChanged,
            HashSet<String> set2BChanged) {
        if (val.length() != 0 && !set2BChanged.contains(val) && set2BChanged.add(val)) {
            list2BChanged.addItem(name, val);
        }
    }

    private void transfer(ListBox listA, ListBox listB, HashSet<String> setA, HashSet<String> setB)
    {
        transfer(listA, listB, setA, setB, false);
    }

    private void transfer(ListBox listA, ListBox listB, HashSet<String> setA, HashSet<String> setB, boolean ignoreSelected)
    {
        int max = listA.getItemCount();
        for (int i = 0; i < max; i++)
        {
            if (listA.isItemSelected(i) || ignoreSelected)
            {
                String val = listA.getValue(i);
                String name = listA.getItemText(i);
                addItemToList(name, val, listB, setB);

                if (setA.remove(val))
                {
                    listA.removeItem(i);
                    i--;
                    max--;
                }
            }
        }
        sourceSelectAll.setValue(false);
        targetSelectAll.setValue(false);        
    }

    public void onClick(ClickEvent event) {
        if (event.getSource() == addInput)
        {
        	if(addConstraint != null && addConstraint.rejectAdd())
        		return;
        	
            transfer(inputList, targetList, inputSet, targetSet);
        }
        else if (event.getSource() == removeInput)
            transfer(targetList, inputList, targetSet, inputSet);
        else if(event.getSource() == sourceSelectAll)
        {
        	selectAll(inputList, sourceSelectAll.getValue());
        }
        else if(event.getSource() == targetSelectAll)
        {
        	selectAll(targetList, targetSelectAll.getValue());
        }
        else if(event.getSource() == targetSort)
        {
        	GWT.log("Sort Target: " + targetSortFlag);
        	sortList(targetList, targetSortFlag);
        	targetSortFlag = !targetSortFlag;
        }
        else if(event.getSource() == sourceSort)
        {
        	GWT.log("Sort Source: " + sourceSortFlag);
        	sortList(inputList, sourceSortFlag);
        	sourceSortFlag  = !sourceSortFlag;
        }
        	
    }
    
    private void selectAll(ListBox box, boolean isSelected)
    {
    	int numOfItems = box.getItemCount();
    	for(int i = 0; i < numOfItems; i++)
    		box.setItemSelected(i, isSelected);
    }

    private void sortList(ListBox box, boolean ascending)
    {
    	int numOfItems = box.getItemCount();
    	if(numOfItems == 0) return; //nothing to sort
    	
        ArrayList<ListEntrySorter> list = new ArrayList<ListEntrySorter>(numOfItems);
    	
    	for(int i = 0; i < numOfItems; i++)
    	{
    		String key = box.getItemText(i);
    		String val = box.getValue(i);
    		list.add(new ListEntrySorter(key, val));
    	}
    	Collections.sort(list);
    	box.clear();
    	int start, end, step;
    	if(ascending)
    	{
    		start = 0;
    		end = list.size();
    		step = 1;
    	}
    	else
    	{
    		start = list.size() - 1;
    		end = -1;
    		step = -1;
    	}
    	
    	while(start != end)
    	{
    		ListEntrySorter entry = list.get(start);
    		box.addItem(entry.key, entry.val);
    		start += step;
    		GWT.log(entry.key);
    	}
    }    
    
    public HashSet<String> getValues()
    {
        return targetSet;
    }

    public void setAddButtonLabel(String label)
    {
        addInput.setText(label);
    }

    public void setRemoveButtonLabel(String label)
    {
        removeInput.setText(label);
    }

    public void reset()
    {
        transfer(targetList, inputList, targetSet, inputSet, true);
    }
    
    public void setAddConstraint(LB2LBAdd constraint)
    {
    	addConstraint = constraint;
    }
    
    public static interface LB2LBAdd
    {
    	public boolean rejectAdd();
    }
    
	final class ListEntrySorter implements Comparable<ListEntrySorter>
	{
		String key, val;
		ListEntrySorter(String key, String val)
		{
			this.key = key;
			this.val = val;
		}
		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(ListEntrySorter o) {
			return this.key.compareTo(o.key);
		}
	}    
}