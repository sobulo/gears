package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.shared.PanelServiceLoginRoles;

public class TeacherSuggestBox extends UserSuggestBox {
	
	public TeacherSuggestBox()
	{
		super();
		label.setText("Select Teacher");
	}

	@Override
	protected void populateSuggestBox() {
		getUserService().getAllTeacherIDs(getPopulateCallBack());
	}

	@Override
	public PanelServiceLoginRoles getRole() {
		return PanelServiceLoginRoles.ROLE_TEACHER;
	}

}
