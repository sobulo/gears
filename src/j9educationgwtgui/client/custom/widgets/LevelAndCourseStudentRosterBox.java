package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class LevelAndCourseStudentRosterBox extends Composite implements ValueChangeHandler<String>, DoneLoadingWidget
{
    @UiField 
    LevelAndCourseBox classAndSubject;
    
    @UiField 
    Label studentListLabel;
    
    @UiField 
    ListBox studentList;
    
    private SimpleDialog errorBox;
    
    private static HashMap<String, HashMap<String, String>> cachedRosterResults;
    
    static
    {
    	cachedRosterResults = new HashMap<String, HashMap<String, String>>();
    }
    
    private String lastSelectedCourse = "";
    private static boolean loadingCompleted = false;
    
    final AsyncCallback<HashMap<String, String>> rosterCallBack =
        new AsyncCallback<HashMap<String, String>>() {
    @Override
    public void onFailure(Throwable caught) {
    	loadingCompleted = true;
    	WidgetHelper.checkForLoginError(caught);
        errorBox.show(caught.getMessage());
    }

    @Override
    public void onSuccess(HashMap<String, String> result) {
        populateStudentBox(result);
        cachedRosterResults.put(lastSelectedCourse, result);
        loadingCompleted = true;
    }
};    

	private static LevelAndCourseStudentRosterBoxUiBinder uiBinder = GWT
			.create(LevelAndCourseStudentRosterBoxUiBinder.class);

	interface LevelAndCourseStudentRosterBoxUiBinder extends
			UiBinder<Widget, LevelAndCourseStudentRosterBox> {
	}

	public LevelAndCourseStudentRosterBox() {
		initWidget(uiBinder.createAndBindUi(this));
		classAndSubject.addValueChangeHandler(this);
		classAndSubject.initData();
		errorBox = new SimpleDialog("An error occured - Try browser refresh to fix");
		studentList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
	}
	
    private void populateStudentBox(HashMap<String, String> result)
    {
        studentList.clear();
        Iterator<Entry<String, String>> studVals = result.entrySet().iterator();
        while(studVals.hasNext())
        {
            Entry<String, String> val = studVals.next();
            studentList.addItem(val.getKey(), val.getValue());
        }
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
        String selectedCourse = getSelectedCourse();
        studentList.clear();
        if(cachedRosterResults.containsKey(selectedCourse))
            populateStudentBox(cachedRosterResults.get(selectedCourse));
        else
        {
            lastSelectedCourse = selectedCourse;
            classAndSubject.getClassService().getCourseRosterMap(selectedCourse, rosterCallBack);
        }		
	}
	
    public void setDisplayHeight(int visibleItemCount)
    {
    	studentList.setMultipleSelect(true);
    	studentList.setVisibleItemCount(visibleItemCount);
    }	

    public String getSelectedStudent()
    {
        return studentList.getValue(studentList.getSelectedIndex());
    }
    
    public HashMap<String, String> getAllSelectedStudents()
    {
    	HashMap<String, String> result = new HashMap<String, String>();
    	for(int i = 0; i < studentList.getItemCount(); i++)
    	{
    		if(studentList.isItemSelected(i))
    			result.put(studentList.getValue(i), studentList.getItemText(i));
    	}
    	return result;
    }

    public String getSelectedClass()
    {
        return classAndSubject.getClassValue();
    }
    
    public String getSelectedClassName()
    {
    	return classAndSubject.getClassName();
    }
    
    public String getSelectedCourseName()
    {
    	return classAndSubject.getCourseName();
    }
    
    public String getSelectedCourse()
    {
        return classAndSubject.getCourseValue();
    }    

    public ClassRosterServiceAsync getClassService()
    {
        return classAndSubject.getClassService();
    }
    
    public void removeCacheEntry(String courseKey)
    {
    	cachedRosterResults.remove(courseKey);
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		//loading considered completed when LevelAndCourse box has fired at least 1 event
		//that triggers student roster being fetched.
		return loadingCompleted;
	}    
}
