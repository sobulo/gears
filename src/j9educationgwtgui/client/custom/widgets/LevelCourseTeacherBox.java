package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LevelCourseTeacherBox extends Composite implements ValueChangeHandler<String>, DoneLoadingWidget{

    @UiField LevelAndCourseBox subjectListBox;
    @UiField TextBox teacherDisplay;
    private SimpleDialog errorBox;
    private static boolean loadingCompleted = false;
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> courseCallback =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	loadingCompleted = true;
        	WidgetHelper.checkForLoginError(caught);
            errorBox.show(caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String, String> result)
        {
        	mappingCache.putAll(result);
        	String selectedCourse = subjectListBox.getCourseValue();
        	if(mappingCache.containsKey(selectedCourse))
        		teacherDisplay.setValue(mappingCache.get(selectedCourse));
        	else
        		errorBox.show("Try refreshing browser. Unable to load teacher information for: " + subjectListBox.getCourseName());
        	loadingCompleted = true;
        }
    };    
    
    private UserManagerAsync userService = GWT.create(UserManager.class);

	private static HashMap<String, String> mappingCache = new HashMap<String, String>();
	
	private static LevelCourseTeacherBoxUiBinder uiBinder = GWT
			.create(LevelCourseTeacherBoxUiBinder.class);

	interface LevelCourseTeacherBoxUiBinder extends
			UiBinder<Widget, LevelCourseTeacherBox> {
	}

	public LevelCourseTeacherBox() {
		initWidget(uiBinder.createAndBindUi(this));
		teacherDisplay.setEnabled(false);
		subjectListBox.addValueChangeHandler(this);
		subjectListBox.initData();
		errorBox = new SimpleDialog("Error Loading Teacher Information");
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) 
	{
		if(mappingCache.containsKey(event.getValue()))
			teacherDisplay.setValue(mappingCache.get(event.getValue()));
		else
			userService.getCourseTeacherMappings(subjectListBox.getClassValue(), courseCallback);
	}
	
	public String getSelectedCourse()
	{
		return subjectListBox.getCourseValue();
	}
	
	public String getSelectedCourseName()
	{
		return subjectListBox.getCourseName() + "/" + subjectListBox.getClassName();
 	}
	
	public String getDisplayedTeacher()
	{
		return teacherDisplay.getValue();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		//loading considered completed when we've fetched at least 1 set of teacher-course mappings
		return loadingCompleted;
	}

}
