/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class YesNoDialog extends SimpleDialog{

	private Button yes, no;
	private String padding;
	private HandlerRegistration yesHandlerReg;
	
	/**
	 * @param title
	 */
	public YesNoDialog(String title, String yesLabel, String noLabel, String padding) 
	{
		super(title);
		this.padding = padding;
		yes.setText(yesLabel);
		no.setText(noLabel);
	}
	
	/**
	 * @param title
	 */
	public YesNoDialog(String title) {
		this(title, "  Yes  ", "  No  ", "     ");
	}

	@Override
	protected Widget getButtonPanel()
	{
		HorizontalPanel panel = new HorizontalPanel();
		
		yes = new Button();
		no = new Button();
		no.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
			}
		});
		
		GWT.log("Button: " + yes);
		panel.add(yes);
		panel.add(new Label(padding));
		panel.add(no);
		return panel;
	}
	
	public void setClickHandler(ClickHandler handler)
	{
		if(yesHandlerReg != null)
			yesHandlerReg.removeHandler();
		
		yesHandlerReg = yes.addClickHandler(handler);
	}
	
	public void hide()
	{
		alertBox.hide();
	}
}
