/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class GuardianStudentPicker extends Composite implements ChangeHandler, DoneLoadingWidget, HasValueChangeHandlers<String>
{
    @UiField ListBox studentList;
    @UiField ListBox classList;
    @UiField Label classListLabel;
    private SimpleDialog alertBox;

    private UserManagerAsync userService =
            GWT.create(UserManager.class);
    
    private boolean doneLoading = false;
    private String lastSelectedStudent;
    private static HashMap<String, HashMap<String, String>> cachedClasses = new HashMap<String, HashMap<String, String>>();

    final AsyncCallback<HashMap<String, String>> childrenCallback =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
            doneLoading = true;
            WidgetHelper.checkForLoginError(caught);
        	studentList.clear();
            alertBox.show("Error loading: " + caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
            studentList.clear();
            Iterator<Entry<String, String>> childrenVals = result.entrySet().iterator();
            while(childrenVals.hasNext())
            {
                Entry<String, String> val = childrenVals.next();
                studentList.addItem(val.getValue(), val.getKey());
            }
            onChange(null);
            if(classList.isVisible())
            {
            	lastSelectedStudent = getSelectedStudent();
                userService.getStudentLevels(lastSelectedStudent, classCallback);
            }
            else
            	doneLoading = true;
        }
    };

    final AsyncCallback<HashMap<String, String>> classCallback =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	doneLoading = true;
        	WidgetHelper.checkForLoginError(caught);
        	classList.clear();
            alertBox.show("Error loading classes: " + caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	populateClassList(result);
        	cachedClasses.put(lastSelectedStudent, result);
        	doneLoading = true;
        }
    };
    
    public void populateClassList(HashMap<String, String> result)
    {
    	classList.clear();
        Iterator<Entry<String, String>> classVals = result.entrySet().iterator();
        while(classVals.hasNext())
        {
            Entry<String, String> val = classVals.next();
            classList.addItem(val.getValue(), val.getKey());
        }    	
    }

    private static GuardianStudentPickerUiBinder uiBinder = GWT.create(GuardianStudentPickerUiBinder.class);

    interface GuardianStudentPickerUiBinder extends UiBinder<Widget, GuardianStudentPicker> {
    }

    public GuardianStudentPicker() {
        initWidget(uiBinder.createAndBindUi(this));
        classList.setVisible(false);
        classListLabel.setVisible(false);
        classList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        userService.getGuardianChildren(childrenCallback);
    }

    public GuardianStudentPicker(boolean setupClassList)
    {
    	initWidget(uiBinder.createAndBindUi(this));
    	studentList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        if(setupClassList)
        {
        	classList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
            studentList.addChangeHandler(this);
        }
        else
        {
        	classList.setVisible(false);
        	classListLabel.setVisible(false);
        }
        userService.getGuardianChildren(childrenCallback);
    }

    public String getSelectedStudent()
    {
        if(studentList.getSelectedIndex() < 0)
            return null;

        return studentList.getValue(studentList.getSelectedIndex());
    }

    public String getSelectedClass()
    {
        return classList.getValue(classList.getSelectedIndex());
    }

    @Override
    public void onChange(ChangeEvent event) {
    	lastSelectedStudent = getSelectedStudent();
    	ValueChangeEvent.fire(this, lastSelectedStudent);
    	
    	if(classList.isVisible())
    	{
	    	if(cachedClasses.containsKey(lastSelectedStudent))
	    		populateClassList(cachedClasses.get(lastSelectedStudent));
	    	else
	    		userService.getStudentLevels(lastSelectedStudent, classCallback);
    	}
    }

    public UserManagerAsync getUserService()
    {
        return userService;
    }

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return doneLoading;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		studentList.addChangeHandler(this);
		return addHandler(handler, ValueChangeEvent.getType());
	}
}