package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.shared.PanelServiceLoginRoles;


public class GuardianSuggestBox extends UserSuggestBox
{
	
	public GuardianSuggestBox()
	{
		super();
		label.setText("Guardian");
	}
	
	@Override
	public void populateSuggestBox()
	{
		getUserService().getAllGuardianIDs(true, getPopulateCallBack());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.UserSuggestBox#getRole()
	 */
	@Override
	public PanelServiceLoginRoles getRole() {
		return PanelServiceLoginRoles.ROLE_GUARDIAN;
	}	
}
