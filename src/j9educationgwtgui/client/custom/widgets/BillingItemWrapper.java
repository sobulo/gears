/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BillingItemWrapper extends Composite implements Text2ListBoxInput{
	
	@UiField TextBox name;
	@UiField TextBox comment;
	@UiField TextBox amount;
	
	private final static String SEPERATOR = "|";
	public final static String SEPERATOR_REGEX = "\\" + SEPERATOR;
	public final static int NAME_IDX = 0;
	public final static int AMOUNT_IDX = 1;
	public final static int COMMENT_IDX = 2;

	private static BillingItemWrapperUiBinder uiBinder = GWT
			.create(BillingItemWrapperUiBinder.class);

	interface BillingItemWrapperUiBinder extends
			UiBinder<Widget, BillingItemWrapper> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public BillingItemWrapper() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#getCustomText()
	 */
	@Override
	public String getCustomText() {
		// TODO Auto-generated method stub
		return new StringBuilder(name.getText().trim()).append(SEPERATOR).
			append(amount.getText().trim()).append(SEPERATOR).
			append(comment.getText().trim()).toString();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#getInputWidget()
	 */
	@Override
	public Widget getInputWidget() {
		return this;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#disableInput()
	 */
	@Override
	public void disableInput() {
		setInputState(false);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#enableInput()
	 */
	@Override
	public void enableInput() {
		setInputState(true);
	}
	
	private void setInputState(boolean enabled)
	{
		name.setEnabled(enabled);
		comment.setEnabled(enabled);
		amount.setEnabled(enabled);		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#reset()
	 */
	@Override
	public void reset() {
		name.setText("");
		comment.setText("");
		amount.setText("");
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.Text2ListBoxInput#getErrorMessage()
	 */
	@Override
	public String getErrorMessage() {
		try
		{
			double price = Double.parseDouble(amount.getText().trim());
			if(price <= 0)
				return "Item price must be greater than 0";
			if(name.getText().trim().length() == 0)
				return "Item name must be entered";
			if(name.getText().contains(SEPERATOR))
				return "Name contains illegal character, please use only alphanumeric characters";
			if(comment.getText().contains(SEPERATOR))
				return "Description contains illegal character, please use only alphanumeric characters";
		}
		catch(NumberFormatException ex)
		{
			return "Enter a valid number for price";
		}
		return "";
	}

}
