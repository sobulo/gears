/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.ClassRosterService;
import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class LevelBox extends Composite implements HasValueChangeHandlers<String>, DoneLoadingWidget, DropDownSelector
{

    @UiField SimplePanel menuSlot;
    @UiField Label classListLabel;
   
    private static HashMap<String, String> cachedDisplayNames;
    private static HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>>
    										cachedClassInfo;
    private String currentClass;
    private ClassRosterServiceAsync classService = GWT.create(ClassRosterService.class);

    private static LevelBoxUiBinder uiBinder = GWT.create(LevelBoxUiBinder.class);
    
    private MenuItem classMenu;
    private static boolean loadingCompleted = false;
    final String PART_TIME_STR = "PART-TIME";
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> > classCallback =
            new AsyncCallback<HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> >() {
        @Override
        public void onFailure(Throwable caught) {
        	loadingCompleted = true;
        	WidgetHelper.checkForLoginError(caught);
            classListLabel.setText("Error Loading: " + caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> result) 
        {
            GWT.log("Inside class callback");
            cachedClassInfo = result;
            cachedDisplayNames = new HashMap<String, String>();
            setupClassMenu(result);
            loadingCompleted = true;
        }
    };

    interface LevelBoxUiBinder extends UiBinder<Widget, LevelBox> {
    }

    public LevelBox() {
        initWidget(uiBinder.createAndBindUi(this));
        //classList.addChangeHandler(this);
        menuSlot.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
    }
    
    public void initData()
    {
        //fetch class list from server if an earlier instance hasn't done this already
        if(cachedClassInfo == null)
        	classService.getClassKeys(classCallback);
        else
        	setupClassMenu(cachedClassInfo);    	
    }
    
    public void setupClassMenu(HashMap<String,HashMap<String,HashMap<String,HashMap<String,String>>>> result)
    {	    
    	//Iterator<Entry<String, HashMap<String,HashMap<String,HashMap<String,String>>>>> 
		//yearVals = result.entrySet().iterator();
		//String displayName;
    	String[] yearKeys = new String[result.keySet().size()];
    	yearKeys = result.keySet().toArray(yearKeys);
    	Arrays.sort(yearKeys);
    	
		MenuBar yearBar = new MenuBar();
		classMenu = new MenuItem("Click to select class", yearBar);
		//classMenu.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
	    MenuBar menu = new MenuBar();
	    //menu.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
	    menu.addItem(classMenu);
	    menuSlot.add(menu);
	    Set<String> tempKeySet;
	    for(int y=0; y < yearKeys.length; y++)
	    {
	        //Entry<String, HashMap<String,HashMap<String,HashMap<String,String>>>> 
	        //	yearValsEntry = yearVals.next();
	        //Iterator<Entry<String,HashMap<String,HashMap<String,String>>>> termVals = yearValsEntry.getValue().entrySet().iterator();
	    	String year = yearKeys[y];
	    	tempKeySet = result.get(year).keySet();
	        String[] termKeys = new String[tempKeySet.size()];
	        termKeys = tempKeySet.toArray(termKeys);
	        Arrays.sort(termKeys);
	        
	        //add year to menu
	        MenuBar termBar = new MenuBar(true);
	        yearBar.addItem(year.toUpperCase(), termBar);
	        if( y < yearKeys.length - 1)
	        	yearBar.addSeparator();
	        for(int t = 0; t < termKeys.length; t++)
	        {
	            //Entry<String, HashMap<String,HashMap<String,String>>> 
	        	//	termValsEntry = termVals.next();
	            //String term = termValsEntry.getKey().toUpperCase();
	            //Iterator<Entry<String,HashMap<String,String>>> levelVals = termValsEntry.getValue().entrySet().iterator();
	            String term = termKeys[t];
	            tempKeySet = result.get(year).get(term).keySet();
	            String[] levelKeys = new String[tempKeySet.size()];
	            levelKeys = tempKeySet.toArray(levelKeys);
	            Arrays.sort(levelKeys);
	            
	            //add term to menu
	            MenuBar levelBar = new MenuBar(true);
	            termBar.addItem(term.toUpperCase(), levelBar);
	            if(t < termKeys.length - 1)
	            	termBar.addSeparator();
	            for(int l = 0; l < levelKeys.length; l++)
	            {
	                //Entry<String,HashMap<String,String>> levelValsEntry = levelVals.next();
	                //String level = levelValsEntry.getKey().toUpperCase();
	                //Iterator<Entry<String,String>> subVals = levelValsEntry.getValue().entrySet().iterator();
	                String level = levelKeys[l];
	                tempKeySet = result.get(year).get(term).get(level).keySet();
	                String[] subKeys = new String[tempKeySet.size()];
	                subKeys = tempKeySet.toArray(subKeys);
	                Arrays.sort(subKeys);
	                
	                //add level to menu
	                MenuBar subLevelBar = new MenuBar(true);
	                boolean asHTML = false;
	                if(level.toUpperCase().contains(PART_TIME_STR))
	                {
	                	asHTML = true;
	                	levelBar.addItem("<font color='brown'>" + level.toUpperCase() + "</font>", true, subLevelBar);
	                }
	                else
	                	levelBar.addItem(level.toUpperCase(), subLevelBar);
	                if(l < levelKeys.length - 1)
	                	levelBar.addSeparator();
	                
	                for(int s = 0; s < subKeys.length; s++)
	                {
	                    //Entry<String,String> resultEntry = subVals.next();
	                    //String subLevel = resultEntry.getKey();
	                    //final String classKey = resultEntry.getValue();
	                	String sublevel = subKeys[s];
	                	final String classKey = (String) result.get(year).get(term).get(level).get(sublevel);
	                    
	                    final String displayName = level.toUpperCase() + "-" + 
	                    		sublevel.toUpperCase() + " ("+ year.toUpperCase() + 
	                    		"-" + term.toUpperCase() + ")";
	                    
	                    cachedDisplayNames.put(classKey, displayName);
	                    
	                    String subName = sublevel.toUpperCase();
	                    if(asHTML)
	                    	subName = "<font color='brown'>" + subName + "</font>";
	                    
	                    subLevelBar.addItem(subName, asHTML, new Command() {							
							@Override
							public void execute()
							{
								fireChangeEvent(classKey);
							}
						});
	                    
	                    if(s < subKeys.length - 1)
	                    	subLevelBar.addSeparator();
	                }                    	
	            }                	
	        }
	    }
	    
	    if(cachedDisplayNames.size() > 0)
	    {
	    	String classKey = cachedDisplayNames.keySet().iterator().next();
	    	GWT.log("SELECTED CLASS A: " + classKey);
	    	fireChangeEvent(classKey);
	    }
    }
    
    public ClassRosterServiceAsync getClassService()
    {
        return classService;
    }
    
    public void fireChangeEvent(String classKey)
    {
    	currentClass = classKey;
    	String displayName = cachedDisplayNames.get(classKey);
    	if(displayName.contains(PART_TIME_STR))
    		classMenu.setHTML("<font color='brown'>" + displayName + "</font>");
    	else
    		classMenu.setText(displayName);
    	//classMenu.getParentMenu().focus();
    	ValueChangeEvent.fire(this, classKey);
    }

    public String getSelectedClass()
    {
    	GWT.log("SELECTED CLASS: " + currentClass);
    	return currentClass;
    }
    
    public String getClassLabel() {
        return classListLabel.getText();
    }

    public void setClassLabel(String classLabel) {
        this.classListLabel.setText(classLabel);
    }    
    
    public String getSelectedClassName()
    {
    	return cachedDisplayNames.get(currentClass);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		/* loading is considered completed once async call for fetching school levels/classes returns */
		return loadingCompleted;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DropDownSelector#getSelectedDropDownValue()
	 */
	@Override
	public String getSelectedDropDownValue() {
		return getSelectedClass();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DropDownSelector#getSelectedDropDownDisplayName()
	 */
	@Override
	public String getSelectedDropDownDisplayName() {
		return getSelectedClassName();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DropDownSelector#getWidgetDisplay()
	 */
	@Override
	public Widget getWidgetDisplay() {
		return this;
	}
}