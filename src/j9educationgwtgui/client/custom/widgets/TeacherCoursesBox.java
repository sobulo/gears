package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class TeacherCoursesBox extends Composite implements HasValueChangeHandlers<String>, ChangeHandler, DoneLoadingWidget
{

	@UiField ListBox courseList;
    private SimpleDialog errorBox;
    private boolean loadingCompleted;
    
    private UserManagerAsync userService = GWT.create(UserManager.class);    
    
	final AsyncCallback<HashMap<String, String>> courseCallback =
        new AsyncCallback<HashMap<String, String>>() 
    {
	    @Override
	    public void onFailure(Throwable caught) {
	    	loadingCompleted = true;
	    	WidgetHelper.checkForLoginError(caught);
	        errorBox.show(caught.getMessage());
	    }
	
	    @Override
	    public void onSuccess(HashMap<String, String> result) {
	        Iterator<Entry<String, String>> classVals = result.entrySet().iterator();
	        while(classVals.hasNext())
	        {
	            Entry<String, String> val = classVals.next();
	            courseList.addItem(val.getValue(), val.getKey());
	        }
	        fireInitialEvent();
	        loadingCompleted = true;
	    }
    };	
	
	private static TeacherCoursesBoxUiBinder uiBinder = GWT
			.create(TeacherCoursesBoxUiBinder.class);

	interface TeacherCoursesBoxUiBinder extends
			UiBinder<Widget, TeacherCoursesBox> {
	}

	public TeacherCoursesBox() {
		initWidget(uiBinder.createAndBindUi(this));
		errorBox = new SimpleDialog("Error Loading Teacher Subjects");
		courseList.addChangeHandler(this);
		courseList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
	}
	
	public void initData()
	{
		userService.getTeacherCourses(courseCallback);
	}
	
	private void fireInitialEvent()
	{
		if(courseList.getItemCount() > 0)
		{
			courseList.setSelectedIndex(0);
			ValueChangeEvent.fire(this, getSelectedCourse());
		}		
	}
	
	public UserManagerAsync getUserService()
	{
		return userService;
	}
	
	public String getSelectedCourse()
	{
		GWT.log("Number of courses available: " + courseList.getItemCount());
		int selectIdx = courseList.getSelectedIndex();
		if(selectIdx < 0)
			return "";
		return courseList.getValue(selectIdx);
	}
	
	public String getSelectedCourseName()
	{
		int selectIdx = courseList.getSelectedIndex();
		if(selectIdx < 0)
			return "";
		return courseList.getItemText(selectIdx);
	}	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		ValueChangeEvent.fire(this, getSelectedCourse());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		// TODO Auto-generated method stub
		return loadingCompleted;
	}
}
