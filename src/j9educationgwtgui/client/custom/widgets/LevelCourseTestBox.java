/**
 * 
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.GUIConstants;
import j9educationgwtgui.client.ScoreManager;
import j9educationgwtgui.client.ScoreManagerAsync;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LevelCourseTestBox extends Composite implements ValueChangeHandler<String>, 
	HasValueChangeHandlers<String>, ChangeHandler, DoneLoadingWidget
{

	@UiField
	LevelAndCourseBox classAndSubject;
	
	@UiField
	ListBox tests;
	
	@UiField
	Label testsLabel;
	
	@UiField
	Label status;
	
    private ScoreManagerAsync testScoreService =
        GWT.create(ScoreManager.class);	
	
	private static LevelCourseTestBoxUiBinder uiBinder = GWT
			.create(LevelCourseTestBoxUiBinder.class);
	
	private SimpleDialog errorBox;
	private static boolean loadingCompleted = false;

	interface LevelCourseTestBoxUiBinder extends
			UiBinder<Widget, LevelCourseTestBox> {
	}
	
	private static HashMap<String, HashMap<String, String>> cachedTests = new HashMap<String, HashMap<String, String>>();

    final AsyncCallback<HashMap<String, String>> testNamesCallback =
        new AsyncCallback<HashMap<String, String>>() {

            @Override
            public void onFailure(Throwable caught) {
            	loadingCompleted = true;
            	WidgetHelper.checkForLoginError(caught);
                errorBox.show("Try reloading browser. Request failed: " + 
                		caught.getMessage());
                tests.setVisible(false);
                WidgetHelper.setErrorStatus(status, " Loading failed", false);
            }

            @Override
            public void onSuccess(HashMap<String, String> result) {
            	GWT.log("Made it in here yo!!!!!!!!");
            	HashMap<String, String> upperCaseResult = new HashMap<String, String>(result.size());
            	for(String key : result.keySet())
            		upperCaseResult.put(key, result.get(key).toUpperCase());
            	populateTests(upperCaseResult);
            	cachedTests.put(classAndSubject.getClassValue(), upperCaseResult);
            	GWT.log("Made it out of here yo!!!!!!");
            	loadingCompleted = true;
            	WidgetHelper.setNormalStatus(status, "");
            }
        };
        
	public LevelCourseTestBox() {
		initWidget(uiBinder.createAndBindUi(this));
		classAndSubject.addValueChangeHandler(this);
		tests.addChangeHandler(this);
		errorBox = new SimpleDialog("An error occured");
        tests.setWidth(GUIConstants.DEFAULT_LB_WIDTH);	
	}
	
	public void initData()
	{
		classAndSubject.initData();
	}
	
	private void populateTests(HashMap<String, String> result)
	{
        tests.clear();
        Iterator<Map.Entry<String,String>> resultItr =
                result.entrySet().iterator();
        while(resultItr.hasNext())
        {
            Map.Entry<String, String> item = resultItr.next();
            tests.addItem(item.getValue(), item.getKey());
        }
        
		if(tests.getItemCount() > 0)
		{
			tests.setSelectedIndex(0);
			ValueChangeEvent.fire(this, getSelectedTest());
		}
	}
	
    private void fetchTestNames(String courseKeyStr) {
        testScoreService.getTestKeys(courseKeyStr, testNamesCallback);
    }
    
    public ScoreManagerAsync getScoreManager()
    {
    	return testScoreService;
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String courseKey = event.getValue();
		tests.clear();
		WidgetHelper.setNormalStatus(status, " Loading ...");
		if(cachedTests.containsKey(courseKey))
		{
			GWT.log("test cache hit!!");
			populateTests(cachedTests.get(courseKey));
		}
		else
			fetchTestNames(event.getValue());
	}
	
	public String getSelectedTest()
	{
		if(tests.getSelectedIndex() < 0)
			return "";
		return tests.getValue(tests.getSelectedIndex());
	}
	
	public String getSelectedTestName()
	{
		if(tests.getSelectedIndex() < 0)
			return "";
		return tests.getItemText(tests.getSelectedIndex());
	}
	
	public String getClassName()
	{
		return classAndSubject.getClassName();
	}
	
	public String getCourseName()
	{
		return classAndSubject.getCourseName();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		ValueChangeEvent.fire(this, getSelectedTest());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		return loadingCompleted;
	}	
}
