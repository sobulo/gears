package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.UserManager;
import j9educationgwtgui.client.UserManagerAsync;
import j9educationgwtgui.shared.PanelServiceLoginRoles;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.Widget;

public abstract class UserSuggestBox extends Composite 
	implements HasValueChangeHandlers<String>, SelectionHandler<Suggestion>, DoneLoadingWidget 
{
	
	//abstract methods
	protected abstract void populateSuggestBox();
	public abstract PanelServiceLoginRoles getRole();

	@UiField
	SuggestBox userBox;
	
	@UiField
	Label label;
	
	private final RegExp ID_MATCHER = RegExp.compile("\\{\\s\\S+\\s\\}");
	
	private static HashMap<PanelServiceLoginRoles, HashMap<String, String>> cachedSuggestions = 
		new HashMap<PanelServiceLoginRoles, HashMap<String, String>>();
	
	private HashMap<String, String> hackedSuggestions;
	private SimpleDialog alertBox;
	private String oldVal = "";		
	private static boolean loadingCompleted = false;
	
    private UserManagerAsync userService =
        GWT.create(UserManager.class);
    
	private final AsyncCallback<HashMap<String, String>> listCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			loadingCompleted = true;
			WidgetHelper.checkForLoginError(caught);
			alertBox.show("Request failed: " + caught.getMessage());
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			GWT.log("Retrieved: " + result.size() + " from server");
			setupSuggestBox(result);
			cachedSuggestions.put(getCacheKey(), result);
			loadingCompleted = true;
		}
	};

	private static UserSuggestBoxUiBinder uiBinder = GWT
			.create(UserSuggestBoxUiBinder.class);

	interface UserSuggestBoxUiBinder extends UiBinder<Widget, UserSuggestBox> {
	}

	public UserSuggestBox() {
		initWidget(uiBinder.createAndBindUi(this));
		hackedSuggestions = new HashMap<String, String>();
		userBox.addSelectionHandler(this);

		// setup alert box
		alertBox = new SimpleDialog("An error occured");
		initSuggestBox();
	}
	
	public AsyncCallback<HashMap<String, String>> getPopulateCallBack()
	{
		return listCallback;
	}
	
	private PanelServiceLoginRoles getCacheKey()
	{
		return getRole();
	}
	
	private void initSuggestBox()
	{
		PanelServiceLoginRoles cacheKey = getCacheKey();
		if(cachedSuggestions.containsKey(cacheKey))
		{
			GWT.log("USER BOX HIT");
			setupSuggestBox(cachedSuggestions.get(cacheKey));
		}
		else
			populateSuggestBox();
	}
	
	public String getUserID(String displayName)
	{
		MatchResult result = ID_MATCHER.exec(displayName);
		if(result.getGroupCount() == 1)
		{
			String matched = result.getGroup(0);
			return matched.substring(2, matched.length()-2);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.logical.shared.SelectionHandler#onSelection(com.
	 * google.gwt.event.logical.shared.SelectionEvent)
	 */
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		String oldValue = oldVal;
		GWT.log("OLD VAL: " + oldValue);
		
		// TODO Auto-generated method stub
		Suggestion suggest = event.getSelectedItem();
		GWT.log("Suggest Display: " + suggest.getDisplayString());
		GWT.log("Suggest Replace: " + suggest.getReplacementString());
		String newValue = suggest.getReplacementString();
		newValue = getUserID(newValue);
		userBox.setValue(newValue);		
		if(newValue.length() > 0)
		{
			ValueChangeEvent.fireIfNotEqual(this, oldValue, newValue);
			oldVal = newValue;
		}
		GWT.log("NEW VAL: " + newValue);
	}


	private void setupSuggestBox(HashMap<String, String> vals) {
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) userBox
				.getSuggestOracle();
		oracle.clear();
		hackedSuggestions.clear();
		Iterator<Map.Entry<String, String>> valItr = vals.entrySet().iterator();
		while (valItr.hasNext()) {
			Map.Entry<String, String> item = valItr.next();
			GWT.log("inputs: " + item.getValue());
			String[] comps = item.getValue().split(",");
			String displayStr = comps[0] + " { " + comps[1] + " }";
			oracle.add(displayStr);
			hackedSuggestions.put(item.getKey(), displayStr);
		}
	}

	public String getSelectedUser() {
		String key = userBox.getValue();
		if (hackedSuggestions.containsKey(key))
			return key;
		else
			return null;
	}
	
	public String getSelectedUserDisplay()
	{
		String key = userBox.getValue();
		GWT.log("Key is: [" + key + "]");
		if (hackedSuggestions.containsKey(key))
			return hackedSuggestions.get(key);
		else
			return null;
	}
	
	public UserManagerAsync getUserService()
	{
		return userService;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	public void setEnabled(boolean enabled)
	{
		userBox.getTextBox().setEnabled(enabled);
		if(!enabled)
			userBox.setValue(null);
	}
	
	@Override
	public boolean doneLoading()
	{
		return loadingCompleted;
	}
	
	public void clear()
	{
		oldVal = "";
		userBox.setValue("");
	}	
}
