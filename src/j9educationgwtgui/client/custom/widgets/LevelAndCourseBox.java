/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client.custom.widgets;

import j9educationgwtgui.client.ClassRosterServiceAsync;
import j9educationgwtgui.client.GUIConstants;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class LevelAndCourseBox extends Composite implements ValueChangeHandler<String>, 
	ChangeHandler, HasValueChangeHandlers<String>, DoneLoadingWidget
{

    @UiField LevelBox classListBox;
    @UiField ListBox courseList;
    @UiField Label courseLabel;
    
    private ClassRosterServiceAsync classService;   
    private String lastSelectedClass = "";
    private static boolean loadingCompleted = false;
    private SimpleDialog errorBox;
    
    private static HashMap<String, HashMap<String, String>> courseListCache;
    
    static
    {
    	courseListCache = new HashMap<String, HashMap<String,String>>();
    }

    private static LevelAndCourseBoxUiBinder uiBinder = GWT.create(LevelAndCourseBoxUiBinder.class);

    interface LevelAndCourseBoxUiBinder extends UiBinder<Widget, LevelAndCourseBox> {
    }

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> courseCallback =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	loadingCompleted = true;
        	WidgetHelper.checkForLoginError(caught);
            errorBox.show(caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String, String> result)
        {
        	HashMap<String, String> uppercaseResult = new HashMap<String, String>(result.size());
        	for(String key : result.keySet())
        		uppercaseResult.put(key.toUpperCase(), result.get(key));
        	courseListCache.put(lastSelectedClass, uppercaseResult);
        	populateCourseList(uppercaseResult);
        	loadingCompleted = true;
        }
    };
    
    public void populateCourseList(HashMap<String, String> result)
    {
    	courseList.clear();
        Iterator<Entry<String, String>> courseVals =
                result.entrySet().iterator();

        while(courseVals.hasNext())
        {
            Entry<String, String> val = courseVals.next();
            courseList.addItem(val.getKey(), val.getValue());            
        }
        
        if(courseList.getItemCount() > 0)
        {
        	GWT.log("Fired first event for: " + getCourseName());
            courseList.setSelectedIndex(0);
            ValueChangeEvent.fire(this, getCourseValue());
        }        
    }
    
    public void fetchCourseList(String classKey)
    {
    	if(courseListCache.containsKey(classKey))
    	{
    		GWT.log("Cache hit, using cahce");
    		populateCourseList(courseListCache.get(classKey));
    	}
    	else
    	{
    		GWT.log("Cache miss, going to server");
    		lastSelectedClass = classKey;
            classService.getCourseKeys(classKey,
                    courseCallback);
    	}
    }    
    
    @Override
    public void onChange(ChangeEvent event)
    {
    	ValueChangeEvent.fire(this, getCourseValue());
    	GWT.log("Fired change event for: " + getCourseName());
    }

    public LevelAndCourseBox()
    {
        initWidget(uiBinder.createAndBindUi(this));
        classService = classListBox.getClassService(); 
        classListBox.addValueChangeHandler(this);
        courseList.addChangeHandler(this);
        courseList.setWidth(GUIConstants.DEFAULT_LB_WIDTH);
        //courseList.addStyleName(GUIConstants.STYLE_CAPITALIZE);
    }
    
    public void initData()
    {
    	classListBox.initData();
    }

    public String getClassValue()
    {
        return classListBox.getSelectedClass();
    }

    public String getCourseValue()
    {
        if(courseList.getSelectedIndex() < 0)
            return "";
        return courseList.getValue(courseList.getSelectedIndex());
    }

    public String getClassName()
    {
    	return classListBox.getSelectedClassName();
    }

    public String getCourseName()
    {
        if(courseList.getSelectedIndex() < 0)
            return "";
        return courseList.getItemText(courseList.getSelectedIndex());
    }

    public String getClassLabel() {
        return classListBox.getClassLabel();
    }

    public void setClassLabel(String classLabel) {
        classListBox.setClassLabel(classLabel);
    }

    public String getCourseLabel() {
        return courseLabel.getText();
    }

    public void setCourseLabel(String courseLabel) {
        this.courseLabel.setText(courseLabel);
    }
    
    public void removeCachEntry(String classKey)
    {
    	courseListCache.remove(classKey);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		courseList.clear();
		fetchCourseList(event.getValue());
	}
	
	public ClassRosterServiceAsync getClassService()
	{
		return classService;
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	public HandlerRegistration addValueChangeHandlerForTopLevel(ValueChangeHandler<String> handler)
	{
		return classListBox.addValueChangeHandler(handler);
	}
	
	public boolean isTopLevelEvent(ValueChangeEvent<String> event)
	{
		return event.getSource().equals(classListBox);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.DoneLoadingWidget#doneLoading()
	 */
	@Override
	public boolean doneLoading() {
		/* when levelbox loads, it fires an event, loading is considered completed the
		 * once this widget has handled that event at least once, i.e. fetched the courses
		 * for the default displayed level.
		 */
		return loadingCompleted;
	}
}