/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.TableMessage;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author Administrator
 */
public interface ScoreManagerAsync {

    public void getSimpleTestNames(String courseKeyStr, AsyncCallback<String[]> asyncCallback);

    public void addTest(String courseKeyStr, String testName, Double maxScore, Double weight, AsyncCallback<String> asyncCallback);

    public void getCourseScoresTable(String courseKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getTestScoresTable(String testKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getTestKeys(String courseKeyStr, AsyncCallback<HashMap<String, String>> asyncCallback);

    public void getStudentScoreSummary(String levelKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getStudentScoreSummary(String loginName, String levelKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getStudentScoreDetails(String levelKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getStudentScoreDetails(String loginName, String levelKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getStudentScoreSummaryWithKey(String studentKeyStr, String levelKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

    public void getStudentScoreDetailsWithKey(String studentKeyStr, String levelKeyStr, AsyncCallback<ArrayList<TableMessage>> asyncCallback);

	public void saveTestScores(String testKeyStr, HashMap<String, Double> testScores,
			AsyncCallback<Void> callback);

	void getCourseTestsTable(String courseKeyStr,
			AsyncCallback<ArrayList<TableMessage>> callback);

	void deleteTest(String testKeyStr, AsyncCallback<Void> callback);

	void totalTestWeight(String courseKeyStr, AsyncCallback<Double> callback);

	void getClassScores(String levelKeyStr, boolean cumulativeOnly,
			AsyncCallback<ArrayList<TableMessage>> callback);

	void getClassScoresWithGPA(String levelKeyStr,
			AsyncCallback<ArrayList<TableMessage>> callback);

	void getClassScoresWithCumulativeGPA(String[] levelStrs,
			AsyncCallback<ArrayList<TableMessage>[]> callback);

	void getCumulativeSummary(String[] levelStrs,
			AsyncCallback<ArrayList<TableMessage>[]> callback);

	void getCumulativeStudentGrades(
			AsyncCallback<ArrayList<TableMessage>[]> callback);

	void getCumulativeStudentGrades(String studentId, boolean isKey,
			AsyncCallback<ArrayList<TableMessage>[]> callback);
	
}
