/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationgwtgui.client;

import j9educationgwtgui.shared.GradeDescription;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.TableMessage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author Administrator
 */
public interface SchoolServiceAsync {

    public void createSchool(String name, String email, String accr, String addr, String url, 
    		String tel, boolean update, AsyncCallback<String> asyncCallback);

    public void updateAcademicYears(HashSet<String> years, AsyncCallback<Boolean> asyncCallback);

    public void updateAcademicTerms(HashSet<String> terms, AsyncCallback<Boolean> asyncCallback);

    public void updateClassNames(HashSet<String> classNames, AsyncCallback<Boolean> asyncCallback);

    public void updateSubClassNames(HashSet<String> subClassNames, AsyncCallback<Boolean> asyncCallback);

    public void getInitialOptions(AsyncCallback<HashMap<String, HashSet<String>>> asyncCallback);

    public void createClass(String className, String subClassName, String year, String term, AsyncCallback<String> asyncCallback);

    public void getSimpleClassNames(AsyncCallback<String[]> asyncCallback);

    public void updateDefaultSubjects(HashSet<String> subjectNames, AsyncCallback<Boolean> asyncCallback);

    public void getInitialOptions(java.lang.String[] optionNames, AsyncCallback<HashMap<String, HashSet<String>>> asyncCallback);

    public void login(String requestUri, AsyncCallback<LoginInfo> asyncCallback);

    public void updateDefaultGradingScheme(LinkedHashMap<Double, GradeDescription> scheme, AsyncCallback<Void> asyncCallback);

    public void getDefaultGradingSchemeDescription(AsyncCallback<String[]> asyncCallback);

	public void deleteLevel(String levelKeyStr, AsyncCallback<Void> callback);

	void getSchoolInfo(AsyncCallback<String[]> callback);

	void getNotice(String id, AsyncCallback<String[]> callback);

	void updateNotice(String id, String title, String content,
			AsyncCallback<Void> callback);

	void getApplicationParameter(String ID,
			AsyncCallback<List<TableMessage>> callback);

	void saveApplicationParameter(String id, HashMap<String, String> val,
			AsyncCallback<String> callback);
}
