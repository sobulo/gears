/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationgwtgui.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Main entry point.
 *
 * @author Administrator
 */
public class WebAppEntryPoint implements EntryPoint {
    /** 
     * Creates a new instance of WebAppEntryPoint
     */
    public WebAppEntryPoint() {
    }

    /** 
     * The entry point method, called automatically by loading a module
     * that declares an implementing class as an entry-point
     */
    public void onModuleLoad() {
        RootLayoutPanel.get().add(new WebAppPanel());
    }
}
