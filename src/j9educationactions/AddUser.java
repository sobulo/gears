/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetUser;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.LoginValidationException;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Segun Razaq Sobulo
 */
public class AddUser extends HttpServlet{
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	InetUser newUser;
    	try 
    	{
	    	LoginPortal.verifyRole(allowedRoles, req);
	    	
	        String fname = req.getParameter("fname");
	        String lname = req.getParameter("lname");
	        String email = req.getParameter("email");
	        String login = req.getParameter("login");
	        String password = req.getParameter("password");

            newUser = InetDAO4Creation.createUser(fname, lname, email,
                    null, login, password);
        }
    	catch(LoginValidationException ex)
    	{
    		throw new RuntimeException(ex.fillInStackTrace());
    	}
        catch(DuplicateEntitiesException ex)
        {
            throw new RuntimeException(ex.fillInStackTrace());
        }
        resp.setContentType("text/html");
        resp.getOutputStream().print("<html><body>");
        resp.getOutputStream().print("<p>Successfully added new user: <ul>");

        resp.getOutputStream().print( "<li>First Name: " + newUser.getFname());
        resp.getOutputStream().print( "<li>Last Name: " + newUser.getLname());
        resp.getOutputStream().print( "<li>Email: " + newUser.getEmail());
        resp.getOutputStream().print( "<li>Login: " + newUser.getKey().getName());
        resp.getOutputStream().println("</body></html>");
        resp.flushBuffer();
    }
}
