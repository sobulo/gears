/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.ExcelManager;


public class TestFileUpload extends HttpServlet {
  private static final Logger log =
      Logger.getLogger(TestFileUpload.class.getName());

  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
  	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
	try 
	{
    	LoginPortal.verifyRole(allowedRoles, req);
    }
	catch(LoginValidationException ex)
	{
		throw new RuntimeException(ex.fillInStackTrace());
	}
	  
    try {
      ServletFileUpload upload = new ServletFileUpload();
      res.setContentType("text/plain");

      FileItemIterator iterator = upload.getItemIterator(req);
      while (iterator.hasNext()) {
        FileItemStream item = iterator.next();
        InputStream stream = item.openStream();

        if (item.isFormField()) {
          log.warning("Got a form field: " + item.getFieldName());
          res.getOutputStream().println(item.getFieldName() + " ---- " + Streams.asString(stream));
        } else {
          log.warning("Got an uploaded file: " + item.getFieldName() +
                      ", name = " + item.getName());

          // You now have the filename (item.getName() and the
          // contents (which you can read from stream).  Here we just
          // print them back out to the servlet output stream, but you
          // will probably want to do something more interesting (for
          // example, wrap them in a Blob and commit them to the
          // datastore).

            res.getOutputStream().println("getData");
            String[] headers = {"FNAME", "EMAIL", "LNAME"};
            ColumnAccessor[] accessors = new ExcelManager.DefaultAccessor[headers.length];
            ExcelManager testManager = new ExcelManager(stream);
            for( int i = 0; i < headers.length; i++ )
            {
                accessors[i] = new ExcelManager.DefaultAccessor(headers[i], true);
                testManager.initializeAccessor(accessors[i]);
            }

            for (int i = 1; i < testManager.totalRows(); i++)
            {
                for( int j = 0; j < headers.length; j++ )
                {
                    String s = testManager.getData( i, accessors[j]);
                    res.getOutputStream().print( headers[j] + ":" + s + " " );
                }
                res.getOutputStream().println();
            }

          /*int len;
          byte[] buffer = new byte[8192];
          while ((len = stream.read(buffer, 0, buffer.length)) != -1) {
            res.getOutputStream().write(buffer, 0, len);
          }*/
        }
      }
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
  }
}
