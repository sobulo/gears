/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationentities.InetConstants;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetDAO4ComplexUpdates;
import j9educationentities.InetSingleTestScores;
import j9educationentities.InetStudent;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.GeneralFuncs;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkGradesUpload extends HttpServlet {
    private static final Logger log =
            Logger.getLogger(TestFileUpload.class.getName());

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException 
   {
    	//TODO, security risk for teachers, ideally should check test being uploaded belongs to a course being taught by the teacher
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_TEACHER};
        res.setContentType("text/html");
        ServletOutputStream out = res.getOutputStream(); 
        log.warning("Processing bulk grade upload file");
        
        try {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) 
		{
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
			"</b></body></html>");
			return;
		}
        
        Objectify ofy = ObjectifyService.begin();

        if (req.getParameter(PanelServiceConstants.LOAD_PARAM_NAME) != null) {
            Object scoresSessObj = req.getSession().getAttribute(InetConstants.USESS_GRADE_UPLD_DATA_NAME);
            Object testKeySessObj = req.getSession().getAttribute(InetConstants.USESS_GRADE_UPLD_TEST_KEY);
            if (scoresSessObj != null && testKeySessObj != null) {
                @SuppressWarnings("unchecked")
				Key<InetSingleTestScores> testKey = (Key<InetSingleTestScores>) testKeySessObj;
                @SuppressWarnings("unchecked")
				HashMap<Key<InetStudent>, GradeStruct> scores = (HashMap<Key<InetStudent>, GradeStruct>) scoresSessObj;

                try 
                {
                    //check that first name and last names matchup
                    Set<Key<InetStudent>> studKeys = scores.keySet();
                    Map<Key<InetStudent>, InetStudent> studentObjs = InetDAO4CommonReads.getEntities(studKeys, ofy, true);
                    HashMap<Key<InetStudent>, Double> scoreVals = new HashMap<Key<InetStudent>, Double>(scores.size());
                    ArrayList<String> nameMismatches = new ArrayList<String>();
                    for(Key<InetStudent> sk : studKeys)
                    {
                    	InetStudent stud = studentObjs.get(sk);
                    	GradeStruct sheetData = scores.get(sk);
                    	if(stud.getFname().equalsIgnoreCase(sheetData.fname) && 
                    			stud.getLname().equalsIgnoreCase(sheetData.lname))
                    		scoreVals.put(sk, sheetData.score);
                    	else
                    		nameMismatches.add("StudentID: " + sk.getName() + " has name: " + stud.getLname()+ ", " + 
                    				stud.getFname()  + 
                    				" but spreadsheet has name: " + sheetData.lname + ", " + sheetData.fname); 
                    }
                    
                    if(nameMismatches.size() > 0)
                    {
                    	StringBuilder output = new StringBuilder();
                    	output.append("<font color='red'>Oops caught some more errors" +
                    			" just before saving, please fix student names in spreadsheet then try again:</font><ul>");
                    	for(String line : nameMismatches)
                    		output.append("<li>").append(line).append("</li>");
                    	output.append("</ul>");
                    	out.println(output.toString());
                    }
                    else
                    {
	                    //update test score
	                    ArrayList<Key<InetStudent>> result = InetDAO4ComplexUpdates.updateTestScores(testKey, scoreVals, true);
	                    out.println("updated: " + result.size() + " scores succesfully");
                    }
                } catch (MissingEntitiesException ex) {
                    log.severe("call to save grade data failed: " + ex.getMessage());
                    out.println("Save request failed, try restarting browser");
                }

            } else {
                out.println("<b>unable to retrieve session info. Try " +
                        "enabling cookies in your browser.</b>");
            }
            req.getSession().setAttribute(InetConstants.USESS_GRADE_UPLD_DATA_NAME, null);
            req.getSession().setAttribute(InetConstants.USESS_GRADE_UPLD_TEST_KEY, null);

        } else {
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iterator = upload.getItemIterator(req);
                InetSingleTestScores testScore;
                if(!iterator.hasNext())
                {
                    log.severe("no param found, expected test key");
                    throw new IllegalStateException("no param found for test key");
                }
                FileItemStream item = iterator.next();
                InputStream stream = item.openStream();
                //get testKey
                Key<InetSingleTestScores> testKey = null;
                if (item.isFormField() && item.getFieldName().equals(PanelServiceConstants.TESTKEY_PARAM_NAME)) {
                    testKey = ofy.getFactory().stringToKey(Streams.asString(stream));
                    log.info("retrieved key: " + testKey);
                    testScore = ofy.find(testKey);
                    
                    if (testScore == null) {
                        out.println("<b>unable to find test, refresh browser and try again</b>");
                        log.severe("missing test: [" + testKey + "]");
                        return;
                    }
                    req.getSession().setAttribute(InetConstants.USESS_GRADE_UPLD_TEST_KEY, testKey);
                } else {
                    out.println("<b>bad request, unable to detect selected test</b>");
                    log.severe("security issue: user sent input that's not a testkey: [" +item.getFieldName() + ']') ;
                    return;
                }

                //get excel file
                if(!iterator.hasNext())
                {
                    log.severe("no param found, expected file contents");
                    throw new IllegalStateException("no param found for file data");
                }
                item = iterator.next();
                stream = item.openStream();
                ColumnAccessor[] headerAccessors = InetConstants.GRADE_UPLOAD_ACCESSORS;
                headerAccessors[headerAccessors.length - 1].setHeaderName(testScore.getTestName().toUpperCase());
                String[] expectedHeaders = ColumnAccessor.getAccessorNames(headerAccessors);
                ExcelManager sheetManager = new ExcelManager(stream);

                String[] sheetHeaders = sheetManager.getHeaders();
                //expectedheaders are uppercase so convert sheet prior to compare
                for (int s = 0; s < sheetHeaders.length; s++) {
                    sheetHeaders[s] = sheetHeaders[s].toUpperCase();
                }
                ArrayList<String> sheetOnly = new ArrayList<String>();
                ArrayList<String> intersect = new ArrayList<String>();
                ArrayList<String> expectedOnly = new ArrayList<String>();
                GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders, intersect, expectedOnly, sheetOnly);
                //handle errors in column headers
                if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
                    out.println("<b>Uploaded sheet should contain only these headers:</b>");
                    printHtmlList(expectedHeaders, out);
                    if (expectedOnly.size() > 0) {
                        out.println("<b>These headers are missing from spreadsheet:</b>");
                        printHtmlList(expectedOnly, out);
                    }
                    if (sheetOnly.size() > 0) {
                        out.println("<b>These headers are NOT expected but were found in spreadsheet</b>");
                        printHtmlList(sheetOnly, out);
                    }
                    return;
                }

                sheetManager.initializeAccessorList(headerAccessors);
                String goodDataHTML = "";
                String badDataHTML = "";

                HashMap<Key<InetStudent>, GradeStruct> sessionData = new HashMap<Key<InetStudent>, GradeStruct>(sheetManager.totalRows());


                for (int i = 1; i < sheetManager.totalRows(); i++) {
                    String row = "<tr>";
                    GradeStruct gradeData = new GradeStruct();
                    try
                    {
	                    for (int j = 0; j < headerAccessors.length; j++) {
	                        String tempVal = fetchColVal(i, headerAccessors[j], sheetManager, gradeData);
	                        row += "<td>" + tempVal + "</td>";
	                    }
	                    
	                    //additional score validation
	                    if(gradeData.exceptionMessage.length() == 0 && (gradeData.score < 0 || gradeData.score > testScore.getMaxScore()))
	                    	gradeData.exceptionMessage = "Column " + testScore.getTestName() + " should have a value between 0 and " + testScore.getMaxScore();
	
	                    Key<InetStudent> studKey;
	                    if (gradeData.exceptionMessage.length() == 0 && (studKey = getStudentKey(testScore, gradeData)) != null) {
	                        row += "<td>Passed prelim checks</td></tr>\n";
	                        goodDataHTML += row;
	                        sessionData.put(studKey, gradeData);
	
	                    } else {
	                        row += "<td>" + gradeData.exceptionMessage + "</td></tr>\n";
	                        badDataHTML += row;
	                    }
                    }
                    catch(EmptyColumnValueException ex) {} //logic to help skip blank lines, see fetchColVals for rest of logic
                }

                //table header
                String htmlTableStart = "<TABLE class='themePaddedBorder' border='1'>";
                for (int i = 0; i < headerAccessors.length; i++) {
                    htmlTableStart += "<TH>" + headerAccessors[i].getHeaderName() + "</TH>";
                }
                htmlTableStart += "<TH width='40%'>Messages</TH>";
                String htmlTableEnd = "</TABLE>";

                if (goodDataHTML.length() > 0) {
                    out.println(PanelServiceConstants.GOOD_DATA_PREFIX);
                    goodDataHTML = "<p><b>Below shows data that passed preliminary checks. Hit save button below to store test/exam grades for" +
                    		"<font color='green'>" + testKey.getName().toUpperCase() + " (" + InetDAO4CommonReads.keyToPrettyString(testKey.getParent()) + ")" +
                    		"</font></b></p>" + htmlTableStart + goodDataHTML + htmlTableEnd;
                }

                if (badDataHTML.length() > 0) {
                    out.println("<b><font color='red'>Below shows records with errors</font></b>");
                    out.println(htmlTableStart);
                    out.print(badDataHTML);
                    out.println(htmlTableEnd);
                }

                out.println(goodDataHTML);

                req.getSession().setAttribute(InetConstants.USESS_GRADE_UPLD_DATA_NAME,
                        sessionData);

            } catch (FileUploadException ex) {
                log.severe(ex.getMessage());
                out.println("<b>File upload failed</b>, exception is: <p>" + ex.getMessage() +
                        "</p>");
            } catch (InvalidColumnValueException ex) {
                log.severe(ex.getMessage());
                out.println("<b>Unable to read excel sheet</b><br/> Error was: " + ex.getMessage());
            }
        }
    }

    private String fetchColVal(int row, ColumnAccessor accessor, ExcelManager sheetManager, GradeStruct gradeData) throws EmptyColumnValueException, InvalidColumnValueException {
        Object val = "";
        String headerName = accessor.getHeaderName();
        try 
        {
            val = sheetManager.getData(row, accessor);
            //or should we use int ids instead of slower string comps below?
            if (headerName.equals(InetConstants.GRD_FNAME_HEADER)) {
                gradeData.fname = (String) val;
            } else if (headerName.equals(InetConstants.GRD_LNAME_HEADER)) {
                gradeData.lname = (String) val;
            } else if (headerName.equals(InetConstants.GRD_LOGIN_HEADER)) {
                gradeData.loginName = (String) val;
            } else {
            	if(val == null) throw new NumberFormatException("[Null Value]");
                gradeData.score = (Double) (val);
            }
        }
        catch(EmptyColumnValueException ex)
        {
        	if(sheetManager.isBlankRow(row))
        		throw ex;
        	gradeData.exceptionMessage += "<li>" + ex.getMessage() + "</li>";
        }
        catch (InvalidColumnValueException ex) {
            gradeData.exceptionMessage += "<li>" + ex.getMessage() + "</li>";
        }catch(NumberFormatException ex)
        {
        	gradeData.exceptionMessage += "<li>Number format exception: " + ex.getMessage() + "</li>";
        }
        return val.toString();
    }

    private Key<InetStudent> getStudentKey(InetSingleTestScores ts, GradeStruct gradeData) {
        Key<InetStudent> k = new Key<InetStudent>(InetStudent.class, gradeData.loginName);
        if (!ts.hasTestScore(k)) {
            gradeData.exceptionMessage += "<li>student " + gradeData.loginName +
                    " not registered for this subject</li>";
            k = null;
        }
        return k;
    }

    private void printHtmlList(String[] list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }

    public void printHtmlList(List<String> list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }

    public static class GradeStruct implements Serializable
    {
        String fname;
        String lname;
        String loginName;
        Double score;
        String exceptionMessage = "";
    }
}

