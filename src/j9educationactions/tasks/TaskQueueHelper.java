/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationactions.tasks;



import j9educationentities.InetConstants;
import j9educationentities.InetStudent;
import j9educationgwtgui.server.ScoreManagerImpl.InetClassGradesStruct;
import j9educationgwtgui.shared.PanelServiceConstants;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskHandle;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Builder;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class TaskQueueHelper {
    private static final Logger log = Logger.getLogger(TaskQueueHelper.class.getName());
    private final static String COMMON_TASK_HANDLER = "/tasks/createschoolsubentity";

    private static int MAX_RETRY_COUNT = 3;
    
    public static void scheduleCreateStringOption(String optionName)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue();
	            queue.add(Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(InetConstants.SERVICE_TYPE,
	                    InetConstants.SERVICE_DEPENDENT_OPTN).
	                    param(InetConstants.SCHL_DEPENDENT_TASK_OPT_NAME_PARAM,
	                    optionName).
	                    method(Method.POST));
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");

    }
    
    public static void scheduleCreateHoldContainer(String levelKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        queue.add(Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_CREATE_HOLD_CONTAINER).
    	                param(InetConstants.LEVEL_KEY_PARAM,
    	                levelKeyStr).
    	                method(Method.POST));
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create holds container failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }    

    public static void scheduleCreateStudent(String levelKey, String fname,
            String lname, String dob, String email, String login, String password)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_CREATE_STUDENT).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKey).
    	                param(InetConstants.FNAME_PARAM, fname).
    	                param(InetConstants.LNAME_PARAM, lname).
    	                param(InetConstants.EMAIL_PARAM, email).
    	                param(InetConstants.PASSWORD_PARAM, password).
    	                param(InetConstants.LOGIN_ID_PARAM, login).
    	                param(InetConstants.BIRTHDAY_PARAM, dob).method(Method.POST);

    	        queue.add(url);

    			succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create student failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }
    
    public static void scheduleCreateUserBill(String billDescriptionKey, String userKey)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_CREATE_USER_BILL).
    	                param(InetConstants.BILL_DESC_KEY_PARAM, billDescriptionKey).
    	                param(InetConstants.STUDENT_KEY_PARAM, userKey).method(Method.POST);

    	        queue.add(url);

    			succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create user bill failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }    
    
    public static void scheduleCreateCourse(String levelKey, String courseName, String[] testInfo, Set<Key<InetStudent>> studKeys)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_CREATE_COURSE).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKey).
    	                param(InetConstants.SUBJECT_KEY_PARAM, courseName);
    	        
    	        for(String ti : testInfo)
    	        	url.param(InetConstants.TEST_KEY_PARAM, ti);
    	        
    	        if(studKeys != null)
    	        {
	    	        for(Key<InetStudent> sk : studKeys)
	     	           url = url.param(InetConstants.LOGIN_ID_PARAM, sk.getName());
    	        }
    	        
    	        url.method(Method.POST);
    	        queue.add(url);
    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create course failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }

    public static void scheduleAddStudentsToClassRoster(String levelKey, ArrayList<String> ids)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();

    	        log.info("scheduling update level roster");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_UPDATE_LEVEL_ROSTER).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKey);

    	        for(String loginId : ids)
    	           url = url.param(InetConstants.LOGIN_ID_PARAM, loginId);

    	        url = url.method(Method.POST);
    	        TaskHandle task = queue.add(url);
    	        log.info("Task " + task.getName() + " added to queue " +
    	                task.getQueueName() + " and will run at: " +
    	                new Date(task.getEtaMillis()) + " url is: " + url.getUrl());
    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task add student to class roster failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }
    
    public static void scheduleAddStudentsToSubjectRoster(String levelKey, String courseKey, String[] ids)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();

    	        log.info("scheduling update level roster");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_UPDATE_COURSE_ROSTER).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKey).
    	                param(InetConstants.SUBJECT_KEY_PARAM, courseKey);

    	        for(String loginId : ids)
    	           url = url.param(InetConstants.LOGIN_ID_PARAM, loginId);

    	        url = url.method(Method.POST);
    	        TaskHandle task = queue.add(url);
    	        log.info("Task " + task.getName() + " added to queue " +
    	                task.getQueueName() + " and will run at: " +
    	                new Date(task.getEtaMillis()) + " url is: " + url.getUrl());
    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task add student to subject roster failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }    

    public static void scheduleAddLevelToSchool(String levelKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        queue.add(Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_ADD_SCHOOL_LEVEL).
    	                param(InetConstants.LEVEL_KEY_PARAM,
    	                levelKeyStr).
    	                method(Method.POST));
    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task add level to school failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }

    public static void scheduleAddCourseToLevel(String courseKeyStr,
            String levelKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        queue.add(Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_ADD_LEVEL_COURSE).
    	                param(InetConstants.LEVEL_KEY_PARAM,
    	                levelKeyStr).
    	                param(InetConstants.SUBJECT_KEY_PARAM,
    	                courseKeyStr).
    	                method(Method.POST));

    			succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task add course to level failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }

    public static void scheduleAddCourseToStudentSubjectList(String courseKeyStr,
            String studentKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();

    	        log.info("scheduling update level roster");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_UPDATE_STUDENT_COURSE_LIST).
    	                param(InetConstants.SUBJECT_KEY_PARAM, courseKeyStr).
    	                param(InetConstants.STUDENT_KEY_PARAM, studentKeyStr).
    	                method(Method.POST);

    	        queue.add(url);

    			succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task add course to student subject list failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }

    public static void scheduleAddLevelToStudentClassList(String levelKeyStr,
            String studentKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();

    	        log.info("scheduling update level roster");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_UPDATE_STUDENT_LEVEL_LIST).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKeyStr).
    	                param(InetConstants.STUDENT_KEY_PARAM, studentKeyStr).
    	                method(Method.POST);

    	        queue.add(url);
    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task add level to student class list failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }

    public static void scheduleUpdateCumulativeScores(String courseKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();

    	        log.info("scheduling update level roster");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_UPDATE_CUMULATIVE_SCORES).
    	                param(InetConstants.SUBJECT_KEY_PARAM, courseKeyStr).
    	                method(Method.POST);

    	        queue.add(url);   			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task update cumulative scores failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }

    public static void scheduleAddGuardianToStudent(String guardKeyStr,
            String studKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        log.info("scheduling add guardian to student");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE,
    	                InetConstants.SERVICE_ADD_STUDENT_GUARDIAN).
    	                param(InetConstants.STUDENT_KEY_PARAM, studKeyStr).
    	                param(InetConstants.GUARDIAN_KEY_PARAM, guardKeyStr).
    	                method(Method.POST);
    	        queue.add(url);    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task add guardian to student failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }
    
    public static void scheduleRemoveStudentFromLevel(String studKeyStr, 
    		String levelKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        log.info("scheduling add guardian to student");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE, 
    	                		InetConstants.SERVICE_REMOVE_LEVEL_ROSTER).
    	                param(InetConstants.STUDENT_KEY_PARAM, studKeyStr).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKeyStr).
    	                method(Method.POST);
    	        queue.add(url);    	
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task remove student from level failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }
    
    public static void scheduleRemoveCourseFromLevel(String courseKeyStr, 
    		String levelKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        log.info("scheduling add guardian to student");
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE, 
    	                		InetConstants.SERVICE_REMOVE_COURSE_FRM_LEVEL).
    	                param(InetConstants.SUBJECT_KEY_PARAM, courseKeyStr).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKeyStr).
    	                method(Method.POST);
    	        queue.add(url);    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task remove course from level failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");    	
    }
    
    public static void scheduleRemoveLevelFromStudentList(String levelKeyStr, 
    		String studKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE, 
    	                		InetConstants.SERVICE_REMOVE_LEVEL_FRM_STUDENT).
    	                param(InetConstants.STUDENT_KEY_PARAM, studKeyStr).
    	                param(InetConstants.LEVEL_KEY_PARAM, levelKeyStr).
    	                method(Method.POST);
    	        queue.add(url);     			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task remove level from student failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");   	
    }
    
    public static void scheduleRemoveCourseFromStudentList(String courseKeyStr, 
    		String studKeyStr)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Objectify ofy = ObjectifyService.begin();
    	    	String studKey = ofy.getFactory().stringToKey(studKeyStr).getName();
    	    	String ck = ofy.getFactory().stringToKey(courseKeyStr).getName();
    	    	log.warning("scheduling removal of " + ck + " from " + studKey + " list");
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE, 
    	                		InetConstants.SERVICE_REMOVE_COURSE_FRM_STUDENT).
    	                param(InetConstants.STUDENT_KEY_PARAM, studKeyStr).
    	                param(InetConstants.SUBJECT_KEY_PARAM, courseKeyStr).
    	                method(Method.POST);
    	        queue.add(url);    	    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task remove course from student failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }
    
    public static void scheduleRemoveGuardianFromStudentList(String guardLogin, 
    		String studLogin)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE, 
    	                		InetConstants.SERVICE_BREAK_GUARD_STUDENT_PAIR).
    	                param(InetConstants.STUDENT_KEY_PARAM, studLogin).
    	                param(InetConstants.GUARDIAN_KEY_PARAM, guardLogin).
    	                method(Method.POST);
    	        queue.add(url);    	    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task remove guardian from student list failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }
    
    public static void scheduleClassScoreUpdate(InetClassGradesStruct gradeStruct)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	Set<String> courseNames = gradeStruct.testNames.keySet();
    	        Queue queue = QueueFactory.getDefaultQueue();
    	  	
    	    	for(String course : courseNames)
    	    	{
    	            TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	            param(InetConstants.SERVICE_TYPE, InetConstants.SERVICE_UPDATE_CLASS_SCORES);  
    	            
    	    		String[] testNames = gradeStruct.testNames.get(course);
    	    		String[] testKeys = gradeStruct.testKeys.get(course);
    	    		String testKeyOrder = "";
    	    		for(int i = 0; i < testNames.length; i++)
    	    		{
    	    			testKeyOrder += testNames[i];
    	    			if(i != testNames.length - 1)
    	    				testKeyOrder +=PanelServiceConstants.DATA_SEPERATOR;
    	    			url.param(InetConstants.TEST_KEY_PARAM, testKeys[i]);
    	    		}
    	    		
    	    		if(testKeyOrder.length() == 0)
    	    			throw new IllegalArgumentException("Course: " + course + " has no tests but included in schedule request");
    	    		
    	    		url.param(InetConstants.TEST_KEY_PARAM_ORDER, testKeyOrder);
    	    		
    	    		HashMap<String, Double[]> studentGrades = gradeStruct.grades.get(course); 
    	    		Set<String> studentLogins = studentGrades.keySet();
    	    		for(String loginId : studentLogins)
    	    		{
    	    			String gradeRecordParam = loginId + PanelServiceConstants.DATA_SEPERATOR;
    	    			Double[] grades = studentGrades.get(loginId);
    	    			for(int i = 0; i < testNames.length; i++)
    	    			{
    	    				gradeRecordParam += grades[i] == null ? "" : String.valueOf(grades[i]);
    	    				if(i != testNames.length - 1)
    	    					gradeRecordParam += PanelServiceConstants.DATA_SEPERATOR;
    	    			}
    	    			url.param(InetConstants.STUDENT_KEY_PARAM, gradeRecordParam);
    	    		}
    	    		
    	    		if(studentLogins.size() > 0)
    	    			queue.add(url);
    	    	}    			
	            succeeded = true;	            
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task class score update failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");

    }
    
	public static void scheduleMessageSending(String controllerId, String toAddress, String messages[])
	{
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	log.warning("scheduling sending of msg to: " + toAddress);
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(InetConstants.SERVICE_TYPE, 
    	                		InetConstants.SERVICE_SEND_MESSAGE).
    	                param(InetConstants.MSG_CONTROLLER_KEY_PARAM, controllerId).
    	                param(InetConstants.TO_ADDR_PARAM, toAddress);
    	        
    	        for(String msg : messages)
    	        	if(msg != null && msg.length() > 0)
    	        		url = url.param(InetConstants.MSG_BODY_PARAM, msg);
    	        url.method(Method.POST);
    	        queue.add(url);    	    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule message sending failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
	}    
}
