/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationactions.tasks;

import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetConstants;
import j9educationentities.InetCourse;
import j9educationentities.InetDAO4ComplexUpdates;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetDAO4Updates;
import j9educationentities.InetLevel;
import j9educationentities.InetSingleTestScores;
import j9educationentities.InetStudent;
import j9educationentities.InetUser;
import j9educationentities.accounting.InetBillDescription;
import j9educationentities.accounting.InetDAO4Accounts;
import j9educationentities.messaging.MessagingController;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.utils.DateColumnAccessor;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class CommonTasksHandler extends HttpServlet{
    private static final Logger log =
            Logger.getLogger(CommonTasksHandler.class.getName());
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
    	log.warning("TASK EXECUTION BEGINNING!!!");
        StringBuilder requestDetails = new StringBuilder(64);
        Enumeration<String> paramNames = req.getParameterNames();
        while(paramNames.hasMoreElements())
        {
        	String name = paramNames.nextElement();
        	requestDetails.append("Param Name: " + name.toString() + " Values: ").append(Arrays.toString(req.getParameterValues(name))).append("\n");
        }
        log.warning(requestDetails.toString());
        
        Objectify ofy = ObjectifyService.begin();
        //TODO, add security constraints in web.xml for all task servlets
        String service = req.getParameter(InetConstants.SERVICE_TYPE).trim();
        validateParamName(service, "No service specified");
        if(service.equals(InetConstants.SERVICE_DEPENDENT_OPTN))
        {
            String optionName = req.getParameter
                    (InetConstants.SCHL_DEPENDENT_TASK_OPT_NAME_PARAM).trim();
            validateParamName(optionName, "No option name specified");
            log.info("about to call string options fn");
            try {
                InetDAO4Creation.createStringOptions(optionName, optionName);
            } catch (DuplicateEntitiesException ex) {
                log.severe(ex.getMessage());
            }
        }
        else if(service.equals(InetConstants.SERVICE_CREATE_STUDENT))
        {
            String classKey = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            String fname = req.getParameter(InetConstants.FNAME_PARAM);
            String lname = req.getParameter((InetConstants.LNAME_PARAM));
            String email = req.getParameter(InetConstants.EMAIL_PARAM);
            String password = req.getParameter(InetConstants.PASSWORD_PARAM);
            String loginName = req.getParameter(InetConstants.LOGIN_ID_PARAM);
            String dobStr = req.getParameter(InetConstants.BIRTHDAY_PARAM);

            validateParamName(lname, "Value must be specified for last name");
            validateParamName(fname, "Value must be specified for first name");
            validateParamName(loginName, "Value must be specified for login");
            validateParamName(password, "Value must be specified for password");
            validateParamName(classKey, "Value must be specified for classkey");

            //parse date
            Date dob;
            if(dobStr != null && !dobStr.equals(""))
            {
                DateFormat df = DateColumnAccessor.getDateFormatter();
                try {
                    dob = df.parse(dobStr);
                } catch (ParseException ex) {
                    log.warning("error parsing date " + dobStr + " for: " +
                            loginName + " --> " + ex.getMessage());
                    dob = null;
                }
            }
            else
                dob = null;

            Key<InetLevel> levelKey = ofy.getFactory().stringToKey(classKey);

            InetStudent inetStud;
            try {
                //create student
                inetStud = InetDAO4Creation.
                        createStudent(fname, lname, dob, email,
                        null, loginName, password, classKey);

            } catch (DuplicateEntitiesException ex) {
                log.severe(ex.getMessage());
                return;
            } catch (MissingEntitiesException ex) {
                throw new IllegalArgumentException(ex.fillInStackTrace());
            }
            log.info("Adding student to roster: " + inetStud.getFname() + " " +
                    inetStud.getLname() + " login: " + inetStud.getLoginName() +
                    " classKey:" + levelKey);
        }
        else if(service.equals(InetConstants.SERVICE_UPDATE_LEVEL_ROSTER))
        {
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String classKey = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            validateParamName(classKey, "Value must be specified for class key");
            String[] loginStrs = req.getParameterValues(InetConstants.LOGIN_ID_PARAM);
            if(loginStrs.length == 0)
                throw new IllegalArgumentException("Need at least 1 student id");

            Key<InetLevel> levelKey = ofy.getFactory().stringToKey(classKey);

            try
            {
                InetDAO4Updates.addStudentsToLevelRoster(levelKey,
                        loginStrs, false);
            }
            catch(DuplicateEntitiesException ex)
            {
            	log.severe("IGNORING DUPLICATE: " + ex.getMessage());
            }
            catch(MissingEntitiesException ex)
            {
                throw new IllegalStateException(ex.fillInStackTrace());
            }
        }
        else if(service.equals(InetConstants.SERVICE_SEND_MESSAGE))
        {
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String controllerKeyStr = req.getParameter(InetConstants.MSG_CONTROLLER_KEY_PARAM);
            String toAddress = req.getParameter(InetConstants.TO_ADDR_PARAM);
            String[] messageBody = req.getParameterValues(InetConstants.MSG_BODY_PARAM);
            validateParamName(controllerKeyStr, "Value must be specified for message controller key");
            validateParamName(toAddress, "Value must be specified for recipeint address");
            validateParamName(messageBody, "Value must be specified for message body");

            Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerKeyStr);
             
        	MessagingController controller = ofy.find(controllerKey);
        	if(controller == null)
        		throw new IllegalStateException("Unable to find controller key");
        	boolean status = controller.sendMessage(toAddress, messageBody);
        	
        	//TODO modify log to reflect byte size of entire messageBody object rather than just text component
        	String msgSuffix = "message of length " + messageBody[0].length() +
			" to " + toAddress + ". Controller Type: " + controller.getClass().getSimpleName();
        	if(status)
        		log.warning("Successfully sent " + msgSuffix);
        	else
        		log.severe("Error sending " + msgSuffix);
        }        
        else if(service.equals(InetConstants.SERVICE_UPDATE_COURSE_ROSTER))
        {
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String classKey = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            String courseKey = req.getParameter(InetConstants.SUBJECT_KEY_PARAM);
            validateParamName(classKey, "Value must be specified for class key");
            validateParamName(courseKey, "Value must be specified for class key");
            String[] loginStrs = req.getParameterValues(InetConstants.LOGIN_ID_PARAM);
            if(loginStrs.length == 0)
                throw new IllegalArgumentException("Need at least 1 student id");

            try
            {
                InetDAO4Updates.addStudentsToCourseTests(classKey, courseKey, loginStrs, false, true);
            }
            catch(DuplicateEntitiesException ex)
            {
            	log.severe("IGNORING DUPLICATE: " + ex.getMessage());
            }
            catch(MissingEntitiesException ex)
            {
                throw new IllegalStateException(ex.fillInStackTrace());
            }
        }        
        else if(service.equals(InetConstants.SERVICE_CREATE_COURSE))
        {
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String classKey = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            String courseName = req.getParameter(InetConstants.SUBJECT_KEY_PARAM);
            String[] testStrs = req.getParameterValues(InetConstants.TEST_KEY_PARAM);
            String[] studLogins = req.getParameterValues(InetConstants.LOGIN_ID_PARAM);
            validateParamName(classKey, "Value must be specified for class key");
            validateParamName(courseName, "Value must be specified for subject key");

            Key<InetLevel> levelKey = ofy.getFactory().stringToKey(classKey);

            try
            {
            	HashMap<String, Double[]> testInfo = new HashMap<String, Double[]>();
            	String[] testProperties;
            	Double[] maxAndWeight;
            	if(testStrs != null)
            	{
	            	for(String ts : testStrs)
	            	{
	            		testProperties = ts.split(InetConstants.DELIMITTER_REGEX);
	            		maxAndWeight = new Double[2];
	            		if(testProperties.length != 3)
	            			throw new IllegalArgumentException("expected 3 parts but not the case with split of: " + ts);
	            		maxAndWeight[0] = Double.valueOf(testProperties[1]);
	            		maxAndWeight[1] = Double.valueOf(testProperties[2]);
	            		testInfo.put(testProperties[0], maxAndWeight);
	            	}
            	}
            	//create course
                ArrayList<GAEPrimaryKeyEntity> objs = InetDAO4Creation.createCourseAndTests(levelKey, courseName, testInfo);
                
                if(studLogins != null && studLogins.length > 0)
                {
                	InetCourse c = (InetCourse) objs.get(0);
                	String ckStr = ofy.getFactory().keyToString(c.getKey());
                	TaskQueueHelper.scheduleAddStudentsToSubjectRoster(classKey, ckStr, studLogins);
                }
            }
            catch(DuplicateEntitiesException ex)
            {
            	log.severe("IGNORING DUPLICATE: " + ex.getMessage());
            }
            catch(MissingEntitiesException ex)
            {
                throw new IllegalStateException(ex.fillInStackTrace());
            }
        }        
        else if(service.equals(InetConstants.SERVICE_ADD_SCHOOL_LEVEL))
        {
            String levelKeyStr = req.getParameter
                    (InetConstants.LEVEL_KEY_PARAM);
            validateParamName(levelKeyStr, "No level specified");
            try {
                InetDAO4Updates.addLevelKeyToSchool(levelKeyStr);
            }catch (DuplicateEntitiesException ex) {
                log.severe("IGNORING DUPLICATE: " + ex.getMessage());
            }catch(MissingEntitiesException ex)
            {
                throw new IllegalStateException(ex.getMessage());
            }
        }
        else if(service.equals(InetConstants.SERVICE_ADD_LEVEL_COURSE))
        {
            String levelKeyStr = req.getParameter
                    (InetConstants.LEVEL_KEY_PARAM);
            String courseKeyStr = req.getParameter
                    (InetConstants.SUBJECT_KEY_PARAM);
            validateParamName(levelKeyStr, "No level specified");
            validateParamName(courseKeyStr, "No course specified");
            try 
            {
            	String[] ckKeyArg = {courseKeyStr};
                InetDAO4Updates.addCourseKeyToLevel(ckKeyArg, levelKeyStr);
            }catch (DuplicateEntitiesException ex) {
                log.severe(ex.getMessage());
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }
        }
        else if(service.equals(InetConstants.SERVICE_UPDATE_STUDENT_COURSE_LIST))
        {
            String courseKeyStr = req.getParameter
                    (InetConstants.SUBJECT_KEY_PARAM);
            String studentKeyStr = req.getParameter
                    (InetConstants.STUDENT_KEY_PARAM);

            validateParamName(courseKeyStr, "No course specified");
            validateParamName(studentKeyStr, "No student specified");
            try {
                InetDAO4Updates.addCourseKeyToStudent(studentKeyStr, courseKeyStr);
            }catch (DuplicateEntitiesException ex) {
                log.severe(ex.getMessage());
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }
        }
        else if(service.equals(InetConstants.SERVICE_UPDATE_STUDENT_LEVEL_LIST))
        {
            String levelKeyStr = req.getParameter
                    (InetConstants.LEVEL_KEY_PARAM);
            String studentKeyStr = req.getParameter
                    (InetConstants.STUDENT_KEY_PARAM);

            validateParamName(levelKeyStr, "No level specified");
            validateParamName(studentKeyStr, "No student specified");
            try {
                InetDAO4Updates.addLevelKeyToStudent(studentKeyStr, levelKeyStr);
            }catch (DuplicateEntitiesException ex) {
                log.severe(ex.getMessage());
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }
        }
        else if(service.equals(InetConstants.SERVICE_UPDATE_CUMULATIVE_SCORES))
        {
            String courseKeyStr = req.getParameter(InetConstants.SUBJECT_KEY_PARAM);
            validateParamName(courseKeyStr, "No course specified");
            try {
                InetDAO4Updates.updateCumulativeScores(courseKeyStr);
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }

        }
        else if(service.equals(InetConstants.SERVICE_ADD_STUDENT_GUARDIAN))
        {
            String guardKeyStr = req.getParameter(InetConstants.GUARDIAN_KEY_PARAM);
            String studKeyStr = req.getParameter(InetConstants.STUDENT_KEY_PARAM);
            validateParamName(guardKeyStr, "No guardian specified");
            validateParamName(studKeyStr, "No student specified");
            try {
                InetDAO4Updates.addGuardianToStudent(guardKeyStr, studKeyStr);
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }
            catch(DuplicateEntitiesException ex)
            {
                log.severe(ex.getMessage());
            }       	            
        }
        else if(service.equals(InetConstants.SERVICE_REMOVE_LEVEL_ROSTER))
        {
            String levelKeyStr = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            String loginName = req.getParameter(InetConstants.STUDENT_KEY_PARAM);
            validateParamName(levelKeyStr, "No level specified");
            validateParamName(loginName, "No student specified");
            try 
            {
            	ArrayList<Key<InetStudent>> studKey = new ArrayList<Key<InetStudent>>(1);
            	Key<InetStudent> sk = new Key<InetStudent>(InetStudent.class, loginName);
            	studKey.add(sk);
            	Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr)
            	
;                InetDAO4ComplexUpdates.removeStudentsFromLevel(studKey, levelKey);
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }      	            
        }        
        else if(service.equals(InetConstants.SERVICE_REMOVE_COURSE_FRM_LEVEL))
        {
            String levelKeyStr = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            String courseKeyStr = req.getParameter(InetConstants.SUBJECT_KEY_PARAM);
            validateParamName(levelKeyStr, "No level specified");
            validateParamName(courseKeyStr, "No course specified");
            try {
                InetDAO4ComplexUpdates.removeCourseFromLevel(courseKeyStr, levelKeyStr);
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }      	            
        }
        else if(service.equals(InetConstants.SERVICE_REMOVE_LEVEL_FRM_STUDENT))
        {
            String levKeyStr = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            String studKeyStr = req.getParameter(InetConstants.STUDENT_KEY_PARAM);
            validateParamName(levKeyStr, "No level specified");
            validateParamName(studKeyStr, "No student specified");
            try {
                InetDAO4ComplexUpdates.removeLevelFromStudentList(levKeyStr, studKeyStr);
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }      	            
        }
        else if(service.equals(InetConstants.SERVICE_REMOVE_COURSE_FRM_STUDENT))
        {
            String courseKeyStr = req.getParameter(InetConstants.SUBJECT_KEY_PARAM);
            String studKeyStr = req.getParameter(InetConstants.STUDENT_KEY_PARAM);
            validateParamName(courseKeyStr, "No course specified");
            validateParamName(studKeyStr, "No student specified");
            try {
                InetDAO4ComplexUpdates.removeCourseFromStudentList(courseKeyStr, studKeyStr);
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }      	            
        }
        else if(service.equals(InetConstants.SERVICE_BREAK_GUARD_STUDENT_PAIR))
        {
            String gl = req.getParameter(InetConstants.GUARDIAN_KEY_PARAM);
            String sl = req.getParameter(InetConstants.STUDENT_KEY_PARAM);
            validateParamName(gl, "No guardian specified");
            validateParamName(sl, "No student specified");
            try {
                InetDAO4ComplexUpdates.removeGuardianFromStudent(gl, sl);
            }catch(MissingEntitiesException ex)
            {
                log.severe(ex.getMessage());
                throw new IllegalStateException(ex.getMessage());
            }      	            
        }
        else if(service.equals(InetConstants.SERVICE_UPDATE_CLASS_SCORES))
        {
            //TODO, put all the pieces together here ... have task scheduler and function to ultimately call
        	//just now need to map the task to the function call
            String[] testKeyStr = req.getParameterValues(InetConstants.TEST_KEY_PARAM);
            String[] loginStrs = req.getParameterValues(InetConstants.STUDENT_KEY_PARAM);
            String testKeyOrder = req.getParameter(InetConstants.TEST_KEY_PARAM_ORDER);
            validateParamName(testKeyOrder, "Value must be specified for test key order");
            
            if(testKeyStr.length == 0)
                throw new IllegalArgumentException("Need at least 1 test");
            
            if(loginStrs.length == 0)
                throw new IllegalArgumentException("Need at least 1 student id");
            
            ArrayList<Key<InetSingleTestScores>> testKeys = new ArrayList<Key<InetSingleTestScores>>();
            String[] testNames = testKeyOrder.split(PanelServiceConstants.DATA_SEPERATOR_REGEX);
            
            if(testNames.length != testKeyStr.length)
            	throw new IllegalStateException("Test Names for order size not same as" +
            			" test key str size: " + Arrays.toString(testNames) + 
            			" **VS** " + Arrays.toString(testKeyStr) + " **ALSO** orderStr is: " + testKeyOrder);
           
            for(int i = 0; i < testKeyStr.length; i++)
            {
            	Key<InetSingleTestScores> temp = ofy.getFactory().stringToKey(testKeyStr[i]);
            	//this check in place because no explicit doc saying getparametervalues respects
            	//order in which parameters are added and ordering is important in this case.
            	if(!temp.getName().equalsIgnoreCase(testNames[i]))
            		throw new IllegalStateException("found mismatch between testname and testkey:"
            				+ testNames[i] + " vs " + temp.getName());
            	testKeys.add(temp);
            }
            
            HashMap<Key<InetStudent>, Double[]> studentData = new HashMap<Key<InetStudent>, Double[]>();
            for(int i = 0; i < loginStrs.length; i++)
            {
            	String[] studDataElems = loginStrs[i].split(PanelServiceConstants.DATA_SEPERATOR_REGEX);
            	
            	//ensure record size matches loginName + numOfTestScores
            	if(studDataElems.length != testKeys.size() + 1)
            		throw new IllegalStateException("student data entry does not match testkey size: " 
            				+ Arrays.toString(studDataElems));
            	
            	//setup student key
            	Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class, studDataElems[0]);
            	
            	//setup scores
            	Double[] studentScores = new Double[testKeys.size()];
            	int scoreIdx = 0;
            	for(int j = 1; j < studDataElems.length; j++)
            		studentScores[scoreIdx++] = studDataElems[j].equals("") ? null : Double.valueOf(studDataElems[j]);
            		
            	studentData.put(studKey, studentScores);	
            }
            

            try
            {
                InetDAO4ComplexUpdates.updateTestScores(testKeys, studentData);
            }
            catch(MissingEntitiesException ex)
            {
                throw new IllegalStateException(ex.fillInStackTrace());
            }
        }
        else if(service.equals(InetConstants.SERVICE_CREATE_HOLD_CONTAINER))
        {
            String levelKeyStr = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
            validateParamName(levelKeyStr, "No level key specified");
            try {
            	Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
                InetDAO4Creation.createHoldContainer(levelKey);
            }catch(DuplicateEntitiesException ex)
            {
                log.warning("IGNORING possible duplicate task run: " + ex.getMessage());
            }      	            
        }
        else if(service.equals(InetConstants.SERVICE_CREATE_USER_BILL))
        {
            String billDescKeyStr = req.getParameter(InetConstants.BILL_DESC_KEY_PARAM);
            String userKeyStr = req.getParameter(InetConstants.STUDENT_KEY_PARAM);
            validateParamName(billDescKeyStr, "No bill description key specified");
            validateParamName(userKeyStr, "No user key specified");
            try {
            	Key<InetBillDescription> billDescKey = ofy.getFactory().stringToKey(billDescKeyStr);
            	Key<? extends InetUser> userKey = ofy.getFactory().stringToKey(userKeyStr);
            	InetBillDescription bd = ofy.get(billDescKey);
                InetDAO4Accounts.createBill(bd, userKey);
            }catch(DuplicateEntitiesException ex)
            {
                log.warning("IGNORING possible duplicate task run: " + ex.getMessage());
            }      	            
        }        
        else
            throw new IllegalArgumentException("Unrecognized task type: " +
                    service);
    }

    private void validateParamName(String param, String message)
    {
        if(param == null || param.length() == 0)
            throw new IllegalArgumentException(message);
    }
    
    private void validateParamName(String[] params, String message)
    {
        if( params.length == 0 || params[0] == null || params[0].length() == 0)
            throw new IllegalArgumentException(message);
    }    
}
