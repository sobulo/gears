/**
 * 
 */
package j9educationactions.pdfreports;

import j9educationactions.login.LoginPortal;
import j9educationentities.InetConstants;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetImageBlob;
import j9educationentities.InetStudent;
import j9educationentities.accounting.InetBill;
import j9educationentities.accounting.InetBillDescription;
import j9educationgwtgui.server.AccountManagerImpl;
import j9educationgwtgui.server.SchoolServiceImpl;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.Box;
import com.pdfjet.Cell;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.Letter;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Point;
import com.pdfjet.Table;
import com.pdfjet.TextLine;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BillingInvoiceGenerator extends HttpServlet
{
    private final static int RIGHT_MARGIN = 70;
    private final static int TOP_MARGIN = 40;
    private final static int ITEM_COL_WIDTH = 170;
    private final static int NOTES_COL_WIDTH = 225;
    private final static int AMOUNT_COL_WIDTH = 75;
    private final static int PADDING_SIZE = 5;
    private final static int SPACE_BTW_BOXES = 40;
    private final static int HEADER_FONT_SIZE = 24;
    private final static int REGULAR_FONT_SIZE = 12;
    private final static int SMALL_FONT_SIZE = 9;
 
    private final static int BOX_STUDENT_INFO_HEIGHT = 80;
    private final static int BOX_COMMENT_INFO_HEIGHT = 80;
    private final static int BOX_OFFICIAL_INFO_HEIGHT = 30;
    private final static int BOX_DEFAULT_SCHOOL_HEIGHT = 80;
 
    final static int IDX_SUBJECT_NAME = 0;
    final static int IDX_GRADE = 1;
    final static int IDX_SCORE = 2;
    
    public final static String NOT_PUBLISHED_MESSAGE = "NOT PUBLISHED";
    
    private static final Logger log = Logger.getLogger(BillingInvoiceGenerator.class.getName());
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
        try 
        {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) 
		{
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
					"</b></body></html>");
			return;
		}
		
    	Objectify ofy = ObjectifyService.begin();
    	
    	String billKeyStr = req.getParameter(InetConstants.BILL_DESC_KEY_PARAM);
    	
    	if(billKeyStr == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unable to process request. Please ensure this page is accessed via a link" +
    				" recently generated from the report cards panel. If problem persists contact IT</font></b>");
    		return;
    	}
    	Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(billKeyStr);
    	HttpSession sess = req.getSession();
    	List<TableMessage> billDesc = (List<TableMessage>) sess.getAttribute(getBillDescSessionName(bdKey));
    	List<TableMessage> reportData = (List<TableMessage>) sess.getAttribute(getBillListSessionName(bdKey));
    	String[] schoolInfo = (String[]) sess.getAttribute(getSchoolInfoSessionName(bdKey));

    	if(reportData == null || billDesc == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unable to retrieve sesison data. Please ensure this page is accessed via a link" +
    				" recently generated from the invoice panel. If problem persists contact IT</font></b>");
    		return;    		
    	}    	
		
		String fileName = InetDAO4CommonReads.getNameFromLastKeyPart(bdKey, 2);
		fileName = fileName.replace(" ", "");
		fileName += "-invoices.pdf";
        res.setContentType("application/octet-stream");
        
        res.setHeader("Content-disposition", "attachment; filename=" + fileName);
        OutputStream out = res.getOutputStream();
        writePDFToOutputStream(ofy, out, billDesc, reportData, schoolInfo);
    }
    
    private static void writePDFToOutputStream(Objectify ofy, OutputStream out, List<TableMessage> billDesc, List<TableMessage> reportData, String[] schoolInfo)
    {
        PDF pdf;
    	
    	//convert bill desc itemized data to format experted by print report fn
        TableMessage billDescSummary = billDesc.remove(0); 
    	List<List<Cell>> itemizedBillDesc = null;
    	
    	//init pdf file
    	try
    	{
	        pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(REGULAR_FONT_SIZE);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(REGULAR_FONT_SIZE);
	        
	        itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(billDesc, f1, f2);
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, InetConstants.SCHOOL_REPORT_CARD_IMAGE);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your school hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	printReportCards(itemizedBillDesc, billDescSummary, reportData, schoolInfo, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("j9"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
    public static byte[] getInvoiceData(String bdKeyStr, String[] studentLoginNames)
    {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	Objectify ofy = ObjectifyService.begin();
    	Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(bdKeyStr);
    
    	//TODO, this part is quite inefficient, share data better possibly, then again is it that inefficient?
    	//so long as we're not loading numerous student objects, the price of loading a inet level/tests probably isn't
    	//much? or is it? ha, if you're reading this and you care? run some performance tests
    	
    	try
    	{
	    	List<TableMessage>[] billingInfo = getBillingInvoicesObjects(ofy, bdKey, studentLoginNames, false);
	    	writePDFToOutputStream(ofy, out, billingInfo[1], billingInfo[0], SchoolServiceImpl.getSchoolInfo(ofy));
    	}
    	catch(Exception ex)
    	{
    		log.severe(ex.getMessage());
    		return null;
    	}
    	return out.toByteArray();
    }
    
    public static String[] getSummaryMessages(String bdKeyStr, String[] studentLoginNames)
    {
    	Objectify ofy = ObjectifyService.begin();
    	Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(bdKeyStr);
    
    	//TODO, this part is quite inefficient, share data better possibly, then again is it that inefficient?
    	//so long as we're not loading numerous student objects, the price of loading a inet level/tests probably isn't
    	//much? or is it? ha, if you're reading this and you care? run some performance tests
    	
    	String[] result = new String[studentLoginNames.length];
    	try
    	{
	    	List<TableMessage>[] billingInfo = getBillingInvoicesObjects(ofy, bdKey, studentLoginNames, false);
	    	String billName = bdKey.getName().toUpperCase();
	    	DecimalFormat df = new DecimalFormat();
	    	df.setGroupingSize(3);
	    	df.setMaximumFractionDigits(0);
	    	for(int i = 0; i < billingInfo[0].size(); i++)
	    	{	    		
	    		TableMessage studentBill = billingInfo[0].get(i);
	    		StringBuilder summaryMessage = new StringBuilder();
	    		summaryMessage.append(studentBill.getText(2)).append(", ").
	    			append(studentBill.getText(1)).append("'s invoice: ").
	    			append(billName).append(" Total:").append(df.format(studentBill.getNumber(0))).
	    			append(" Paid:").append(df.format(studentBill.getNumber(1))).append(" Balance:").
	    			append(df.format(studentBill.getNumber(0)-studentBill.getNumber(1)));	
	    		result[i] = summaryMessage.toString();
	    	}
    	}
    	catch(Exception ex)
    	{
    		log.severe(ex.getMessage());
    		return null;
    	}
    	return result;
    }
    
    private static void printReportCards(List<List<Cell>> itemizedBill, TableMessage billDescSummary,
    		List<TableMessage> studentBills, String[] schoolInfo, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        logo.scaleBy(0.75);
        
        final int BOX_SCHOOL_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_SCHOOL_HEIGHT);
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;
        
        int studentBoxY = TOP_MARGIN + BOX_SCHOOL_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_STUDENT_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        
        for(int i = 0; i < studentBills.size(); i++)
        {
        	TableMessage studentBill = studentBills.get(i);
		    //new page for student report card		    
		    Page page = new Page(pdf, Letter.PORTRAIT);
		    
		    //setup table
		    Table table = new Table(f1, f2);
		    table.setData(itemizedBill, Table.DATA_HAS_1_HEADER_ROWS);
		    table.setLineWidth(0.2);
		    table.setPosition(RIGHT_MARGIN, tableY);
		    table.setCellPadding(PADDING_SIZE);
		    table.autoAdjustColumnWidths();
		    table.setColumnWidth(0, ITEM_COL_WIDTH);
		    table.setColumnWidth(1, NOTES_COL_WIDTH);
		    table.setColumnWidth(2, AMOUNT_COL_WIDTH);
		    table.rightAlignNumbers();
		    int numOfPages = table.getNumberOfPages(page);
		    if(numOfPages != 1)
		    	throw new IllegalArgumentException("Billing template data too Large. Contact IT to setup custom invoice report for this template");
		    
		    Point tableEnd = table.drawOn(page);
		    
		    //setup school info
		    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, table.getWidth(), BOX_SCHOOL_INFO_HEIGHT);
		    TextLine text = new TextLine(f3, schoolInfo[PanelServiceConstants.SCHOOL_INFO_NAME_IDX]);
		    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
		    text.setPosition(textStart, textY);
		    
		    TextLine text1 = new TextLine(f4, schoolInfo[PanelServiceConstants.SCHOOL_INFO_ADDR_IDX] );
		    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
		    text1.setPosition(textStart, text1Y);
		    
		    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[PanelServiceConstants.SCHOOL_INFO_NUMS_IDX]);
		    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2.setPosition(textStart, text2Y);
		    
		    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[PanelServiceConstants.SCHOOL_INFO_WEB_IDX] + "   Email: " + schoolInfo[PanelServiceConstants.SCHOOL_INFO_EMAIL_IDX]);
		    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2b.setPosition(textStart, text2bY);		    
	    
		    logo.setPosition(RIGHT_MARGIN + table.getWidth() - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + PADDING_SIZE);
		    
		    schoolInfoBox.drawOn(page);
		    text.drawOn(page);
		    text1.drawOn(page);
		    text2.drawOn(page);
		    text2b.drawOn(page, true);
		    logo.drawOn(page);
		    
		    //setup student info
		    Box studentBox = new Box(RIGHT_MARGIN, studentBoxY , table.getWidth(), BOX_STUDENT_INFO_HEIGHT);
		    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Name: " + studentBill.getText(2) + ", " + studentBill.getText(1), 
		    		textStart, textY, f2, page);
		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Bill: " + billDescSummary.getText(0), textStart, textY, f2, page);
		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Year: " + billDescSummary.getText(1), textStart, textY, f2, page);

		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Term: " + billDescSummary.getText(2), textStart, textY, f2, page);
		    
		    studentBox.drawOn(page);

		    
		    //setup comments box
		    double commentsBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
		    Box commentsBox = new Box(RIGHT_MARGIN, commentsBoxY , table.getWidth(), BOX_COMMENT_INFO_HEIGHT);
		    
		    textY = (int) Math.ceil(commentsBoxY + PADDING_SIZE + REGULAR_FONT_SIZE);
		    printLine("Total Due: " + InetConstants.NUMBER_FORMAT.format(studentBill.getNumber(0)), 
		    		textStart, textY, f2, page);
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Due Date: " + (billDescSummary.getDate(0) == null?"":InetConstants.DATE_FORMAT.format(billDescSummary.getDate(0))), 
		    		textStart, textY, f2, page);		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Amount Paid: " + InetConstants.NUMBER_FORMAT.format(studentBill.getNumber(1)), 
		    		textStart, textY, f2, page);
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    Font fob = studentBill.getText(3).equals("True") ? f2 : f1;
		    printLine("Outstanding Balance: " + InetConstants.NUMBER_FORMAT.format(studentBill.getNumber(0) - studentBill.getNumber(1)), 
		    		textStart, textY, fob, page);  
		    
            commentsBox.drawOn(page);

            //setup official box
            double officialBoxY = commentsBoxY + BOX_COMMENT_INFO_HEIGHT + SPACE_BTW_BOXES;
            Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , table.getWidth(), BOX_OFFICIAL_INFO_HEIGHT);
            TextLine text7 = new TextLine(f2, "Official Name:                                           ");
            double text7Y = officialBoxY + PADDING_SIZE * 2+ REGULAR_FONT_SIZE;
            text7.setPosition(textStart, text7Y);
            text7.setUnderline(true);
            TextLine text8 = new TextLine(f2, "Official Signature:                                      ");
            text8.setUnderline(true);
            text8.setPosition(textStart + table.getWidth()/2, text7Y);
            
            text7.drawOn(page);
            text8.drawOn(page);
            officialBox.drawOn(page);       
        }
    }
    
    private static void printLine(String line, int xPos, int yPos, Font font, Page page) throws Exception
    {
	    TextLine text = new TextLine(font, line);
	    text.setPosition(xPos, yPos);
	    text.drawOn(page);
    }

    public static List<TableMessage>[] getBillingInvoicesObjects(Objectify ofy, Key<InetBillDescription> bdKey,String[] billingKeyStrs, boolean isBillingKey) throws MissingEntitiesException
    {
        HashSet<Key<InetBill>> billKeys = new HashSet<Key<InetBill>>(billingKeyStrs.length);
        
        for(String bkStr : billingKeyStrs)
        {
            String keyStr = bkStr;
        	if(!isBillingKey)
        	{
        		Key<InetStudent> userKey = new Key<InetStudent>(InetStudent.class, bkStr);
        		keyStr = InetBill.getKeyString(bdKey, userKey);
        	}
        	Key<InetBill> sk = new Key<InetBill>(InetBill.class, keyStr);
        	billKeys.add(sk);
        }
        
        Collection<InetBill> billList = InetDAO4CommonReads.getEntities(billKeys, ofy, true).values();
        
        List<TableMessage> billListMessages = AccountManagerImpl.getBillListSummary(billList, true, false, ofy);
        List<TableMessage> billDescMessage = AccountManagerImpl.getBillDescriptionInfo(bdKey);
        List<TableMessage>[] result = new List[2];
        result[0] = billListMessages;
        result[1] = billDescMessage;
        return result;
    }
    
	public static String getBillingInvoicesLink(String billDescKeyStr, String[] billingKeyStrs, 
			HttpServletRequest req)
		throws MissingEntitiesException
	{	
        Objectify ofy = ObjectifyService.begin();
        Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(billDescKeyStr);

        List<TableMessage>[] billingInfo = getBillingInvoicesObjects(ofy, bdKey, billingKeyStrs, true);
        List<TableMessage> billListMessages = billingInfo[0];
        List<TableMessage> billDescMessage = billingInfo[1];        
        HttpSession sess = req.getSession();
        sess.setAttribute(getBillListSessionName(bdKey), billListMessages);
        sess.setAttribute(getBillDescSessionName(bdKey), billDescMessage);
        sess.setAttribute(getSchoolInfoSessionName(bdKey), SchoolServiceImpl.getSchoolInfo(ofy));
        
        return "<a href='" + "http://" + req.getHeader("Host") + "/bill-invoice?" + 
        		InetConstants.BILL_DESC_KEY_PARAM + "=" + billDescKeyStr + "'>Download " + 
        		InetDAO4CommonReads.keyToPrettyString(bdKey) + " invoices for " + 
        		billingKeyStrs.length + " students</a>"; 
        
	}
	
	public static String getBillListSessionName(Key<InetBillDescription> bdKey)
	{
		return InetConstants.SESS_PREFIX + bdKey.getName() + "bioinv";
	}
	
	public static String getBillDescSessionName(Key<InetBillDescription> bdKey)
	{
		return InetConstants.SESS_PREFIX + bdKey.getName() + "tableinv";
	}
	
	public static String getSchoolInfoSessionName(Key<InetBillDescription> bdKey)
	{
		return InetConstants.SESS_PREFIX + bdKey.getName() + "schoolinfoinv";
	}	
}
