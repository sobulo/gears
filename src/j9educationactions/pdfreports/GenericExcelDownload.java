package j9educationactions.pdfreports;

import j9educationentities.InetConstants;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.TableMessageHeader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GenericExcelDownload extends HttpServlet{
    private final static String SESS_PREFIX = "fertiletech.";
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
    	String id = req.getParameter(InetConstants.EXCEL_DOWN_KEY_PARAM);
    	
    	if(id == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unable to process request. Please ensure this page is accessed via a link" +
    				" recently generated from the report cards panel. If problem persists contact IT</font></b>");
    		return;
    	}
    	HttpSession sess = req.getSession();
		String fileName = InetConstants.DATE_FORMAT.format(new Date(Long.valueOf(id)));
		fileName = fileName.replace(" ", "-") + ".xls";    	
    	ArrayList<TableMessage> data = (ArrayList<TableMessage>) sess.getAttribute(getGenericDownloadSessionName(id));
        //download as ssheet to client comp
        ExcelDownloadHelper.doDownload(res, fileName, data);

    }
    
    public static String getGenericExcelDownloadLink(List<TableMessage> data, TableMessageHeader header, HttpServletRequest req)
    {
    	HttpSession sess = req.getSession();
        String id = String.valueOf(new Date().getTime());
        ArrayList<TableMessage> cachedData = new ArrayList<TableMessage>(data.size() + 1);
        cachedData.add(header);
        cachedData.addAll(data);
        sess.setAttribute(getGenericDownloadSessionName(id), cachedData);
        return "<b>DOWNLOAD: </b><a href='" + "http://" + req.getHeader("Host") + "/excel/generic?" + 
        		InetConstants.EXCEL_DOWN_KEY_PARAM + "=" + id + "'>Click here for excel download</a>";         
    }
    
	public static String getGenericDownloadSessionName(String id)
	{
		return SESS_PREFIX + id + "exldown";
	}    
}
