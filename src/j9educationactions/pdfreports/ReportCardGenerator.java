/**
 * 
 */
package j9educationactions.pdfreports;

import j9educationactions.login.LoginPortal;
import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetConstants;
import j9educationentities.InetCourse;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetGradeDescriptions;
import j9educationentities.InetImageBlob;
import j9educationentities.InetLevel;
import j9educationentities.InetStudent;
import j9educationentities.InetTestScores;
import j9educationgwtgui.server.InetObjectDataPopulator;
import j9educationgwtgui.server.InetPopulatorInitializer;
import j9educationgwtgui.server.SchoolServiceImpl;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.Box;
import com.pdfjet.Cell;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.Letter;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Point;
import com.pdfjet.Table;
import com.pdfjet.TextLine;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ReportCardGenerator extends HttpServlet
{
    private final static int RIGHT_MARGIN = 70;
    private final static int TOP_MARGIN = 40;
    private final static int SUBJECT_COL_WIDTH = 150;
    private final static int NOTES_COL_WIDTH = 200;
    private final static int PADDING_SIZE = 5;
    private final static int SPACE_BTW_BOXES = 20;
    private final static int HEADER_FONT_SIZE = 24;
    private final static int REGULAR_FONT_SIZE = 12;
    private final static int SMALL_FONT_SIZE = 9;
 
    private final static int BOX_STUDENT_INFO_HEIGHT = 40;
    private final static int BOX_COMMENT_INFO_HEIGHT = 50;
    private final static int BOX_OFFICIAL_INFO_HEIGHT = 30;
    private final static int BOX_DEFAULT_SCHOOL_HEIGHT = 80;
 
    final static int IDX_SUBJECT_NAME = 0;
    final static int IDX_GRADE = 1;
    final static int IDX_SCORE = 2;
    
    public final static String NOT_PUBLISHED_MESSAGE = "NOT PUBLISHED";
    
    private static final Logger log = Logger.getLogger(ReportCardGenerator.class.getName());
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
    	Objectify ofy = ObjectifyService.begin();
    	String levelKeyStr = req.getParameter(InetConstants.LEVEL_KEY_PARAM);
    	if(levelKeyStr == null)
    		throw new IllegalArgumentException("Unable to process request because level parameter not found. " +
				"If problem persists contact IT");   	
    	Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);            
		String fileName = InetDAO4CommonReads.getNameFromLastKeyPart(levelKey, 2);
		fileName = fileName.replace(" ", "");
		fileName += "-report-cards.pdf";

		res.setContentType("application/octet-stream");
        res.setHeader("Content-disposition", "attachment; filename=" + fileName);
        OutputStream out = res.getOutputStream();        
        try
        {    	
        	writePDFToOutputStream(levelKey, ofy, out, req);
        }
        catch(LoginValidationException e) 
		{
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
					"</b></body></html>");
			return;
		}      
    }
    
    public static byte[] getReportCardData(String levelKeyStr, String[] studentLoginNames)
    {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	Objectify ofy = ObjectifyService.begin();
    	Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
    
    	//TODO, this part is quite inefficient, share data better possibly, then again is it that inefficient?
    	//so long as we're not loading numerous student objects, the price of loading a inet level/tests probably isn't
    	//much? or is it? ha, if you're reading this and you care? run some performance tests
    	try
    	{
	    	InetReportCardSingleClassPopulator rcInfo = getClassReportCards(ofy, levelKey, studentLoginNames, true, false);
	    	writePDFToOutputStream(levelKey, ofy, out, rcInfo.getStudentNames(), rcInfo.studentScores, SchoolServiceImpl.getSchoolInfo(ofy));
    	}
    	catch(Exception ex)
    	{
    		log.severe(ex.getMessage());
    		return null;
    	}
    	return out.toByteArray();
    }
    
    public static String[] getReportCardSummarry(String levelKeyStr, String[] loginNames)
    {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	Objectify ofy = ObjectifyService.begin();
    	Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);    	
    	InetReportCardSingleClassPopulator rcInfo;
		try {
			rcInfo = getClassReportCards(ofy, levelKey, loginNames, true, false);
		} catch (MissingEntitiesException e) {
			log.severe(e.getMessage());
			return null;
		}
    	List<List<String[]>> allStudScores = rcInfo.getStudentData();
    	List<String> studentNames = rcInfo.getStudentNames();
    	String[] result = new String[allStudScores.size()];
    	final int subjectMax = 5;
    	for(int i = 0; i < allStudScores.size(); i++)
    	{
    		List<String[]> studScores = allStudScores.get(i);
    		StringBuilder summaryMessage = new StringBuilder();
    		summaryMessage.append(studentNames.get(i)).append("'s grades are now available on school website. ");
			for(int j = 0; j < studScores.size(); j++)
			{
				String[] courseScores = studScores.get(j);
				if(courseScores.length != 3)
				{
					log.severe("Non cumulative mode currently isn't supported");
					return null;
				}

				summaryMessage.append(courseScores[IDX_SUBJECT_NAME].substring(0, subjectMax)).append(" ");
				if(courseScores[IDX_GRADE] == null || courseScores[IDX_GRADE].length() == 0)
					summaryMessage.append(courseScores[IDX_SCORE]);
				else
					summaryMessage.append(courseScores[IDX_GRADE]);
				summaryMessage.append(", ");
			}
			result[i] = summaryMessage.toString();
    	}
    	return result;
    }
    
    private void writePDFToOutputStream(Key<InetLevel> levelKey, Objectify ofy, OutputStream out, HttpServletRequest req) throws LoginValidationException
    {
		PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
		LoginPortal.verifyRole(allowedRoles, req);    	
    	HttpSession sess = req.getSession();
    	List<String> studentNames = (List<String>) sess.getAttribute(getStudentBioSessionName(levelKey));
    	List<List<String[]>> reportData = (List<List<String[]>>) sess.getAttribute(getReportDataSessionName(levelKey));
    	String[] schoolInfo = (String[]) sess.getAttribute(getSchoolInfoSessionName(levelKey));    	
    	writePDFToOutputStream(levelKey, ofy, out, studentNames, reportData, schoolInfo);
    }
    
    private static void writePDFToOutputStream(Key<InetLevel> levelKey, Objectify ofy, OutputStream out, 
    		List<String> studentNames, List<List<String[]>> reportData, String[] schoolInfo) 
    	throws LoginValidationException
    {	
    	if(reportData == null || studentNames == null || schoolInfo == null)
    		throw new IllegalArgumentException("Unable to retrieve sesison data. If problem " +
    				"persists contact IT</font></b>");

    	
		Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, InetConstants.SCHOOL_REPORT_CARD_IMAGE);
		InetImageBlob logo = ofy.find(logoKey);
		if(logo == null)
			throw new IllegalArgumentException("Unable to load report header image for school." +
					" Please contact IT to investigate further");

        PDF pdf;
    	
    	//convert data to format experted by print report fn
    	List<List<Cell>>[] tableData = new List[reportData.size()];
    	String[] names = new String[reportData.size()];
    	
    	//init pdf file
    	try
    	{
	        pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(REGULAR_FONT_SIZE);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(REGULAR_FONT_SIZE);
	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	for(int i = 0; i < names.length; i++)
	    	{
	    		names[i] = studentNames.get(i);
	    		List<List<Cell>> studentReport = getPDFRawData(i, reportData, f1, f2);
	    		tableData[i] = studentReport;
	    	}
	    	printReportCards(tableData, names, InetDAO4CommonReads.keyToPrettyString(levelKey), schoolInfo, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("j9"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}
    }
    
    private static List<List<Cell>> getPDFRawData(int dataIndex, List<List<String[]>> reportData, Font f1, Font f2)
    {
		List<String[]> studScores = reportData.get(dataIndex);
		int scoreSize = studScores.size();
		List<List<Cell>> studentReport = new ArrayList<List<Cell>>(scoreSize);
		
		//TODO ... this breaks if student data not obtained via cumulative only mode
		List<Cell> headerRow = new ArrayList<Cell>(3);
		headerRow.add(new Cell(f1, "Subject"));
		headerRow.add(new Cell(f1, "Total %"));
		headerRow.add(new Cell(f1, "Grade"));
		headerRow.add(new Cell(f1, "Notes")); //just a blank column for now
		studentReport.add(headerRow);
		
		for(int j = 0; j < scoreSize; j++)
		{
			
			String[] courseScores = studScores.get(j);
			if(courseScores.length != 3)
				throw new IllegalArgumentException("Non cumulative mode currently isn't supported");
			List<Cell> studentReportRow = new ArrayList<Cell>(courseScores.length);
			
			studentReportRow.add(new Cell(f2, courseScores[IDX_SUBJECT_NAME]));
			studentReportRow.add(new Cell(f2, courseScores[IDX_SCORE]));
			studentReportRow.add(new Cell(f2, courseScores[IDX_GRADE]));
			studentReportRow.add(new Cell(f2, " ")); //just a blank column for now
			
			/*//loop setup will/should make sense for non-cumulative mode once that's supported
			for(int k = 0; k < courseScores.length; k++)
				studentReportRow.add(new Cell(f2, courseScores[k]));*/		
			
			studentReport.add(studentReportRow);
		}
		return studentReport;
    }
    
    private static void printReportCards(List<List<Cell>>[] studentResults, String[] studentNames,
    		String className, String[] schoolInfo, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        logo.scaleBy(0.75);
        
        final int BOX_SCHOOL_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_SCHOOL_HEIGHT);
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;
        
        int studentBoxY = TOP_MARGIN + BOX_SCHOOL_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_STUDENT_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        
        for(int i = 0; i < studentResults.length; i++)
        {
		    //new page for student report card		    
		    Page page = new Page(pdf, Letter.PORTRAIT);
		    
		    //setup table
		    Table table = new Table(f1, f2);
		    table.setData(studentResults[i], Table.DATA_HAS_1_HEADER_ROWS);
		    table.setLineWidth(0.2);
		    table.setPosition(RIGHT_MARGIN, tableY);
		    table.setCellPadding(PADDING_SIZE);
		    table.autoAdjustColumnWidths();
		    table.setColumnWidth(0, SUBJECT_COL_WIDTH);
		    table.setColumnWidth(3, NOTES_COL_WIDTH);
		    table.rightAlignNumbers();
		    int numOfPages = table.getNumberOfPages(page);
		    if(numOfPages != 1)
		    	throw new IllegalArgumentException("Grade Data too Large. Contact IT to setup custom report for your class");
		    
		    Point tableEnd = table.drawOn(page);
		    
		    //setup school info
		    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, table.getWidth(), BOX_SCHOOL_INFO_HEIGHT);
		    TextLine text = new TextLine(f3, schoolInfo[PanelServiceConstants.SCHOOL_INFO_NAME_IDX]);
		    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
		    text.setPosition(textStart, textY);
		    
		    TextLine text1 = new TextLine(f4, schoolInfo[PanelServiceConstants.SCHOOL_INFO_ADDR_IDX] );
		    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
		    text1.setPosition(textStart, text1Y);
		    
		    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[PanelServiceConstants.SCHOOL_INFO_NUMS_IDX]);
		    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2.setPosition(textStart, text2Y);
		    
		    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[PanelServiceConstants.SCHOOL_INFO_WEB_IDX] + "   Email: " + schoolInfo[PanelServiceConstants.SCHOOL_INFO_EMAIL_IDX]);
		    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2b.setPosition(textStart, text2bY);		    
	    
		    logo.setPosition(RIGHT_MARGIN + table.getWidth() - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + PADDING_SIZE);
		    
		    System.out.println("Height: " + logo.getHeight());
		    
		    schoolInfoBox.drawOn(page);
		    text.drawOn(page);
		    text1.drawOn(page);
		    text2.drawOn(page);
		    text2b.drawOn(page, true);
		    logo.drawOn(page);
		    
		    //setup student info
		    Box studentBox = new Box(RIGHT_MARGIN, studentBoxY , table.getWidth(), BOX_STUDENT_INFO_HEIGHT);
		    int text3Y = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    TextLine text3 = new TextLine(f2, "Name: " + studentNames[i]);
		    text3.setPosition(textStart, text3Y);
		    
		    TextLine text4 = new TextLine(f2, "Class: " + className);
		    int text4Y = text3Y + PADDING_SIZE + REGULAR_FONT_SIZE;
		    text4.setPosition(textStart, text4Y);
		    
		    studentBox.drawOn(page);
		    text3.drawOn(page);
		    text4.drawOn(page);
		    
		    //setup comments box
		    double commentsBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
		    Box commentsBox = new Box(RIGHT_MARGIN, commentsBoxY , table.getWidth(), BOX_COMMENT_INFO_HEIGHT);
		    double text5Y = commentsBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;		    
            TextLine text5 = new TextLine(f2, "Comments: ");
            text5.setPosition(textStart, text5Y);
            
            commentsBox.drawOn(page);
            text5.drawOn(page);

            //setup official box
            double officialBoxY = commentsBoxY + BOX_COMMENT_INFO_HEIGHT + SPACE_BTW_BOXES;
            Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , table.getWidth(), BOX_OFFICIAL_INFO_HEIGHT);
            TextLine text7 = new TextLine(f2, "Official Name:                                           ");
            double text7Y = officialBoxY + PADDING_SIZE * 2+ REGULAR_FONT_SIZE;
            text7.setPosition(textStart, text7Y);
            text7.setUnderline(true);
            TextLine text8 = new TextLine(f2, "Official Signature:                                      ");
            text8.setUnderline(true);
            text8.setPosition(textStart + table.getWidth()/2, text7Y);
            
            text7.drawOn(page);
            text8.drawOn(page);
            officialBox.drawOn(page);       
        }
    }
    
    private static class InetReportCardSingleClassPopulator implements InetObjectDataPopulator, InetPopulatorInitializer
    {
        List<List<String[]>> studentScores;
        List<String> studentNames;
        Map<Key<InetTestScores>, InetTestScores>  testScores;
        Collection<InetCourse> courseList;
        private InetGradeDescriptions gradeDesc;
        boolean cumulativeOnly;
        

        final NumberFormat numberFormat = NumberFormat.getInstance();

        InetReportCardSingleClassPopulator(boolean cumulativeOnly)
        {
        	this.cumulativeOnly = cumulativeOnly;
            studentScores = new ArrayList<List<String[]>>(InetConstants.DEFAULT_NUM_OF_STUDENTS_PER_COURSE);
            studentNames = new ArrayList<String>(InetConstants.DEFAULT_NUM_OF_STUDENTS_PER_COURSE);
            numberFormat.setMaximumFractionDigits(2);
            numberFormat.setRoundingMode(RoundingMode.HALF_UP);
        }

        @Override
        public void populateInfo(GAEPrimaryKeyEntity[] studentWrapper)
        {
            InetStudent student = (InetStudent) studentWrapper[0];
            List<String[]> studScore = new ArrayList<String[]>(courseList.size());
            
            for(InetCourse course : courseList)
            {
            	String[] data;
            	String grade = "";
            	Double score = null;
            	if(cumulativeOnly)
            	{
            		InetTestScores ts = testScores.get(course.getCumulativeScores());
            		score = ts.getTestScore(student.getKey());
            		if(!ts.isPublished()) throw new IllegalArgumentException(NOT_PUBLISHED_MESSAGE + " " + course.getCourseName());
                    
            		if(score == null) continue;
                    
                    if(gradeDesc == null)
                    	grade = "N/A";
                    else
                    	grade = gradeDesc.getLetterGrade(score);
                 
                    data = new String[3];
                    data[IDX_SCORE] = numberFormat.format(score);
            	}
            	else
            	{
            		List<Key<? extends InetTestScores>> tKeys = course.getAllTestKeys();
            		int scoreIndex = IDX_SCORE;
            		data = new String[2 + tKeys.size()];
            		for(Key<? extends InetTestScores> tk : tKeys)
            		{
            			InetTestScores ts = testScores.get(tk);
            			if(!ts.isPublished()) throw new IllegalArgumentException(NOT_PUBLISHED_MESSAGE + " " + course.getCourseName());
            			score = ts.getTestScore(student.getKey());
            			if(score == null) break;
            			data[scoreIndex++] = numberFormat.format(score);
            		}
            		
            		if(score == null) continue;
            	}
            	
            	if(gradeDesc == null)
            		grade = "N/A";
            	else
            		grade = gradeDesc.getLetterGrade(score);
            	
                data[IDX_SUBJECT_NAME] = course.getCourseName();
                data[IDX_GRADE] = grade;
                studScore.add(data);
            }
            studentScores.add(studScore);
            studentNames.add(student.getLname() + ", " + student.getFname());
        }

        public List<List<String[]>> getStudentData() {
            return studentScores;
        }
        
        public List<String> getStudentNames()
        {
        	return studentNames;
        }

        @Override
        public void init(GAEPrimaryKeyEntity[] dataContainer, Objectify ofy)
                throws MissingEntitiesException
        {
            //intialize grade description
            gradeDesc = InetDAO4CommonReads.getSchool(ofy).getDefaultGradingScheme();

            //fetch test scores and their header names
            InetLevel level = (InetLevel) dataContainer[0];
            courseList = InetDAO4CommonReads.getEntities(level.getCourseList(), ofy, true).values();
            ArrayList<Key<? extends InetTestScores>> testKeys = new ArrayList<Key<? extends InetTestScores>>();
            for(InetCourse course : courseList)
            {
            	if(cumulativeOnly)
            		testKeys.add(course.getCumulativeScores());
            	else
            		testKeys.addAll(course.getAllTestKeys());
            }            
            testScores = InetDAO4CommonReads.getMixedEntities(testKeys, ofy, true);            
        }
    }
    
    private static InetReportCardSingleClassPopulator getClassReportCards(Objectify ofy, Key<InetLevel> levelKey, String[] studentKeyStrs, 
			boolean cumulativeOnly, boolean isKeyStr) throws MissingEntitiesException
	{
        LinkedHashSet<Key<InetStudent>> studKeys = new LinkedHashSet<Key<InetStudent>>(studentKeyStrs.length);
        
        for(String skStr : studentKeyStrs)
        {
        	
        	Key<InetStudent> sk = null;
        	if(isKeyStr)
        		sk = ofy.getFactory().stringToKey(skStr);
        	else
        		sk = new Key<InetStudent>(InetStudent.class, skStr);
        	studKeys.add(sk);
        }
        
        InetReportCardSingleClassPopulator resultPopulator = new InetReportCardSingleClassPopulator(cumulativeOnly);
        
        try
        {
        	InetDAO4CommonReads.getClassRosterInfo(levelKey, resultPopulator, studKeys, ofy);
        }
        catch(IllegalArgumentException ex)
        {
        	if(ex.getMessage().startsWith(NOT_PUBLISHED_MESSAGE))
        	{
        		throw new MissingEntitiesException(ex.getMessage());
        	}
        }
        return resultPopulator;
	}
    
	public static String getClassReportCards(String levelKeyStr, String[] studentKeyStrs, 
			HttpServletRequest req, boolean cumulativeOnly, boolean isKeyStr)
		throws MissingEntitiesException, LoginValidationException
	{	
        Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        InetReportCardSingleClassPopulator resultPopulator = getClassReportCards(ofy, levelKey, studentKeyStrs, cumulativeOnly, isKeyStr);
        
        HttpSession sess = req.getSession();
        sess.setAttribute(getStudentBioSessionName(levelKey), resultPopulator.getStudentNames());
        sess.setAttribute(getReportDataSessionName(levelKey), resultPopulator.getStudentData());
        sess.setAttribute(getSchoolInfoSessionName(levelKey), SchoolServiceImpl.getSchoolInfo(ofy));
        
        return "<a href='" + "http://" + req.getHeader("Host") + "/reportcards?" + 
        		InetConstants.LEVEL_KEY_PARAM + "=" + levelKeyStr + "'>Download " + 
        		InetDAO4CommonReads.keyToPrettyString(levelKey) + " report cards for " + 
        		studentKeyStrs.length + " students"; 
        
	}
	
	private static String getStudentBioSessionName(Key<InetLevel> levelKey)
	{
		return InetConstants.SESS_PREFIX + levelKey.getName() + "biorptcd";
	}
	
	private static String getReportDataSessionName(Key<InetLevel> levelKey)
	{
		return InetConstants.SESS_PREFIX + levelKey.getName() + "tablerptcd";
	}
	
	private static String getSchoolInfoSessionName(Key<InetLevel> levelKey)
	{
		return InetConstants.SESS_PREFIX + levelKey.getName() + "schoolinforptcd";
	}	
}
