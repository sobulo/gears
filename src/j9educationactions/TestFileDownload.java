/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class TestFileDownload extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
      	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
    	try 
    	{
        	LoginPortal.verifyRole(allowedRoles, req);
        }
    	catch(LoginValidationException ex)
    	{
    		throw new RuntimeException(ex.fillInStackTrace());
    	}
    	
        res.setContentType("application/vnd.ms-excel");
        //res.setContentType("application/octet-stream");
        res.setHeader("Content-disposition", "attachment; filename=sample.xls");

        try
        {
            WritableWorkbook w = Workbook.createWorkbook(res.getOutputStream());
            WritableSheet s = w.createSheet("example", 0);
            WritableCellFormat headerFormat = new WritableCellFormat(new
                    WritableFont(WritableFont.ARIAL, WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD));

            //setup headers
            Label header1 = new Label(0, 0, "Name", headerFormat);
            Label header2 = new Label(1,0, "Amount", headerFormat);
            Label header3 = new Label(2, 0, "DOB", headerFormat);

            s.addCell(header1);
            s.addCell(header2);
            s.addCell(header3);

            //add sample content
            s.addCell(new Label(0,1,"Segun"));
            s.addCell(new jxl.write.Number(1,1, 20));
            s.addCell(new DateTime(2, 1, new Date()));

            s.addCell(new Label(0,2,"Lekan"));
            s.addCell(new jxl.write.Number(1,2, 40));
            s.addCell(new DateTime(2, 2, new Date(79, 11, 23)));

            w.write();
            w.close();
        }
        catch (WriteException ex)
        {
                throw new RuntimeException(ex.fillInStackTrace());
        }
    }
}
