/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.InetConstants;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetLevel;
import j9educationgwtgui.server.ScoreManagerImpl;
import j9educationgwtgui.server.ScoreManagerImpl.InetClassGradesStruct;
import j9educationgwtgui.server.ScoreManagerImpl.StudentNameStruct;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.GeneralFuncs;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.fertiletech.utils.NumberColumnAccessor;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkClassGradesUpload extends HttpServlet {
    private static final Logger log =
            Logger.getLogger(TestFileUpload.class.getName());

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException 
   {
    	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
        res.setContentType("text/html");
        ServletOutputStream out = res.getOutputStream(); 
        log.warning("Processing bulk grade upload file");
        
        try {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) 
		{
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
			"</b></body></html>");
			return;
		}
        
        Objectify ofy = ObjectifyService.begin();

        if (req.getParameter(PanelServiceConstants.LOAD_PARAM_NAME) != null) {
            Object scoresSessObj = req.getSession().getAttribute(InetConstants.USESS_CLASS_GRADE_UPLD_DATA_NAME);
            Object levelKeySessObj = req.getSession().getAttribute(InetConstants.USESS_CLASS_GRADE_UPLD_LEVEL_KEY);
            if (scoresSessObj != null && levelKeySessObj != null) {
                @SuppressWarnings("unchecked")
				Key<InetLevel> levelKey = (Key<InetLevel>) levelKeySessObj;
				InetClassGradesStruct scores = (InetClassGradesStruct) scoresSessObj;
                
                //do some work here to save ... going the route of tasks for now
                TaskQueueHelper.scheduleClassScoreUpdate(scores);
                out.println("<p>Scheduled update scores for " + InetDAO4CommonReads.keyToPrettyString(levelKey) + "</p>");
                out.println("<p>Please check scores within a few minutes to ensure update processed successfully</p>");

            } else {
                out.println("<b>unable to retrieve session info. Try " +
                        "enabling cookies in your browser.</b>");
            }
            req.getSession().setAttribute(InetConstants.USESS_CLASS_GRADE_UPLD_DATA_NAME, null);
            req.getSession().setAttribute(InetConstants.USESS_CLASS_GRADE_UPLD_LEVEL_KEY, null);

        } else {
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iterator = upload.getItemIterator(req);
                InetLevel level;
                if(!iterator.hasNext())
                {
                    log.severe("no param found, expected class key");
                    throw new IllegalStateException("no param found for class key");
                }
                FileItemStream item = iterator.next();
                InputStream stream = item.openStream();
                //get testKey
                Key<InetLevel> levelKey = null;
                if (item.isFormField() && item.getFieldName().equals(PanelServiceConstants.CLASSKEY_PARAM_NAME)) {
                    levelKey = ofy.getFactory().stringToKey(Streams.asString(stream));
                    log.warning("retrieved key: " + levelKey);
                    level = ofy.find(levelKey);
                    
                    if (level == null) {
                        out.println("<b>unable to find level, " + levelKey.getName() + ", refresh browser and try again</b>");
                        log.severe("missing test: [" + levelKey + "]");
                        return;
                    }
                    req.getSession().setAttribute(InetConstants.USESS_CLASS_GRADE_UPLD_LEVEL_KEY, levelKey);
                } else {
                    out.println("<b>bad request, unable to detect selected class</b>");
                    log.severe("security issue: user sent input that's not a levelkey: [" +item.getFieldName() + "]") ;
                    return;
                }

                //get excel file
                if(!iterator.hasNext())
                {
                    log.severe("no param found, expected file contents");
                    throw new IllegalStateException("no param found for file data");
                }
                item = iterator.next();
                stream = item.openStream();
                ArrayList<ColumnAccessor> headerAccessorsList = new ArrayList<ColumnAccessor>();
                for(int i = 0; i < InetConstants.CLASS_GRADE_UPLOAD_ACCESSORS.length; i++)
                	headerAccessorsList.add(InetConstants.CLASS_GRADE_UPLOAD_ACCESSORS[i]); 
                
                //load grades for class to help in validating ssheet input
                InetClassGradesStruct levelGrades;
				try {
					levelGrades = ScoreManagerImpl.getClassScores(level.getKey(), ofy);
				} catch (MissingEntitiesException e) {
                    out.println("<b>unable to load level grades, " + level.getKey().getName() + ", refresh browser and try again</b>");
                    log.severe("missing level grades: [" + level.getKey() + "]");
                    log.severe(e.getMessage());
                    return;
				}
  
                //load excel spread sheet
                ExcelManager sheetManager = new ExcelManager(stream);
                
                //fetch group names
                boolean noErrors = true;
                String[] ignoredSubHeaders = {InetConstants.GRADE_HEADER, InetConstants.CUMULATIVE_TEST_NAME + InetConstants.PERCENT_SIGN};
                LinkedHashMap<String, String[]> tempGroupedHeaders = sheetManager.getGroupedHeaders(ignoredSubHeaders);
                LinkedHashMap<String, String[]> groupedHeaders = new LinkedHashMap<String, String[]>(tempGroupedHeaders.size());
                for(String grpName : tempGroupedHeaders.keySet())
                	groupedHeaders.put(grpName.toLowerCase(), tempGroupedHeaders.get(grpName));
                
                String[] bioHeaders = groupedHeaders.get(InetConstants.BIO_GROUP_HEADER.toLowerCase());
                
                if(bioHeaders == null)
                {
                	out.println("<li>Spreadsheet is missing:  " + InetConstants.BIO_GROUP_HEADER + " group header</li>");
                	noErrors = false;
                }
                else
                {
                	String[] expectedBioHeaders = ColumnAccessor.getAccessorNames(headerAccessorsList.toArray(new ColumnAccessor[headerAccessorsList.size()]));            	
                	noErrors = noErrors & printCompareHeaders(InetConstants.BIO_GROUP_HEADER, expectedBioHeaders, bioHeaders, out, false);
                }
                groupedHeaders.remove(InetConstants.BIO_GROUP_HEADER.toLowerCase()); //we've compared this already
                
                for(String groupName : groupedHeaders.keySet())
                {
                	if(!levelGrades.testNames.containsKey(groupName))
                	{
                    	out.println("<li>Spreadsheet contains group header:  " + groupName + " which does not match any subject in class: " + 
                    			InetDAO4CommonReads.keyToPrettyString(level.getKey()) + "</li>");
                    	noErrors = false;                		
                	}
                	else
                	{
                		log.warning("About to compare group: " + groupName);
                		log.warning("ssheet: " + Arrays.toString(groupedHeaders.get(groupName)));
                		log.warning("expected: " + Arrays.toString(levelGrades.testNames.get(groupName)));
                		noErrors = noErrors & printCompareHeaders(groupName, levelGrades.testNames.get(groupName), 
                				groupedHeaders.get(groupName), out, true);
                	}
                }                
                
                
                if(!noErrors)
                {
                	out.println("<font color='red'>Please fix errors highlighted above then reupload spread-sheet</font><br/>");
                	return;
                }
                
                //could probably have used an arraylist for testnames, below just makes it easier when calling fetchColVal to avoid searching for test name idx on each call
                HashMap<String, LinkedHashMap<String, Integer>> testNames = new HashMap<String, LinkedHashMap<String, Integer>>();
                HashMap<String, HashMap<String, Double>> maxScores = new HashMap<String, HashMap<String, Double>>();
                InetClassGradesStruct studentData = new InetClassGradesStruct();
                //initialize accessor list
                for(String groupName : groupedHeaders.keySet())
                {
                	testNames.put(groupName, new LinkedHashMap<String, Integer>());
                	maxScores.put(groupName, new HashMap<String, Double>());
                	String[] testKeys = new String[groupedHeaders.get(groupName).length];
                	if(testKeys.length == 0) continue;
                	int testCount = 0;
                	for(String headerName : groupedHeaders.get(groupName))
                	{                		
                		//check that testnames in ssheet are also in grep system
                		int scoreIdx = -1;
                		String[] storedTestNames = levelGrades.testNames.get(groupName);
                		for(int i = 0; i < storedTestNames.length && scoreIdx < 0; i++)
                		{
                			//log.warning(i + ": " + storedTestNames[i]);
                			if(storedTestNames[i].equalsIgnoreCase(headerName))
                				scoreIdx = i;
                		}                		
                		
                		NumberColumnAccessor accessor = new NumberColumnAccessor(headerName, false);
                		accessor.setGroupName(groupName);
                		headerAccessorsList.add(accessor);
                		testNames.get(groupName).put(headerName, testCount);
                		
                		if(scoreIdx == -1)
                			throw new IllegalStateException("Unable to find test: " + 
                					headerName + " even though header checks passed");
                		
                		Double maxScr = levelGrades.maxScores.get(groupName)[scoreIdx];
                		maxScores.get(groupName).put(headerName, maxScr);
                		testKeys[testCount] = levelGrades.testKeys.get(groupName)[scoreIdx];
                		testCount++;
                		//log.warning("Group " + groupName + " Test name: " + headerName + " Max: " + maxScr);
                	}
                	
                	Set<String> temp = testNames.get(groupName).keySet();
                	studentData.testNames.put(groupName, temp.toArray(new String[temp.size()]));
                	studentData.testKeys.put(groupName, testKeys);
                	studentData.grades.put(groupName, new HashMap<String, Double[]>());
                }

                ColumnAccessor[] headerAccessors = headerAccessorsList.toArray(new ColumnAccessor[headerAccessorsList.size()]);
                sheetManager.initializeAccessorList(headerAccessors);
                String goodDataHTML = "";
                String badDataHTML = "";
                
                for (int i = 2; i < sheetManager.totalRows(); i++) {
                    String row = "<tr>";
                    GradeStruct gradeData = new GradeStruct();
                    try
                    {
	                    for (int j = 0; j < headerAccessors.length; j++) {
	                        String tempVal = fetchColVal(i, headerAccessors[j], sheetManager, gradeData, testNames, maxScores);
	                        row += "<td>" + tempVal + "</td>";
	                    }
	
	                    if(gradeData.exceptionMessage.length() == 0 && (addStudentData(levelGrades, studentData, gradeData))) {
	                        row += "<td>Passed prelim checks</td></tr>\n";
	                        goodDataHTML += row;
	
	                    } else {
	                        row += "<td>" + gradeData.exceptionMessage + "</td></tr>\n";
	                        badDataHTML += row;
	                    }
                    }
                    catch(EmptyColumnValueException ex){} //helps skip blank lines
                }

                //table header
                String htmlTableStart = "<tr>";
                String htmlGroupHeader = "<TABLE class='themePaddedBorder' border='1'><tr>";
                String currentGroup = headerAccessors[0].getGroupName();
                int groupCount = 0;
                for (int i = 0; i < headerAccessors.length; i++) 
                {
                    htmlTableStart += "<TH>" + headerAccessors[i].getHeaderName() + "</TH>";
                    if(!currentGroup.equals(headerAccessors[i].getGroupName()))
                    {
                    	htmlGroupHeader += "<TH colspan='" + groupCount + "'>" + currentGroup + "</TH>";
                    	groupCount = 0;
                    	currentGroup = headerAccessors[i].getGroupName();
                    }
                    groupCount++;
                }
                htmlTableStart += "<TH width='30%'>Messages</TH></tr>";
                htmlGroupHeader += "<TH colspan='" + groupCount + "'>" + currentGroup + "</TH><TH width='30%'>Status</TH></tr>";
                htmlTableStart = htmlGroupHeader + htmlTableStart;
                String htmlTableEnd = "</TABLE>";

                if (goodDataHTML.length() > 0) {
                    out.println(PanelServiceConstants.GOOD_DATA_PREFIX);
                    goodDataHTML = "<p><b>Below shows data that passed preliminary checks. Hit save button below to store class grades for: " +
                    		"<font color='green'>" + InetDAO4CommonReads.keyToPrettyString(levelKey) +
                            "</font>. To abort upload click on load new file button.</b></p>"
                            + htmlTableStart + goodDataHTML + htmlTableEnd;
                }

                if (badDataHTML.length() > 0) {
                    out.println("<b><font color='red'>Below shows records with errors</font></b><br/>");
                    out.println(htmlTableStart);
                    out.print(badDataHTML);
                    out.println(htmlTableEnd);
                }

                out.println(goodDataHTML);

                studentData.classRoster = null; //don't need this anymore
                req.getSession().setAttribute(InetConstants.USESS_CLASS_GRADE_UPLD_DATA_NAME,
                        studentData);

            } catch (FileUploadException ex) {
                log.severe(ex.getMessage());
                out.println("<b>File upload failed</b>, exception is: <p>" + ex.getMessage() +
                        "</p>");
            } catch (InvalidColumnValueException ex) {
                log.severe(ex.getMessage());
                out.println("<b>Unable to read excel sheet headers</b><br/> Exception was: " + ex.getMessage());
            }
        }
    }

    private String fetchColVal(int row, ColumnAccessor accessor, ExcelManager sheetManager, 
    		GradeStruct gradeData, HashMap<String, LinkedHashMap<String, Integer>> testNameDict, 
    		HashMap<String, HashMap<String, Double>> maxScoreDict) 
    			throws EmptyColumnValueException, InvalidColumnValueException 
    {
        Object val = "";
        String headerName = accessor.getHeaderName();
        String groupName = accessor.getGroupName();
        try {
            val = sheetManager.getData(row, accessor);
            //or should we use int ids instead of slower string comps below?
            if (groupName.equals(InetConstants.BIO_GROUP_HEADER) && headerName.equals(InetConstants.GRD_FNAME_HEADER)) {
                gradeData.fname = (String) val;
            } else if (groupName.equals(InetConstants.BIO_GROUP_HEADER) && headerName.equals(InetConstants.GRD_LNAME_HEADER)) {
                gradeData.lname = (String) val;
            } else if (groupName.equals(InetConstants.BIO_GROUP_HEADER) && headerName.equals(InetConstants.GRD_LOGIN_HEADER)) {
                gradeData.loginName = ((String) val).toLowerCase();
            } else {
            	int testNameIdx = testNameDict.get(groupName).get(headerName);
                Double[] grades = gradeData.score.get(groupName);
                if(grades == null)
                {
                	grades = new Double[testNameDict.get(groupName).size()];
                }
                Double temp = null;
                if(val != null)
                {
                	temp = (Double) val;
                	Double max = maxScoreDict.get(groupName).get(headerName);
                	if(temp < 0 || temp > max)
                		throw new InvalidColumnValueException(groupName + " -- " + headerName + " col has a value less than 0 or greater than max: " + max);
                }
                grades[testNameIdx] = temp;
                gradeData.score.put(groupName, grades);
            }
        } 
    	catch(EmptyColumnValueException subEx)
    	{
    		if(sheetManager.isBlankRow(row))
    			throw subEx;
    		gradeData.exceptionMessage += "<li>" + subEx.getMessage() + "</li>";
    	}        
        catch (InvalidColumnValueException ex) 
        {
            gradeData.exceptionMessage += "<li>" + ex.getMessage() + "</li>";
        }catch(NumberFormatException ex) {
        	//TODO perhaps this should be handled by excel manager to begin with
        	gradeData.exceptionMessage += "<li>" + ex.getMessage() + "</li>";
        }
        return val == null? "" : val.toString();
    }

    private boolean addStudentData(InetClassGradesStruct template, 
    		InetClassGradesStruct studentData, GradeStruct gradeData) 
    {
        Set<String> courseNames = gradeData.score.keySet();
        boolean addedRecord = false;
        for(String course : courseNames)
        {
        	
        	//check if all nulls
        	Double[] scores = gradeData.score.get(course);
        	log.warning("checking course: " + course + " -- " + Arrays.toString(scores) + " -- " + gradeData.loginName);
        	boolean allNulls = true;
        	boolean foundNull = false;
        	for(int i = 0; i < scores.length; i++)
        	{
        		if(scores[i] != null)
        			allNulls = false;
        		else
        			foundNull = true;
        	}
        	
        	if(!allNulls)
        	{
        		StudentNameStruct studNames = template.classRoster.get(gradeData.loginName);
        		//log.warning("Checking key: " + gradeData.loginName);
        		if(foundNull)
        		{
                    gradeData.exceptionMessage += "<li>subject " + course +
                    " has a test with no score entered for student " + gradeData.loginName + ". All tests under a particular subject must either have a score entered or not. i.e. Student is either registered for the subject or not</li>";
                    return false;        			
        		}
        		else if(!template.grades.get(course).containsKey(gradeData.loginName))
        		{
                    gradeData.exceptionMessage += "<li>student " + gradeData.loginName +
                    " not registered for subject " + course + "</li>";
                    return false;
        		}
        		else if(!studNames.fname.equalsIgnoreCase(gradeData.fname) || !studNames.lname.equalsIgnoreCase(gradeData.lname))
        		{
        			gradeData.exceptionMessage += "<li>Student " + gradeData.loginName + " has name: " + studNames.lname + 
        			", " + studNames.fname + " but spread sheet has " + gradeData.lname + ", " + gradeData.fname + "</li>";
        			return false;
        		}
        		else
        		{
        			studentData.grades.get(course).put(gradeData.loginName, gradeData.score.get(course));
        			addedRecord = true;
        		}
        	}
        }
        
        if(!addedRecord)
        {
        	gradeData.exceptionMessage += "<li>student " + gradeData.loginName + 
        		" is listed under bio column but has no data entered for any test</li>";
        	return false;
        }
        
        return true;
    }

    private void printHtmlList(String[] list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }

    public void printHtmlList(List<String> list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }
    
    public boolean printCompareHeaders(String groupName, String expected[], 
    		String[] sheet, ServletOutputStream out, boolean subsetOk) throws IOException
    {
    	log.warning("bout to do a comparison");
        ArrayList<String> sheetOnly = new ArrayList<String>();
        ArrayList<String> intersect = new ArrayList<String>();
        ArrayList<String> expectedOnly = new ArrayList<String>();
        
        String[] sheetHeaders = Arrays.copyOf(sheet, sheet.length);
        String[] expectedHeaders = Arrays.copyOf(expected, expected.length);
        
        //convert both arrarys to lowercase prior to diff
        for(int i = 0; i < expectedHeaders.length; i++)
        	expectedHeaders[i] = expectedHeaders[i].toLowerCase();
        
        for(int i = 0; i < sheetHeaders.length; i++)
        	sheetHeaders[i] = sheetHeaders[i].toLowerCase();
        
        GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders, intersect, expectedOnly, sheetOnly);
        
        if(expectedOnly.size() > 0 && sheetOnly.size() == 0 && subsetOk)
        	return true;	//allow cases where tests setup but user doesn't want to enter any grades yet
        
        //handle errors in column headers
        if (expectedOnly.size() > 0 || sheetOnly.size() > 0) 
        { 	
            out.println("<p><b>Group header: " + groupName + " has mismatch on its subheaders</b><br/>");
            out.println("<b>Expected the following headers: </b><br/>");
            printHtmlList(expectedHeaders, out);
            if (expectedOnly.size() > 0) {
                out.println("<b>These headers are missing from spreadsheet:</b><br/>");
                printHtmlList(expectedOnly, out);
            }
            if (sheetOnly.size() > 0) {
                out.println("<b>These headers are NOT expected but were found in spreadsheet</b><br/>");
                printHtmlList(sheetOnly, out);
            }
            out.println("</p><hr>");
            return false;
        }
        return true;
    }

    public static class GradeStruct
    {
        String fname;
        String lname;
        String loginName;
        HashMap<String, Double[]> score = new HashMap<String, Double[]>(); //coursename->testscores
        String exceptionMessage = "";
    }
}

