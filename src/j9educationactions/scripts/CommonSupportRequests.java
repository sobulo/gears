/**
 * 
 */
package j9educationactions.scripts;

import j9educationactions.login.NamespaceFilter;
import j9educationentities.ApplicationParameters;
import j9educationentities.InetConstants;
import j9educationentities.InetCourse;
import j9educationentities.InetCumulativeScores;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetDAO4ComplexUpdates;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetLevel;
import j9educationentities.InetSchool;
import j9educationentities.InetSingleTestScores;
import j9educationentities.InetStudent;
import j9educationentities.messaging.EmailController;
import j9educationentities.messaging.EmailReportCardController;
import j9educationentities.messaging.MessagingDAO;
import j9educationentities.messaging.SMSController;
import j9educationgwtgui.shared.DTOConstants;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyOpts;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class CommonSupportRequests extends HttpServlet{
    private static final Logger log =
        Logger.getLogger(CommonSupportRequests.class.getName());
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException
    {
        resp.setContentType("text/html");
        resp.getWriter().println("<b>Running patch ...</b><br/>");
        
        ArrayList<String> levStudBreaks;
        String type = req.getParameter("op");
        if(type == null)
        {
        	resp.getWriter().println("aborting, didn't find support type");
        	return;
        }
        
        try
        {
        	if(type.equals("deletecs"))
        	{
        		String key = "2012/2013~harmattan (1st) semester~nd year 1~computer science";
        		Objectify ofy = ObjectifyService.begin();
        		InetLevel lev = ofy.get(new Key(InetLevel.class, key));
        		Set<Key<InetStudent>> slist = lev.getStudentKeys();
        		HashSet<Key<InetStudent>> newSet = new HashSet<Key<InetStudent>>(slist.size());
        		newSet.addAll(slist);
        		for(Key<InetStudent> sk : newSet)
        			lev.removeStudentFromRoster(sk);
        		ofy.put(lev);
        	}
            if(type.equals("fixstudentlevel"))
            {
            	Objectify ofy = ObjectifyService.begin();
            	String login = req.getParameter("sk");
            	String error = "";
            	if(login == null) error += "no student specified ";
            	String levelName = req.getParameter("lk");
            	if(levelName == null) error += "no class specified";
            	
            	if(error.length() > 0)
            	{
                	resp.getWriter().println("errors: " + error);
                	return;            		
            	}
            	
            	Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class, login);
            	Key<InetLevel> levelKey = new Key<InetLevel>(InetLevel.class, levelName);
            	resp.getWriter().println("received sk: " + studKey.toString() + "<br/>");
            	resp.getWriter().println("received lk: " + levelKey.toString() + "<br/>");
            	removeLevelFromStudent(ofy.getFactory().keyToString(levelKey), ofy.getFactory().keyToString(studKey));            	
            }
            else if(type.equals("levstudrec"))
            {
            	levStudBreaks = reconcileLevelStudentInfo();
                StringBuilder output = new StringBuilder();
                if(levStudBreaks.size() > 0)
                	output.append("<b>found  " + levStudBreaks.size() + "breaks details below</b><ul>");
                for(String breaks : levStudBreaks)
                	output.append("<li>").append(breaks).append("</li>");
                if(levStudBreaks.size() > 0)
                	output.append("</ul>");
                else
                	output.append("Reconcile checks passed!!");
                resp.getWriter().println(output.toString());            	
            }
            else if(type.equals("createnotice"))
            {
            	log.warning("About to call create notice fn");
            	resp.getWriter().println(createNotices());
            }     
            else if(type.equals("emailcontroller"))
            {
            	log.warning("about to create email controller");
            	resp.getWriter().println(createMessageController(req));
            }
            else if(type.equals("smscontroller"))
            {
            	log.warning("about to create sms controller");
            	resp.getWriter().println(createSMSController(req));
            }
            else if(type.equals("pdfcontroller"))
            {
            	log.warning("about to create pdf controller");
            	resp.getWriter().println(createMessagePDFControllers(req));
            }
            else if(type.equals("courseNames"))
            {
            	ApplicationParameters a = InetDAO4Creation.createApplicationParameters(DTOConstants.APP_PARAM_COURSE_NAMES, 
            			new HashMap<String, String>(), null);
            	resp.getWriter().println("Created: " + a.getKey());
            	
            }            
            else
            {
            	resp.getWriter().println("aborting, unrecognized support type");
            	return;            	
            }

        }
        catch(MissingEntitiesException ex)
        {
        	resp.getWriter().println("<font color = 'red'>Error occured<br/>" + ex.getMessage());
        	log.severe(ex.getMessage());
        	return;
        }
        catch(DuplicateEntitiesException ex)
        {
        	resp.getWriter().println("<font color = 'red'>Error occured<br/>" + ex.getMessage());
        	log.severe(ex.getMessage());
        	return;
        }        
        resp.getWriter().println("<b>Patch completed, see log for details ...</b>");
 
    }
    
    public void removeLevel(String levelName)
    {
    	Key<InetLevel> lk = new Key<InetLevel>(InetLevel.class, levelName);
    	Objectify ofy = ObjectifyService.begin();
    	InetSchool s = InetDAO4CommonReads.getSchool(ofy);
    	if(s.removeInetLevelKey(lk))
    		log.warning("Successfully removed level: " + lk);
    	else
    		log.warning("Unable to remove level: " + lk);
    	ofy.put(s);
    	
    }
    
    public void fixTestWeights()
    {
    	Objectify ofy = ObjectifyService.begin();
    	List<InetSingleTestScores> testScores = ofy.query(InetSingleTestScores.class).filter("testName", "Midterm Tests").list();
    	ArrayList<InetSingleTestScores> toSave = new ArrayList(testScores.size());
    	log.warning("Found: " + testScores.size() + " tests");
    	StringBuilder savedTests = new StringBuilder();
    	savedTests.append("Tests below modified: ");
    	for(InetSingleTestScores ts : testScores)
    	{
    		if(ts.getWeight() != 40)
    		{
    			ts.setWeight(40);
    			ts.setMaxScore(40);
    			savedTests.append(ts.getKey().toString()).append("*****\n");
    			toSave.add(ts);
    		}
    	}
    	ofy.put(toSave);
    	log.warning("Saved: " + toSave.size() + " tests");
    	log.warning(savedTests.toString());
    }
    
    public void fixCourseCumScores()
    {
        Objectify ofy = ObjectifyService.begin();
        Map<Key<InetCourse>, InetCourse> courseMap= ofy.query(InetCumulativeScores.class).fetchParents();
        QueryResultIterable<Key<InetCumulativeScores>> cumKeys = ofy.query(InetCumulativeScores.class).fetchKeys();
        int count = 0;
        ArrayList<InetCourse> toSave = new ArrayList<InetCourse>(); 
        for(Key<InetCumulativeScores> ck : cumKeys)
        {
        	count++;
        	Key<InetCourse> courseKey = ck.getParent();
        	InetCourse course = courseMap.get(courseKey);
        	if(course.getCumulativeScores() == null)
        	{
        		course.setCumulativeScores(ck);
        		toSave.add(course);
        	}
        }
        ofy.put(toSave);
        log.warning("BUGFIX: num of courses checked: " + count + "\n num of courses fixed: " + toSave.size());
    }
    
    public ArrayList<String> reconcileLevelStudentInfo() throws MissingEntitiesException
    {
    	ObjectifyOpts opts = new ObjectifyOpts();
    	opts.setSessionCache(true);
    	Objectify ofy = ObjectifyService.begin(opts);
    	Set<Key<InetLevel>> levelKeys = InetDAO4CommonReads.getSchoolLevels(ofy);
    	Map<Key<InetLevel>, InetLevel> levels = InetDAO4CommonReads.getEntities(levelKeys, ofy, true);
    	log.warning("Retrieved: " + levels.size() + " levels");
    	
    	List<InetStudent> students = ofy.query(InetStudent.class).list();
    	HashMap<Key<InetStudent>, InetStudent> studentMap = new HashMap<Key<InetStudent>, InetStudent>(students.size());
    	for(InetStudent stud : students)
    		studentMap.put(stud.getKey(), stud);  	
    	log.warning("Retrieved: " + students.size() + " students");
 
    	ArrayList<String> result = new ArrayList<String>();
    	InetStudent stud;
    	for(InetLevel lev : levels.values())
    	{
    		for(Key<InetStudent> sk : lev.getStudentKeys())
    		{
    			stud = studentMap.get(sk); 
    			if(stud == null)
    				result.add("<font color='blue'>" + sk.getName() + " found in " + lev.getKey().getName() + 
    						" but doesn't exist in database</font>");
    			else if(!stud.getLevels().contains(lev.getKey()))
    				result.add("<font color='red'>" + sk.getName() + " found in " + lev.getKey().getName() + 
					" but the level is NOT in the student's list</font>");
    		}
    	}
    	
    	//reconcile the other way
    	InetLevel lev;
    	for(InetStudent stu : students)
    	{
    		for(Key<InetLevel> lk : stu.getLevels())
    		{
    			lev = levels.get(lk);
    			if(lev == null)
    				result.add("<font color='black'>" + lk.getName() + " found in " + stu.getKey().getName() + 
					" student list but doesn't exist in database</font>");
    			else if(!lev.getStudentKeys().contains(stu.getKey()))
    				result.add("<font color='green'>" + lk.getName() + " found in " + stu.getKey().getName() + 
					" student list but doesn't exist in level list</font>");
    		}
    	}
    	return result;
    }
    
    public void removeLevelFromStudent(String levKeyStr, String studKeyStr) throws MissingEntitiesException
    {
    	InetDAO4ComplexUpdates.removeLevelFromStudentList(levKeyStr, studKeyStr);
    }
    
    public String createNotices() throws DuplicateEntitiesException
    {
    	StringBuilder result = new StringBuilder("<ul>");
    	for(String id : PanelServiceConstants.ROLE_NOTES)
    		result.append("<li>").append(InetDAO4Creation.createNotice(id, "Notice Board").getKey().getName()).append("</li>");
    	result.append("</ul>");
    	return result.toString();
    }
    
    public String createMessageController(HttpServletRequest req) throws DuplicateEntitiesException
    {
    	String fromAddress = NamespaceFilter.getNamespaceName(req) + InetConstants.APP_EMAIL_SUFFIX;
    	EmailController controller = MessagingDAO.createEmailController(InetConstants.EMAIL_CONTROLLER_ID, fromAddress, 5000, 100, 1000);
    	return "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    }
    
    public String createMessagePDFControllers(HttpServletRequest req) throws DuplicateEntitiesException
    {
    	String result = "";
    	String fromRCAddress = "no-reply" + "." + NamespaceFilter.getNamespaceName(req) + InetConstants.APP_EMAIL_SUFFIX;
    	EmailReportCardController controller = MessagingDAO.createEmailReportCardController(InetConstants.EMAIL_RC_CONTROLLER_ID, fromRCAddress, 5000, 100, 1000);
    	result += "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    	
    	fromRCAddress = "no-reply" + "." + NamespaceFilter.getNamespaceName(req) + InetConstants.APP_EMAIL_SUFFIX;
    	controller = MessagingDAO.createBillingController(InetConstants.EMAIL_BILL_CONTROLLER_ID, fromRCAddress, 5000, 100, 1000);
    	result += "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    	return result;
    }
    
    public String createSMSController(HttpServletRequest req) throws DuplicateEntitiesException
    {
    	String fromAddress = NamespaceFilter.getNamespaceName(req) + " Alert";
    	SMSController controller = MessagingDAO.createSMSController(InetConstants.SMS_CONTROLLER_ID, 
    			fromAddress, 100, 100, 200, "smpp5.routesms.com", 8080, "ferti", "kf3fidlf", "1", "0");
    	return "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    }
}
