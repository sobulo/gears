/**
 * 
 */
package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationutilities.SMSMessage;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class TestSMSMessage extends HttpServlet
{
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
	  	PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
		try 
		{
	    	LoginPortal.verifyRole(allowedRoles, req);
	    }
		catch(LoginValidationException ex)
		{
			throw new RuntimeException(ex.fillInStackTrace());
		}
		
		String destination = req.getParameter("destination");		
		String message = req.getParameter("message");		
		SMSMessage smsSender = new SMSMessage("smpp4.routesms.com", 8080, "ts-fertil", "olu13se", message,
				"1", "0", destination, "mcigrades");
		
		String result = smsSender.submitMessage();
		
		//print result
		res.getOutputStream().println("<html><body>");
		res.getOutputStream().println("SMS Call returned with <b>" + result + "</b>");
		res.getOutputStream().println("</html></body>");
	}
}
