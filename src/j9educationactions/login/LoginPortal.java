/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions.login;


import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetGuardian;
import j9educationentities.InetHold;
import j9educationentities.InetSchool;
import j9educationentities.InetStudent;
import j9educationentities.InetTeacher;
import j9educationentities.InetUser;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationgwtgui.shared.exceptions.MultipleEntitiesException;
import j9educationutilities.BCrypt;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyOpts;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 *
 * Logic in here is hack after hack. Do not modify this file without extensive
 * code reviews. Ideally this should all be replaced by OpenID mechanism.
 */
public class LoginPortal extends HttpServlet{
    final static String USER_SESSION = "j9educationactions.login";
    final static String PROVIDER_SESSION = "j9educationactions.provider";
    final static String LOGIN_URL_SESSION = "j9educationactions.loginurl";
    final static String LOGOUT_URL_SESSION = "j9educationactions.logouturl";
    final static String CACHED_LOGIN_INFO_SESSION = "j9educationactions.loginconfirmed";
    final static String ROLE_TYPE_SESSION = "j9educationactions.loginrole";
    final static String TYPE = "service";
    final static String TYPE_SIGN_OUT = "signout";
    final static String TYPE_SIGN_IN = "signin";
    final static String PROVIDER_GOOGLE = "google";
    final static String PROVIDER_GREP = PanelServiceConstants.PROVIDER_GREP;
    private static final Logger log = Logger.getLogger(LoginPortal.class.getName());
    private static LoginProvider[] registeredProviders;


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String user = req.getParameter("user");
        String password = req.getParameter("password");
        String signinDestination = req.getParameter("continue");

        if (user == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid Username"));
            return;
        }

        if (password == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid Password"));
            return;
        }

        if (signinDestination == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid Signin Destination"));
            return;
        }

        //TODO, store hash of password not actual password
        HttpSession session = req.getSession();
        UserSessionObj sessionObj = new UserSessionObj();
        sessionObj.setUserName(user);
        sessionObj.setPassword(password);
        session.setAttribute(USER_SESSION, sessionObj);

        //redirect to continue url
        resp.sendRedirect(signinDestination);
    }

    private static LoginProvider[] getRegisteredProviders() {
        if (registeredProviders == null) {
            LoginProvider[] loginProviders = new LoginProvider[2];
            loginProviders[0] = new GREPLoginProvider();
            loginProviders[1] = new GoogleLoginProvider();
            registeredProviders = loginProviders;
        }
        return registeredProviders;
    }

    public static void clearAllSessions(HttpServletRequest req) {
        HttpSession session = req.getSession();
        session.removeAttribute(USER_SESSION);
        session.removeAttribute(PROVIDER_SESSION);
        session.removeAttribute(CACHED_LOGIN_INFO_SESSION);
        session.removeAttribute(LOGIN_URL_SESSION);
        session.removeAttribute(ROLE_TYPE_SESSION);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String output = "";
        String type = req.getParameter(TYPE);
        String provider = req.getParameter("continue");
        String roleType = req.getParameter(PanelServiceConstants.ROLE_TYPE_REQ);

        if (provider == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid URL"));
            return;
        }

        if (type == null) {
            output = displayError("Unable to determine access type");
        } else if (type.equals(TYPE_SIGN_IN)) {
            if (roleType == null) {
                resp.setContentType("text/html");
                resp.getOutputStream().println(displayError("Invalid URL, no user role specified"));
                return;
            }
            roleType = roleType.toUpperCase();
            HttpSession session = req.getSession();

            //signout if we're already signed in, we do not remove login_url_session
            //as it is needed during login process
            session.removeAttribute(USER_SESSION);
            session.removeAttribute(CACHED_LOGIN_INFO_SESSION);
            session.removeAttribute(PROVIDER_SESSION);

            HashMap<String, String> whereTO = (HashMap<String, String>) session.getAttribute(LOGIN_URL_SESSION);

            if (whereTO == null || !whereTO.containsKey(provider)) {
                resp.setContentType("text/html");
                resp.getOutputStream().println(displayError("Invalid Login Provider, possibly due to your" +
                		" user session timing out. Restarting your browser should fix, please contact IT if problem persists."));
                return;
            }


            //store provider so we know what type of logut url to generate
            LoginProvider[] loginProviders = getRegisteredProviders();
            boolean found = false;
            for (int i = 0; i < loginProviders.length; i++) {
                if (provider.equals(loginProviders[i].getProviderName())) {
                    session.setAttribute(PROVIDER_SESSION, loginProviders[i]);
                    session.setAttribute(ROLE_TYPE_SESSION, roleType);
                    found = true;
                    break;
                }
            }

            if (found == false) {
                resp.setContentType("text/html");
                //unexpected cuz this shouldn't happen since whereTO check above passed
                //if we get this error most likely a bug has been introduced by a recent
                //code change
                resp.getOutputStream().println(displayError("Unexpected Invalid Login Provider. Please notify IT of this issue, restarting your browser should fix"));
                return;
            }

            //route request
            String url = whereTO.get(provider);
            if (provider.equals(PROVIDER_GOOGLE)) {
                resp.sendRedirect(url);
            } else if (provider.equals(PROVIDER_GREP)) {
                output = drawForm(url);
            } else {
                resp.setContentType("text/html");
                resp.getOutputStream().println(displayError("Improper setup of" +
                        " login provider"));
                return;
            }
        } else if (type.equals(TYPE_SIGN_OUT)) {
            clearAllSessions(req);
            HttpSession session = req.getSession();
            String logouturl = (String) session.getAttribute(LOGOUT_URL_SESSION);
            if (logouturl == null)
                logouturl = "http://" + req.getHeader("Host");
            
            //redirect to signout url
            resp.sendRedirect(logouturl);
            
            
        } else {
            output = displayError("Unauthorized Access");
        }

        //display result to client
        resp.setContentType("text/html");
        resp.getOutputStream().println(output);
    }

    private String drawForm(String url) {
        return "<html><body><center><table><tr><td>" +
        		"<img src='grepgui/images/keys_icon.png' /></td><td valign='middle'>" + 
        		"<form name='username' action='/greplogin' method='post' autocomplete='off'>" +
        		"<table bgcolor='#fdf69d'>\n" +
                "<tr><td>Username</td><td><input type='text' name='user'/></td></tr>\n" +
                "<tr><td>Password</td><td><input type='password' name='password'/></td></tr>\n" +
                "<tr><td colspan='2' align='center'><input type='submit' value ='Login' /></td></tr>" +
                "</table><input type='hidden' name='continue' value='" + url +
                "'/></form></td></tr></table>" +
                "</center></body></html>";
    }

    private String displayError(String error) {
        return "<html><body><center><b><font color='red'>" + error + "</font></b></center></body></html>";
    }

    private static String createLougoutUrl(String url, LoginProvider provider,
            HttpServletRequest req)
    {
        HttpSession session = req.getSession();

        //store actual checkout url
        session.setAttribute(LOGOUT_URL_SESSION, provider.getCheckoutUrl(url));

        //return url that we'll use to determine chosen checkout provider
        return "http://" + req.getHeader("Host") + "/greplogin?" + TYPE + "=" +
                TYPE_SIGN_OUT + "&continue=" + provider.getProviderName();
    }

    private static HashMap<String, String> createLoginUrls(String url, LoginProvider[] providers,
            HttpServletRequest req) {
        HttpSession session = req.getSession();
        HashMap<String, String> savedUrls =
                (HashMap<String, String>) session.getAttribute(LOGIN_URL_SESSION);

        if (savedUrls == null) {
            savedUrls = new HashMap<String, String>(providers.length);
        }

        //build brokered urls
        HashMap<String, String> result = new HashMap<String, String>(providers.length);
        for (LoginProvider provider : providers) {
            //build url that will use to delegate to provider appropiate provider
            result.put(provider.getProviderName(), "http://" + req.getHeader("Host") + "/greplogin?" +
                    TYPE + "=" + TYPE_SIGN_IN + "&continue=" + provider.getProviderName());

            //store actual checkin url
            savedUrls.put(provider.getProviderName(), provider.getCheckInUrl(url));
        }
        session.setAttribute(LOGIN_URL_SESSION, savedUrls);
        return result;
    }

    public static void checkBrokeredLogin(HttpServletRequest req) throws LoginValidationException {
        HttpSession session = req.getSession();
        LoginInfo loginInfo = (LoginInfo) session.getAttribute(CACHED_LOGIN_INFO_SESSION);

        if (loginInfo == null) {
            throw new LoginValidationException("Not logged in");
        }
    }

    public static LoginInfo getLoginInfo(HttpServletRequest req) throws LoginValidationException
    {
        HttpSession session = req.getSession();
        LoginProvider provider = (LoginProvider) session.getAttribute(PROVIDER_SESSION);

        if (provider != null) {
            //try getting previously stored login calc
            LoginInfo loginInfo = (LoginInfo) session.getAttribute(CACHED_LOGIN_INFO_SESSION);

            //if provider session still valid
            if (provider.obtainedToken(req) && loginInfo != null) {
                return loginInfo;
            }
        }
        throw new LoginValidationException("User not logged in");
    }

    public static LoginInfo brokeredLogin(String requestUri, HttpServletRequest req) throws MissingEntitiesException, LoginValidationException {
        HttpSession session = req.getSession();

        LoginProvider provider = (LoginProvider) session.getAttribute(PROVIDER_SESSION);

        if (provider != null && provider.obtainedToken(req)) {
            //try getting previously stored login calc
            LoginInfo loginInfo = (LoginInfo) session.getAttribute(CACHED_LOGIN_INFO_SESSION);

            //cached object still valid?
            if (loginInfo != null) {
                return loginInfo;
            }

            //no valid cached object so recalculate, i.e. check passwords etc
            session.removeAttribute(CACHED_LOGIN_INFO_SESSION);
            loginInfo = new LoginInfo();

            Objectify ofy = ObjectifyService.begin(new ObjectifyOpts().
                    setSessionCache(true));
            //throws exception if not valid, also sets user key
            try 
            {
                provider.confirmToken(loginInfo, req, ofy);
                
                //at this point we've confirmed username/password. now check if any holds
                //before granting final blessing
                List<InetHold> studentHolds= InetDAO4CommonReads.getHolds(ofy, loginInfo);
                if(studentHolds != null && studentHolds.size() > 0)
                {
                	String msg = "<ul>";
                	for(InetHold hold : studentHolds)
                		msg += "<li>" + InetDAO4CommonReads.holdToString(hold) + "</li>";
                	throw new LoginValidationException(msg);
                }
            }
            catch(LoginValidationException ex)
            {
            	clearAllSessions(req); //clear state if we can't validate
            	loginInfo.setLoggedIn(false);
            	loginInfo.setLoginUrl(createLoginUrls(requestUri, getRegisteredProviders(), req));
            	loginInfo.setMessage("<div class='themePaddedBorder'><p>Login attempt failed because:</p>" + ex.getMessage() + "</div>");
            	fillInSchoolInfo(loginInfo, ofy);
            	return loginInfo;
            }
            catch (Exception ex)
            {
                clearAllSessions(req); //clear state if we can't validate
                throw new LoginValidationException(ex.getMessage()); //rethrow
            }

            //done setting provider specific fields, now set general fields
            loginInfo.setLoggedIn(true);
            loginInfo.setLogoutUrl(createLougoutUrl(requestUri, provider, req));

            //cache login obj
            session.setAttribute(CACHED_LOGIN_INFO_SESSION, loginInfo);

            return loginInfo;
        } else 
        {
            LoginInfo loginInfo = new LoginInfo();
            loginInfo.setLoggedIn(false);
            loginInfo.setMessage("Login using one of the links below");
            loginInfo.setLoginUrl(createLoginUrls(requestUri, getRegisteredProviders(), req));
            Objectify ofy = ObjectifyService.begin();
            fillInSchoolInfo(loginInfo, ofy);
            return loginInfo;
        }
    }    

    public static String getStudentID(HttpServletRequest req) throws LoginValidationException
    {
        LoginInfo info = LoginPortal.getLoginInfo(req);

        if(info.getRole().equals(PanelServiceLoginRoles.ROLE_STUDENT))
            return info.getLoginID();
        else
        {
            //TODO, write a toString function for info and use here
            log.severe("Security issue: illegal access by " + info.getLoginID());
            throw new IllegalStateException("This service should only be " +
                    "accessed by students");
        }
    }

    public static String getGuardianID(HttpServletRequest req) throws LoginValidationException
    {
        LoginInfo info = LoginPortal.getLoginInfo(req);

        if(info.getRole().equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
            return info.getLoginID();
        else
        {
            //TODO, write a toString function for info and use here
            log.severe("Security issue: illegal access by " + info.getLoginID());
            throw new IllegalStateException("This service should only be " +
                    "accessed by guardians");
        }
    }
    

    
    public static Key<? extends InetUser> getUserKey(HttpServletRequest req) throws LoginValidationException
    {
        LoginInfo info = LoginPortal.getLoginInfo(req);
        return InetDAO4CommonReads.getUserKey(info.getLoginID(), info.getRole());
    }    

    private static void fillInLoginUserInfo( LoginInfo loginInfo, InetUser user, Objectify ofy)
    {
        loginInfo.setLoginID(user.getKey().getName());
        loginInfo.setName(new StringBuilder(user.getFname()).append(" ").
                append(user.getLname()).toString());
        fillInSchoolInfo(loginInfo, ofy);
    }
    
    private static void fillInSchoolInfo(LoginInfo loginInfo, Objectify ofy)
    {      
    	try
    	{
    		InetSchool s = InetDAO4CommonReads.getSchool(ofy);
            loginInfo.setSchoolAcrronym(s.getAccronym());
            loginInfo.setSchoolName(s.getSchoolName());      		
    	}
    	catch(NotFoundException ex)
    	{
    		//school hasn't been created yet
            loginInfo.setSchoolAcrronym("NAY");
            loginInfo.setSchoolName("Not Applicable Yet");
    	}          	
    }

    //@SuppressWarnings("rawtypes")
	public static Class<? extends InetUser> getClassType(PanelServiceLoginRoles role) throws LoginValidationException
    {
        switch(role)
        {
	        case ROLE_STUDENT:
	            return InetStudent.class;
	        case ROLE_GUARDIAN:
	            return InetGuardian.class;
	        case ROLE_ADMIN:
	            return InetUser.class;
	        case ROLE_TEACHER:
	            return InetTeacher.class;
	        default:
	        	throw new LoginValidationException("Unrecognized role: " + role.toString());
        }
    }
    
    public static String verifyRole(PanelServiceLoginRoles[] allowedRoles, HttpServletRequest req) throws LoginValidationException
    {
    	User user = UserServiceFactory.getUserService().getCurrentUser();
    	if(user != null && UserServiceFactory.getUserService().isUserAdmin())
    	{
    		String id = null;
    		if(user.getEmail().length() == 0)
    			id = "anonymous-superuser";
    		else
    			id = user.getEmail();
    		log.warning("**SUPER USER ACCESS: " + id);
    		return id;
    	}
    	
    	LoginInfo loginInfo = getLoginInfo(req);
    	Arrays.sort(allowedRoles);
    	if(Arrays.binarySearch(allowedRoles, loginInfo.getRole()) >= 0)
    		return loginInfo.getLoginID();
    	else
    	{
    		String msg = "Potential security issue: user " + loginInfo.getLoginID() + "/" +
			loginInfo.getRole() + " attempting to access restricted resource\n";
    		LoginValidationException ex = new LoginValidationException(msg);
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("j9"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);
    		throw ex;
    		
    	}
    }

    private static interface LoginProvider {

        String getCheckoutUrl(String url);

        String getCheckInUrl(String url);

        String getProviderName();

        boolean obtainedToken(HttpServletRequest req);

        void confirmToken(LoginInfo loginInfo, HttpServletRequest req,
                Objectify ofy) throws LoginValidationException;
    }

    private static class GREPLoginProvider implements LoginProvider, Serializable {

        @Override
        public String getCheckoutUrl(String url) {
            return url;
        }

        @Override
        public String getCheckInUrl(String url) {
            return url;
        }

        @Override
        public String getProviderName() {
            return PROVIDER_GREP;
        }

        @Override
        public boolean obtainedToken(HttpServletRequest req) {
            return getUser(req) != null;
        }

        @Override
        public void confirmToken(LoginInfo loginInfo, HttpServletRequest req,
                Objectify ofy) throws LoginValidationException {
            //get loginname and school to log into
            UserSessionObj user = getUser(req);
            String loginName = user.getUserName();

            PanelServiceLoginRoles roleType = PanelServiceLoginRoles.valueOf((String) req.getSession().getAttribute(ROLE_TYPE_SESSION));
            InetUser persistedUser = (InetUser) ofy.find(getClassType(roleType), loginName);

            if (persistedUser != null) {
                validateUser(user, persistedUser);
                fillInLoginUserInfo(loginInfo, persistedUser, ofy);
                loginInfo.setRole(roleType);
                return;
            }  

            //else couldn't find the loginname
            throw new LoginValidationException("Invalid login name");
        }

        private void validateUser(UserSessionObj userLogin,
                InetUser persistedUser) throws LoginValidationException
        {
            if(!BCrypt.checkpw(userLogin.getPassword(), persistedUser.getPassword()))
                throw new LoginValidationException("Invalid password");
        }

        public UserSessionObj getUser(HttpServletRequest req) {
            return (UserSessionObj) req.getSession().getAttribute(USER_SESSION);
        }
    }

    private static class GoogleLoginProvider implements LoginProvider, Serializable {

        @Override
        public String getCheckoutUrl(String url) {
            return UserServiceFactory.getUserService().createLogoutURL(url);
        }

        @Override
        public String getCheckInUrl(String url) {
            return UserServiceFactory.getUserService().createLoginURL(url);
        }

        @Override
        public String getProviderName() {
            return PROVIDER_GOOGLE;
        }

        @Override
        public boolean obtainedToken(HttpServletRequest req) {
            return UserServiceFactory.getUserService().isUserLoggedIn();
        }

        @Override
        public void confirmToken(LoginInfo loginInfo, HttpServletRequest req, Objectify ofy) throws LoginValidationException {
            User user = UserServiceFactory.getUserService().getCurrentUser();
            String email = user.getEmail().toLowerCase();
            if (Arrays.binarySearch(LoginConstants.SUPER_USERS, email) >= 0)
            {
            	fillInSchoolInfo(loginInfo, ofy);
                loginInfo.setName(user.getNickname());
                loginInfo.setLoginID(user.getEmail());
                loginInfo.setRole(PanelServiceLoginRoles.ROLE_SUPER);
                return;
            }

            PanelServiceLoginRoles roleType = PanelServiceLoginRoles.valueOf((String) req.getSession().getAttribute(ROLE_TYPE_SESSION));

            try
            {
                InetUser persistedUser = InetDAO4CommonReads.
                        getUserByEmail(ofy, getClassType(roleType), email);
                if (persistedUser != null) {
                    fillInLoginUserInfo(loginInfo, persistedUser, ofy);
                    loginInfo.setRole(roleType);
                    return;
                }
                
                //else couldn't find the loginname
                throw new LoginValidationException("This google account is not" +
                        " registered on our systems");
            }
            catch(MultipleEntitiesException ex)
            {
                throw new LoginValidationException(ex.getMessage());
            }
        }
    }
}

