/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationactions.login;

import java.io.Serializable;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class UserSessionObj implements Serializable{

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName.toLowerCase();
    }
    private String userName;
    private String password;
}
