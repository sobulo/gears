/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationactions.login;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginConstants
{
    public final static String DEVELOPMENT_NAMESPACE = "test";

    public final static HashMap<String, String> ALLOWED_NAMESPACES = new HashMap<String, String>();

    /**
     * super user login list
     */
    public final static String[] SUPER_USERS =
    {
        "admin@fertiletech.com", "gears@9jeducation.org"
    };
    
    public final static String LANDING_FLAG = "__ENTRY_PAGE__";

    //set init state of constants
    static
    {
        Arrays.sort(SUPER_USERS);
        
        //Maverick College Ibadan namespaces
        ALLOWED_NAMESPACES.put("tertiary.gears-drive.appspot.com", "tertiary");
        ALLOWED_NAMESPACES.put("secondary.gears-drive.appspot.com", "secondary");
        ALLOWED_NAMESPACES.put("gears-drive.appspot.com", LANDING_FLAG);
                
        //fcahptib domain
        ALLOWED_NAMESPACES.put("fcahptib.gears-drive.appspot.com", "fcahpt");
        ALLOWED_NAMESPACES.put("gears.fcahptib.edu.ng", "fcahpt");        
        ALLOWED_NAMESPACES.put("grades.fcahptib.edu.ng", "fcahpt");        
    }
}
