/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions.login;

import j9educationentities.InetDAO4CommonReads;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.utils.SystemProperty;

/**
 *
 * @author Segun Razaq Sobulo
 */
// Filter to set the Google Apps domain as the namespace.
public class NamespaceFilter implements javax.servlet.Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException 
    {
        //Each school has its data segregated via namespace. We determine namespace
        //via the request url
        if (NamespaceManager.get() == null) 
        {
        	String nms = getNamespaceName(req);
        	if(nms == null)
        	{
        		res.setContentType("text/html");
        		res.getOutputStream().println("This domain is not registered. " +
        				"Please visit <a href='https://gears-drive.appspot.com'> Demo Landing Page</a> instead");
        		return;
        	}
        	else if(nms == LoginConstants.LANDING_FLAG)
        	{
        		res.setContentType("text/html");
        		res.getOutputStream().println("<ul> " +
        				"<li><a href='https://secondary.gears-drive.appspot.com'>Secondary School Demo</a></li>" + 
        				"<li><a href='https://tertiary.gears-drive.appspot.com'>College/University Demo</a></li><ul>" +
        				"Google Drive features available under uploads menu options when logged in as an admin or a teacher");
        		return;
        	}
            NamespaceManager.set(nms);
        }
        //regisering objectify here doesn't seem ideal but can't think of a
        //better way to ensure registration happens before datastore access
        InetDAO4CommonReads.registerClassesWithObjectify();        
        chain.doFilter(req, res);
    }

    public static String getNamespaceName(ServletRequest req)
    {
        String candidateNamespace = req.getServerName();
        if(LoginConstants.ALLOWED_NAMESPACES.containsKey(candidateNamespace))
            return LoginConstants.ALLOWED_NAMESPACES.get(candidateNamespace);

        if(SystemProperty.environment.value() !=
                SystemProperty.Environment.Value.Production)
            return LoginConstants.DEVELOPMENT_NAMESPACE;

        return null;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
