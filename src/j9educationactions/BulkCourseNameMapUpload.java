/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationentities.ApplicationParameters;
import j9educationentities.InetConstants;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetLevel;
import j9educationentities.InetStudent;
import j9educationgwtgui.server.SchoolServiceImpl;
import j9educationgwtgui.shared.DTOConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.GeneralFuncs;
import j9educationutilities.PasswordGenerator;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.DateColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkCourseNameMapUpload extends HttpServlet {
    //TODO review classes of j9educationaction package and ensure no instance
    //of services being created. instead create static versions of such fns
    private static final Logger log =
            Logger.getLogger(BulkCourseNameMapUpload.class.getName());
    private final static String STUD_UPLD_SESSION_NAME = "courseUploadData";
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException 
    {
        PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN}; 	
    	try {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) {
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
			"</b></body></html>");
			return;
		} 
		
        res.setContentType("text/html");
        ServletOutputStream out = res.getOutputStream();
        if (req.getParameter(LOAD_PARAM_NAME) != null)
        {
            Object upldSessObj = req.getSession().getAttribute(STUD_UPLD_SESSION_NAME);
            //log.warning("Session is: " + STUD_UPLD_SESSION_NAME + " Val: " + upldSessObj);
            if (upldSessObj != null)
            {
                //retrive data from session object
                ArrayList<CourseStruct> studData = (ArrayList<CourseStruct>) upldSessObj;

                String okMsg = "<b>Course Code,Course Name</b><br/>";
                StringBuilder badMsg = new StringBuilder();
                ArrayList<String> loginIDs = new ArrayList<String>(studData.size());

                
                
                ArrayList<Key<InetStudent>> studentKeys = new ArrayList<Key<InetStudent>>(studData.size());
                HashMap<String, String> nameIndex = new HashMap<String, String>(studData.size());
                Objectify ofy = ObjectifyService.begin();
                
                HashMap<String, String> newCourseNames = new HashMap<String, String>();
                //first pass lets see if we can fetch any logins
                for(CourseStruct elem : studData)
                	newCourseNames.put(elem.code.toLowerCase().trim(), elem.name);
                

                try
                {
                	Key<ApplicationParameters> paramKey = ApplicationParameters.getKey(DTOConstants.APP_PARAM_COURSE_NAMES);
                	String changes = SchoolServiceImpl.updateApplicationParameters(paramKey, newCourseNames);
                	out.println("<H3>Course Map Loaded Succesfully</H3>" + changes);
	                
                }
                catch(MissingEntitiesException ex)
                {
                	log.severe("This should not have occured!!!" + ex.getMessage());
                	out.println("<font color='red'>" + ex.getMessage());
                }                
                //set session data to null as we've saved the data
                req.setAttribute(STUD_UPLD_SESSION_NAME, null);
            }
            else
            {
                out.println("<b>unable to retrieve session info. Try " +
                        "enabling cookies in your browser.</b>");
            }
        }
        else
        {
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iterator = upload.getItemIterator(req);
                while (iterator.hasNext()) {
                    FileItemStream item = iterator.next();
                    InputStream stream = item.openStream();
                    if (item.isFormField()) {
                    	log.warning("TODO: delete this conditional: " + Streams.asString(stream));
                    } else {
                        log.warning("Bulk course name upload attempt: " + item.getFieldName() + ", name = " + item.getName());
                        ColumnAccessor[] headerAccessors = InetConstants.COURSE_NAME_UPLOAD_ACCESSORS;
                        String[] expectedHeaders = ColumnAccessor.getAccessorNames(headerAccessors);
                        //TODO, return error page if count greater than 200 rows
                        ExcelManager sheetManager = new ExcelManager(stream);
                        //confirm headers in ssheet match what we expect
                        String[] sheetHeaders;
                        try {
                            sheetHeaders = sheetManager.getHeaders();

                            //expectedheaders are uppercase so convert sheet prior to compare
                            for (int s = 0; s < sheetHeaders.length; s++) {
                                sheetHeaders[s] = sheetHeaders[s].toUpperCase();
                            }
                        } catch (InvalidColumnValueException ex) {
                        	log.severe(ex.getMessage());
                            out.println("<b>Unable to read excel sheet headers</b><br/> Error was: " 
                            		+ ex.getMessage());
                            return;
                        }
                        ArrayList<String> sheetOnly = new ArrayList<String>();
                        ArrayList<String> intersect = new ArrayList<String>();
                        ArrayList<String> expectedOnly = new ArrayList<String>();
                        GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders, intersect, expectedOnly, sheetOnly);
                        //handle errors in column headers
                        if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
                            out.println("<b>Uploaded sheet should contain only these headers:</b>");
                            printHtmlList(expectedHeaders, out);
                            if (expectedOnly.size() > 0) {
                                out.println("<b>These headers are missing from ssheet:</b>");
                                printHtmlList(expectedOnly, out);
                            }
                            if (sheetOnly.size() > 0) {
                                out.println("<b>These headers are not expected but were found in ssheet</b>");
                                printHtmlList(sheetOnly, out);
                            }
                            return;
                        }

                        sheetManager.initializeAccessorList(headerAccessors);
                        //todo, replace these with string builders
                        String goodDataHTML = "";
                        String badDataHTML = "";
                        String tempVal = "";
                        String[] htmlHeaders = new String[headerAccessors.length];
                        ArrayList<CourseStruct> sessionData = new ArrayList<CourseStruct>();
                        PasswordGenerator passwordGen = new PasswordGenerator();
                        String[] loginNameComponents = new String[2];

                        for (int i = 0; i < headerAccessors.length; i++) {
                            htmlHeaders[i] = headerAccessors[i].getHeaderName();
                        }

                        int numRows = sheetManager.totalRows();
                        HashMap<String, String> courseTable = new HashMap<String, String>(numRows);
                        for (int i = 1; i < numRows; i++) 
                        {
                        	try
                        	{
	                            String row = "<tr>";
	                            CourseStruct tempStudData = new CourseStruct();
	                            for (int j = 0; j < headerAccessors.length; j++) {
	                                tempVal = getValue(i, headerAccessors[j], sheetManager, tempStudData);
	                                row += "<td>" + tempVal + "</td>";
	                            }
	                            
	
	                            if (tempStudData.isValid)
	                            {
	                                if(courseTable.containsKey(tempStudData.name.toLowerCase()))
	                                {
	                                	String errMsg = String.format("Cannot add %s with unique id %s. An earlier record (%s) in the file maps to that" +
	                                			" same id. Remove one of these rows from the file or edit the id", tempStudData.name, tempStudData.code, courseTable.get(tempStudData.code.toLowerCase()));
	                                	row = new StringBuilder("<tr><td colspan=").append(htmlHeaders.length).append(">").append(errMsg).
	                                	append("</td>").toString();
	                                	tempStudData.isValid = false;
	                                }
	                                else
	                                {
	                                	courseTable.put(tempStudData.code.toLowerCase().trim(), tempStudData.name);
		                                row += "<td>" + tempStudData.code + "</td>";
		                                row += "<td>" + tempStudData.name + "</td></tr>";
	                                }
	                            }
	                            
	                            if(tempStudData.isValid)
	                            {
	                                //append to display result
	                                goodDataHTML += row;
	                                sessionData.add(tempStudData);
	                            }
	                            else 
	                            {
	                                badDataHTML += (row + "</tr>");
	                            }
                        	}
                        	catch(EmptyColumnValueException ex) {} //logic to help skip blank lines
                        }

                        String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
                        String htmlHeader = "";
                        for (String s : htmlHeaders) {
                            htmlHeader += "<TH>" + s + "</TH>";
                        }
                        String htmlTableEnd = "</TABLE>";

                        if (goodDataHTML.length() > 0) 
                        {
                            out.println(GOOD_DATA_PREFIX);
                            String goodHeaderSuffix = "<TH>CODE</TH><TH>Name</TH>";
                            goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data</b>" + 
                            htmlTableStart + 
                            "<TR>" + htmlHeader + goodHeaderSuffix + "</TR>" + goodDataHTML + htmlTableEnd;
                        }

                        if (badDataHTML.length() > 0) 
                        {
                            out.println("<b>Below shows records with errors</b>");
                            out.println(htmlTableStart);
                            out.println("<TR>");
                            out.println(htmlHeader);
                            out.println("</TR>");
                            out.print(badDataHTML);
                            out.println(htmlTableEnd);
                        }
                        req.getSession().setAttribute(STUD_UPLD_SESSION_NAME,
                                sessionData);
                        //Object upldSessObj = req.getSession().getAttribute(STUD_UPLD_SESSION_NAME);
                        out.println(goodDataHTML);
                        //out.println(STUD_UPLD_SESSION_NAME + " SESSION IS: " + upldSessObj);
                    }
                }
            } catch (FileUploadException ex) {
                out.println("<b>File upload failed:</b>" + ex.getMessage());
            }
            catch (InvalidColumnValueException ex) {
                log.severe(ex.getMessage());
                out.println("<b>Unable to read excel sheet</b><br/> Error was: " + ex.getMessage());
            }            
        }
    }

    private String getValue(int row, ColumnAccessor accessor, ExcelManager sheetManager, CourseStruct courseData) throws InvalidColumnValueException {
        Object temp;
        String val;
        String headerName = accessor.getHeaderName();
        try {
            temp = sheetManager.getData(row, accessor);

            //handle other fields
            if(temp == null)
                val = "";
            else
                val = ((String) temp);

            if (headerName.equals(InetConstants.COURSE_NAME_HEADER)) {
                courseData.name = val;
            } else if (headerName.equals(InetConstants.COURSE_CODE_HEADER)) {
                courseData.code = val;
            } 
        }
        catch(EmptyColumnValueException ex)
        {
        	if(sheetManager.isBlankRow(row))
        		throw ex;
        	val = ex.getMessage();
        	log.warning(ex.getMessage());
        	courseData.isValid = false;
        }        
        catch (InvalidColumnValueException ex) {
            val = ex.getMessage();
            courseData.isValid = false;
        }
        return val;
    }

    private void printHtmlList(String[] list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }

    public void printHtmlList(List<String> list, ServletOutputStream out) throws IOException
    {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }
}

class CourseStruct implements Serializable
{
    String name;
    String code;
    boolean isValid = true;
}