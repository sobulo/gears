/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions;

import j9educationactions.login.LoginPortal;
import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.InetConstants;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetLevel;
import j9educationentities.InetStudent;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.GeneralFuncs;
import j9educationutilities.PasswordGenerator;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.DateColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkStudentUpload extends HttpServlet {
    //TODO review classes of j9educationaction package and ensure no instance
    //of services being created. instead create static versions of such fns
    private static final Logger log =
            Logger.getLogger(BulkStudentUpload.class.getName());
    private final static String STUD_UPLD_SESSION_NAME = "studentUploadData";
    private final static String CLASS_KEY_SESSION_NAME = "classkey";
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";
    private final static DateFormat DATE_FORMATTER =
            DateColumnAccessor.getDateFormatter();

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException 
    {
        PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN}; 	
    	try {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) {
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
			"</b></body></html>");
			return;
		} 
		
        res.setContentType("text/html");
        ServletOutputStream out = res.getOutputStream();
        if (req.getParameter(LOAD_PARAM_NAME) != null)
        {
            Object upldSessObj = req.getSession().getAttribute(STUD_UPLD_SESSION_NAME);
            Object clsSessObj = req.getSession().getAttribute(CLASS_KEY_SESSION_NAME);
            if (upldSessObj != null && clsSessObj != null) {
                //retrive data from session object
                ArrayList<StudentStruct> studData = (ArrayList<StudentStruct>) upldSessObj;
                String classKey = (String) clsSessObj;

                String okMsg = "<b>FName,LName,Login,Password</b><br/>";
                StringBuilder badMsg = new StringBuilder();
                ArrayList<String> loginIDs = new ArrayList<String>(studData.size());

                
                
                ArrayList<Key<InetStudent>> studentKeys = new ArrayList<Key<InetStudent>>(studData.size());
                HashMap<String, String> nameIndex = new HashMap<String, String>(studData.size());
                Objectify ofy = ObjectifyService.begin();
                
                //first pass lets see if we can fetch any logins
                for(StudentStruct elem : studData)
                {
                	studentKeys.add(new Key<InetStudent>(InetStudent.class, elem.login));
                	nameIndex.put(elem.login, elem.lname + ", " + elem.fname);
                }
                

                try
                {
                    Map<Key<InetStudent>, InetStudent> existingStudentObjs = 
                    	InetDAO4CommonReads.getEntities(studentKeys, ofy, false);                	
	                
                    if(existingStudentObjs.size() == 0) //no existing students with that key
	                {
                        badMsg.append("<ul>");
		                for(StudentStruct elem : studData)
		                {
		                    //schedule student creation
		                	try
		                	{
			                    TaskQueueHelper.scheduleCreateStudent(classKey, elem.fname, 
			                            elem.lname, elem.dob, elem.email, elem.login,
			                            elem.pwd);
			                    okMsg += elem.fname + "," + elem.lname + "," + elem.login + "," +
	                            elem.pwd + "<br/>";
			                    loginIDs.add(elem.login);			                    
		                	}
		                	catch(Exception e)
		                	{
		                		badMsg.append("<li>").append(elem.login).append("</li>");
		                	}
		                }
		                badMsg.append("</ul>");
		
		                //schedule addition of student keys to class roster
		                try
		                {
		                	TaskQueueHelper.scheduleAddStudentsToClassRoster(classKey, loginIDs);
		                }
		                catch(Exception e)
		                {
		                	out.println("<font color='red'><h3>Please notify IT that this class update was only" +
		                			" partially completed as per error: " + e.getMessage() +"</h3></font>");
		                }
	                }
                    else
                    {
                    	badMsg.append("<font color='red'>Upload request ignored because the following students IDs already esist: </font><ul>");
                    	for(InetStudent stud : existingStudentObjs.values())
                    	{
                    		badMsg.append("<li>").append("Student ID: ").append(stud.getKey().getName()).
                    			append(" Name: ").append(stud.getLname()).append(", ").
                    			append(stud.getFname());
                    		badMsg.append(" (spreedsheet contained name: ").append(nameIndex.get(stud.getKey().getName())).append(")</li>");
                    	}
                    	badMsg.append("</ul>If this is a different student but system generated IDs happen to match, you will" +
                    			" need to remove the student from spreadsheet and manually add later using the new student page");                   
                    	out.println(badMsg.toString());
                    	return;
                    }
                }
                catch(MissingEntitiesException ex)
                {
                	log.severe("This should not have occured!!!" + ex.getMessage());
                	out.println("<font color='red'>" + ex.getMessage());
                }
                
                int addedCount = loginIDs.size();

                if(addedCount != studData.size())
                {
                	out.println("<p><b><font color='red'>Students below not added, possibly due to a transient error" +
                			"Please try reuploading just these students in a few minutes</font></b></p>");
                	out.println(badMsg.toString());
                }

                if(addedCount > 0)
                {
                    out.println("<p>Scheduled addition of <b>" + addedCount + "</b> new " +
                            "students to: <font color='green'><b>" + InetDAO4CommonReads.keyToPrettyString(ofy.getFactory().stringToKey(classKey))
                            +"</font></b></p>");                	
                    out.println("<p>Please store login/password in a text file (recommend saving with .csv extension) so you can" +
                    		" forward to students for their initial login. They'll be able to change their passwords once logged in</p>" +
                            "<p>You will <b>NOT</b> be able to retrieve passwords later on. Though you can reset passwords on a per student basis</p>");
                    out.println("<ul>" + okMsg + "</ul>");
                }

                //set session data to null as we've saved the data
                req.setAttribute(STUD_UPLD_SESSION_NAME, null);
                req.setAttribute(CLASS_KEY_SESSION_NAME, null);
            }
            else
            {
                out.println("<b>unable to retrieve session info. Try " +
                        "enabling cookies in your browser.</b>");
            }
        }
        else
        {
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iterator = upload.getItemIterator(req);
                String classKey = "";
                while (iterator.hasNext()) {
                    FileItemStream item = iterator.next();
                    InputStream stream = item.openStream();
                    if (item.isFormField() && item.getFieldName().equals(CLASS_KEY_SESSION_NAME)) {
                    	classKey = Streams.asString(stream);
                        req.getSession().setAttribute(CLASS_KEY_SESSION_NAME, classKey);
                    } else {
                        log.warning("Bulk student upload attempt: " + item.getFieldName() + ", name = " + item.getName());
                        ColumnAccessor[] headerAccessors = InetConstants.STUDENT_UPLOAD_ACCESSORS;
                        String[] expectedHeaders = ColumnAccessor.getAccessorNames(headerAccessors);
                        //TODO, return error page if count greater than 200 rows
                        ExcelManager sheetManager = new ExcelManager(stream);
                        //confirm headers in ssheet match what we expect
                        String[] sheetHeaders;
                        try {
                            sheetHeaders = sheetManager.getHeaders();

                            //expectedheaders are uppercase so convert sheet prior to compare
                            for (int s = 0; s < sheetHeaders.length; s++) {
                                sheetHeaders[s] = sheetHeaders[s].toUpperCase();
                            }
                        } catch (InvalidColumnValueException ex) {
                        	log.severe(ex.getMessage());
                            out.println("<b>Unable to read excel sheet headers</b><br/> Error was: " 
                            		+ ex.getMessage());
                            return;
                        }
                        ArrayList<String> sheetOnly = new ArrayList<String>();
                        ArrayList<String> intersect = new ArrayList<String>();
                        ArrayList<String> expectedOnly = new ArrayList<String>();
                        GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders, intersect, expectedOnly, sheetOnly);
                        //handle errors in column headers
                        if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
                            out.println("<b>Uploaded sheet should contain only these headers:</b>");
                            printHtmlList(expectedHeaders, out);
                            if (expectedOnly.size() > 0) {
                                out.println("<b>These headers are missing from ssheet:</b>");
                                printHtmlList(expectedOnly, out);
                            }
                            if (sheetOnly.size() > 0) {
                                out.println("<b>These headers are not expected but were found in ssheet</b>");
                                printHtmlList(sheetOnly, out);
                            }
                            return;
                        }

                        sheetManager.initializeAccessorList(headerAccessors);
                        //todo, replace these with string builders
                        String goodDataHTML = "";
                        String badDataHTML = "";
                        String tempVal = "";
                        String[] htmlHeaders = new String[headerAccessors.length];
                        ArrayList<StudentStruct> sessionData = new ArrayList<StudentStruct>();
                        PasswordGenerator passwordGen = new PasswordGenerator();
                        String[] loginNameComponents = new String[2];

                        for (int i = 0; i < headerAccessors.length; i++) {
                            htmlHeaders[i] = headerAccessors[i].getHeaderName();
                        }

                        int numRows = sheetManager.totalRows();
                        HashMap<String, String> loginTable = new HashMap<String, String>(numRows);
                        HashMap<String, String> emailTable = new HashMap<String, String>(numRows);
                        for (int i = 1; i < numRows; i++) 
                        {
                        	try
                        	{
	                            String row = "<tr>";
	                            StudentStruct tempStudData = new StudentStruct();
	                            for (int j = 0; j < headerAccessors.length; j++) {
	                                tempVal = getValue(i, headerAccessors[j], sheetManager, tempStudData);
	                                row += "<td>" + tempVal + "</td>";
	                            }
	                            
	
	                            if (tempStudData.isValid)
	                            {
	                            	log.warning("Lname: " + tempStudData.lname + " Fname: " + tempStudData.fname);
	                                //generate login name and password
	                                String[] lnameComponents = tempStudData.lname.split(" ");
	                                String[] fnameComponents = tempStudData.fname.split(" ");
	                                
	                                //TODO: Fix this, make it more generic ... URGH ... quick way to deploy for moor plantation
	                                String generatedEmail = fnameComponents[0] + "." + lnameComponents[0] + "@students.fcahptib.edu.ng";
	                                
	                                String password = passwordGen.generatePassword();
	                                if(tempStudData.email.trim().length() == 0)
	                                	tempStudData.email = generatedEmail;
	                                tempStudData.pwd = password;
	                                String fullName = tempStudData.lname + ", " + tempStudData.fname;
	                                if(loginTable.containsKey(tempStudData.login))
	                                {
	                                	String errMsg = String.format("Cannot add %s with unique id %s. An earlier record (student: %s) in the file maps to that" +
	                                			" same id. Remove one of these rows from the file or edit the id", fullName, tempStudData.login, loginTable.get(tempStudData.login));
	                                	row = new StringBuilder("<tr><td colspan=").append(htmlHeaders.length).append(">").append(errMsg).
	                                	append("</td>").toString();
	                                	tempStudData.isValid = false;
	                                }
	                                else if(emailTable.containsKey(tempStudData.email))
	                                {
	                                	String errMsg = String.format("Cannot add %s with generated email %s. An earlier record (student: %s) in the file maps to that" +
	                                			" same email. Remove one of these rows from the file and later try adding via new student form if" +
	                                			" this is not a duplicate record", fullName, tempStudData.email, emailTable.get(tempStudData.email));
	                                	row = new StringBuilder("<tr><td colspan=").append(htmlHeaders.length).append(">").append(errMsg).
	                                	append("</td>").toString();
	                                	tempStudData.isValid = false;	                                	
	                                }
	                                else
	                                {
	                                	emailTable.put(tempStudData.email, fullName);
	                                	loginTable.put(tempStudData.login, fullName);
		                                row += "<td>" + tempStudData.login + "</td>";
		                                row += "<td>" + password + "</td></tr>";
	                                }
	                            }
	                            
	                            if(tempStudData.isValid)
	                            {
	                                //append to display result
	                                goodDataHTML += row;
	                                sessionData.add(tempStudData);
	                            }
	                            else 
	                            {
	                                badDataHTML += (row + "</tr>");
	                            }
                        	}
                        	catch(EmptyColumnValueException ex) {} //logic to help skip blank lines
                        }

                        String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
                        String htmlHeader = "";
                        for (String s : htmlHeaders) {
                            htmlHeader += "<TH>" + s + "</TH>";
                        }
                        String htmlTableEnd = "</TABLE>";

                        if (goodDataHTML.length() > 0) 
                        {
                        	Key<InetLevel> lk = ObjectifyService.begin().getFactory().stringToKey(classKey);
                            out.println(GOOD_DATA_PREFIX);
                            String goodHeaderSuffix = "<TH>Login ID</TH><TH>Temporary Password</TH>";
                            goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data for: " + 
                            InetDAO4CommonReads.keyToPrettyString(lk) + "</b>" + htmlTableStart + 
                            "<TR>" + htmlHeader + goodHeaderSuffix + "</TR>" + goodDataHTML + htmlTableEnd;
                        }

                        if (badDataHTML.length() > 0) 
                        {
                            out.println("<b>Below shows records with errors</b>");
                            out.println(htmlTableStart);
                            out.println("<TR>");
                            out.println(htmlHeader);
                            out.println("</TR>");
                            out.print(badDataHTML);
                            out.println(htmlTableEnd);
                        }

                        out.println(goodDataHTML);

                        req.getSession().setAttribute(STUD_UPLD_SESSION_NAME,
                                sessionData);
                    }
                }
            } catch (FileUploadException ex) {
                out.println("<b>File upload failed:</b>" + ex.getMessage());
            }
            catch (InvalidColumnValueException ex) {
                log.severe(ex.getMessage());
                out.println("<b>Unable to read excel sheet</b><br/> Error was: " + ex.getMessage());
            }            
        }
    }

    private String getValue(int row, ColumnAccessor accessor, ExcelManager sheetManager, StudentStruct studData) throws InvalidColumnValueException {
        Object temp;
        String val;
        String headerName = accessor.getHeaderName();
        try {
            //handle special case date, only non-string input
            if(headerName.equals(InetConstants.DOB_HEADER))
            {
            	try
            	{            	
	            	temp = sheetManager.getData(row, accessor);
	                if(temp == null)
	                    val = "";
	                else
	                	val = DATE_FORMATTER.format((Date) temp);            
            	}                
            	catch(Exception e)
            	{
            		val = "";
            	}            	
                studData.dob = val;
                return val;
            }
            temp = sheetManager.getData(row, accessor);

            //handle other fields
            if(temp == null)
                val = "";
            else
                val = ((String) temp);

            if (headerName.equals(InetConstants.FNAME_HEADER)) {
                studData.fname = val;
            } else if (headerName.equals(InetConstants.LNAME_HEADER)) {
                studData.lname = val;
            } else if (headerName.equals(InetConstants.EMAIL_HEADER)) {
                studData.email = val.toLowerCase();
            }else if(headerName.equals(InetConstants.LOGIN_HEADER))
            {
            	//TODO: transformation options, e.g. i want my ID to keep case and not be all caps
            	studData.login = val.toLowerCase();
            }
        }
        catch(EmptyColumnValueException ex)
        {
        	if(sheetManager.isBlankRow(row))
        		throw ex;
        	val = ex.getMessage();
        	log.warning(ex.getMessage());
        	studData.isValid = false;
        }        
        catch (InvalidColumnValueException ex) {
            val = ex.getMessage();
            studData.isValid = false;
        }
        return val;
    }

    private void printHtmlList(String[] list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }

    public void printHtmlList(List<String> list, ServletOutputStream out) throws IOException
    {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }
}

class StudentStruct implements Serializable
{
    String fname;
    String lname;
    String dob;
    String email;
    String login;
    String pwd;
    boolean isValid = true;
}