/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationactions;



import j9educationactions.login.LoginPortal;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetLevel;
import j9educationentities.InetSingleTestScores;
import j9educationgwtgui.server.ClassRosterServiceImpl;
import j9educationgwtgui.server.GPAHelper;
import j9educationgwtgui.server.ScoreManagerImpl;
import j9educationgwtgui.shared.PanelServiceConstants;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.TableMessage;
import j9educationgwtgui.shared.exceptions.LoginValidationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.ExcelDownloadHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class BulkStudentDownload extends HttpServlet {

    private static final Logger log =
            Logger.getLogger(BulkStudentDownload.class.getName());
    private Objectify ofy = ObjectifyService.begin();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_ADMIN}; 	    	
        String keyStr = req.getParameter("id");
        String downloadType = req.getParameter("type");
        if(downloadType.equals(PanelServiceConstants.DOWNLOAD_GRADES))
        	allowedRoles[1] = PanelServiceLoginRoles.ROLE_TEACHER;
    	try {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) {
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
			"</b></body></html>");
			return;
		}
		
        if (keyStr == null || downloadType == null) {
            log.warning("security issue: received request with one or more empty" +
                    " params");
            return;
        }
        

        try {
            if(downloadType.equals(PanelServiceConstants.DOWNLOAD_CLASS_GRADES_WITH_CUM))
            {
                ArrayList<TableMessage>[] sessList = GPAHelper.getClassCummulativeGPAGradesViaSession(req.getSession(),
                		keyStr);
                //reverse the array
                ArrayList<TableMessage>[] dataList = new ArrayList[sessList.length];
                int j = 0;
                for(int i = sessList.length-1; i >= 0; i--)
                {
                	dataList[j++] = sessList[i];
                }
                ExcelDownloadHelper.doDownload(res, "Cumulative-GPA-Report-" + keyStr + ".xls" , dataList, true);
                return;
            }
            
            if(downloadType.equals(PanelServiceConstants.DOWNLOAD_CLASS_WITH_SUM))
            {
                ArrayList<TableMessage>[] sessList = GPAHelper.getClassCummulativeGPAGradesViaSession(req.getSession(),
                		keyStr);
                //reverse the array
                ArrayList<TableMessage>[] dataList = new ArrayList[sessList.length];
                int j = 0;
                for(int i = sessList.length-1; i >= 0; i--)
                {
                	dataList[j++] = sessList[i];
                }
                ExcelDownloadHelper.doDownload(res, "Exam-Summary-Report-" + keyStr + ".xls" , dataList, true);
                return;
            }            

            Key key = ofy.getFactory().stringToKey(keyStr);
            String fileName = InetDAO4CommonReads.keyToPrettyString(key).replace(' ', '_') + ".xls";

            //fetch data
            ArrayList<TableMessage> data;
            if(downloadType.equals(PanelServiceConstants.DOWNLOAD_ROSTER))
            {
                Key<InetLevel> lk = (Key<InetLevel>) key;
                data = ClassRosterServiceImpl.getClassRosterTable(lk, ofy);
            }
            else if(downloadType.equals(PanelServiceConstants.DOWNLOAD_GRADES))
            {
                Key<InetSingleTestScores> tk = (Key<InetSingleTestScores>) key;
                data = ScoreManagerImpl.getTestScoresTable(tk, ofy);
            }
            else if(downloadType.equals(PanelServiceConstants.DOWNLOAD_CLASS_GRADES))
            {
                Key<InetLevel> lk = (Key<InetLevel>) key;
                data = ScoreManagerImpl.getClassScores(lk, false, ofy);
            }
            else if(downloadType.equals(PanelServiceConstants.DOWNLOAD_CLASS_GRADES_WITH_GPA))
            {
                Key<InetLevel> lk = (Key<InetLevel>) key;
                data = GPAHelper.getClassGPAGrades(lk, ofy);
            }                
            else
            {
                log.warning("security issue: unexpected type");
                return;
            }
            //download as ssheet to client comp
            ExcelDownloadHelper.doDownload(res, fileName, data);
        }
        catch (MissingEntitiesException ex)
        {
            throw new RuntimeException(ex.fillInStackTrace());
        }
        catch (LoginValidationException ex)
        {
            throw new RuntimeException(ex.fillInStackTrace());
        }
    }
}
