package j9educationentities;

import com.googlecode.objectify.Key;



/**
 *
 * @author Administrator
 */
public interface GAEPrimaryKeyEntity {
    public <T> Key<T> getKey();
}
