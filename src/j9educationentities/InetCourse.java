/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationentities;


import j9educationutilities.GeneralFuncs;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 *
 * @author Segun Razaq Sobulo
 */
@Cached
@Unindexed
public class InetCourse implements GAEPrimaryKeyEntity{
    @Id
    private String key;

    @Indexed private Key<InetTeacher> instructor;

    private Key<InetLevel> level;

    private LinkedHashSet<Key<InetSingleTestScores>> testScoresList;

    private Key<InetCumulativeScores> cumulativeScores;

    @Indexed private String courseName;
    
    int numOfUnits = 1;

    InetCourse()
    {
        testScoresList = new LinkedHashSet<Key<InetSingleTestScores>>();
    }

    InetCourse(String courseName, Key<InetLevel> levelKey) 
    {
        this();
        this.courseName = courseName;
        level = levelKey;
        key = getKeyString(level, courseName);
    }
    
    public static String getKeyString(Key<InetLevel> levelKey, String subjectName)
    {
        String format = "%s~%s";
        return String.format(format, levelKey.getName(), subjectName.toLowerCase());
    }

    public Key<InetCumulativeScores> getCumulativeScores() {
        return cumulativeScores;
    }

    //FIXME change this back to package access, this is a temporary bugfix
    public void setCumulativeScores(Key<InetCumulativeScores> cumScoreKey)
    {
        this.cumulativeScores = cumScoreKey;
    }

    public Key<InetLevel> getInetLevelKey() {
        return level;
    }

    @Override
    public Key<InetCourse> getKey() {
        return new Key(InetCourse.class, key);
    }

    void setTeacherKey(Key<InetTeacher> tk)
    {
        instructor = tk;
    }

    public Key<InetTeacher> getTeacherKey()
    {
        return instructor;
    }

    public Set<Key<InetSingleTestScores>> getTestScoresKeys()
    {
        return testScoresList;
    }

    boolean addTestScoresToCourse(Key<InetSingleTestScores> scoresKey)
    {
        return GeneralFuncs.addToHashSet(testScoresList, scoresKey);
    }

    public int getNumberOfTestScores()
    {
        return testScoresList.size();
    }

    boolean removeTestScoresFromCourse(Key<InetSingleTestScores> scoresKey)
    {
        return GeneralFuncs.removeFromHashSet(testScoresList, scoresKey);
    }

    public String getCourseName() {
        return courseName;
    }

    public List<Key<? extends InetTestScores>> getAllTestKeys()
    {
        //fetch test scores and their header names
        Set<Key<InetSingleTestScores>> testScoreKeys = testScoresList;
        List<Key<? extends InetTestScores>> batchList =
                new ArrayList<Key<? extends InetTestScores>>(testScoresList.size() + 1);
        batchList.addAll(testScoreKeys);
        batchList.add(cumulativeScores);
        return batchList;
    }
    
    public int getNumberOfUnits()
    {
    	return numOfUnits;
    }
    
    public void setNumberOfUnits(int units)
    {
    	numOfUnits = units;
    }
}
