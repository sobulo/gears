/**
 * 
 */
package j9educationentities;

import java.util.HashMap;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfNotEmpty;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class ApplicationParameters {
	
	@Id
	String myId;
	
	@Serialized
	HashMap<String, String> params;
	
	@Indexed(IfNotEmpty.class) String type;
	
	ApplicationParameters()
	{
		this(null, null, null);
	}
	
	ApplicationParameters(String name)
	{
		this(name, null, null);
	}
	
	ApplicationParameters(String name, HashMap<String, String> params)
	{
		this(name, params, null);
	}
	
	ApplicationParameters(String name, HashMap<String, String> params, String type)
	{
		this.myId = name;
		this.params = params == null? new HashMap<String, String>() : params;
		this.type = type;
	}
		
	public HashMap<String, String> getParams() {
		return params;
	}

	public void setParams(HashMap<String, String> params) {
		this.params = params;
	}

	public Key<ApplicationParameters> getKey()
	{
		return getKey(myId);
	}
	
	public static Key<ApplicationParameters> getKey(String id)
	{
		return new Key<ApplicationParameters>(ApplicationParameters.class, id);
	}
}
