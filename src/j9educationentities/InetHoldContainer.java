/**
 * 
 */
package j9educationentities;

import j9educationutilities.GeneralFuncs;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Cached
@Unindexed
public class InetHoldContainer implements GAEPrimaryKeyEntity{
	@Id
	String key;
	
	private HashSet<Key<InetHold>> studentHolds;

	private InetHoldContainer()
	{
		studentHolds = new HashSet<Key<InetHold>>();
	}
	
	public InetHoldContainer(Key<InetLevel> levelKey)
	{
		this();
		key = levelKey.getName();
	}
	
	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<InetHoldContainer> getKey() {
		return new Key<InetHoldContainer>(InetHoldContainer.class, key);
	}
	
    boolean addHold(Key<InetHold> holdKey)
    {
        return GeneralFuncs.addToHashSet(studentHolds, holdKey);
    }
    
    boolean removeHold(Key<InetHold> holdKey)
    {
        return GeneralFuncs.removeFromHashSet(studentHolds, holdKey);
    }
    
    public Set<Key<InetHold>> getHolds()
    {
    	return studentHolds;
    }

}
