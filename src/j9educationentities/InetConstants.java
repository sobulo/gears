/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.DateColumnAccessor;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.NumberColumnAccessor;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetConstants {
	
    public final static String NOT_PUBLISHED_SCORE_TEXT = "Not Available Now";
    public final static Double NOT_PUBLISHED_SCORE_DISPLAYED = Double.NaN;	
    public final static String EXCEL_DOWN_KEY_PARAM = "excl";
	//number and date formats
	public final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat("d MMM yyyy");
	static
	{
		NUMBER_FORMAT.setMaximumFractionDigits(2);
		NUMBER_FORMAT.setMinimumIntegerDigits(1);
	}
	
	public final static String DELIMITTER_REGEX = "\\|\\|";
	public final static String DELIMITTER = "||";
	public final static String SCHOOL_REPORT_CARD_IMAGE = "Report Card Image";

    public final static ColumnAccessor[] STUDENT_UPLOAD_ACCESSORS =
            new ColumnAccessor[5];
    public final static ColumnAccessor[] COURSE_NAME_UPLOAD_ACCESSORS =
            new ColumnAccessor[2];
    public final static String FNAME_HEADER = "FNAME";
    public final static String LNAME_HEADER = "LNAME";
    public final static String EMAIL_HEADER = "EMAIL";
    public final static String DOB_HEADER = "DOB";
    public final static String LOGIN_HEADER = "UNIQUE ID";
    public final static String COURSE_NAME_HEADER = "COURSE NAME";
    public final static String COURSE_CODE_HEADER = "COURSE CODE";
    
    static
    {
        STUDENT_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(FNAME_HEADER, true);
        STUDENT_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(LNAME_HEADER, true);
        STUDENT_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(EMAIL_HEADER, false);
        STUDENT_UPLOAD_ACCESSORS[3] = new DateColumnAccessor(DOB_HEADER, false);
        STUDENT_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(LOGIN_HEADER, true);
        
        COURSE_NAME_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(COURSE_CODE_HEADER, true);
        COURSE_NAME_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(COURSE_NAME_HEADER, true);
        
    }

    public final static int DEFAULT_NUM_OF_STUDENTS_PER_COURSE = 32;

    //user session names
    public final static String USESS_GRADE_UPLD_DATA_NAME = 
            "j9education.data4gradeupld";
    public final static String USESS_GRADE_UPLD_TEST_KEY = 
            "j9education.key4gradeupld";
    public final static String USESS_CLASS_GRADE_UPLD_DATA_NAME = 
        "j9education.data4classgradeupld";
    public final static String USESS_CLASS_GRADE_UPLD_LEVEL_KEY = 
        "j9education.key4classgradeupld";
    public final static String SESS_PREFIX = "j9education.";

    //grade upload accessors
    public final static ColumnAccessor[] GRADE_UPLOAD_ACCESSORS =
            new ColumnAccessor[4];

    //TODO merge contstants so we dont have multiple fname lname headers etc
    public final static String GRD_FNAME_HEADER = "FIRST NAME";
    public final static String GRD_LNAME_HEADER = "LAST NAME";
    public final static String GRD_LOGIN_HEADER = "LOGIN NAME";
    public final static String GRD_SCORE_STUB = "SCORE_STUB";

    static
    {
        GRADE_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(GRD_FNAME_HEADER, true);
        GRADE_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(GRD_LNAME_HEADER, true);
        GRADE_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(GRD_LOGIN_HEADER, true);
        GRADE_UPLOAD_ACCESSORS[3] = new NumberColumnAccessor(GRD_SCORE_STUB, true);
    }
    
    public final static String BIO_GROUP_HEADER = "Student Bio";
    public final static String GRADE_HEADER = "Grade";
    public final static String CUMULATIVE_TEST_NAME = "total";
    public final static String PERCENT_SIGN = " %";

    public final static ColumnAccessor[] CLASS_GRADE_UPLOAD_ACCESSORS = new ColumnAccessor[3];
    
    static
    {
    	CLASS_GRADE_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(GRD_LNAME_HEADER, true);
    	CLASS_GRADE_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(GRD_FNAME_HEADER, true);
    	CLASS_GRADE_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(GRD_LOGIN_HEADER, true);
    	for(int i = 0; i < CLASS_GRADE_UPLOAD_ACCESSORS.length; i++)
    		CLASS_GRADE_UPLOAD_ACCESSORS[i].setGroupName(BIO_GROUP_HEADER);
    }
    
    public final static String LOGIN_PASSWORD_MAPNAME_SUFFIX = "PWD";
    public final static String LOGIN_FOREIGNLIST_NAME_SUFFIX = "FRN";
    
    //task constants

    /**
     * param name specyfing type of service to perform, servlets can choose to use
     * this field to trigger different actions based on corresponding value specified
     */
    public final static String SERVICE_TYPE = "service";

    /**
     * param name specifying that task should create a stringoption
     */
    public final static String SERVICE_DEPENDENT_OPTN = "stringoption";

    /**
     * param name specifying that task should create new student
     */
    public final static String SERVICE_CREATE_STUDENT = "newstudent";
    
    /**
     * param name specifying that task should create new student
     */
    public final static String SERVICE_CREATE_USER_BILL = "newuserbill";    
    
    /**
     * param name specifying that task should create course
     */
    public final static String SERVICE_CREATE_COURSE = "newcourse";

    /**
     * param name specifying that student roster for class/level be updated
     */
    public final static String SERVICE_UPDATE_LEVEL_ROSTER = "classroster";
    
    
	public static final String SERVICE_UPDATE_COURSE_ROSTER = "courserosterupdt";    
    
    /**
     * param name specifying that student be removed from level roster
     */
    public final static String SERVICE_REMOVE_LEVEL_ROSTER = "removeclassroster";    

    /**
     * param name for update of level list for school
     */
    public final static String SERVICE_ADD_SCHOOL_LEVEL = "level2school";

    /**
     * param name for requesting update of course list for a given level
     */
    public final static String SERVICE_ADD_LEVEL_COURSE = "crs2lev";

    /**
     * param name for requesting update of course list for a given level
     */
    public final static String SERVICE_ADD_STUDENT_GUARDIAN = "addguard2stud";

     /**
     * param name for requesting of course to student course list
     */
    public final static String SERVICE_UPDATE_STUDENT_COURSE_LIST = "studcrslst";

     /**
     * param name for requesting addition of level to student level list
     */
    public final static String SERVICE_UPDATE_STUDENT_LEVEL_LIST = "studlevlst";

     /**
     * session name for name of school user is logging into
     */
    public final static String SERVICE_UPDATE_CUMULATIVE_SCORES = "cumscoreupdt";
    
    /**
     * session name for name of school user is logging into
     */
    public final static String SERVICE_UPDATE_CLASS_SCORES = "classcrupdt";    
    
    /**
     * session name for name of school user is logging into
     */
    public final static String SERVICE_REMOVE_COURSE_FRM_LEVEL = "remcrsfrmlev";
    
    /**
     * session name for name of school user is logging into
     */
    public final static String SERVICE_REMOVE_LEVEL_FRM_STUDENT = "remlevfrmstud";
        
    /**
     * session name for name of school user is logging into
     */
    public final static String SERVICE_CREATE_HOLD_CONTAINER = "hldcntcreate";
    
    /**
     * session name for name of school user is logging into
     */
    public final static String SERVICE_BREAK_GUARD_STUDENT_PAIR = "brkguardstud";    
    
    /**
     * session name for name of school user is logging into
     */
    public final static String SERVICE_REMOVE_COURSE_FRM_STUDENT = "remcrsfrmstud";     
    
    /**
     * session name for requesting sending messages (text or email)
     */
    public final static String SERVICE_SEND_MESSAGE = "sndmsg";    

    /**
     * param name for stringoption to be created
     */
    public final static String SCHL_DEPENDENT_TASK_OPT_NAME_PARAM = "optname";
    
    /**
     * school id. each namespace should only have 1 school thus ok to have
     * this value be a constant.
     */
    public final static String SCHOOL_ID = "school";
    public final static String EMAIL_CONTROLLER_ID = "EMAIL Controller";
    public final static String EMAIL_RC_CONTROLLER_ID = "EMAIL Report Card Controller";    
    public final static String SMS_CONTROLLER_ID = "SMS Controller";
    public final static String EMAIL_BILL_CONTROLLER_ID = "EMAIL Bill-Invoice Controller";
    public final static String APP_EMAIL_SUFFIX = "@schoolgrep.appspotmail.com";

    /**
     * param name for toAddress
     */
    public final static String TO_ADDR_PARAM = "toAddy";
    
    /**
     * param name for msg
     */
    public final static String MSG_BODY_PARAM = "msgCntent";    
    
    /**
     * param name for first name
     */
    public final static String FNAME_PARAM = "fname";

    /**
     * param name for students last name
     */
    public final static String LNAME_PARAM = "lname";

    /**
     * param name for email
     */
    public final static String EMAIL_PARAM = "email";
    
    

    /**
     * param name for login id
     */
    public final static String LOGIN_ID_PARAM = "in";

    /**
     * param name for password
     */
    public final static String PASSWORD_PARAM = "or";

    /**
     * param name for level/class key
     */
    public final static String LEVEL_KEY_PARAM = "levks";
    
    /**
     * param name for billDescription key
     */
    public final static String BILL_DESC_KEY_PARAM = "btks";    
    
    /**
     * param name for Message Controller key
     */
    public final static String MSG_CONTROLLER_KEY_PARAM = "mcks";      

    /**
     * param name for subject/course key
     */
    public final static String SUBJECT_KEY_PARAM = "subks";

    /**
     * param name for test key
     */
    public final static String TEST_KEY_PARAM = "tstks";
    
    /**
     * param name for test key
     */
    public final static String TEST_KEY_PARAM_ORDER = "tstksord";    

    /**
     * param name for cumulative scores key str
     */
    public final static String CUMULATIVE_KEY_PARAM = "cumks";

    /**
     * param name for cumulative scores key str
     */
    public final static String STUDENT_KEY_PARAM = "studks";

    /**
     * param name for cumulative scores key str
     */
    public final static String GUARDIAN_KEY_PARAM = "guardks";

    /**
     * param name for test key
     */
    public final static String BIRTHDAY_PARAM = "dob";

    /**
     * session name for name of school user is logging into
     */
    public final static String SCHOOL_NAME_SESSION = "j9education.schoolname";


}





