/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import j9educationgwtgui.shared.GradeDescription;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetGradeDescriptions implements Serializable{
    private static final long serialVersionUID = -3674368302498532134L;
    TreeMap<Double, GradeDescription> gradeCutoffMap;

    public InetGradeDescriptions(){}

    /**
     *
     * @param descriptionForZero description for lowest possible
     * score (e.g. F for 0)
     * @param gpa
     */
    public InetGradeDescriptions(String descriptionForZero, Double gpa)
    {
        gradeCutoffMap = new TreeMap<Double, GradeDescription>();
        gradeCutoffMap.put(0.0, new GradeDescription(descriptionForZero, gpa));
    }

    public int getNumberOfGrades()
    {
        return gradeCutoffMap.size();
    }

    public void addGradeDescription(String gradeDescription, Double gradeDescCutoff, Double gpa)
    {
        if(gradeDescCutoff <= 0.0)
            throw new IllegalArgumentException("Only the first grade can be lower than 0");

        if(gradeDescCutoff > 100.0)
            throw new IllegalArgumentException("Can't register a grade greater than 100.0");

        if(gradeCutoffMap.containsKey(gradeDescCutoff))
            throw new IllegalArgumentException("There is already a grade: " +
                    gradeCutoffMap.get(gradeDescCutoff).letterGrade +
                    " registered with that score");

        Map.Entry<Double, GradeDescription> priorEntry = gradeCutoffMap.floorEntry(gradeDescCutoff);

        if(gpa <= priorEntry.getValue().gpa)
        {
            throw new IllegalArgumentException("Grade: " + gradeDescription +
                    "with cutoff score: " + gradeDescCutoff + " cannot be registered with gpa: "
                    + gpa + " because there is already a registered grade: " +
                    priorEntry.getValue().letterGrade + " which has a lower cutoff score: "
                    + priorEntry.getKey() + " but a higher/equal gpa: " + priorEntry.getValue().gpa);
        }

        priorEntry = gradeCutoffMap.ceilingEntry(gradeDescCutoff);
        if(priorEntry != null && gpa >= priorEntry.getValue().gpa)
        {
            throw new IllegalArgumentException("Grade: " + gradeDescription +
                    "with cutoff score: " + gradeDescCutoff + " cannot be registered with gpa: "
                    + gpa + " because there is already a registered grade: " +
                    priorEntry.getValue().letterGrade + " which has a higher cutoff score: "
                    + priorEntry.getKey() + " but a lower/equal gpa: " + priorEntry.getValue().gpa);
        }


        gradeCutoffMap.put(gradeDescCutoff, new GradeDescription(gradeDescription, gpa));
    }

    private GradeDescription getGradeDescription(Double score)
    {
        if(score < 0.0 || score > 100.0)
            throw new IllegalArgumentException("Unable to generate grade " +
                    "description for score " + score +
                    ". Value should be between 0-100");
        return gradeCutoffMap.floorEntry(score).getValue();
    }
    
    public String getLowestLetterGrade()
    {
    	return gradeCutoffMap.firstEntry().getValue().letterGrade;
    }
    
    public String getHighestLetterGrade()
    {
    	return gradeCutoffMap.lastEntry().getValue().letterGrade;
    }    

    public String getLetterGrade(Double score)
    {
        return getGradeDescription(score).letterGrade;
    }

    public double getGpa(Double score)
    {
        return getGradeDescription(score).gpa;
    }

    public String[] gradeDescriptionsToStrings()
    {
        String[] result = new String[gradeCutoffMap.size()];
        Iterator<Map.Entry<Double, GradeDescription>> gradeDescs =
                gradeCutoffMap.entrySet().iterator();
        int i = 0;
        while(gradeDescs.hasNext())
        {
            Map.Entry<Double, GradeDescription> desc = gradeDescs.next();
            result[i++] = "Grade: " + desc.getValue().letterGrade + " Cutoff: "
                    + desc.getKey() + " GPA: " + desc.getValue().gpa;
        }
        return result;
    }

    public boolean removeGrade(Double score)
    {
        return (gradeCutoffMap.remove(score) != null);
    }
    
    
}
