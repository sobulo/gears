/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import java.util.HashSet;

import javax.persistence.Id;

import com.googlecode.objectify.Key;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class StringOptions implements GAEPrimaryKeyEntity
{
    @Id
    private String key;

    private String displayName;

    private HashSet<String> optionElements;

    StringOptions()
    {
        optionElements = new HashSet<String>();
    }

    StringOptions(String id, String displayName)
    {
        setDisplayName(displayName);
        this.key = id;
    }
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public Key<StringOptions> getKey() {
        return new Key(StringOptions.class, key);
    }

    public HashSet<String> getOptionElements() {
        return optionElements;
    }

    public void setOptionElements(HashSet<String> optionElements) {
        this.optionElements = optionElements;
    }
}
