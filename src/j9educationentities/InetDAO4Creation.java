/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package j9educationentities;



import j9educationentities.InetHold.InetHoldType;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetDAO4Creation{

    private static final Logger log = Logger.getLogger(InetDAO4Creation.class.getName());

    /**
     *
     * Create a new school in the datastore using the specified input params,
     * throws an exception if message already exists in datastore
     *
     * @param schoolName
     * @param accronym
     * @param email
     * @param webAddress
     * @param address
     * @param phoneNums 
     * @return
     * @throws DuplicateEntitiesException
     */
    public static InetSchool createSchool(String schoolName, String accronym,
            String email, String webAddress, String address, HashSet<String> phoneNums)
            throws DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        InetSchool s = new InetSchool(schoolName, accronym, email, webAddress, 
                address, phoneNums);
        try {
            InetSchool schl = ofy.find(s.getKey());
            if (schl != null) {
                String exMsg = "a school [" +
                        schl.getSchoolName() + "] already exists in this namespace";
                log.severe(exMsg);
                throw new DuplicateEntitiesException(exMsg);
            }
            ofy.put(s);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
        log.warning("Created School[" + s.getSchoolName() + "] with Key[" +
                s.getKey() + "]");
        return s;
    }

    public static InetLevel createLevel(String term,
            String academicYear, String levelName, String subLevelName) 
            throws DuplicateEntitiesException
    {
        InetLevel level = new InetLevel(term, academicYear, levelName,
                subLevelName);
        Objectify ofy = ObjectifyService.beginTransaction();
        try {
            InetLevel levCheck = ofy.find(level.getKey());
            if (levCheck != null) {
                DuplicateEntitiesException ex = new DuplicateEntitiesException("Level already exists. Details: " + level.toString());
                log.severe(ex.getMessage());
                throw ex;
            }
            ofy.put(level);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
        return level;
    }
    
    public static InetHoldContainer createHoldContainer(Key<InetLevel> levelKey) throws DuplicateEntitiesException
    {
    	InetHoldContainer hc = new InetHoldContainer(levelKey);
        Objectify ofy = ObjectifyService.beginTransaction();
        try {
            InetHoldContainer hcCheck = ofy.find(hc.getKey());
            if (hcCheck != null) {
            	String msgFmt = "Hold container already exists for: ";
                DuplicateEntitiesException ex = new DuplicateEntitiesException(msgFmt + hc.getKey().getName());
                log.severe(msgFmt + hc.toString());
                throw ex;
            }
            ofy.put(hc);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
        return hc;    	
    }
    
    public static InetHold createHold(Key<InetHoldContainer> holdContainerKey, Key<InetStudent> studKey,
    		InetHoldType type, String message, String note, boolean resolutionStatus) 
    	throws MissingEntitiesException
    {
    	InetHold hold = new InetHold(holdContainerKey, studKey, type);
    	hold.setMessage(new Text(message));
    	hold.setNote(new Text(note));
    	hold.setResolutionStatus(resolutionStatus);
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
        	InetHoldContainer hc = ofy.find(holdContainerKey);
        	if(hc == null)
        	{
        		String msgFmt = "Unable to find holdcontainer for: ";
        		log.severe(msgFmt + holdContainerKey.toString());
        		throw new MissingEntitiesException(msgFmt + holdContainerKey.getName());
        	}
        	ofy.put(hold);
        	hc.addHold(hold.getKey());
        	ofy.put(hc);
        	ofy.getTxn().commit();
        }
        finally
        {
        	if(ofy.getTxn().isActive())
        		ofy.getTxn().rollback();
        }
        return hold;
    }

    public static InetCourse createCourse(Key<InetLevel> lk, String courseName)
            throws DuplicateEntitiesException, MissingEntitiesException
    {
        InetCourse c = new InetCourse(courseName, lk);
        Objectify ofy = ObjectifyService.beginTransaction();
        try 
        {	
            InetCourse cTemp = ofy.find(c.getKey());
            if (cTemp != null) {
                log.severe("Duplicate key [" + c.getKey() + "]");
                throw new DuplicateEntitiesException("Course: " +
                        c.getKey().getName() + " already exists");
            }
            
            InetCumulativeScores cumScore = new InetCumulativeScores(c.getKey());
            ofy.put(cumScore);
            c.setCumulativeScores((Key<InetCumulativeScores>) cumScore.getKey());
            ofy.put(c);
            ofy.getTxn().commit();
        }
        finally 
        {
            if (ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }

        log.info("Created Course[" + c.getCourseName() + "] with Key[" +
                c.getKey() + "]");
        return c;
    }

    public static InetSingleTestScores createTestScore(Key<InetCourse> ck,
            String testName, double maxScore, double weight)
            throws DuplicateEntitiesException, MissingEntitiesException
    {
    	//check testname doesn't match reserved cumulative name
    	String cumName = InetCumulativeScores.getCumScoreKey(ck).getName();
    	String testNameCmp = testName.toLowerCase().trim();
    	log.warning("Comparing: " + cumName + " with " + testNameCmp);
    	if(cumName.equalsIgnoreCase(testNameCmp))
    	{
            DuplicateEntitiesException ex = new DuplicateEntitiesException("test name: " +
                    testName + " is reserved for displaying total scores");
            throw ex;    		
    	}
    	
        InetSingleTestScores test = new InetSingleTestScores(testName, maxScore,
                weight, ck);

        Objectify ofy = ObjectifyService.beginTransaction();
        //go ahead and add test score to course
        try
        {
            //find course object
            Key<InetCumulativeScores> cumKey = InetCumulativeScores.getCumScoreKey(ck);
            ArrayList<Key<? extends Object>> batchKeys = new ArrayList<Key<? extends Object>>();
            batchKeys.add(ck);
            batchKeys.add(cumKey);
            Map<Key<Object>, Object> batchObjs = ofy.get(batchKeys);
      
            if (batchObjs.size() != batchKeys.size()) {
                MissingEntitiesException ex = new MissingEntitiesException("Unable" +
                        " to load course/cumulative info for: " + ck);
                log.severe(ex.getMessage());
                throw ex;
            }

            InetCourse course = (InetCourse) batchObjs.get(ck);
            InetCumulativeScores cumScore = (InetCumulativeScores) batchObjs.get(cumKey);

            InetSingleTestScores temp =(InetSingleTestScores) ofy.find(test.getKey());
            if(temp != null)
            {
            	String msgFmt = "test already exists: ";
                DuplicateEntitiesException ex = new DuplicateEntitiesException(msgFmt + 
                		test.getKey().getName());
                log.severe(msgFmt + test.getKey());
                throw ex;
            }

            if (!course.addTestScoresToCourse((Key<InetSingleTestScores>) test.getKey())) {
                String exMsg = "Test score: " +
                        test.getKey().getName() + " already exists in: " +
                        course.getKey().getName();
                log.severe(exMsg);
                throw (new DuplicateEntitiesException(exMsg));
            }

            Iterator<Key<InetStudent>> studKeys = cumScore.getStudentKeys().iterator();
            while(studKeys.hasNext())
                test.addStudentScore(studKeys.next(), 0.0);

            ofy.put(course, test);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }

        }
        log.info("Created Test Score[" + test.getTestName() + "] with Key[" +
                test.getKey() + "]");
        return test;
    }
    
    public static ArrayList<GAEPrimaryKeyEntity> createCourseAndTests(Key<InetLevel> levelKey,
    		String courseName, HashMap<String, Double[]> testInfo) 
    	throws DuplicateEntitiesException, MissingEntitiesException
    {
        ArrayList<GAEPrimaryKeyEntity> objectsToSave = new ArrayList<GAEPrimaryKeyEntity>(testInfo.size() + 2);
        InetCourse c = new InetCourse(courseName, levelKey);
        objectsToSave.add(c);    
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {	
	        InetCourse cTemp = ofy.find(c.getKey());
	        if (cTemp != null) 
	        {
	            log.severe("Duplicate key [" + c.getKey() + "]");
	            throw new DuplicateEntitiesException("Course: " +
	                    c.getKey().getName() + " already exists");
	        }
	        InetCumulativeScores cumScore = new InetCumulativeScores(c.getKey());
	    	c.setCumulativeScores((Key<InetCumulativeScores>) cumScore.getKey());
	    	objectsToSave.add(c);
	    	objectsToSave.add(cumScore);
	    	
	    	//check testname doesn't match reserved cumulative name
	    	String cumName = cumScore.getKey().getName();
	    	for(String testName : testInfo.keySet())
	    	{
		    	String testNameCmp = testName.toLowerCase().trim();
		    	if(cumName.equalsIgnoreCase(testNameCmp))
		    	{
		            DuplicateEntitiesException ex = new DuplicateEntitiesException("test name: " +
		                    testName + " is reserved for displaying total scores");
		            log.severe(ex.getMessage());
		            throw ex;    		
		    	}
		    	
		    	Double[] properties = testInfo.get(testName);
		        InetSingleTestScores test = new InetSingleTestScores(testName, properties[0],
		                properties[1], c.getKey());
		        
	            InetSingleTestScores temp =(InetSingleTestScores) ofy.find(test.getKey());
	            if(temp != null)
	            {
	                DuplicateEntitiesException ex = new DuplicateEntitiesException("test" +
	                        " already exists: " + test.getKey());
	                log.severe(ex.getMessage());
	                throw ex;
	            }

	            if (!c.addTestScoresToCourse((Key<InetSingleTestScores>) test.getKey())) {
	                String exMsg = "Test score: " +
	                        test.getKey().getName() + " already exists in: " +
	                        c.getKey().getName();
	                log.severe(exMsg);
	                throw (new DuplicateEntitiesException(exMsg));
	            }
	            objectsToSave.add(test);
	    	}
	    	ofy.put(objectsToSave);
	    	ofy.getTxn().commit();
        }
        finally 
        {
            if (ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }
        return objectsToSave;    	
    }

    public static InetUser createUser(String fname, String lname, 
            String email, HashSet<String> phoneNums, String loginName, String password)
            throws DuplicateEntitiesException
    {
    	
        InetUser u = new InetUser(fname, lname, email, phoneNums, loginName,
                InetDAO4Updates.getHashPwd(password));
        Objectify ofy = ObjectifyService.beginTransaction();
        try {
            InetUser uTemp = ofy.find(u.getKey());
            if (uTemp != null) {
                DuplicateEntitiesException ex =
                        new DuplicateEntitiesException("User: " +
                        u.getKey().getName() + " already exists");
                log.severe(ex.getMessage());
                throw ex;
            }
            ofy.put(u);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
        log.info("Created User[" + u.getFname() + " " + u.getLname() +
                "] with Key[" + u.getKey() + "]");
        return u;
    }

    public static InetStudent createStudent(String fname,
            String lname, Date dob, String email, HashSet<String> phoneNums,
            String loginName, String password, String levelKeyStr) throws
            DuplicateEntitiesException, MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        InetStudent s = new InetStudent(fname, lname, dob, email, phoneNums,
                loginName, InetDAO4Updates.getHashPwd(password), levelKey);
        try
        {
            //confirm new student obj doesn't exist already
            InetStudent sTemp = (InetStudent) ofy.find(s.getKey());
            if (sTemp != null) {
                DuplicateEntitiesException ex = new DuplicateEntitiesException(
                        "Student already exists: " + s.getKey().getName());
                log.severe(ex.getMessage());
                throw ex;
            }

            //persist student object
            ofy.put(s);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }

        log.info("Created Student[" + s.getFname() + " " + s.getLname() +
                "] with Key[" + s.getKey() + "]");
        return s;
    }

    public static InetGuardian createGuardian(Key<InetStudent> stdK, String fname, 
    		String lname, String email, HashSet<String> phoneNums, String loginName, String password)
            throws DuplicateEntitiesException, MissingEntitiesException
    {
        InetGuardian g = new InetGuardian(fname, lname, email, phoneNums,
                loginName, InetDAO4Updates.getHashPwd(password));

        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            InetGuardian gTemp = (InetGuardian) ofy.find(g.getKey());

            if (gTemp != null) {
                DuplicateEntitiesException ex = new DuplicateEntitiesException(
                        "Guardian already exists: " + g.getKey().getName());
                log.severe(ex.getMessage());
                throw ex;
            }
            ofy.put(g);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }

        log.info("Created Guardian[" + g.getFname() + " " + g.getLname() +
                "] with Key[" + g.getKey() + "]");

        return g;
    }

    public static InetTeacher createTeacher(String fname, String lname, String email, 
    		HashSet<String> phoneNums, String loginName, String password)
            throws DuplicateEntitiesException
    {
        InetTeacher t = new InetTeacher(fname, lname, email, phoneNums, 
                loginName, InetDAO4Updates.getHashPwd(password));
                
        Objectify ofy = ObjectifyService.beginTransaction();
        
        try {
            InetTeacher tTemp = (InetTeacher) ofy.find(t.getKey());
            if (tTemp != null)
            {
                DuplicateEntitiesException ex =
                        new DuplicateEntitiesException("Teacher already exists: "
                        + t.getKey().getName());
                log.severe(ex.getMessage());
                throw ex;
            }
            ofy.put(t);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }

        log.info("Created Teacher[" + t.getFname() + " " + t.getLname() +
                "] with Key[" + t.getKey() + "]");
        return t;
    }

    public static StringOptions createStringOptions(String id, String optionName)
            throws DuplicateEntitiesException
    {
        log.info("entered string options fn");
        StringOptions option = new StringOptions(id, optionName);
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            StringOptions tempOpt = ofy.find(option.getKey());
            if(tempOpt != null)
                throw new DuplicateEntitiesException("String Option exists" +
                        " already: " + option.getKey().getName());
            ofy.put(option);
            ofy.getTxn().commit();
        }
        finally
        {
            if(ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }
        return option;
    }
    
	public static InetNotice createNotice(String id, String title) throws DuplicateEntitiesException
	{
		InetNotice note = new InetNotice(id, title, "");
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetNotice n = ofy.find(note.getKey());
			if(n != null)
			{
				String msgFmt = "Notice exists already: ";
				log.severe(msgFmt + n.getKey());
				throw new DuplicateEntitiesException(msgFmt + n.getKey().getName());
			}
			ofy.put(note);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return note;
	}
	
	public static ApplicationParameters createApplicationParameters(String id,
			HashMap<String, String> parameters, String type)
			throws DuplicateEntitiesException {
		ApplicationParameters paramsObject = new ApplicationParameters(id,
				parameters, type);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Key<ApplicationParameters> existingKey = paramsObject.getKey();
			ApplicationParameters temp = ofy.find(existingKey);
			if (temp != null)
				throw new DuplicateEntitiesException("Application parameter already exists");
			else {
				ofy.put(paramsObject);
				ofy.getTxn().commit();
			}
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return paramsObject;
	}
	
}
