/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import j9educationutilities.GeneralFuncs;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 *
 * @author Segun Razaq Sobulo
 */
@Cached
@Unindexed
public class InetSchool implements GAEPrimaryKeyEntity{
    @Id
    private String key;

    private String schoolName;

    private String accronym;

    private String email;

    private String address;

    private HashSet<String> phoneNumbers;

    private Set<Key<InetLevel>> inetLevels;

    private final static int DEFAULT_NUM_OF_LEVELS = 40;

    private String webAddress;

    @Serialized
    private InetGradeDescriptions defaultGradingScheme;

    InetSchool()
    {
        this.inetLevels = new HashSet<Key<InetLevel>>(DEFAULT_NUM_OF_LEVELS);
    }

    InetSchool(String schoolName, String accronym, String email, String webAddress,
            String address, HashSet<String> phoneNumbers)
    {
        this();
        this.schoolName = schoolName;
        this.accronym = accronym;
        this.email = email;
        this.address = address;
        this.phoneNumbers = phoneNumbers;
        this.webAddress = webAddress;

        //note data for each school is segregated in the appengine datastore via
        //namespaces. thus it's safe for the id to be constant. we enforce the 1
        //school per namespace by ensuring the dao checks for this and throws an
        //exception if an attempt is made to break this requirement
        this.key = InetConstants.SCHOOL_ID;
    }

    public InetGradeDescriptions getDefaultGradingScheme() {
        return defaultGradingScheme;
    }

    public void setDefaultGradingScheme(InetGradeDescriptions defaultGradingScheme) {
        this.defaultGradingScheme = defaultGradingScheme;
    }

    public Set<Key<InetLevel>> getInetLevelKeys() {
        return inetLevels;
    }

    boolean addInetLevelKey(Key<InetLevel> lk)
    {
        //return inetLevels.add(level);
        return GeneralFuncs.addToHashSet(inetLevels, lk);
    }
    
    //FIXME bugfix to remove errant level, need to add a remove level feature to super admin menu
    public boolean removeInetLevelKey(Key<InetLevel> lk)
    {
        //return inetLevels.remove(level);
        return GeneralFuncs.removeFromHashSet(inetLevels, lk);
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getAccronym() {
        return accronym;
    }

    void setAccronym(String accronym) {
        this.accronym = accronym;
    }

    public String getAddress() {
        return address;
    }

    void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    void setEmail(String email) {
        this.email = email;
    }

    public Key<InetSchool> getKey() {
        return new Key<InetSchool>(InetSchool.class, key);
    }

    void setPhoneNumbers(HashSet<String> pn)
    {
        phoneNumbers = pn;
    }

    public HashSet<String> getPhoneNumbers()
    {
        return phoneNumbers;
    }

    public String getSchoolName() {
        return schoolName;
    }

    void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public int getInetLevelSize()
    {
        return inetLevels.size();
    }
}
