/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import java.util.HashSet;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 *
 * @author Segun Razaq Sobulo
 */
@Unindexed
public class InetUser implements GAEPrimaryKeyEntity{
    @Id
    protected String key;

    private String fname;
    
    private String lname;

    @Indexed private String email;

    private HashSet<String> phoneNumbers;

    private String password;

    InetUser(){}

    InetUser(String fname, String lname, String email, HashSet<String> phoneNumbers,
            String loginName, String password)
    {
        setFname(fname);
        setLname(lname);
        setEmail(email);
        setPassword(password);
        key = loginName.toLowerCase();
    }

    public String getPassword() {
        return password;
    }

    void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return fname + " " + lname;
    }

    @Override
    public Key<? extends InetUser> getKey() {
        return new Key(this.getClass(), key);
    }

    public String getLoginName()
    {
        return key;
    }

    public String getEmail() {
        return email;
    }

    void setEmail(String email) {
        this.email = email.toLowerCase();
    }

    public String getFname() {
        return fname;
    }

    void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    void setLname(String lname) {
        this.lname = lname;
    }

    void setPhoneNumbers(HashSet<String> pn)
    {
        phoneNumbers = pn;
    }

    public HashSet<String> getPhoneNumbers()
    {
        return phoneNumbers;
    }
}
