/**
 * 
 */
package j9educationentities;

import javax.persistence.Id;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetNotice implements GAEPrimaryKeyEntity{
    @Id
    protected String key;

    private String title;

    private Text content;
	
    public InetNotice(){};
    
    public InetNotice(String id, String title, String content)
    {
    	this.key = id;
    	this.title = title;
    	this.content = new Text(content);
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content.getValue();
	}

	public void setContent(String content) {
		this.content = new Text(content);
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.entities.GAEPrimaryEntity#getKey()
	 */
	@Override
	public Key<InetNotice> getKey() {
		return new Key<InetNotice>(InetNotice.class, key);
	}
}
