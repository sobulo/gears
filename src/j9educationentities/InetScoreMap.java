/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetScoreMap implements Serializable
{
    private static final long serialVersionUID = -965291684333883770L;
    private static final ObjectifyFactory KEY_FACTORY = ObjectifyService.factory();
    
    HashMap<String, Double> studentScores;

    InetScoreMap()
    {
        studentScores = new HashMap<String, Double>();
    }

    public int getNumOfScores()
    {
        return studentScores.size();
    }
    

    boolean addScore(Key<InetStudent> k, Double s)
    {
    	String keyString = KEY_FACTORY.keyToString(k);
        if(studentScores.containsKey(keyString))
            return false;
        studentScores.put(keyString, s);
        return true;
    }

    boolean removeScore(Key<InetStudent> studKey)
    {
    	String k = KEY_FACTORY.keyToString(studKey);
        return( studentScores.remove(k) == null? false : true);
    }

    boolean updateScore(Key<InetStudent> studKey, Double s)
    {
    	String k = KEY_FACTORY.keyToString(studKey);
        if(!studentScores.containsKey(k))
            return false;
        studentScores.put(k, s);
        return true;
    }

    Iterator<Map.Entry<String, Double>> getScores()
    {
        return studentScores.entrySet().iterator();
    }

    
    Set<Key<InetStudent>> getStudentKeys()
    {
    	//TODO switch callers to use rawKeys instead so we don't take performance hit on loop
    	//in the past underlying score map used objectify keys so wasn't an issue but later
    	//switched to strings due to objectify changing serialized id of their keys
    	Set<String> rawKeys = getRawStudentKeys();
    	Set<Key<InetStudent>> studentKeys = new HashSet<Key<InetStudent>>(rawKeys.size());
    	for(String stringifiedKey : rawKeys)
    	{
    		Key<InetStudent> studKey = KEY_FACTORY.stringToKey(stringifiedKey);
    		studentKeys.add(studKey);
    	}
    	return studentKeys;
    }
    
    Set<String> getRawStudentKeys()
    {
        return studentScores.keySet();
    }    

    Double getScore(Key<InetStudent> studKey)
    {
    	String stringifiedKey = KEY_FACTORY.keyToString(studKey);
        return studentScores.get(stringifiedKey);
    }

    boolean hasScore(Key<InetStudent> studKey)
    {
    	String stringifiedKey = KEY_FACTORY.keyToString(studKey);
        return studentScores.containsKey(stringifiedKey);
    }    
}
