/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;


/**
 *
 * @author Segun Razaq Sobulo
 */
@Cached
public class InetSingleTestScores extends InetTestScores{
    @Unindexed private double weight;
    
    InetSingleTestScores(){ super(); }

    public InetSingleTestScores(String name, Double maxScore,
            Double weight, Key<InetCourse> courseKey)
    {
        super(name, maxScore, courseKey);
        setWeight(weight);
    }
    
    public double getWeight() {
        return weight;
    }

    public double getWeightedScore(Key<InetStudent> sk)
    {
        return getPercentageScore(sk) * getWeight();
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
