/**
 * 
 */
package j9educationentities;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Blob;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;


/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetImageBlob implements GAEPrimaryKeyEntity{
    @Id
    private String key;

    private Blob image;
    
    public InetImageBlob(){}
    
    public InetImageBlob(String key)
    {
    	this.key = key;
    }
    
    public void setImage(byte[] bytes)
    {
    	image = new Blob(bytes);
    }
    
    public byte[] getImage()
    {
    	return image.getBytes();
    }

	@Override
	public Key<InetImageBlob> getKey() {
		// TODO Auto-generated method stub
		return new Key<InetImageBlob>(InetImageBlob.class, key);
	}

}


