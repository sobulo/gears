/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import j9educationentities.accounting.InetBill;
import j9educationentities.accounting.InetBillDescription;
import j9educationentities.accounting.InetBillPayment;
import j9educationentities.messaging.EmailBillingController;
import j9educationentities.messaging.EmailController;
import j9educationentities.messaging.EmailReportCardController;
import j9educationentities.messaging.MessagingController;
import j9educationentities.messaging.SMSController;
import j9educationgwtgui.server.InetObjectDataPopulator;
import j9educationgwtgui.server.InetPopulatorInitializer;
import j9educationgwtgui.shared.LoginInfo;
import j9educationgwtgui.shared.PanelServiceLoginRoles;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationgwtgui.shared.exceptions.MultipleEntitiesException;
import j9educationutilities.GeneralFuncs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetDAO4CommonReads
{
    private static final Logger log =
            Logger.getLogger(InetDAO4CommonReads.class.getName());

    private static boolean isOfyRegistered = false;

    //register classes being managed
    public static void registerClassesWithObjectify()
    {
        if(isOfyRegistered)
            return;

        log.warning("Registering services with Objectify");
        ObjectifyService.register(InetCourse.class);
        ObjectifyService.register(InetCumulativeScores.class);
        ObjectifyService.register(InetGuardian.class);
        ObjectifyService.register(InetLevel.class);
        ObjectifyService.register(InetSchool.class);
        ObjectifyService.register(StringOptions.class);
        ObjectifyService.register(InetStudent.class);
        ObjectifyService.register(InetSingleTestScores.class);
        ObjectifyService.register(InetTeacher.class);
        ObjectifyService.register(InetUser.class);
        ObjectifyService.register(InetTestScores.class); //abstract class ok?
        ObjectifyService.register(InetHoldContainer.class);
        ObjectifyService.register(InetHold.class);
        ObjectifyService.register(InetImageBlob.class);
        ObjectifyService.register(InetNotice.class);
        ObjectifyService.register(InetBillDescription.class);
        ObjectifyService.register(InetBill.class);
        ObjectifyService.register(InetBillPayment.class);
        ObjectifyService.register(MessagingController.class);
        ObjectifyService.register(EmailController.class);
        ObjectifyService.register(SMSController.class);
        ObjectifyService.register(EmailReportCardController.class);
        ObjectifyService.register(EmailBillingController.class); 
        ObjectifyService.register(ApplicationParameters.class);

        isOfyRegistered = true;
    }

    public static InetSchool getSchool(Objectify ofy)
    {
        return ofy.get(InetSchool.class, InetConstants.SCHOOL_ID);
    }

    public static Set<Key<InetLevel>> getSchoolLevels(Objectify ofy)
            throws MissingEntitiesException
    {
        InetSchool schl = ofy.find(InetSchool.class, InetConstants.SCHOOL_ID);

        if(schl == null)
            throw new MissingEntitiesException("Unable to find school" +
                    " details");

        int numOfClasses = schl.getInetLevelSize();
        if(numOfClasses == 0)
            return null;
        return schl.getInetLevelKeys();
    }
    
    public static void populateSchoolLevelInfo(InetObjectDataPopulator result, Objectify ofy)
    throws MissingEntitiesException
	{
    	Set<Key<InetLevel>> levelKeys = getSchoolLevels(ofy);
    	populateEntityInformation(levelKeys, result, ofy);
	}    
     
    public static void getClassRosterInfo(Key<InetLevel> levelKey,
            InetObjectDataPopulator result, Set<Key<InetStudent>> studKeys, Objectify 
            ofy) throws MissingEntitiesException
    {
        InetLevel lev = ofy.find(levelKey);

        if (lev == null)
        {
            String msg = "unable to retrieve class obj for key: "
                    + levelKey.getName();
            throw new MissingEntitiesException(msg);
        }
        
        if(result instanceof InetPopulatorInitializer)
        {
        	InetPopulatorInitializer pop = (InetPopulatorInitializer) result;
        	GAEPrimaryKeyEntity[] levelWrapper = {lev};
        	pop.init(levelWrapper, ofy);
        }

        Set<Key<InetStudent>> tempStudKeys = lev.getStudentKeys();
        if(studKeys != null)
        {
        	for(Key<InetStudent> sk : studKeys)
        	{
        		if(!tempStudKeys.contains(sk))
        		{
        			String msgFmt = "Requested stud key %s but it is not present in class %s";
        			log.severe(String.format(msgFmt, sk.toString(), levelKey.toString()));
        			throw new MissingEntitiesException(String.format(msgFmt, sk.getName(), levelKey.getName()));
        		}
        	}
        	tempStudKeys = studKeys;
        }
        populateEntityInformation(tempStudKeys, result, ofy);
    }
    
    public static void getClassRosterInfo(Key<InetLevel> levelKey,
            InetObjectDataPopulator result, Objectify 
            ofy) throws MissingEntitiesException
    {    
    	getClassRosterInfo(levelKey, result, null, ofy);
    }
    
    public static void getCourseTestsInfo(Key<InetCourse> courseKey,
            InetObjectDataPopulator result, Objectify ofy) throws MissingEntitiesException
    {
        InetCourse course = ofy.find(courseKey);

        if (course == null)
        {
            String msg = "unable to retrieve course obj for key: ";
            log.severe(msg + courseKey);
            throw new MissingEntitiesException(msg + courseKey.getName());
        }

        Set<Key<InetSingleTestScores>> testKeys = course.getTestScoresKeys();
        populateEntityInformation(testKeys, result, ofy);
    }    

    private static <T extends GAEPrimaryKeyEntity> void populateEntityInformation(Collection<Key<T>> entityKeys,
            InetObjectDataPopulator result, Objectify ofy) throws MissingEntitiesException
    {
        GAEPrimaryKeyEntity[] dataContainer = new GAEPrimaryKeyEntity[1];
        if(entityKeys == null || entityKeys.size() == 0) return;
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(entityData.size() != entityKeys.size())
        {
        	printKeyDifferences(entityKeys, entityData.keySet());
            throw new MissingEntitiesException("Batch get failed, see server log for details");
        }

        Iterator<Map.Entry<Key<T>, T>> entityIterator =
                entityData.entrySet().iterator();
        while (entityIterator.hasNext()) {
            dataContainer[0] = entityIterator.next().getValue();
            result.populateInfo(dataContainer);
        }
    }
    
    public static void populateStudentParentInformation(Collection<Key<InetStudent>> studentKeys,
    		InetObjectDataPopulator result, boolean includeParents, Objectify ofy) throws MissingEntitiesException
    {
        GAEPrimaryKeyEntity[] dataContainer = new GAEPrimaryKeyEntity[3]; //1 student plus 2 potential parents
        if(studentKeys == null || studentKeys.size() == 0) return;
        
        //fetch student objects
        Map<Key<InetStudent>, InetStudent> studentData = getEntities(studentKeys, ofy, true);
        
        //fetch parent objects
        HashSet<Key<InetGuardian>> guardianKeys = null;
        if(includeParents)
        {
	        guardianKeys = new HashSet<Key<InetGuardian>>(studentData.size());
	        for(InetStudent stud : studentData.values())
	        {
	        	if(stud.getFirstGuardianKey() != null)
	        		guardianKeys.add(stud.getFirstGuardianKey());
	        	if(stud.getSecondGuardianKey() != null)
	        		guardianKeys.add(stud.getSecondGuardianKey());
	        }
        }
        Map<Key<InetGuardian>, InetGuardian> guardianData = getEntities(guardianKeys, ofy, true);
        
        //populate result
        Iterator<Map.Entry<Key<InetStudent>, InetStudent>> studentIterator =
            studentData.entrySet().iterator();
        
        while (studentIterator.hasNext()) 
        {
        	InetStudent stud = studentIterator.next().getValue();
        	dataContainer[0] = stud;
        	dataContainer[1] = dataContainer[2] = null;
        	if(includeParents)
        	{
        		if(stud.getFirstGuardianKey() != null)
        			dataContainer[1] = guardianData.get(stud.getFirstGuardianKey());
        		if(stud.getSecondGuardianKey() != null)
        			dataContainer[2] = guardianData.get(stud.getSecondGuardianKey());        		
        	}
        	result.populateInfo(dataContainer);
        }        
    }
    
    public static <T> void printKeyDifferences(Collection<Key<T>> requestedCollection, Collection<Key<T>> retrievedCollection)
    {
    	Key<T>[] requestedKeys = new Key[requestedCollection.size()];
    	requestedKeys = requestedCollection.toArray(requestedKeys);
    	Key<T>[] retrievedKeys = new Key[retrievedCollection.size()];
    	retrievedKeys = retrievedCollection.toArray(retrievedKeys);
    	ArrayList<Key<T>> retrievedOnly, requestedOnly, intersect;
    	retrievedOnly = new ArrayList<Key<T>>();
    	requestedOnly = new ArrayList<Key<T>>();
    	intersect = new ArrayList<Key<T>>();
    	GeneralFuncs.arrayDiff(requestedKeys, retrievedKeys, intersect, requestedOnly, retrievedOnly);
    	
    	log.severe("Unable to load some entites from database ....");
    	for(Key<T> k : requestedOnly)
    		log.severe("Unable to retrieve: " + k);
        log.severe("Keys initially requested are ...... ");
        for(Key<T> errKey : requestedCollection)
            log.severe("Requested Key: " + errKey);    	
    }
    
    public static <T extends GAEPrimaryKeyEntity> void printMixedKeyDifferences(Collection<Key<? extends T>> requestedCollection, Collection<Key<T>> retrievedCollection)
    {
    	Key<T>[] requestedKeys = new Key[requestedCollection.size()];
    	requestedKeys = requestedCollection.toArray(requestedKeys);
    	Key<T>[] retrievedKeys = new Key[retrievedCollection.size()];
    	retrievedKeys = retrievedCollection.toArray(retrievedKeys);
    	ArrayList<Key<T>> retrievedOnly, requestedOnly, intersect;
    	retrievedOnly = new ArrayList<Key<T>>();
    	requestedOnly = new ArrayList<Key<T>>();
    	intersect = new ArrayList<Key<T>>();
    	GeneralFuncs.arrayDiff(requestedKeys, retrievedKeys, intersect, requestedOnly, retrievedOnly);
    	
    	log.severe("Unable to load some entites from database ....");
    	for(Key<T> k : requestedOnly)
    		log.severe("Unable to retrieve: " + k);
        log.severe("Keys initially requested are ...... ");
        for(Key<? extends T> errKey : requestedCollection)
            log.severe("Requested Key: " + errKey);    	
    }    
    
    public static <T> Map<Key<T>, T> getEntities(Collection<Key<T>> entityKeys, Objectify ofy, boolean throwException) throws MissingEntitiesException
    {
    	if(entityKeys == null || entityKeys.size() == 0) return new HashMap<Key<T>, T>(1);
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(throwException && entityData.size() != entityKeys.size())
        {
        	printKeyDifferences(entityKeys, entityData.keySet());
    		throw new MissingEntitiesException("Batch get failed, see server log for details");
        }
        return entityData;
    }    
    
    public static <T extends GAEPrimaryKeyEntity> Map<Key<T>, T> getMixedEntities(Collection<Key<? extends T>> entityKeys, Objectify ofy, boolean throwException) throws MissingEntitiesException
    {
    	if(entityKeys == null || entityKeys.size() == 0) return new HashMap<Key<T>, T>(1);    	
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(throwException && entityData.size() != entityKeys.size())
        {
        	printMixedKeyDifferences(entityKeys, entityData.keySet());
            throw new MissingEntitiesException("Batch get failed, see server log for details");
        }
        return entityData;
    }

    private static <T extends GAEPrimaryKeyEntity> void populateMixedEntityInformation(Collection<Key<? extends T>> entityKeys,
            InetObjectDataPopulator result, Objectify ofy) throws MissingEntitiesException
    {
    	if(entityKeys == null || entityKeys.size() == 0) return;
        GAEPrimaryKeyEntity[] dataContainer = new GAEPrimaryKeyEntity[1];
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(entityData.size() != entityKeys.size())
        {
        	printMixedKeyDifferences(entityKeys, entityData.keySet());
            throw new MissingEntitiesException("Batch get failed, see server log for details");
        }

        Iterator<Map.Entry<Key<T>, T>> entityIterator =
                entityData.entrySet().iterator();
        while (entityIterator.hasNext()) {
            dataContainer[0] = entityIterator.next().getValue();
            result.populateInfo(dataContainer);
        }
    }
    
    public static void getLevelCourseInfo(Key<InetLevel> levelKey, InetObjectDataPopulator
            result, Objectify ofy) throws
            MissingEntitiesException
    {
        InetLevel level = ofy.find(levelKey);

        if (level == null) {
            String msg = "unable to retrieve level obj for key: " + levelKey;
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }

        Set<Key<InetCourse>> courseKeys = level.getCourseList();
        populateEntityInformation(courseKeys, result, ofy);
    }    


    public static void getCourseRosterInfo(Key<InetCourse> courseKey, InetObjectDataPopulator
            result, InetPopulatorInitializer resultInitiator, Objectify ofy) throws
            MissingEntitiesException
    {
        InetCourse course = ofy.find(courseKey);

        if (course == null) {
            String msg = "unable to retrieve course obj for key: " + courseKey;
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }

        GAEPrimaryKeyEntity[] dataContainer = new GAEPrimaryKeyEntity[1];
        dataContainer[0] = course;
        if (resultInitiator != null) {
            resultInitiator.init(dataContainer, ofy);
        }

        InetCumulativeScores cumScore = ofy.find(course.getCumulativeScores());
        if (cumScore == null) {
            String msg = "unable to retrieve course obj for key: " + courseKey;
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }

        Set<Key<InetStudent>> studentKeys = cumScore.getStudentKeys();
        populateEntityInformation(studentKeys, result, ofy);
    }

    public static void getTestRosterInfo(Key<InetSingleTestScores> testKey, InetObjectDataPopulator result,
            InetPopulatorInitializer resultInitiator, Objectify ofy) throws MissingEntitiesException
    {
        InetSingleTestScores testScores;

        testScores = ofy.find(testKey);

        if (testScores == null) {
            String msg = "unable to retrieve course obj for key: " + testKey;
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }

        GAEPrimaryKeyEntity[] dataContainer = new GAEPrimaryKeyEntity[1];
        dataContainer[0] = testScores;
        if (resultInitiator != null) {
            resultInitiator.init(dataContainer, ofy);
        }

        Set<Key<InetStudent>> studentKeys = testScores.getStudentKeys();
        populateEntityInformation(studentKeys, result, ofy);
    }

    public static void getCumulativeStudentScores(Key<InetStudent> studentKey,
            Key<InetLevel> levelKey, InetObjectDataPopulator dataPopulator,
            InetPopulatorInitializer initObj, Objectify ofy) throws
            MissingEntitiesException
    {
        InetStudent student = ofy.find(studentKey);
        if(student == null)
        {
            String msg = "Missing Entity: " + studentKey.getName();
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }
        Map<Key<InetCourse>, InetCourse> courses = ofy.get(student.getCourseKeys(levelKey));
        GAEPrimaryKeyEntity[] objWrapper = new GAEPrimaryKeyEntity[1 + courses.size()];
        objWrapper[0] = student;
        int idx = 1;
        for(InetCourse c: courses.values())
        	objWrapper[idx++] = c;
        
        initObj.init(objWrapper, ofy);

        Set<Key<InetCumulativeScores>> cumKeys = 
                student.getCumulativeScoreKeys(levelKey);
        populateEntityInformation(cumKeys, dataPopulator, ofy);
    }
    
    public static void getLevelBreakDowns(Objectify ofy, InetObjectDataPopulator dataPopulator) throws MissingEntitiesException
    {
    	Set<Key<InetLevel>> levelKeys = getSchoolLevels(ofy);
    	populateEntityInformation(levelKeys, dataPopulator, ofy);
    }

    public static void getAllStudentScores(Key<InetStudent> studentKey,
            Key<InetLevel> levelKey, InetObjectDataPopulator dataPopulator,
            Objectify ofy) throws MissingEntitiesException
    {
        InetStudent student = ofy.find(studentKey);
        if(student == null)
        {
            String msg = "Missing Entity: " + studentKey.getName();
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }

        Set<Key<InetCourse>> courseKeys =
                student.getCourseKeys(levelKey);
        Map<Key<InetCourse>, InetCourse> courseMap = ofy.get(courseKeys);
        Collection<InetCourse> courseList = courseMap.values();

        //build test keys
        List<Key<? extends InetTestScores>> testKeys = new ArrayList<Key<? extends InetTestScores>>();
        for(InetCourse course : courseList)
        {
            testKeys.add(course.getCumulativeScores());
            testKeys.addAll(course.getTestScoresKeys());
        }
        populateMixedEntityInformation(testKeys, dataPopulator, ofy);
    }
    
    public static void getStudentHolds(Key<InetHoldContainer> holdContKey, 
    		InetObjectDataPopulator dataPopulator, Objectify ofy) throws MissingEntitiesException
    {
        InetHoldContainer holdContainer = ofy.find(holdContKey);
        if(holdContainer == null)
        {
            String msgFmt = "Unable to find holds for ";
            log.severe(msgFmt + holdContainer.getKey());
            throw new MissingEntitiesException(msgFmt + holdContainer.getKey().getName());
        }

        Set<Key<InetHold>> holdKeys = holdContainer.getHolds();
        populateEntityInformation(holdKeys, dataPopulator, ofy);
    }    

    public static HashMap<String, String> getStudentLevels(Key<InetStudent>
            studentKey, Objectify ofy) throws MissingEntitiesException
    {
        InetStudent student = ofy.find(studentKey);
        if(student == null)
        {
            String msg = "Missing Entity: " + studentKey.getName();
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }

        Set<Key<InetLevel>> levs = student.getLevels();
        HashMap<String, String> result = new HashMap<String, String>(levs.size());
        Iterator<Key<InetLevel>> levelKeys = levs.iterator();
        while(levelKeys.hasNext())
        {
            Key<InetLevel> level = levelKeys.next();
            result.put(ofy.getFactory().keyToString(level),
                    keyToPrettyString(level));
        }
        return result;
    }
    
    public static List<Key<InetCourse>> getTeacherCourses(Key<InetTeacher> teachKey, Objectify ofy)
    {
    	return ofy.query(InetCourse.class).filter("instructor", teachKey).listKeys(); 
    }
    
    public static HashMap<String, String> getTeacherCourseMap(Key<InetTeacher> teachKey, Objectify ofy)
    {
    	QueryResultIterable<Key<InetCourse>> courseKeys = ofy.query(InetCourse.class).filter("instructor", teachKey).fetchKeys();
    	HashMap<String, String> result = new HashMap<String, String>();
    	for(Key<InetCourse> ck : courseKeys)
    		result.put(ofy.getFactory().keyToString(ck), keyToPrettyString(ck));
    	return result;
    }

    public static <T extends InetUser> T getUserByEmail(Objectify ofy,
            java.lang.Class<T> c, String userEmail) throws MultipleEntitiesException
    {
        //we limit result set to 2 because anything more than 1 is an issue.i.e.
        //same headache whether 2 users share same email or 20 users share it
        List<T> results = ofy.query(c).filter("email", userEmail).limit(2).list();

        if (results.size() == 1)
            return results.get(0);
        else if(results.size() > 1)
        {
            String msg = "Email: " + userEmail + " matched more than" +
                    " 1 entity. Showing a couple below";
            for(T user : results)
                msg += user.getKey().getName() + ", ";
            throw new MultipleEntitiesException(msg);
        }
        return null;
    }
    
    public static ArrayList<InetHold> getHolds(Objectify ofy, LoginInfo loginInfo)
    {
    	log.warning("checking login for holds");
    	ArrayList<InetHold> result = new ArrayList<InetHold>();
    	PanelServiceLoginRoles role = loginInfo.getRole();
    	List<Key<InetStudent>> studKeys; 
    	if(role.equals(PanelServiceLoginRoles.ROLE_STUDENT))
    	{
    		log.warning("setting student");
    		studKeys = new ArrayList<Key<InetStudent>>(1);
    		studKeys.add(new Key<InetStudent>(InetStudent.class, loginInfo.getLoginID()));
    	}
    	else if(role.equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
    	{
    		Key<InetGuardian> guardKey = new Key<InetGuardian>(InetGuardian.class, loginInfo.getLoginID());
    		studKeys = InetDAO4CommonReads.getGuardianStudentsKeys(ofy, guardKey);
    	}
    	else
    		return null;
    	
    	for(Key<InetStudent> sk : studKeys)
    	{
    		List<InetHold> queryResults = ofy.query(InetHold.class).filter("studentKey", sk).filter("resolutionStatus", false).list();
    		result.addAll(queryResults);
    	}  	
    	log.warning("number of holds found: " + result.size());
    	return result;
    }
    
    public static String holdToString(InetHold hold)
    {
    	return hold.getHoldType().toString().replace('_', ' ') + " on " + 
    			hold.getStudentKey().getName() + " for " + 
    			keyToPrettyString(hold.getHoldContainer()) + ". Reason: " + 
    					hold.getMessage().getValue();
    }

    public static List<Key<InetStudent>> getGuardianStudentsKeys(Objectify ofy,
            Key<InetGuardian> gk)
    {
    	List<Key<InetStudent>> result = new ArrayList<Key<InetStudent>>();
        QueryResultIterable<Key<InetStudent>> firstList = ofy.query(InetStudent.class).
        									filter("firstGuardianKey", gk).fetchKeys();
        
        QueryResultIterable<Key<InetStudent>> secondList = ofy.query(InetStudent.class).
        									filter("secondGuardianKey", gk).fetchKeys();
        
        if(firstList != null)
        {
            Iterator<Key<InetStudent>> firstListItr = firstList.iterator();
            while(firstListItr.hasNext())
            	result.add(firstListItr.next());
        }
        if(secondList != null)
        {
            Iterator<Key<InetStudent>> secondListItr = secondList.iterator();
            while(secondListItr.hasNext())
            	result.add(secondListItr.next());
        }
        
        return result;
    }
    
    public static List<InetStudent> getGuardianStudents(Objectify ofy,
            Key<InetGuardian> gk)
    {
        log.info("checking for guardian children: " + gk);
        List<InetStudent> firstList = ofy.query(InetStudent.class).filter("firstGuardianKey", gk).list();
        log.info("first list: " + firstList.size());
        List<InetStudent> secondList = ofy.query(InetStudent.class).filter("secondGuardianKey", gk).list();
        log.info("second list: " + secondList.size());
        
        if(firstList.size() == 0)
            return secondList;
        else if(secondList.size() == 0)
            return firstList;
        else
            firstList.addAll(secondList);

        return firstList;
    }
    
    public static Collection<InetGuardian> getStudentGuardians(Objectify ofy, Key<InetStudent> sk) 
    	throws MissingEntitiesException
    {
    	ArrayList<Key<InetGuardian>> guardKeys = getStudentGuardKeys(ofy, sk);    	
    	Map<Key<InetGuardian>, InetGuardian> guardData = ofy.get(guardKeys);
    	
    	
    	if(guardData.size() != guardKeys.size())
    	{
    		String msgFmt = "Unable to fetch all guardians(" + guardData.size() + ") for" +
    				" student guardian keys(" + guardKeys.size() + ") key: ";
    		log.severe(msgFmt + sk);
    		throw new MissingEntitiesException(msgFmt + sk.getName());
    	}
    	
    	return guardData.values();
    }
    
    public static ArrayList<Key<InetGuardian>> getStudentGuardKeys(Objectify ofy, Key<InetStudent> sk) throws MissingEntitiesException
    {
    	InetStudent stud = ofy.find(sk);
    	
    	if(stud == null)	
    	{
    		String msgFmt = "Unable to get guardians as student non existent: ";
    		log.severe(msgFmt + sk);
    		throw new MissingEntitiesException(msgFmt + sk.getName());
    	}
    	
    	ArrayList<Key<InetGuardian>> guardKeys = new ArrayList<Key<InetGuardian>>(2);
    	if(stud.getFirstGuardianKey() != null)
    		guardKeys.add(stud.getFirstGuardianKey());
    	if(stud.getSecondGuardianKey() != null)
    		guardKeys.add(stud.getSecondGuardianKey());
    	return guardKeys;
    }

    public static String keyToPrettyString(Key<?> k)
    {
        return k.getName().replace('~', '-').toUpperCase();
    }

    public static String getNameFromLastKeyPart(Key<?> k)
    {
        String[] keyParts = k.getName().split("~");
        return(keyParts[keyParts.length - 1]);
    }

    public static String getNameFromLastKeyPart(Key<?> k, int n)
    {
        String[] keyParts = k.getName().split("~");
        StringBuilder result = new StringBuilder();
        for(int i= n; i > 0; i--)
        	result.append(keyParts[keyParts.length - i]);
        return result.toString();
    }
    
    public static Map<Key<InetTestScores>, InetTestScores> getCourseTests(Key<InetCourse> courseKey, Objectify ofy)
            throws MissingEntitiesException
    {
        //get course
        InetCourse course = ofy.get(courseKey);
        if (course == null) {
            MissingEntitiesException ex =
                    new MissingEntitiesException("Unable to find course: " +
                    courseKey);
            log.severe(ex.getMessage());
            throw ex;
        }

        //get all tests
        List<Key<? extends InetTestScores>> batchKeys = course.getAllTestKeys();
        Map<Key<InetTestScores>, InetTestScores> batchScores = ofy.get(batchKeys);

        if(batchScores.size() != batchKeys.size())
        {
            String msg = "unable to retrieve all tests for: " +
                    course.getKey();
            log.severe(msg);
            throw new MissingEntitiesException(msg);
        }
        return batchScores;
    }
    
    public static <T extends InetUser> T getUser(Key<T> userKey, Objectify ofy) 
    	throws MissingEntitiesException
    {
		T user = ofy.find(userKey);
		if(user == null)
		{
			String msg = "Unable to find user: " + userKey;
			log.severe(msg);
			throw new MissingEntitiesException(msg);
		}
		return user;
    }
 
	public static List<InetStudent> fetchAllEnrolledStudents(Objectify ofy)
	{
		List<InetStudent> result = ofy.query(InetStudent.class).list();
		return result;
	}
	
	public static List<InetGuardian> fetchAllGuardians(Objectify ofy)
	{
		List<InetGuardian> result = ofy.query(InetGuardian.class).list();
		return result;
	}
	
	public static List<InetTeacher> fetchAllTeachers(Objectify ofy)
	{
		List<InetTeacher> result = ofy.query(InetTeacher.class).list();
		return result;
	}	
	
    public static Key<? extends InetUser> getUserKey(String loginName, PanelServiceLoginRoles role)
    {
        if(role.equals(PanelServiceLoginRoles.ROLE_STUDENT))
            return new Key<InetStudent>(InetStudent.class, loginName);
        else if(role.equals(PanelServiceLoginRoles.ROLE_GUARDIAN))
            return new Key<InetGuardian>(InetGuardian.class, loginName);
        else if(role.equals(PanelServiceLoginRoles.ROLE_ADMIN))
            return new Key<InetUser>(InetUser.class, loginName);  
        else if(role.equals(PanelServiceLoginRoles.ROLE_TEACHER))
            return new Key<InetTeacher>(InetTeacher.class, loginName);        
        else
        	throw new UnsupportedOperationException("Role not supported for user key retrieve");
    }
}