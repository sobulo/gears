/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import java.util.HashSet;

import com.googlecode.objectify.Key;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetTeacher extends InetUser{

    InetTeacher(){ super(); }

    InetTeacher(String fname, String lname, String email,
            HashSet<String> phoneNums, String loginName, String password)
    {
        super(fname, lname, email, phoneNums, loginName, password);
    }
    
    public Key<InetTeacher> getKey()
    {
    	return (Key<InetTeacher>) super.getKey();
    }
}