/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import j9educationactions.tasks.TaskQueueHelper;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;
import j9educationutilities.BCrypt;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetDAO4Updates {
    private static final Logger log = Logger.getLogger(InetDAO4Updates.class.getName());
    
    public static InetSchool updateSchool(String schoolName, String accronym,
            String email, String webAddress, String address, HashSet<String> phoneNums)
            throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        InetSchool schl = null;
        try {
            schl = InetDAO4CommonReads.getSchool(ofy);
            if (schl == null) {
                String exMsg = "unable to update as no school exists in this namespace";
                log.severe(exMsg);
                throw new MissingEntitiesException(exMsg);
            }
            schl.setSchoolName(schoolName);
            schl.setAccronym(accronym);
            schl.setEmail(email);
            schl.setAddress(address);
            schl.setWebAddress(webAddress);
            schl.setPhoneNumbers(phoneNums);
            ofy.put(schl);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
        return schl;
    }    
    
    public static void updateStringOptions(String optionName, HashSet<String> optionElements)
            throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            StringOptions optionList = ofy.find(StringOptions.class, optionName);
            if(optionList == null)
            {
                throw new MissingEntitiesException("unable to find: " +
                        optionName);
            }
            optionList.setOptionElements(optionElements);
            ofy.put(optionList);
            ofy.getTxn().commit();
        }
        finally
        {
            if(ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }
    }

    public static void addLevelKeyToSchool(String levelKeyStr)
            throws MissingEntitiesException, DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        try {

            InetSchool s = ofy.find(InetSchool.class, InetConstants.SCHOOL_ID);
            if (s == null) {
                MissingEntitiesException ex = new MissingEntitiesException("Unable" +
                        " to establish school details for this namespace");
                log.severe(ex.getMessage());
                throw ex;
            }

            log.info("retrieved school: " + s.getSchoolName());

            if (!s.addInetLevelKey(levelKey)) {
                String exMsg = levelKey.getName() + " already added to school";
                log.severe(exMsg);
                throw (new DuplicateEntitiesException(exMsg));
            }
            ofy.put(s);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
    }

    public static void addCourseKeyToLevel(String[] courseKeyStrings, String lkStr)
            throws MissingEntitiesException, DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(lkStr);
        InetLevel level = ofy.find(levelKey);
        
        try 
        {
            if (level == null) {
                MissingEntitiesException ex = new MissingEntitiesException("Can't" +
                        " add course, couldn't find level: " +
                        levelKey.getName());
                throw ex;
            }
            
            for(String ckStr : courseKeyStrings)
            {
            	Key<InetCourse> courseKey = ofy.getFactory().stringToKey(ckStr);
                if(!level.addSubjectToCourseList(courseKey))
                {
                    throw new DuplicateEntitiesException("request to add " + courseKeyStrings.length +
                            " subject(s) to class ignored, as class," + courseKey.getName() +
                            ", already added");
                }
            }
            
        	ofy.put(level);
            ofy.getTxn().commit();
        } 
        finally 
        {
            if (ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }
    }


    public static void addStudentsToCourseTests(String levelKeyStr, String courseKeyStr,
            String[] studentKeyStrings, boolean isKey) 
            throws MissingEntitiesException, DuplicateEntitiesException
    {
    	addStudentsToCourseTests(levelKeyStr, courseKeyStr, studentKeyStrings, isKey, false);
    }
    
    public static void addStudentsToCourseTests(String levelKeyStr, String courseKeyStr,
            String[] studentKeyStrings, boolean isKey, boolean ignoreExceptionsForTesting) 
            throws MissingEntitiesException, DuplicateEntitiesException
    {
    	
    	Objectify ofy = ObjectifyService.begin();
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        HashSet<Key<InetStudent>> studentCourseListsToUpdate = new HashSet<Key<InetStudent>>(studentKeyStrings.length);
        
        InetLevel level = ofy.find(levelKey);
        
        if(level == null)
        {
        	String msgFmt = "Unable to find level info %s needed for level roster validation";
        	log.severe(String.format(msgFmt, levelKey.toString()));
        	throw new MissingEntitiesException(String.format(msgFmt, levelKey.getName()));
        }
        else if(!level.getCourseList().contains(courseKey))
        {
        	String msgFmt = "class %s subject %s mismatch, please reselect class and try again";
        	if(!ignoreExceptionsForTesting)
        	{
        		log.severe(String.format(msgFmt, levelKey.toString(), courseKey.toString()));
        		throw new MissingEntitiesException(String.format(msgFmt, levelKey.toString(), courseKey.toString()));
        	}
        	else
        		log.warning("IGNORED ISSUE: " + String.format(msgFmt, levelKey.toString(), courseKey.toString()));
        		
        }
        
        Set<Key<InetStudent>> levelRoster = level.getStudentKeys();    	

        ofy = ObjectifyService.beginTransaction();
        
      
        try
        {
            Map<Key<InetTestScores>, InetTestScores> batchScores =
                    InetDAO4CommonReads.getCourseTests(courseKey, ofy);
            
            //for each student, initialize all test scores to 0
            Iterator<Map.Entry<Key<InetTestScores>, InetTestScores>> 
                    scoreItr = batchScores.entrySet().iterator();

            boolean firstIteration = true;
            while(scoreItr.hasNext())
            {
                InetTestScores test = scoreItr.next().getValue();
                for (int i = 0; i < studentKeyStrings.length; i++)
                {
                    Key<InetStudent> studentKey = null;
                    
                    if(isKey)
                    	studentKey = ofy.getFactory().stringToKey(studentKeyStrings[i]);
                    else
                    	studentKey = new Key<InetStudent>(InetStudent.class, studentKeyStrings[i]);
                    
                    if(firstIteration && !levelRoster.contains(studentKey))
                    {
                    	String msgFmt = "Addition of students aborted because student: %s not in level %s roster";
                    	if(!ignoreExceptionsForTesting)
                    	{
                    		log.severe(String.format(msgFmt, studentKey.toString(), levelKey.toString()));
                    		throw new MissingEntitiesException(String.format(msgFmt, studentKey.getName(), levelKey.getName()));
                    	}
                    	else
                    	{
                    		log.warning("IGNORED ISSUE: " + String.format(msgFmt, studentKey.toString(), levelKey.toString()));
                    		continue;
                    	}
                    }
                    
                    if(!test.addStudentScore(studentKey, 0.0))
                        throw new DuplicateEntitiesException("Addition of " +
                                "students aborted because student: " +
                                studentKey.getName() + " already in list");
                    else if(firstIteration)
                    	studentCourseListsToUpdate.add(studentKey);	
                }
                firstIteration = false;
            }
            ofy.put(batchScores.values());
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
        
        //schedule addition of the course to student rosters/index
        ofy = ObjectifyService.begin();
        for(Key<InetStudent> studentKey : studentCourseListsToUpdate)
        	TaskQueueHelper.scheduleAddCourseToStudentSubjectList(courseKeyStr, ofy.getFactory().keyToString(studentKey));
    }
    
    public static void updateCumulativeScores(Key<InetCourse> courseKey, Objectify ofy)
    	throws MissingEntitiesException
    {
        try
        {
        	log.warning("calling cumulative score update for: " + courseKey);
            Map<Key<InetTestScores>, InetTestScores> batchScores =
                    InetDAO4CommonReads.getCourseTests(courseKey, ofy);

            //for each student, sum up tescore weights
            Iterator<Map.Entry<Key<InetTestScores>, InetTestScores>>
                    scoreItr = batchScores.entrySet().iterator();

            InetSingleTestScores[] testScores = new InetSingleTestScores[batchScores.size() - 1];
            for(int i = 0; i < testScores.length; i++)
                testScores[i] = (InetSingleTestScores) scoreItr.next().getValue();
            
            InetCumulativeScores cumScores = (InetCumulativeScores) scoreItr.next().getValue();
            Iterator<Key<InetStudent>> studKeyItr = cumScores.getStudentKeys().iterator();
            Double weightedCumScore, weightedTestScore, totalCumWeights;
            while(studKeyItr.hasNext())
            {
                weightedCumScore = totalCumWeights = 0.0;
                Key<InetStudent> studKey = studKeyItr.next();
                for (int i = 0; i < testScores.length; i++)
                {
                	//if(!testScores[i].isPublished())
                	//	 continue;
                    weightedTestScore = testScores[i].getWeightedScore(studKey);
                    if(weightedTestScore == null)
                        throw new MissingEntitiesException("Update of cumulative" +
                                " scores aborted because student: " +
                                studKey.getName() + " not found in" +
                                testScores[i].getTestName());
                    weightedCumScore += weightedTestScore;
                    totalCumWeights += testScores[i].getWeight();
                }
                cumScores.updateStudentScore(studKey, weightedCumScore/totalCumWeights);
            }
            ofy.put(cumScores);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }    	
    }
    

    public static void updateCumulativeScores(String courseKeyStr)
            throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        updateCumulativeScores(courseKey, ofy);
    }

    public static void addCourseKeyToStudent(String studentKeyStr,
            String courseKeyStr) throws MissingEntitiesException, DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        Key<InetStudent> studentKey = ofy.getFactory().stringToKey(studentKeyStr);
        Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
        try
        {
            InetStudent student = ofy.find(studentKey);
            if (student == null) {
                MissingEntitiesException ex =
                        new MissingEntitiesException("Unable to add course info." +
                        " Non existent student: " + studentKey.getName());
                log.severe(ex.getMessage());
                throw ex;
            }

            if(!student.addCourse(courseKey))
            {
                DuplicateEntitiesException ex =
                        new DuplicateEntitiesException("Student: " + studentKey
                        + " already registered for: " + courseKey);
                log.severe(ex.getMessage());
                throw ex;
            }

                ofy.put(student);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
    }

    public static void addLevelKeyToStudent(String studentKeyStr,
            String levelKeyStr) throws MissingEntitiesException, DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        Key<InetStudent> studentKey = ofy.getFactory().stringToKey(studentKeyStr);
        Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
        try
        {
            InetStudent student = ofy.find(studentKey);
            if (student == null) {
                MissingEntitiesException ex =
                        new MissingEntitiesException("Unable to add level info." +
                        " Non existent student: " + studentKey.getName());
                log.severe(ex.getMessage());
                throw ex;
            }

            if(!student.addLevel(levelKey))
            {
                DuplicateEntitiesException ex =
                        new DuplicateEntitiesException("Student: " + studentKey
                        + " already registered for: " + levelKey);
                log.severe(ex.getMessage());
                throw ex;
            }
            ofy.put(student);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
    }

    public static void addStudentsToLevelRoster(Key<InetLevel> levelKey, String[] loginStrs, boolean isAllOrNothing)
            throws MissingEntitiesException, DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            InetLevel level = ofy.find(levelKey);
            if(level == null) {
                MissingEntitiesException ex = new MissingEntitiesException("Can't" +
                        " add students to level roster. Can't find level " +
                        "object: " + levelKey);
                log.severe(ex.getMessage());
                throw ex;
            }

            for(String loginId : loginStrs)
            {
                //get student key
                Key<InetStudent> sk = new Key<InetStudent>(InetStudent.class, loginId);

                //add to level roster
                if (!level.addStudentToRoster(sk))
                {
                    String exMsgFmt = "%s already in %s"; 
                    log.severe(String.format(exMsgFmt, sk.toString(), level.getKey().toString()));
                    if(isAllOrNothing)
                    	throw new DuplicateEntitiesException(String.format(exMsgFmt, sk.getName(), level.getKey().getName()));
                }
            }

            ofy.put(level);
            ofy.getTxn().commit();
        }
        finally
        {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
    }
    
    public static void addGuardianToStudent(Key<InetGuardian> guardKey, Key<InetStudent> studKey)
    	throws MissingEntitiesException, DuplicateEntitiesException
    {
    	Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            InetStudent student = ofy.find(studKey);
            if (student == null) {
                MissingEntitiesException ex = new MissingEntitiesException("Unable" +
                        " to add guardian. Student obj missing: " +
                        studKey.getName());
                log.severe(ex.getMessage());
                throw ex;
            }


            if(student.getFirstGuardianKey() != null)
            {
                if(student.getSecondGuardianKey() != null)
                {
                    throw new DuplicateEntitiesException("Both guardians already specified: " +
                            student .getFirstGuardianKey().getName() + 
                            " and " + student.getSecondGuardianKey().getName());
                }
                else
                {
                    if(!student.getFirstGuardianKey().equals(guardKey))
                        student.setSecondGuardianKey(guardKey);
                    else
                        throw new DuplicateEntitiesException("Guardian already registered");
                }
            }
            else
                student.setFirstGuardianKey(guardKey);

            ofy.put(student);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }    	
    }

    public static void addGuardianToStudent(String gkStr, String skStr)
            throws MissingEntitiesException, DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.begin();
        Key<InetStudent> studKey = ofy.getFactory().stringToKey(skStr);
        Key<InetGuardian> guardKey = ofy.getFactory().stringToKey(gkStr);
        addGuardianToStudent(guardKey, studKey);
    }

    public static void setCourseTeacher(Key<InetTeacher> teacherKey, Key<InetCourse> courseKey, boolean isAdd) throws MissingEntitiesException, DuplicateEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        try {
            //refresh course info
            InetCourse course = ofy.find(courseKey);
            if (course == null) {
                MissingEntitiesException ex = new MissingEntitiesException("Unable" +
                        " to add Teacher. Can't find course object: " + courseKey);
                log.severe(ex.getMessage());
                throw ex;
            }
            
            if(isAdd)
            {
	            if(course.getTeacherKey() != null) 
	            {
	            	String msgFmt = "Course already has a teacher specified: %s . Remove teacher from course first";
	            	log.warning(String.format(msgFmt, course.getTeacherKey().toString())); 
	                throw new DuplicateEntitiesException(String.format(msgFmt, course.getTeacherKey().getName()));
	            }
	            else
	                course.setTeacherKey(teacherKey);	            
            }
            else
            {
            	Key<InetTeacher> currentTeacher = course.getTeacherKey();
            	if(currentTeacher == null || !currentTeacher.equals(teacherKey))
            	{
	            	String msgFmt = "Requested removal of teacher [%s] but current teacher is [%s]";
	            	log.warning(String.format(msgFmt, teacherKey.toString(), (currentTeacher==null?"":currentTeacher.toString()))); 
	                throw new MissingEntitiesException(String.format(msgFmt, teacherKey.getName(), (currentTeacher==null?"":currentTeacher.getName())));            		
            	}
            	else
            		course.setTeacherKey(null);
            }
            ofy.put(course);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
    }

    public static void updateSchoolGradingScheme(InetGradeDescriptions scheme)
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            InetSchool s = InetDAO4CommonReads.getSchool(ofy);
            s.setDefaultGradingScheme(scheme);
            ofy.put(s);
            ofy.getTxn().commit();
        }
        finally
        {
            if(ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }
    }
    
    public static void updateStudent(String fname, String lname, Date dob, String email, 
    		HashSet<String> phoneNums, String loginName) 
    	throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class, loginName);
        try
        {
            //confirm new student obj doesn't exist already
            InetStudent student = (InetStudent) ofy.find(studKey);
            if (student == null) {
                MissingEntitiesException ex = new MissingEntitiesException(
                        "Student already exists: " + studKey);
                log.severe(ex.getMessage());
                throw ex;
            }
            
            student.setBirthday(dob);            
            updateUserInfo(fname, lname, email, phoneNums, student);

            //persist student object
            ofy.put(student);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }

        log.warning("Updated student: " + studKey.getName());
    }

    public static <T extends InetUser> void updateUser(String fname, String lname, String email, 
    		HashSet<String> phoneNums, Key<? extends InetUser> userKey) 
    	throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            //confirm new student object doesn't exist already
            InetUser user = ofy.find(userKey);
            if (user == null) {
                MissingEntitiesException ex = new MissingEntitiesException(
                        "Unable to find user info for: " + userKey);
                log.severe(ex.getMessage());
                throw ex;
            }
                        
            updateUserInfo(fname, lname, email, phoneNums, user);

            //persist student object
            ofy.put(user);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }

        log.warning("Updated user: " + userKey);
    }    
    
	private static void updateUserInfo(String fname, String lname, String email, 
			HashSet<String> phoneNums, InetUser user)
	{
		user.setFname(fname);
		user.setLname(lname);
		user.setEmail(email);
		user.setPhoneNumbers(phoneNums);
	}
	
	
    public static <T extends InetUser> void changePassword(Key<T> userKey, String pwd) 
    	throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            //confirm new student obj doesn't exist already
            InetUser user = ofy.find(userKey);
            if (user == null) {
                MissingEntitiesException ex = new MissingEntitiesException(
                        "Unable to find info for: " + userKey);
                log.severe(ex.getMessage());
                throw ex;
            }
            
            user.setPassword(getHashPwd(pwd));

            //persist student object
            ofy.put(user);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
    }
    
    public static String getHashPwd(String pwd)
    {
    	return BCrypt.hashpw(pwd, BCrypt.gensalt());
    }
    
}
