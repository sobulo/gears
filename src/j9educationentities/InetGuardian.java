/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import java.util.HashSet;

import com.googlecode.objectify.Key;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetGuardian extends InetUser
{
    InetGuardian(){ super(); }
    InetGuardian(String fname, String lname, String email, HashSet<String> phoneNumbers,
            String loginName, String password)
    {
        super(fname, lname, email, phoneNumbers, loginName, password);
    }
    
    public Key<InetGuardian> getKey()
    {
    	return (Key<InetGuardian>) super.getKey();
    }
}
