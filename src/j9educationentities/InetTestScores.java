/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 *
 * @author Segun Razaq Sobulo
 */
@Unindexed
public abstract class InetTestScores implements GAEPrimaryKeyEntity{
    private static final Logger log = Logger.getLogger(InetTestScores.class.getName());

    @Id
    protected String key;

    @Indexed private String testName;

    private double maxScore;

    @Parent private Key<InetCourse> courseKey; //TODO, test whether parent still indexed
    
    @Serialized
    private InetScoreMap studentScores;

    private final static double DEFAULT_MIN_SCORE = 0.0;

    private boolean published = false;  
    
    InetTestScores(){}

    InetTestScores(String testName, double maxScore, Key<InetCourse> courseKey)
    {
        setTestName(testName);
        setMaxScore(maxScore);
        studentScores = new InetScoreMap();
        this.courseKey = courseKey;
        this.key = testName.toLowerCase();
    }
    
    public boolean isPublished()
    {
    	return published;
    }
    
    public void setPublished(boolean published)
    {
    	this.published = published;
    }
    
    @Override
    public Key<? extends InetTestScores> getKey() {
        return new Key(courseKey, this.getClass(), key);
    }

    public Key<InetCourse> getCourseKey() {
        return courseKey;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InetTestScores other = (InetTestScores) obj;
        if (this.key != other.key && (this.key == null || !this.key.equals(other.key))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (this.key != null ? this.key.hashCode() : 0);
    }

    public double getMaxScore() {
        return maxScore;
    }

    //BUGFIX, revert to package access in a later update
    public void setMaxScore(double maxScore) {
        this.maxScore = maxScore;
    }

    public double getMinScore() {
        return DEFAULT_MIN_SCORE;
    }

    public Set<Key<InetStudent>> getStudentKeys()
    {
        return studentScores.getStudentKeys();
    }

    public Iterator<Map.Entry<String, Double>> getStudentScores() {
        return studentScores.getScores();
    }

    public Double getTestScore(Key<InetStudent> k)
    {
        return studentScores.getScore(k);
    }

    public Double getPercentageScore(Key<InetStudent> k)
    {
        return getTestScore(k) / getMaxScore() * 100;
    }

    public boolean hasTestScore(Key<InetStudent> sk)
    {
        return studentScores.hasScore(sk);
    }

    boolean addStudentScore(Key<InetStudent> sk, Double scr)
    {
        boolean result = studentScores.addScore(sk, scr);
        return result;
    }

    public int getNumOfScores()
    {
        return studentScores.getNumOfScores();
    }

    boolean removeStudentScore(Key<InetStudent> sk)
    {
        return studentScores.removeScore(sk);
    }

    boolean updateStudentScore(Key<InetStudent> sk, Double scr)
    {
        return studentScores.updateScore(sk, scr);
    }

    public String getTestName() {
        return testName;
    }

    void setTestName(String testName) {
        this.testName = testName;
    }
    
}
