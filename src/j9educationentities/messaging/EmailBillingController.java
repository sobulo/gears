/**
 * 
 */
package j9educationentities.messaging;

import j9educationactions.pdfreports.BillingInvoiceGenerator;

import java.util.Arrays;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class EmailBillingController extends EmailReportCardController{
	
	EmailBillingController(){}
	
	EmailBillingController(String key, String fromAddress, int characterLimit, int dailyMessageLimit, int totalMessageLimit)
	{
		super(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
	}

	
	@Override
	public Key<EmailBillingController> getKey() {
		return (Key<EmailBillingController>) new Key(EmailBillingController.class, key);
	}

	protected String getTextBody(String[] messageInfo)
	{
		return "Attached pdf file contains invoice for " + (messageInfo.length - 1) + " student(s)";
	}
	
	protected String getFileName(String[] messageInfo)
	{
		return messageInfo[1] + "-invoice.pdf";
	}
	
	protected String getSubject(String[] messageInfo)
	{
		return "Important: Invoice is ready!";
	}
	
	protected byte[] getAttachmentData(String[] messageInfo)
	{
		String[] loginNames = Arrays.copyOfRange(messageInfo, 1, messageInfo.length);		
		return BillingInvoiceGenerator.getInvoiceData(messageInfo[0], loginNames);		
	}	
}
