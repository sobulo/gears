/**
 * 
 */
package j9educationentities.messaging;

import j9educationactions.pdfreports.ReportCardGenerator;

import java.util.Arrays;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class EmailReportCardController extends MessagingController{
	
	EmailReportCardController() {}
	
	EmailReportCardController(String key, String fromAddress, int characterLimit, int dailyMessageLimit, int totalMessageLimit)
	{
		super(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
	}

	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<? extends EmailReportCardController> getKey() {
		return (Key<? extends EmailReportCardController>) new Key(EmailReportCardController.class, key);
	}

	/* (non-Javadoc)
	 * @see j9educationentities.messaging.MessagingController#sendMessage(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean sendMessage(String toAddress, String[] messageInfo) {
		byte[] pdfDataTemp = getAttachmentData(messageInfo);
		if(pdfDataTemp == null) return false;
		DataSource source = new ByteArrayDataSource(pdfDataTemp, "application/pdf");
		
		//Multi
        Properties props = new Properties(); 
        Session session = Session.getDefaultInstance(props, null);
        try
        {
        	//create multipart message
        	Multipart multipart = new MimeMultipart();
        	
        	//add plain text
        	MimeBodyPart textPart = new MimeBodyPart();
        	textPart.setContent(getTextBody(messageInfo), "text/plain");
        	multipart.addBodyPart(textPart);
        	
        	//add attachment
        	MimeBodyPart attachmentPart = new MimeBodyPart();
        	//attachmentPart.setContent(pdfData, "application/pdf");
        	attachmentPart.setDataHandler(new DataHandler(source));
        	attachmentPart.setFileName(getFileName(messageInfo));
        	multipart.addBodyPart(attachmentPart);
        	
        	//setup the message
        	Message message = new MimeMessage(session);
        	message.setFrom(new InternetAddress(getFromAddress()));
        	message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
        	message.setSubject(getSubject(messageInfo));
        	message.setContent(multipart);
        	
        	//send message
        	Transport.send(message);
        }
        catch(Exception ex)
        {
        	ex.printStackTrace();
        	return false;
        }
        return true;
	}
	
	protected String getTextBody(String[] messageInfo)
	{
		return "Attached pdf file contains report card for " + (messageInfo.length - 1) + " student(s)";
	}
	
	protected String getFileName(String[] messageInfo)
	{
		return messageInfo[1] + "-report-card.pdf";
	}
	
	protected String getSubject(String[] messageInfo)
	{
		return "Important: Report Card is ready!";
	}
	
	protected byte[] getAttachmentData(String[] messageInfo)
	{
		String[] loginNames = Arrays.copyOfRange(messageInfo, 1, messageInfo.length);		
		return ReportCardGenerator.getReportCardData(messageInfo[0], loginNames);		
	}

}
