/**
 * 
 */
package j9educationentities.messaging;

import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class MessagingDAO {
	
	private static final Logger log = Logger.getLogger(MessagingDAO.class.getName());	
	
	public static EmailController createEmailController(String key, String fromAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit) throws DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		EmailController controller = new EmailController(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
		Key<EmailController> controllerKey = controller.getKey();
		try
		{
			EmailController tempController = ofy.find(controllerKey);
			if(tempController != null)
			{
				log.severe("Duplicate Controller: " + controllerKey);
				throw new DuplicateEntitiesException(controllerKey.toString());
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return controller;
	}
	
	public static EmailReportCardController createEmailReportCardController(String key, String fromAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit) throws DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		EmailReportCardController controller = new EmailReportCardController(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
		Key<EmailReportCardController> controllerKey = (Key<EmailReportCardController>) controller.getKey();
		try
		{
			EmailReportCardController tempController = ofy.find(controllerKey);
			if(tempController != null)
			{
				log.severe("Duplicate Controller: " + controllerKey);
				throw new DuplicateEntitiesException(controllerKey.toString());
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return controller;
	}
	
	public static EmailBillingController createBillingController(String key, String fromAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit) throws DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		EmailBillingController controller = new EmailBillingController(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
		Key<EmailBillingController> controllerKey = controller.getKey();
		try
		{
			EmailBillingController tempController = ofy.find(controllerKey);
			if(tempController != null)
			{
				log.severe("Duplicate Controller: " + controllerKey);
				throw new DuplicateEntitiesException(controllerKey.toString());
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return controller;
	}	
	
	public static SMSController createSMSController(String key, String fromAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit, 
			String server, int port, String username, String password, String dlr, String type) throws DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		SMSController controller = new SMSController(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit, server, port, username, password, dlr, type);
		Key<SMSController> controllerKey = controller.getKey();
		try
		{
			SMSController tempController = ofy.find(controllerKey);
			if(tempController != null)
			{
				log.severe("Duplicate Controller: " + controllerKey);
				throw new DuplicateEntitiesException(controllerKey.toString());
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return controller;
	}	
	
	/*public static void scheduleMessageSending(Key<? extends MessagingController> controllerKey, 
			HashMap<String, String[]> messageMap) throws MissingEntitiesException, ManualVerificationException 
	{	
		String controllerId = validateController(controllerKey, messageMap.size());
		
		for(String address : messageMap.keySet())
			TaskQueueHelper.scheduleMessageSending(controllerId, address, messageMap.get(address));		
	}*/
	
	public static String validateController(Key<? extends MessagingController> controllerKey, int numOfMessages)
	throws MissingEntitiesException, ManualVerificationException
	{
		if(numOfMessages == 0) return null;
		Objectify ofy = ObjectifyService.beginTransaction();
		MessagingController controller = ofy.find(controllerKey);
		
		if(controller == null)
		{
			String msgFmt = "Unable to find messaging controller: ";
			log.warning(msgFmt + controllerKey.toString());
			throw new MissingEntitiesException(msgFmt + controllerKey.getName());
		}
		
		String controllerId = ofy.getFactory().keyToString(controllerKey);
		try
		{
			int currentTotal = controller.updateNumberOfMessagesSent(numOfMessages);
			
			if( currentTotal == 0)
				throw new IllegalStateException("Controller not initiated properly: " + controllerKey.toString());
			
			int limit = controller.getDailyMessageLimit();
			if(currentTotal > limit)
			{
				String msgFmt = "Sending messages would cause new counts (%d) to exceed daily limits (%d)";
				msgFmt = String.format(msgFmt, currentTotal, limit);
				log.warning(msgFmt);
				throw new ManualVerificationException(msgFmt);
			}
			
			limit = controller.getTotalMessageLimit();
			if(controller.getTotalSentMessages() > limit)
			{
				String msgFmt = "Sending messages would cause new counts (%d) to exceed maximum limits(%d)";
				msgFmt = String.format(msgFmt, currentTotal, limit);
				log.warning(msgFmt);
				throw new ManualVerificationException(msgFmt);				
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return controllerId;
	}
	
	public static HashMap<String, String> getMessagingControllerNames()
	{
		Objectify ofy = ObjectifyService.begin();
		//TODO replace this with polymorphic queries, will need to do a data conversion first though
		//before updating to latest build of objectify
		Class[] controllerClasses = {EmailController.class, SMSController.class, EmailBillingController.class, EmailReportCardController.class};
		HashMap<String, String> result = new HashMap<String, String>();
		for(Class cls : controllerClasses)
		{
			List controllerKeys = ofy.query(cls).listKeys();
			for(int i = 0; i < controllerKeys.size(); i++)
			{
				Key ck = (Key) controllerKeys.get(i);
				result.put(ofy.getFactory().keyToString(ck), ck.getName());
			}
		}
		return result;
	}
}
