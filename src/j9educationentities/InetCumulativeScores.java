/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;

/**
 *
 * @author Segun Razaq Sobulo
 */
@Cached
public class InetCumulativeScores extends InetTestScores{
    private final static double CUMULATIVE_MAX_SCORE = 100.0;

    InetCumulativeScores(){}

    InetCumulativeScores(Key<InetCourse> courseKey) {
        super(InetConstants.CUMULATIVE_TEST_NAME, CUMULATIVE_MAX_SCORE, courseKey);
    }

    public static Key<InetCumulativeScores> getCumScoreKey(Key<InetCourse> courseKey)
    {
        return new Key(courseKey, InetCumulativeScores.class, InetConstants.CUMULATIVE_TEST_NAME);
    }
}
