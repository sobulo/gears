package j9educationentities;

import j9educationactions.tasks.TaskQueueHelper;
import j9educationentities.InetHold.InetHoldType;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


/**
 * Functions defined in this class handle updates that are NOT the side effect of
 * the creation of a new entity which is handled by class InetDAO4Updates. E.g.
 * updating test scores involves creation of no entities so handled by this class, whereas
 * adding students to a level roster is possibly the side effect of creating a new student so
 * handled by InetDAO4Updates
 * 
 * @author Segun Razaq Sobulo
 *
 */

public class InetDAO4ComplexUpdates 
{
    private static final Logger log =
        Logger.getLogger(InetDAO4ComplexUpdates.class.getName());
    
	public static InetLevel deleteLevel(String levelKeyStr) 
		throws MissingEntitiesException, ManualVerificationException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		InetLevel level = null;
		try
		{
			Key<InetLevel> levKey = ofy.getFactory().stringToKey(levelKeyStr);
			level = ofy.find(levKey);		
			if(level == null)
			{
				MissingEntitiesException ex = new MissingEntitiesException("Requesting" +
						" to delete an object that doesn't exist: " + levKey);
				log.severe(ex.getMessage());
				throw ex;
			}
			
			boolean hasStudents = level.numOfStudents() > 0;
			boolean hasCourses = level.numOfCourses() > 0;
			
			if(hasStudents || hasCourses)
			{
				ManualVerificationException ex = new ManualVerificationException("unable" +
						" to delete as level(" + levKey + ") still contains students(" + 
						level.numOfStudents() + ") and/or courses("  + 
						level.numOfCourses() +")");
				log.severe(ex.getMessage());
				throw ex;
			}
			ofy.delete(level);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return level;
	}
	
	public static void removeLevelFromSchool(String levelKeyStr) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<InetLevel> levKey = ofy.getFactory().stringToKey(levelKeyStr);
			InetSchool s = InetDAO4CommonReads.getSchool(ofy);
			if(!s.removeInetLevelKey(levKey))
			{
				String msg = "Unable to remove level from school as its not in level list";
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			ofy.put(s);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	public static InetCourse deleteCourse(String courseKeyStr) 
		throws MissingEntitiesException, ManualVerificationException
	{
		InetCourse course;
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
			course = ofy.find(courseKey);
			if(course == null)
			{
				String msgFmt = "Requesting to delete an object that doesn't exist: ";
				log.warning(msgFmt + courseKey);
				throw new MissingEntitiesException(msgFmt + courseKey.getName());
			}
			
			InetCumulativeScores cumScores = ofy.find(course.getCumulativeScores());
			if(cumScores == null)
			{
				String msg = "Unable to find cumulative scores: " 
					+ course.getCumulativeScores();
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			
			boolean hasStudents = cumScores.getNumOfScores() > 0;
			boolean hasTests = course.getNumberOfTestScores() > 0;
			
			if(hasStudents || hasTests)
			{
				String msg = "Unable to delete course as it contains students(" + 
				cumScores.getNumOfScores() + ") and/or tests(" + 
				course.getNumberOfTestScores() + ")";
				log.severe(msg);
				throw new ManualVerificationException(msg);
			}
			ArrayList<GAEPrimaryKeyEntity> deleteList = 
				new ArrayList<GAEPrimaryKeyEntity>(2);
			deleteList.add(cumScores);
			deleteList.add(course);
			ofy.delete(deleteList);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return course;
	}
	
	public static void removeCourseFromLevel(String courseKeyStr, String levelKeyStr ) 
		throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
			Key<InetLevel> levelKey = ofy.getFactory().stringToKey(levelKeyStr);
			InetLevel level = ofy.find(levelKey);
			if(level == null)
			{
				String msg = "Unable to find level: " + levelKey;
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			
			if(!level.removeSubjectFromCourseList(courseKey))
			{
				String msg = "Unable to remove course from level as not existent in " +
						"course list: " + courseKey;
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			
			ofy.put(level);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	public static InetSingleTestScores deleteTest(String testKeyStr) 
		throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		InetSingleTestScores test = null;
		try
		{
			Key<InetSingleTestScores> testKey = ofy.getFactory().stringToKey(testKeyStr);
			test = ofy.find(testKey);		

			if(test == null)
			{
				String msgFmt = "Requesting to delete a test/exam that doesn't exist: "; 
				MissingEntitiesException ex = new MissingEntitiesException(msgFmt + testKey.getName());
				log.severe(msgFmt + testKey);
				throw ex;
			}
			
			Key<InetCourse> parentCourseKey = test.getCourseKey();
			InetCourse parentCourse = ofy.find(parentCourseKey);
			
			if(parentCourse == null)
			{
				MissingEntitiesException ex = new MissingEntitiesException("Requesting" +
						" to delete a test with no parent course: " + parentCourseKey);
				log.severe(ex.getMessage());
				throw ex;
			}
			
			if(!parentCourse.removeTestScoresFromCourse(testKey))
			{
				String msg = "Unable to remove test from course as not existent in " +
				"test list: " + testKey;
				log.severe(msg);
				throw new MissingEntitiesException(msg);				
			}
			
			ofy.put(parentCourse);
			ofy.delete(test);
			ofy.getTxn().commit();
			
			//schedule update of cumulative scores
			TaskQueueHelper.scheduleUpdateCumulativeScores(ofy.getFactory().keyToString(parentCourseKey));
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return test;
	}
	
	public static ArrayList<Key<InetStudent>> updateTestScores(Key<InetSingleTestScores> testKey,
	        HashMap<Key<InetStudent>, Double> studentScores, boolean scheduleCumulativeUpdate) throws MissingEntitiesException
	{
	    ArrayList<Key<InetStudent>> updatedKeys = new ArrayList<Key<InetStudent>>();
	    Objectify ofy = ObjectifyService.beginTransaction();
	    try {
	        InetSingleTestScores test = ofy.find(testKey);
	        if (test == null)
	        {
	            MissingEntitiesException ex = new MissingEntitiesException("Unable" +
	                    " to add student scores. Can't locate: " + testKey);
	            //log.severe(ex.getMessage());
	            throw ex;
	        }
	
	        Iterator<Map.Entry<Key<InetStudent>, Double>> scoreItr = studentScores.entrySet().iterator();
	
	        while(scoreItr.hasNext())
	        {
	            Map.Entry<Key<InetStudent>, Double> score = scoreItr.next();
	            if (test.updateStudentScore(score.getKey(), score.getValue()))
	                updatedKeys.add(score.getKey());
	
	        }
	        ofy.put(test);
	        if(scheduleCumulativeUpdate)
                TaskQueueHelper.scheduleUpdateCumulativeScores(ofy.
                        getFactory().keyToString(testKey.getParent()));
	        ofy.getTxn().commit();
	    } finally {
	        if (ofy.getTxn().isActive()) {
	            ofy.getTxn().rollback();
	        }
	    }
	    return updatedKeys;
	}

	public static InetStudent deleteStudent(String loginName, Objectify ofyTxn) 
		throws MissingEntitiesException, ManualVerificationException
	{
		InetStudent student;
		try
		{
			Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class, loginName);
			student = ofyTxn.find(studKey);		
			if(student == null)
			{
				String msgFmt = "Requesting to delete an object that doesn't exist: ";
				MissingEntitiesException ex = new MissingEntitiesException(msgFmt + studKey.getName());
				log.severe(msgFmt + studKey);
				throw ex;
			}
			
			boolean hasCourses = student.getNumberOfCourses() > 0;
			
			if(hasCourses)
			{
				String msgFmt = "unable to delete as student (%s) still contains courses (%d)";
				log.severe(String.format(msgFmt, studKey.toString(), student.getNumberOfCourses()));
				throw new ManualVerificationException(String.format(msgFmt, studKey.getName(),
						student.getNumberOfCourses()));
			}
			ofyTxn.delete(student);
			ofyTxn.getTxn().commit();
		}
		finally
		{
			if(ofyTxn.getTxn().isActive())
				ofyTxn.getTxn().rollback();
		}
		return student;
	}
	
	public static void removeStudentsFromLevel(Collection<Key<InetStudent>> studentKeys, Key<InetLevel> levKey) 
		throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetLevel level = ofy.find(levKey);
			if(level == null)
			{
				String msg = "Unable to find level: " + levKey;
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			
			for(Key<InetStudent> studKey : studentKeys)
			{
				if(!level.removeStudentFromRoster(studKey))
				{
					String msg = "Aborted!! Unable to remove student from level(" + 
						levKey +  ")as not existent in level student list: " + studKey;
					log.severe(msg);
					throw new MissingEntitiesException(msg);
				}
			}
			
			ofy.put(level);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	public static void removeStudentsFromCourse(String[] studentKeys, String courseKeyStr) 
		throws MissingEntitiesException
	{
		log.warning("Received request to remove: " + studentKeys.length + " students");
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
			
			Map<Key<InetTestScores>, InetTestScores> scores = 
				InetDAO4CommonReads.getCourseTests(courseKey, ofy);
			
			Iterator<InetTestScores> scoresItr = scores.values().iterator();

			HashMap<String, Key<InetStudent>> convertedKeys = 
				new HashMap<String, Key<InetStudent>>(studentKeys.length);
			Key<InetStudent> studentKey;
			while(scoresItr.hasNext())
			{				
				InetTestScores ts = scoresItr.next();
				for(String studKeyStr : studentKeys)
				{
					if(convertedKeys.containsKey(studKeyStr))
						studentKey = convertedKeys.get(studKeyStr);
					else
						studentKey = ofy.getFactory().stringToKey(studKeyStr);
					
					log.warning("deleting: " + studentKey);				
					if(!ts.removeStudentScore(studentKey))
					{
						String msg = "aborting removal of student from course due to " +
								"student(" + studentKey.getName() +") missing from test(" + 
								ts.getKey() + ")";
						log.severe(msg);
						throw new MissingEntitiesException(msg);
					}
				}
			}
			ofy.put(scores.values());
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
	}
	
	public static void updateTestScores(ArrayList<Key<InetSingleTestScores>> testKeys, 
			HashMap<Key<InetStudent>, Double[]> scoreEntries) throws MissingEntitiesException
	{
		if(testKeys.size() == 0)
			return;
		
		Key<InetCourse> courseKey = testKeys.get(0).getParent();
		
		for(int i = 0; i < testKeys.size(); i++)
			if(!courseKey.equals(testKeys.get(i).getParent()))
				throw new IllegalArgumentException("Attempting to update tests that do" +
						" not belong to the same course, " + courseKey + " vs " + testKeys.get(i).getParent());
		
		Objectify ofy = ObjectifyService.beginTransaction();
		boolean success = false;
		try
		{
			Map<Key<InetSingleTestScores>, InetSingleTestScores> tks = InetDAO4CommonReads.getEntities(testKeys, ofy, true);
			
			Iterator<Map.Entry<Key<InetStudent>, Double[]>> studScoresItr = 
				scoreEntries.entrySet().iterator();
			
			while(studScoresItr.hasNext())
			{
				Map.Entry<Key<InetStudent>, Double[]> studentScoreRecord = studScoresItr.next();
				Key<InetStudent> studKey = studentScoreRecord.getKey();
				
				for(int i = 0; i < testKeys.size(); i++)
				{
					InetSingleTestScores testScores = tks.get(testKeys.get(i));
					Double score = studentScoreRecord.getValue()[i];
					
					if(score == null)
						continue;
					
					if(!testScores.updateStudentScore(studKey, score))
					{
						String msg = "Aborting update of test scores("+ testKeys.get(i) + ") due to" +
								" missing record for: " + studKey;
						log.severe(msg);
						throw new MissingEntitiesException(msg);						
					}
				}
			}
			
			for(int i = 0; i < testKeys.size(); i++)
			{
				InetSingleTestScores testScores = tks.get(testKeys.get(i));
				ofy.put(testScores);
			}
			
			
			ofy.getTxn().commit();
			success = true;
		}
		finally
		{
			if(ofy.getTxn().isActive())
			{
				ofy.getTxn().rollback();
				success = false;
			}
		}
		if(success)
			//schedule update of cumulative scores
			TaskQueueHelper.scheduleUpdateCumulativeScores(ofy.getFactory().keyToString(courseKey));
	}
	
	public static void updateTestScores(Key<InetSingleTestScores> testKey, 
			HashMap<String, Double> studScores, boolean isLoginName, Objectify ofy) 
		throws MissingEntitiesException
	{
		try
		{
			InetSingleTestScores testScores = ofy.find(testKey);
			if(testScores == null)
			{
				String msg = "Unable to update scores for missing test: " + testKey;
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			
			Iterator<Map.Entry<String, Double>> studScoresItr = 
				studScores.entrySet().iterator();
			
			while(studScoresItr.hasNext())
			{
				Map.Entry<String, Double> studentScoreRecord = studScoresItr.next();
				Key<InetStudent> studKey;
				
				if(isLoginName)
					studKey = new Key<InetStudent>(InetStudent.class, 
							studentScoreRecord.getKey());
				else
					studKey = ofy.getFactory().stringToKey(studentScoreRecord.getKey());
				
				if(!testScores.updateStudentScore(studKey, studentScoreRecord.getValue()))
				{
					String msg = "Aborting update of test scores("+ testKey + ") due to" +
							" missing record for: " + studKey;
					log.severe(msg);
					throw new MissingEntitiesException(msg);						
				}
			}
			ofy.put(testScores);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	public static void removeLevelFromStudentList(String levKeyStr, String studKeyStr) 
		throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<InetLevel> levKey = ofy.getFactory().stringToKey(levKeyStr);
			Key<InetStudent> studKey = ofy.getFactory().stringToKey(studKeyStr);
			InetStudent stud = ofy.find(studKey);
			if(stud == null)
			{
				String msg = "unable to remove level(" + levKey + ") from student list" +
						" as student(" + studKey + ") is non-existent"; 
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			
			if(!stud.removeLevel(levKey))
			{
				String msg = "unable to remove level(" + levKey + ") from student(" + 
				studKey + ") as level is non-existent in student list";
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			ofy.put(stud);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		
	}

	public static void removeCourseFromStudentList(String courseKeyStr, String studKeyStr) 
		throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<InetCourse> courseKey = ofy.getFactory().stringToKey(courseKeyStr);
			Key<InetStudent> studKey = ofy.getFactory().stringToKey(studKeyStr);
			log.warning("About to remove " + courseKey + " from " + studKey);
			InetStudent stud = ofy.find(studKey);
			if(stud == null)
			{
				String msg = "unable to remove level(" + courseKey + ") from student list" +
						" as student(" + studKey + ") is non-existent"; 
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			
			if(!stud.removeCourse(courseKey))
			{
				String msg = "unable to remove level(" + courseKey + ") from student(" + 
				studKey + ") as level is non-existent in student list";
				log.severe(msg);
				throw new MissingEntitiesException(msg);
			}
			ofy.put(stud);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	
	}
	
	public static InetGuardian deleteGuardian(String loginName) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		InetGuardian guard;
		try
		{
			Key<InetGuardian> guardKey = new Key<InetGuardian>(InetGuardian.class, loginName);
			guard = ofy.find(guardKey);
			
			if(guard == null)
			{
				String msgFmt = "Unable to delete guardian(%s) as non-existent";
				log.severe(String.format(msgFmt, guardKey.toString()));
				throw new MissingEntitiesException(String.format(msgFmt, guardKey.getName()));
			}			
			ofy.delete(guard);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().commit();
		}

		return guard;
	}
	
	public static void removeGuardianFromStudent(String guardLogin, String studLogin) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{			
			Key<InetGuardian> guardKey = new Key<InetGuardian>(InetGuardian.class, guardLogin);
			Key<InetStudent> studKey = new Key<InetStudent>(InetStudent.class, studLogin);			
			InetStudent stud = ofy.find(studKey);
			if(stud == null)
			{
				String msgFmt = "Unable to remove guardian/parent(%s) from student(%s) as" +
						" student doesn't exist";
				log.severe(String.format(msgFmt, guardKey.toString(), studKey.toString()));
				throw new MissingEntitiesException(String.format(guardKey.getName(), studKey.getName()));
			}
			
			if(guardKey.equals(stud.getFirstGuardianKey()))
				stud.setFirstGuardianKey(null);
			else if(guardKey.equals(stud.getSecondGuardianKey()))
				stud.setSecondGuardianKey(null);
			else
			{
				String msgFmt = "Unable to remove guardian/parent(%s) from student(%s) as" +
				" student not paired with gaurdian";
				log.severe(String.format(msgFmt, guardKey.toString(), studKey.toString()));
				throw new MissingEntitiesException(String.format(guardKey.getName(), studKey.getName()));			
			}
			ofy.put(stud);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
    public static InetHold editHold(Key<InetHold> holdKey, InetHoldType type, 
    		String message, String note, boolean resolutionStatus) 
    	throws MissingEntitiesException
    {
    	InetHold hold;
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
        	hold = ofy.find(holdKey);
        	if(hold == null)
        	{
        		String msgFmt = "Unable to find hold for: ";
        		log.severe(msgFmt + holdKey.toString());
        		throw new MissingEntitiesException(msgFmt + holdKey.getId());
        	} 
        	hold.setHoldType(type);
        	hold.setMessage(new Text(message));
        	hold.setNote(new Text(note));
        	hold.setResolutionStatus(resolutionStatus);

        	ofy.put(hold);
        	ofy.getTxn().commit();
        }
        finally
        {
        	if(ofy.getTxn().isActive())
        		ofy.getTxn().rollback();
        }
        return hold;
    }	
}
