/**
 * 
 */
package j9educationentities;

import javax.persistence.Id;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Cached
@Unindexed
public class InetHold implements GAEPrimaryKeyEntity{
	
	@Id
	private Long key;
	
	@Parent private Key<InetHoldContainer> levelGrouping;
	
	private Text message;
	
	private Text note;
	
	@Indexed
	private Key<InetStudent> studentKey;
	
	@Indexed private boolean resolutionStatus; 
	
	private InetHoldType type;
	
	public InetHold(){}
	
	public InetHold(Key<InetHoldContainer> holdContainer, Key<InetStudent> studentKey, InetHoldType type)
	{
		this.studentKey = studentKey;
		this.levelGrouping = holdContainer;
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<InetHold> getKey() {
		return new Key<InetHold>(levelGrouping, InetHold.class, key);
	}

	public Key<InetHoldContainer> getHoldContainer() {
		return levelGrouping;
	}
	
	public InetHoldType getHoldType()
	{
		return type;
	}
	
	public void setHoldType(InetHoldType type)
	{
		this.type = type;
	}

	public Text getMessage() {
		return message;
	}

	public void setMessage(Text message) {
		this.message = message;
	}

	public Text getNote() {
		return note;
	}

	public void setNote(Text note) {
		this.note = note;
	}

	public Key<InetStudent> getStudentKey() {
		return studentKey;
	}

	public boolean isResolved() {
		return resolutionStatus;
	}

	public void setResolutionStatus(boolean resolutionStatus) {
		this.resolutionStatus = resolutionStatus;
	}
	
	public static enum InetHoldType
	{
		HOLD_FINANCIAL, HOLD_ACADEMIC_PROBATION, HOLD_OTHER;
	}
}
