/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;

import j9educationutilities.GeneralFuncs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;


/**
 *
 * @author Segun Razaq Sobulo
 */
@Cached
@Unindexed
public class InetLevel implements GAEPrimaryKeyEntity{
    @Id
    private String key;

    //TODO, indexed fields below beg the question, why do we keep list in school obj
    @Indexed private String term;

    @Indexed private String academicYear;

    private String levelName;

    private String subLevelName;

    private HashSet<Key<InetStudent>> studentRoster; //e.g. especially useful for historical levels

    private HashSet<Key<InetCourse>> courseList;

    private static final Logger log = Logger.getLogger(InetLevel.class.getName());

    InetLevel()
    {
        this.studentRoster = new HashSet<Key<InetStudent>>();
        this.courseList = new HashSet<Key<InetCourse>>();
    }

    InetLevel(String term, String academicYear, String levelName, String subLevelName)
    {
        this();
        this.term = term;
        this.academicYear = academicYear;
        this.levelName = levelName;
        this.subLevelName = subLevelName;

        //get key string
        this.key = getKeyString();
    }

    private String getKeyString()
    {
        String format = "%s~%s~%s~%s";
        return String.format(format, getAcademicYear(),
                getTerm(), getLevelName(), getSubLevelName()).toLowerCase();
    }

    @Override
    public int hashCode()
    {
        return (key == null ? 0 : key.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InetLevel other = (InetLevel) obj;
        if (this.key != other.key && (this.key == null || !this.key.equals(other.key))) {
            return false;
        }
        return true;
    }
    
    public String getAcademicYear() {
        return academicYear;
    }

    @Override
    public Key<InetLevel> getKey() {
        return new Key(InetLevel.class, key);
    }

    public String getLevelName() {
        return levelName;
    }

    public Set<Key<InetStudent>> getStudentKeys()
    {
        return studentRoster;
    }

    boolean addStudentToRoster(Key<InetStudent> sk){
        return GeneralFuncs.addToHashSet(studentRoster, sk);
    }

    public boolean removeStudentFromRoster(Key<InetStudent> sk)
    {
        return GeneralFuncs.removeFromHashSet(studentRoster, sk);
    }

    public Set<Key<InetCourse>> getCourseList() {
        return courseList;
    }

    boolean addSubjectToCourseList(Key<InetCourse> ck){
        return GeneralFuncs.addToHashSet(courseList, ck);
    }

    boolean removeSubjectFromCourseList(Key<InetCourse> ck)
    {
        return GeneralFuncs.removeFromHashSet(courseList, ck);
    }

    public int numOfStudents()
    {
        return studentRoster.size();
    }

    public int numOfCourses()
    {
        return courseList.size();
    }

    public String getSubLevelName() {
        return subLevelName;
    }

    public String getTerm() {
        return term;
    }
    
    //works only if keys can be sorted alphabetically
    public static Key<InetLevel>[] sortLevels(Iterable<Key<InetLevel>> levelKeys)
    {
    	int count = 0;
    	HashMap<String, Key<InetLevel>> levelMap = new HashMap<String, Key<InetLevel>>();
    	ArrayList<String> levelNames = new ArrayList<String>();
    	for(Key<InetLevel> lk : levelKeys)
    	{
    		levelMap.put(lk.getName(), lk);
    		levelNames.add(lk.getName());
    	}
    	Key<InetLevel>[] result = new Key[levelMap.size()];
    	Collections.sort(levelNames);
    	for(int i = 0; i < levelNames.size(); i++)
    		result[i] = levelMap.get(levelNames.get(i));
    	return result;
    }

    @Override
    public String toString()
    {
        String format = "%s %s (%s-%s)";
        return String.format(format, levelName, subLevelName, academicYear, term);
    }
}
