/**
 * 
 */
package j9educationentities.accounting;

import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetUser;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Indexed
@Cached
public class InetBill implements GAEPrimaryKeyEntity{
	@Id private String key;
	private Key<InetBillDescription> billTemplateKey;
	@Unindexed private double totalAmount;
	@Unindexed private double settledAmount;
	private Key<? extends InetUser> userKey;
	private boolean settled;
	
	final static double THRESHOLD = 0.01;
	
	InetBill(){}
	
	InetBill(InetBillDescription billTemplate, Key<? extends InetUser> billedUserKey)
	{
		this();
		this.billTemplateKey = billTemplate.getKey();		
		this.key = getKeyString(billTemplate.getKey(), billedUserKey);
		this.settled = false;
		this.userKey = billedUserKey;
		this.totalAmount = billTemplate.getTotalAmount();
	}
	
    public static String getKeyString(Key<InetBillDescription> billTemplateKey, Key<? extends InetUser> userKey)
    {
        String format = "%s~%s";
        return String.format(format, billTemplateKey.getName(), userKey.getName());
    }	

	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<InetBill> getKey() {
		return new Key(InetBill.class, key);
	}
	
	public boolean isSettled()
	{
		return settled;
	}
	
	public double getTotalAmount()
	{
		return totalAmount;
	}
	
	public double getSettledAmount()
	{
		return settledAmount;
	}
	
	public void setSettledAmount(double amount)
	{
		settledAmount = amount;
	}
	
	public Key<? extends InetUser> getUserKey()
	{
		return userKey;
	}
	
	public Key<InetBillDescription> getBillTemplateKey()
	{
		return billTemplateKey;
	}
	
	@PrePersist void updateSettled() { settled = Math.abs(totalAmount - settledAmount) < THRESHOLD; }; 
}
