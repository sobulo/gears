/**
 * 
 */
package j9educationentities.accounting;

import j9educationentities.GAEPrimaryKeyEntity;
import j9educationgwtgui.shared.BillDescriptionItem;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class InetBillDescription implements GAEPrimaryKeyEntity
{
    @Id private String key;
    @Indexed private String term;
    @Indexed private String academicYear;   
    private String name;
    private Date dueDate;
    
    @Serialized
    private LinkedHashSet<BillDescriptionItem> itemizedBill;
    
	public InetBillDescription() {
		itemizedBill = new LinkedHashSet<BillDescriptionItem>();
	}
    
    InetBillDescription(String name, String term, String year, Date dueDate, LinkedHashSet<BillDescriptionItem> itemizedBill)
    {
    	this();
    	this.term = term;
    	this.academicYear = year;
    	this.name = name;
    	this.dueDate = dueDate;
    	this.itemizedBill = itemizedBill;
    	key = getKeyString();
    }
    
    private String getKeyString()
    {
        String format = "%s~%s~%s";
        return String.format(format, academicYear,
                term, name.toLowerCase());
    }    
    
	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<InetBillDescription> getKey() {
		return new Key(InetBillDescription.class, key);
	}

	public String getTerm() {
		return term;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public String getName() {
		return name;
	}
	
	public Date getDueDate()
	{
		return dueDate;
	}

	public HashSet<BillDescriptionItem> getItemizedBill() {
		return itemizedBill;
	}
	
	public void setItemizedBill(LinkedHashSet<BillDescriptionItem> itemizedBill)
	{
		this.itemizedBill = itemizedBill;
	}
	
	public double getTotalAmount()
	{
		double total = 0;
		for(BillDescriptionItem bdi : itemizedBill)
			total += bdi.getAmount();
		return total;
	}
}