/**
 * 
 */
package j9educationentities.accounting;

import j9educationentities.GAEPrimaryKeyEntity;

import java.util.Date;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class InetBillPayment implements GAEPrimaryKeyEntity{
	@Id Long key;
	@Indexed private Date paymentDate;
	@Parent private Key<InetBill> allocatedBill;
	private String referenceId;
	private String comments;
	private double amount;
	
	InetBillPayment(){}
	
	InetBillPayment(Key<InetBill> billKey, double amount, String referenceId, String comments, Date paymentDate)
	{
		this.amount = amount;
		this.comments = comments;
		this.referenceId = referenceId;
		this.paymentDate = paymentDate;
		this.allocatedBill = billKey;
	}

	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<InetBillPayment> getKey() {
		return new Key<InetBillPayment>(allocatedBill, InetBillPayment.class, key);
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public String getComments() {
		return comments;
	}

	public double getAmount() {
		return amount;
	}
}
