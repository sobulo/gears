/**
 * 
 */
package j9educationentities.accounting;

import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetUser;
import j9educationgwtgui.shared.BillDescriptionItem;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.ManualVerificationException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetDAO4Accounts {
	private static final Logger log = Logger.getLogger(InetDAO4Accounts.class.getName());
	
    public static InetBillDescription createBillDescription(String academicYear, String term, Date dueDate, String name, 
    		LinkedHashSet<BillDescriptionItem> itemizedBill) throws DuplicateEntitiesException
    {
    	InetBillDescription bd = new InetBillDescription(name, term, academicYear, dueDate, itemizedBill);
    	//bd.setItemizedBill(itemizedBill);
    	Objectify ofy = ObjectifyService.beginTransaction();
    	try
    	{
    		InetBillDescription bdCheck = ofy.find(bd.getKey());
            if (bdCheck != null) 
            {
            	String msgPrefix = "Bill description already exists. Details: ";
                DuplicateEntitiesException ex = new DuplicateEntitiesException(msgPrefix + InetDAO4CommonReads.keyToPrettyString(bd.getKey()));
                log.severe(msgPrefix + bd.getKey());
                throw ex;
            }
            ofy.put(bd);
            ofy.getTxn().commit();    	
        }
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
    	return bd;
    }
    
    public static InetBill createBill(InetBillDescription billTemplate, Key<? extends InetUser> billedUserKey) throws DuplicateEntitiesException
    {
    	InetBill bill = new InetBill(billTemplate, billedUserKey);
    	Objectify ofy = ObjectifyService.beginTransaction();
    	try
    	{
    		InetBill billCheck = ofy.find(bill.getKey());
    		if(billCheck != null)
    		{
            	String msgPrefix = "Bill already exists. Details: ";
                DuplicateEntitiesException ex = new DuplicateEntitiesException(msgPrefix + InetDAO4CommonReads.keyToPrettyString(bill.getKey()));
                log.severe(msgPrefix + bill.getKey());
                throw ex;    			
    		}
    		ofy.put(bill);
    		ofy.getTxn().commit();
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
    	return bill;
    }
    
    public static InetBillPayment createPayment(Key<InetBill> billKey, double amount, String referenceId, String comments, Date paymentDate) 
    	throws MissingEntitiesException, ManualVerificationException
    {	
    	InetBillPayment bp = new InetBillPayment(billKey, amount, referenceId, comments, paymentDate);
    	Objectify ofy = ObjectifyService.beginTransaction();
    	
    	InetBill bill = ofy.find(billKey);
    	if(bill == null)
    	{
    		String msgFmt = "Unable to find Bill: %s"; 
    		log.warning(String.format(msgFmt, billKey));
    		throw new MissingEntitiesException(String.format(msgFmt, billKey.getName()));
    	}
    	
    	double unpaidBillAmount = bill.getTotalAmount() - bill.getSettledAmount();
    	if(unpaidBillAmount - amount < (0 - InetBill.THRESHOLD))
    	{
    		String msgFmt = "Payment amount %f exceeds unpaid bill amount %f." +
    				" Use the view bill option on the navigation panel to see details of this bill (Bill ID: %s)."; 
    		log.warning(String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
    		throw new ManualVerificationException(String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
    	}
    	
    	try
    	{
    		bill.setSettledAmount(amount + bill.getSettledAmount());
    		ofy.put(bp, bill);        
    		ofy.getTxn().commit();
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}

    	return bp;
    }
    
    public static List<InetBill> getBills(Key<InetBillDescription> bdKey, Objectify ofy)
    {
    	return ofy.query(InetBill.class).filter("billTemplateKey", bdKey).list();
    }
    
    public static List<InetBill> getStudentBills(Key<? extends InetUser> userKey, Objectify ofy)
    {
    	return ofy.query(InetBill.class).filter("userKey", userKey).list();
    }    
    
    public static HashMap<String, String> getStudentBillKeys(Key<? extends InetUser> userKey, Objectify ofy)
    {
    	HashMap<String, String> result = new HashMap<String, String>();
    	QueryResultIterable<Key<InetBill>> billKeys = ofy.query(InetBill.class).filter("userKey", userKey).fetchKeys();
    	for(Key<InetBill> bk : billKeys)
    		result.put(ofy.getFactory().keyToString(bk), InetDAO4CommonReads.keyToPrettyString(bk));
    	return result;
    }    
    
    public static List<InetBillPayment> getPayments(Key<InetBill> bk, Objectify ofy)
    {
    	return ofy.query(InetBillPayment.class).ancestor(bk).list();
    }
    
    public static HashMap<String, String> getAllBillDescriptions(Objectify ofy)
    {
    	HashMap<String, String> result = new HashMap<String, String>();
    	
    	QueryResultIterable<Key<InetBillDescription>> bdKeySet = ofy.query(InetBillDescription.class).fetchKeys();
    	for(Key<InetBillDescription> bdKey : bdKeySet)
    		result.put(ofy.getFactory().keyToString(bdKey), InetDAO4CommonReads.keyToPrettyString(bdKey));
    	
    	return result;
    }
}
