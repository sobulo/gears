/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationentities;


import j9educationutilities.GeneralFuncs;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;


/**
 *
 * @author Segun Razaq Sobulo
 */
public class InetStudent extends InetUser{

    private HashSet<Key<InetCourse>> courseKeys;

    private HashSet<Key<InetLevel>> levelsAttended;

    @Indexed private Key<InetGuardian> firstGuardianKey;
    @Indexed private Key<InetGuardian> secondGuardianKey;

    private Date birthday;

    InetStudent()
    {
        super();
        initializeSets();
    }

    InetStudent(String fname, String lname, Date dob, String email, 
            HashSet<String> phoneNums, String loginName, String password,
            Key<InetLevel> levelKey)
    {
        super(fname, lname, email, phoneNums, loginName, password);
        setBirthday(dob);
        initializeSets();
        levelsAttended.add(levelKey);
    }

    private void initializeSets()
    {
        courseKeys = new HashSet<Key<InetCourse>>();
        levelsAttended = new HashSet<Key<InetLevel>>();
    }

    @Override
    public Key<InetStudent> getKey()
    {
        return new Key<InetStudent>(InetStudent.class, key);
    }

    public Date getBirthday() {
        return birthday;
    }

    void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Key<InetGuardian> getFirstGuardianKey() {
        return firstGuardianKey;
    }

    public void setFirstGuardianKey(Key<InetGuardian> firstGuardianKey) {
        this.firstGuardianKey = firstGuardianKey;
    }

    public Key<InetGuardian> getSecondGuardianKey() {
        return secondGuardianKey;
    }

    public void setSecondGuardianKey(Key<InetGuardian> secondGuardianKey) {
        this.secondGuardianKey = secondGuardianKey;
    }
    
    public Set<Key<InetGuardian>> getGuardianKeys()
    {
    	HashSet<Key<InetGuardian>> result = new HashSet<Key<InetGuardian>>(2);
    	if(firstGuardianKey != null)
    		result.add(firstGuardianKey);
    	if(secondGuardianKey != null)
    		result.add(secondGuardianKey);
    	return result;
    }

    public Set<Key<InetCourse>> getCourseKeys(Key<InetLevel> levelKey) {
        Set<Key<InetCourse>> result = new HashSet(courseKeys.size() / levelsAttended.size());
        for(Key<InetCourse> ck : courseKeys)
            if(ck.getName().startsWith(levelKey.getName()))
                result.add(ck);
        return result;
    }

    public Set<Key<InetCumulativeScores>> getCumulativeScoreKeys(Key<InetLevel> levelKey)
    {
        //TODO, as cool as this may seem, why not make it bullet proof by serializing the fields?
        //GIT commit first, change just the class definitions and see how it goes
        Set<Key<InetCumulativeScores>> cumScores = new HashSet(courseKeys.size() / levelsAttended.size());
        for(Key<InetCourse> ck : courseKeys)
            if(ck.getName().startsWith(levelKey.getName()))
                cumScores.add(InetCumulativeScores.getCumScoreKey(ck));
        return cumScores;
    }

    boolean addCourse(Key<InetCourse> ck)
    {
        return GeneralFuncs.addToHashSet(courseKeys, ck);
    }

    boolean removeCourse(Key<InetCourse> ck)
    {
        return GeneralFuncs.removeFromHashSet(courseKeys, ck);
    }

    public Set<Key<InetLevel>> getLevels() {
        return levelsAttended;
    }

    boolean addLevel(Key<InetLevel> lk)
    {
        return GeneralFuncs.addToHashSet(levelsAttended, lk);
    }

    boolean removeLevel(Key<InetLevel> lk)
    {
        return GeneralFuncs.removeFromHashSet(levelsAttended, lk);
    }
    
    public int getNumberOfLevels()
    {
    	return levelsAttended.size();
    }
    
    public int getNumberOfCourses()
    {
    	return courseKeys.size();
    }
}
