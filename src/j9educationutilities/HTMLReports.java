package j9educationutilities;

import j9educationentities.GAEPrimaryKeyEntity;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetGuardian;
import j9educationentities.InetStudent;
import j9educationentities.InetTeacher;
import j9educationentities.InetUser;

import java.text.DateFormat;
import java.util.Locale;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class HTMLReports {
	
	private static DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.UK);
	
	public static String getStudentDetails(InetStudent stud)
	{
		String birthDate = stud.getBirthday() == null? "" : dateFormat.format(stud.getBirthday());
		StringBuilder result = new StringBuilder("<table cellspacing='1' cellpadding='10'>");
		result.append("<tr><td  align='right'><b>Date of Birth</b></td><td  style='border: 1px solid green'>").append(birthDate).append("</td></tr>\n");
		result.append(getUserDetails(stud, false));
		result.append("<tr><td  align='right'><b>Online Records</b></td><td  style='border: 1px solid green'>").append(generateList(stud.getLevels(), new InetKeyStringGenerator())).append("</td></tr>\n");
		result.append("<tr><td  align='right'><b>Parent/Guardian IDs</b></td><td  style='border: 1px solid green'>").append(generateList(stud.getGuardianKeys(), new InetKeyStringGenerator())).append("</td></tr>\n");		
		return result.append("</table>").toString();
	}
	
	public static String getGuardianDetails(InetGuardian guard, Objectify ofy)
	{
		StringBuilder result = new StringBuilder("<table cellspacing='1' cellpadding='10'>").append(getUserDetails(guard, false));
		result.append("<tr><td  align='right'><b>Children IDs</b></td><td  style='border: 1px solid green'>").	append(generateList(InetDAO4CommonReads.getGuardianStudentsKeys(ofy, 
				guard.getKey()), new InetKeyStringGenerator())).append("</td></tr>\n");
		return result.append("</table>").toString();
	}
	
	public static String getTeacherDetails(InetTeacher teach, Objectify ofy)
	{
		StringBuilder result = new StringBuilder("<table cellspacing='1' cellpadding='10'>").append(getUserDetails(teach, false));
		result.append("<tr><td  align='right'><b>Online Records</b></td><td  style='border: 1px solid green'>").append(generateList(InetDAO4CommonReads.getTeacherCourses(teach.getKey(), ofy), new InetKeyStringGenerator())).append("</td></tr>\n");
		return result.append("</table>").toString();
	}
	
	private static String getUserDetails(InetUser user, boolean includeTableTags)
	{
		StringBuilder result = new StringBuilder();
		if(includeTableTags)
			result.append("<table cellspacing='1' cellpadding='10'>");
		result.append("<tr><td  align='right'><b>First Name</b></td><td  style='border: 1px solid green'>").append(user.getFname()).append("</td></tr>\n");
		result.append("<tr><td  align='right'><b>Last Name</b></td><td  style='border: 1px solid green'>").append(user.getLname()).append("</td></tr>\n");
		result.append("<tr><td  align='right'><b>Email</b></td><td  style='border: 1px solid green'>").append(user.getEmail()).append("</td></tr>\n");
		result.append("<tr><td  align='right'><b>Phone Numbers</b></td><td  style='border: 1px solid green'>").append(generateList(user.getPhoneNumbers(), new DefaultStringGenerator())).append("</td></tr>\n");
		if(includeTableTags)
			result.append("</table>");
		return result.toString();
	}
	
	private static String generateList(Iterable<? extends Object> list, StringGenerator gen)
	{
		if(list == null) return "";
		StringBuilder result = new StringBuilder();
		for(Object item : list)
			result.append(gen.convertToString(item)).append("<br/>");
		return result.toString();
	}
	
	private static interface StringGenerator
	{
		public String convertToString(Object o);
	}
	
	private static class DefaultStringGenerator implements StringGenerator
	{

		/* (non-Javadoc)
		 * @see j9educationutilities.HTMLReports.StringGenerator#convertToString(java.lang.Object)
		 */
		@Override
		public String convertToString(Object o) {
			return o.toString();
		}
	}
	
	private static class InetKeyStringGenerator implements StringGenerator
	{

		/* (non-Javadoc)
		 * @see j9educationutilities.HTMLReports.StringGenerator#convertToString(java.lang.Object)
		 */
		@Override
		public String convertToString(Object o) {
			Key<? extends GAEPrimaryKeyEntity> key = (Key<? extends GAEPrimaryKeyEntity>) o;
			return InetDAO4CommonReads.keyToPrettyString(key);
		}
		
	}

}
