/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package j9educationutilities;

import java.util.Random;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class PasswordGenerator
{
    Random randGenerator;
    final static int MIN_NUM = 1000;
    final static int MAX_NUM = 9999;
    
    public PasswordGenerator()
    {
        randGenerator = new Random();
    }
    
    public String generatePassword()
    {
        return String.valueOf(MIN_NUM + randGenerator.nextInt(MAX_NUM - MIN_NUM));
    }
}
