package j9educationutilities;


import j9educationentities.InetCourse;
import j9educationentities.InetDAO4CommonReads;
import j9educationentities.InetDAO4Creation;
import j9educationentities.InetDAO4Updates;
import j9educationentities.InetGuardian;
import j9educationentities.InetLevel;
import j9educationentities.InetSchool;
import j9educationentities.InetSingleTestScores;
import j9educationentities.InetStudent;
import j9educationentities.InetTeacher;
import j9educationentities.InetUser;
import j9educationgwtgui.shared.exceptions.DuplicateEntitiesException;
import j9educationgwtgui.shared.exceptions.MissingEntitiesException;

import java.util.HashSet;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Segun Razaq Sobulo
 *
 * wrapper class to provide objects used by junit tests
 * ideally should be built up from a data structure containing expected results
 * for now though we simply build the objects here and leave it to the test
 * classes to assert the appropriate values
 */
public final class InstanceTestHelper {
    private static final Logger log =
            Logger.getLogger(InstanceTestHelper.class.getName());

    private static InetSchool testSchool;
    private static InetLevel[] testLevelList;
    private static InetCourse[] testCourseList;
    private static InetSingleTestScores[] testScoreList;
    private static InetUser[] testUserList;
    private static InetStudent[] testStudentList;
    private static InetGuardian[] testGuardianList;
    private static InetTeacher[] testTeacherList;

    public static void releaseObjects() {
        testSchool = null;
        testLevelList = null;
        testCourseList = null;
        testScoreList = null;
        testUserList = null;
        testStudentList = null;
        testGuardianList = null;
        testTeacherList = null;
    }

    public static void initializeObjects() {
        try
        {
            HashSet<String> phoneNums = new HashSet<String>();
            phoneNums.add("123456789");
            phoneNums.add("800buynow");

            //intialize school;
            InetDAO4CommonReads.registerClassesWithObjectify();
            Objectify ofy = ObjectifyService.begin();

            System.out.println("about to create school");
            testSchool = InetDAO4Creation.createSchool("Kobe High School", "K24",
                    "baller@greats.com", null, "123 watch me 3peat, nba state", phoneNums);

            //initialize level
            System.out.println("Created School Successfully");
            testLevelList = new InetLevel[2];
            testLevelList[0] = InetDAO4Creation.createLevel("2nd Term",
                    "2009-2010", "JSS 1", "A");

            testLevelList[1] = InetDAO4Creation.createLevel("3rd Term",
                    "2008", "Sophomore", "Science");

            System.out.println("Created level successfully: " +
                    testLevelList[0].getKey() + " & " +
                    testLevelList[1].getKey());
            //initialize course
            Key<InetLevel>[] lk = new Key[2];
            lk[0] = testLevelList[0].getKey();
            lk[1] = testLevelList[1].getKey();
            testCourseList = new InetCourse[2];
            testCourseList[0] = InetDAO4Creation.createCourse(lk[0], "English");
            testCourseList[1] = InetDAO4Creation.createCourse(lk[1], "Mathematics");
            System.out.println("Created course succesfully");

            //initialize test scores
            testScoreList = new InetSingleTestScores[2];
            testScoreList[0] = InetDAO4Creation.createTestScore(testCourseList[0].getKey(), "Quiz A", 20, .2);
            testScoreList[1] = InetDAO4Creation.createTestScore(testCourseList[1].getKey(), "Midterm 100", 100, 1);
            System.out.println("Created score list succesfully");
            //initialize users
            testUserList = new InetUser[2];
            testUserList[0] = InetDAO4Creation.createUser("James", "Wade",
                    "notasgood@nba.com", phoneNums, "jwade", "1234");
            testUserList[1] = InetDAO4Creation.createUser("Einstein", "Genius",
                    "emc2@useful.com", null, "RIP", "345");
            System.out.println("Created user succesfully");
            //initialize students
            testStudentList = new InetStudent[2];
            testStudentList[0] = InetDAO4Creation.createStudent(
                    "john", "doe", GeneralFuncs.getDate(1990, 1, 14), "j.doe@x.com", 
                    null, "jdoe", "123", ofy.getFactory().keyToString(lk[0]));

            testStudentList[1] = InetDAO4Creation.createStudent(
                    "amy", "wyn", null, "s.luv@ht.com", null, "awyn", "123", ofy.getFactory().keyToString(lk[1]));

            System.out.println("Created student succesfully");
            //initialize guardians
            testGuardianList = new InetGuardian[2];
            testGuardianList[0] = InetDAO4Creation.createGuardian
                    (testStudentList[0].getKey(), "daddy", "rules",
                    "d.rules@home.com", phoneNums, "drules", "1234");
            testGuardianList[1] = InetDAO4Creation.createGuardian(
                    testStudentList[1].getKey(), "mummy", "rocks",
                    "m.rocks@olumo.com", null, "mrocks", "4567");
            System.out.println("Created guardian succesfully");
            //initiazlize teachers
            testTeacherList = new InetTeacher[2];
            testTeacherList[0] = InetDAO4Creation.createTeacher(
                    "cool", "teachers", "nblecturer@byname.com", null, "cteach", "9098");
            testTeacherList[1] = InetDAO4Creation.createTeacher(
                    "chalky", "board", "c.b@write.com", phoneNums, "cboard", "998");
            System.out.println("created teacher succesfully");
            
            InetDAO4Updates.setCourseTeacher(testTeacherList[0].getKey(), testCourseList[0].getKey(), true);

            //refresh all objects
            testSchool = ofy.get(testSchool.getKey());
            testLevelList[0] = ofy.get(testLevelList[0].getKey());
            testLevelList[1] = ofy.get(testLevelList[1].getKey());
            testCourseList[0] = ofy.get(testCourseList[0].getKey());
            testCourseList[1] = ofy.get(testCourseList[1].getKey());
            testUserList[0] = ofy.get(testUserList[0].getKey());
            testUserList[1] = ofy.get(testUserList[1].getKey());
            testStudentList[0] = ofy.get(testStudentList[0].getKey());
            testStudentList[1] = ofy.get(testStudentList[1].getKey());
            testGuardianList[0] = (InetGuardian) ofy.get(testGuardianList[0].getKey());
            testGuardianList[1] = ofy.get((Key<InetGuardian>) testGuardianList[1].getKey());
            testTeacherList[0] = ofy.get((Key<InetTeacher>) testTeacherList[0].getKey());
            testTeacherList[1] = (InetTeacher) ofy.get(testTeacherList[1].getKey());

        } catch(DuplicateEntitiesException x) {
            throw new RuntimeException(x.fillInStackTrace());
        }
        catch( MissingEntitiesException x)
        {
            throw new RuntimeException(x.fillInStackTrace());
        }
    }

    public static InetCourse[] getTestCourseList() {
        return testCourseList;
    }

    public static InetGuardian[] getTestGuardianList() {
        return testGuardianList;
    }

    public static InetLevel[] getTestLevelList() {
        return testLevelList;
    }

    public static InetSchool getTestSchool() {
        return testSchool;
    }

    public static InetSingleTestScores[] getTestScoreList() {
        return testScoreList;
    }

    public static InetStudent[] getTestStudentList() {
        return testStudentList;
    }

    public static InetTeacher[] getTestTeacherList() {
        return testTeacherList;
    }

    public static InetUser[] getTestUserList() {
        return testUserList;
    }
}
